core autoconf
core bash
core binutils
core bison
core btrfs-progs
core bzip2
core coreutils
core dialog
core diffutils
core dosfstools
core e2fsprogs
core file
core findutils
core flex
core gawk
core gettext
core gpm
core grep
core groff
core grub
core gzip
core hdparm
core inetutils
core iproute2
core iptables-nft
core iputils
core kbd
core kmod
core less
core logrotate
core lz4
core make
core man-pages
core mkinitcpio
core nano
core ncurses
core net-tools
core nfs-utils
core openssh
core openssl-3.4.0
core pacman
core pam
core pciutils
core perl
core ppp
core python
core reiserfsprogs
core sed
core systemd
core systemd-libs
core systemd-resolvconf
core systemd-sysvcompat
core systemd-ukify
core tar
core texinfo
core tzdata
core usbutils
core which
core xfsprogs
core zstd
extra arch-install-scripts
extra archinstall
extra at
extra banner
extra bc
extra bsd-games
extra cdrtools
extra cpio
extra cups
extra devtools
extra ed
extra expect
extra fortune-mod
extra gdb
extra ghostscript
extra gnumeric
extra icewm
extra indent
extra iptraf-ng
extra lesspipe
extra lilypond
extra linuxdoc-tools
extra lynx
extra mkosi
extra most
extra mtools
extra mutt
extra namcap
extra ncompress
extra netctl
extra netpbm
extra opensmtpd
extra pacman-contrib
extra pacutils
extra parted
extra psutils
extra quota-tools
extra rcs
extra rpm-tools
extra rsync
extra samba
extra sane
extra sane-airscan
extra sbsigntools
extra sc
extra sharutils
extra sysstat
extra traceroute
extra tree
extra unzip
extra uucp
extra vorbis-tools
extra wdiff
extra wget
extra whois
extra wireless_tools
extra xorg-iceauth
extra xorg-sessreg
extra xorg-xcmsdb
extra xorg-xgamma
extra xorg-xhost
extra xorg-xinit
extra xorg-xload
extra xorg-xmodmap
extra xorg-xrandr
extra xorg-xrdb
extra xorg-xrefresh
extra xorg-xset
extra xorg-xsetroot
extra xorg-xvidtune
extra xorg-xwd
