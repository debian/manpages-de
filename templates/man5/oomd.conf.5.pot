# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:33+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OOMD\\&.CONF"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "oomd.conf"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "oomd.conf, oomd.conf.d - Global B<systemd-oomd> configuration files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/systemd/oomd\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "/run/systemd/oomd\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "/usr/local/lib/systemd/oomd\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "/usr/lib/systemd/oomd\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/systemd/oomd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "/run/systemd/oomd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "/usr/local/lib/systemd/oomd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/usr/lib/systemd/oomd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These files configure the various parameters of the B<systemd>(1)  userspace "
"out-of-memory (OOM) killer, B<systemd-oomd.service>(8)\\&. See "
"B<systemd.syntax>(7)  for a general description of the syntax\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. The main "
"configuration file is loaded from one of the listed directories in order of "
"priority, only the first file found is used: /etc/systemd/, /run/systemd/, /"
"usr/local/lib/systemd/ \\&\\s-2\\u[1]\\d\\s+2, /usr/lib/systemd/\\&. The "
"vendor version of the file contains commented out entries showing the "
"defaults as a guide to the administrator\\&. Local overrides can also be "
"created by creating drop-ins, as described below\\&. The main configuration "
"file can also be edited for this purpose (or a copy in /etc/ if it\\*(Aqs "
"shipped under /usr/), however using drop-ins for local configuration is "
"recommended over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"In addition to the main configuration file, drop-in configuration snippets "
"are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/systemd/"
"*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins have "
"higher precedence and override the main configuration file\\&. Files in the "
"*\\&.conf\\&.d/ configuration subdirectories are sorted by their filename in "
"lexicographic order, regardless of in which of the subdirectories they "
"reside\\&. When multiple files specify the same option, for options which "
"accept just a single value, the entry in the file sorted last takes "
"precedence, and for options which accept a list of values, entries are "
"collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering\\&. This also defines a concept of drop-in "
"priorities to allow OS vendors to ship drop-ins within a specific range "
"lower than the range used by users\\&. This should lower the risk of package "
"drop-ins overriding accidentally drop-ins defined by users\\&. It is "
"recommended to use the range 10-40 for drop-ins in /usr/ and the range 60-90 "
"for drop-ins in /etc/ and /run/, to make sure that local and transient drop-"
"ins take priority over drop-ins shipped by the OS vendor\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "[OOM] SECTION OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following options are available in the [OOM] section:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<SwapUsedLimit=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the limit for memory and swap usage on the system before B<systemd-"
"oomd> will take action\\&. If the fraction of memory used and the fraction "
"of swap used on the system are both more than what is defined here, "
"B<systemd-oomd> will act on eligible descendant control groups with swap "
"usage greater than 5% of total swap, starting from the ones with the highest "
"swap usage\\&. Which control groups are monitored and what action gets taken "
"depends on what the unit has configured for I<ManagedOOMSwap=>\\&. Takes a "
"value specified in percent (when suffixed with \"%\"), permille (\"‰\") or "
"permyriad (\"‱\"), between 0% and 100%, inclusive\\&. Defaults to 90%\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 247\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<DefaultMemoryPressureLimit=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the limit for memory pressure on the unit\\*(Aqs control group before "
"B<systemd-oomd> will take action\\&. A unit can override this value with "
"I<ManagedOOMMemoryPressureLimit=>\\&. The memory pressure for this property "
"represents the fraction of time in a 10 second window in which all tasks in "
"the control group were delayed\\&. For each monitored control group, if the "
"memory pressure on that control group exceeds the limit set for longer than "
"the duration set by I<DefaultMemoryPressureDurationSec=>, B<systemd-oomd> "
"will act on eligible descendant control groups, starting from the ones with "
"the most reclaim activity to the least reclaim activity\\&. Which control "
"groups are monitored and what action gets taken depends on what the unit has "
"configured for I<ManagedOOMMemoryPressure=>\\&. Takes a fraction specified "
"in the same way as I<SwapUsedLimit=> above\\&. Defaults to 60%\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<DefaultMemoryPressureDurationSec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"Sets the amount of time a unit\\*(Aqs control group needs to have exceeded "
"memory pressure limits before B<systemd-oomd> will take action\\&. A unit "
"can override this value with I<ManagedOOMMemoryPressureDurationSec=>\\&. "
"Memory pressure limits are defined by I<DefaultMemoryPressureLimit=> and "
"I<ManagedOOMMemoryPressureLimit=>\\&. Must be set to 0, or at least 1 "
"second\\&. Defaults to 30 seconds when unset or 0\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 248\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd.resource-control>(5), B<systemd-oomd.service>(8), "
"B<oomctl>(1)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"💣💥🧨💥💥💣 Please note that those configuration files must be available at "
"all times. If /usr/local/ is a separate partition, it may not be available "
"during early boot, and must not be used for configuration."
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. "
"Initially, the main configuration file in /etc/systemd/ contains commented "
"out entries showing the defaults as a guide to the administrator\\&. Local "
"overrides can be created by editing this file or by creating drop-ins, as "
"described below\\&. Using drop-ins for local configuration is recommended "
"over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0
msgid ""
"In addition to the \"main\" configuration file, drop-in configuration "
"snippets are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins "
"have higher precedence and override the main configuration file\\&. Files in "
"the *\\&.conf\\&.d/ configuration subdirectories are sorted by their "
"filename in lexicographic order, regardless of in which of the "
"subdirectories they reside\\&. When multiple files specify the same option, "
"for options which accept just a single value, the entry in the file sorted "
"last takes precedence, and for options which accept a list of values, "
"entries are collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sets the amount of time a unit\\*(Aqs control group needs to have exceeded "
"memory pressure limits before B<systemd-oomd> will take action\\&. Memory "
"pressure limits are defined by I<DefaultMemoryPressureLimit=> and "
"I<ManagedOOMMemoryPressureLimit=>\\&. Must be set to 0, or at least 1 "
"second\\&. Defaults to 30 seconds when unset or 0\\&."
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. The main "
"configuration file is either in /usr/lib/systemd/ or /etc/systemd/ and "
"contains commented out entries showing the defaults as a guide to the "
"administrator\\&. Local overrides can be created by creating drop-ins, as "
"described below\\&. The main configuration file can also be edited for this "
"purpose (or a copy in /etc/ if it\\*(Aqs shipped in /usr/) however using "
"drop-ins for local configuration is recommended over modifications to the "
"main configuration file\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&. This also defined a concept "
"of drop-in priority to allow distributions to ship drop-ins within a "
"specific range lower than the range used by users\\&. This should lower the "
"risk of package drop-ins overriding accidentally drop-ins defined by "
"users\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&. This also defines a concept "
"of drop-in priorities to allow OS vendors to ship drop-ins within a specific "
"range lower than the range used by users\\&. This should lower the risk of "
"package drop-ins overriding accidentally drop-ins defined by users\\&. It is "
"recommended to use the range 10-40 for drop-ins in /usr/ and the range 60-90 "
"for drop-ins in /etc/ and /run/, to make sure that local and transient drop-"
"ins take priority over drop-ins shipped by the OS vendor\\&."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 256.8"
msgstr ""
