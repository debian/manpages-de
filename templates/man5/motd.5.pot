# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:05+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "motd"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "motd - message of the day"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"The contents of I</etc/motd> are displayed by B<login>(1)  after a "
"successful login but just before it executes the login shell."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"The abbreviation \"motd\" stands for \"message of the day\", and this file "
"has been traditionally used for exactly that (it requires much less disk "
"space than mail to all users)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "I</etc/motd>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "B<login>(1), B<issue>(5)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#.  Patched in Debian, maybe other distribs
#.  End of patch
#.  .BR login (1)
#. type: Plain text
#: debian-bookworm
msgid ""
"The contents of I</etc/motd> are displayed by B<pam_motd>(8)  B<login>(1)  "
"after a successful login but just before it executes the login shell."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"On Debian GNU/Linux, dynamic content configured at I</etc/pam.d/login> is "
"also displayed by I<pam_exec>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I</etc/pam.d/login>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<login>(1), B<issue>(5)  B<pam_motd>(8)"
msgstr ""

#.  Patched in Debian, maybe other distribs
#.  End of patch
#.  .BR login (1)
#. type: Plain text
#: debian-unstable
msgid ""
"The contents of I</etc/motd> are displayed by B<pam_motd>(8)  after a "
"successful login but just before it executes the login shell."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""
