# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-27 06:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "ORG\\&.FREEDESKTOP\\&.TIMESYNC1"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "org.freedesktop.timesync1"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "org.freedesktop.timesync1 - The D-Bus interface of systemd-timesyncd"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "INTRODUCTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"B<systemd-timesyncd.service>(8)  is a system service that may be used to "
"synchronize the local system clock with a remote Network Time Protocol (NTP) "
"server\\&. This page describes the D-Bus interface\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "THE MANAGER OBJECT"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"The service exposes the following interfaces on the Manager object on the "
"bus:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid ""
"node /org/freedesktop/timesync1 {\n"
"  interface org\\&.freedesktop\\&.timesync1\\&.Manager {\n"
"    methods:\n"
"      SetRuntimeNTPServers(in  as runtime_servers);\n"
"    properties:\n"
"      readonly as LinkNTPServers = [\\*(Aq\\&.\\&.\\&.\\*(Aq, \\&.\\&.\\&.];\n"
"      readonly as SystemNTPServers = [\\*(Aq\\&.\\&.\\&.\\*(Aq, \\&.\\&.\\&.];\n"
"      readonly as RuntimeNTPServers = [\\*(Aq\\&.\\&.\\&.\\*(Aq, \\&.\\&.\\&.];\n"
"      readonly as FallbackNTPServers = [\\*(Aq\\&.\\&.\\&.\\*(Aq, \\&.\\&.\\&.];\n"
"      @org\\&.freedesktop\\&.DBus\\&.Property\\&.EmitsChangedSignal(\"false\")\n"
"      readonly s ServerName = \\*(Aq\\&.\\&.\\&.\\*(Aq;\n"
"      @org\\&.freedesktop\\&.DBus\\&.Property\\&.EmitsChangedSignal(\"false\")\n"
"      readonly (iay) ServerAddress = \\&.\\&.\\&.;\n"
"      @org\\&.freedesktop\\&.DBus\\&.Property\\&.EmitsChangedSignal(\"const\")\n"
"      readonly t RootDistanceMaxUSec = \\&.\\&.\\&.;\n"
"      @org\\&.freedesktop\\&.DBus\\&.Property\\&.EmitsChangedSignal(\"const\")\n"
"      readonly t PollIntervalMinUSec = \\&.\\&.\\&.;\n"
"      @org\\&.freedesktop\\&.DBus\\&.Property\\&.EmitsChangedSignal(\"const\")\n"
"      readonly t PollIntervalMaxUSec = \\&.\\&.\\&.;\n"
"      @org\\&.freedesktop\\&.DBus\\&.Property\\&.EmitsChangedSignal(\"false\")\n"
"      readonly t PollIntervalUSec = \\&.\\&.\\&.;\n"
"      readonly (uuuuittayttttbtt) NTPMessage = \\&.\\&.\\&.;\n"
"      @org\\&.freedesktop\\&.DBus\\&.Property\\&.EmitsChangedSignal(\"false\")\n"
"      readonly x Frequency = \\&.\\&.\\&.;\n"
"  };\n"
"  interface org\\&.freedesktop\\&.DBus\\&.Peer { \\&.\\&.\\&. };\n"
"  interface org\\&.freedesktop\\&.DBus\\&.Introspectable { \\&.\\&.\\&. };\n"
"  interface org\\&.freedesktop\\&.DBus\\&.Properties { \\&.\\&.\\&. };\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "Provides information about the manager\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"B<Example\\ \\&1.\\ \\&Introspect org\\&.freedesktop\\&.timesync1\\&.Manager "
"on the bus>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid ""
"$ gdbus introspect --system \\e\n"
"  --dest org\\&.freedesktop\\&.timesync1 \\e\n"
"  --object-path /org/freedesktop/timesync1\n"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "VERSIONING"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"These D-Bus interfaces follow \\m[blue]B<the usual interface versioning "
"guidelines>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "B<systemd>(1), B<systemd-timesync.service>(8)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "the usual interface versioning guidelines"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "\\%https://0pointer.de/blog/projects/versioning-dbus.html"
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""
