# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:18+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "autolog.conf"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Configuration Files"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Linux"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "autolog.conf - Configuration file for the autolog command"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The configuration file consists of multiple lines, each of which describes a "
"class of processes subject (or not subject) to a certain auto logout "
"procedure.  A line consists of any number of switches.  Value switches are "
"of the form: \"name=value\".  Boolean switches are of the form: \"name\" or "
"\"noname\"."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Using these switches, you can define a username, a group, and a tty line.  "
"These descriptions can contain wildcard characters (regular expressions).  "
"You can also define an idle time, a grace period and a few other options.  "
"When reading the configuration file, the program creates a record for each "
"configuration line.  A value is assigned to each variable in the record "
"regardless of whether or not you specify one explicitly.  Values for missing "
"variables are provided by defaults which are compiled in and can be modified "
"from the command line."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If no entries are found matching a given process, that process will be "
"spared from an untimely demise.  Therefore, it is a good idea to always have "
"a \"cleanup\" line at the end of the configuration file to catch anything "
"that might have been missed by the more explicit definitions.  Since the "
"default name, group, and line are all \".+\", a simple line like:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid "        idle=30\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"will do.  Actually, any one switch can be specified on the line and all the "
"others will get the default values."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If no configuration file is found, the program will create a single entry "
"which has all values set from the defaults.  This entry will match any "
"process on any port (name=.+ line=.+ group=.+).  Therefore, the default "
"action is to kill all processes."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "ENTRIES"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<name=>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "A regular expression specifying which username(s) to match."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<group=>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "A regular expression specifying which group(s) to match."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<line=>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A regular expression specifying which tty line(s) to match.  Omit the \"/dev/"
"\" part of the special name."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<idle=>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"An integer specifying the number of --minutes-- of idle (or connect) time to "
"allow before beginning automatic logoff.  An idle time of 0 exempts the "
"process from automatic logoff."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<grace=>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"An integer specifying the number of --seconds-- from the initial warning to "
"killing the process."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<ban=>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"An integer specifying the number of --minutes-- from killing the process to "
"the moment, the user may login again. (after exceeding his session)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<hard>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A boolean value indicating total connect time will be considered rather than "
"idle time."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<mail>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A boolean value indicating that mail will be sent to the user explaining "
"that he was killed."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<clear>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A boolean value indicating that the screen will be cleared before a warning "
"message is sent."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<warn>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A boolean value indicating that a warning message will be sent at the "
"beginning of the \"grace\" period."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<log>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A boolean value indicating that activities will be logged to the logfile (if "
"it exists)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FURTHER ENTRIES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"There is another group of entries, which allows one to set some general "
"options. Each of them takes a whole line.  Don't mix them with the other "
"entries from before."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<nolostkill>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A boolean value indicating whether lost processes should be killed.  If "
"there is a process with uid between 500 and 60000 and the owner is not "
"logged in, it is assumed as lost and will be killed."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<ps=command>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"on some strange or old systems the ps-command has different parameters.  "
"This makes it possible to set a completely different command. It is only "
"important, that this command delivers one heading line and then lines with "
"usernames and process-ids (pid). e.g.: ps=ps aux"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"        name=root line=tty[1-7] idle=0\n"
"        name=guest idle=5 grace=60 nomail hard warn\n"
"        group=lynx-.* idle=10 grace=60 clear\n"
"        idle=60 grace=30\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Kyle Bateman E<lt>kyle@actarg.comE<gt> (autolog 0.35),"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "James Dingwall E<lt>james.dingwall@zynstra.comE<gt>"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "                                  (autolog 0.41)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This manual page was modified for B<Debian> by Paul Telford "
"E<lt>pxt@debian.orgE<gt>"
msgstr ""

#. type: TP
#: debian-unstable
#, no-wrap
msgid "James Dingwall E<lt>james.dingwall@ncrvoyix.comE<gt>"
msgstr ""

#. type: TP
#: debian-unstable
#, no-wrap
msgid "                                  (autolog 0.42.1)"
msgstr ""
