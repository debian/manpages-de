# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:31+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "MODULES-LOAD\\&.D"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "modules-load.d"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "modules-load.d - Configure kernel modules to load at boot"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "/etc/modules-load\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "/run/modules-load\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "/usr/local/lib/modules-load\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "/usr/lib/modules-load\\&.d/*\\&.conf"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd-modules-load.service>(8)  reads files from the above directories "
"which contain kernel modules to load during boot in a static list\\&. Each "
"configuration file is named in the style of /etc/modules-load\\&.d/"
"I<program>\\&.conf\\&. Note that it is usually a better idea to rely on the "
"automatic module loading by PCI IDs, USB IDs, DMI IDs or similar triggers "
"encoded in the kernel modules themselves instead of static configuration "
"like this\\&. In fact, most modern kernel modules are prepared for automatic "
"loading already\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CONFIGURATION FORMAT"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"The configuration files should simply contain a list of kernel module names "
"to load, separated by newlines\\&. Empty lines and lines whose first non-"
"whitespace character is # or ; are ignored\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"Configuration files are read from directories in /etc/, /run/, /usr/local/"
"lib/, and /usr/lib/, in order of precedence, as listed in the SYNOPSIS "
"section above\\&. Files must have the \"\\&.conf\" extension\\&. Files in /"
"etc/ override files with the same name in /run/, /usr/local/lib/, and /usr/"
"lib/\\&. Files in /run/ override files with the same name under /usr/\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"All configuration files are sorted by their filename in lexicographic order, "
"regardless of which of the directories they reside in\\&. If multiple files "
"specify the same option, the entry in the file with the lexicographically "
"latest name will take precedence\\&. Thus, the configuration in a certain "
"file may either be replaced completely (by placing a file with the same name "
"in a directory with higher priority), or individual settings might be "
"changed (by specifying additional settings in a file with a different name "
"that is ordered later)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid ""
"Packages should install their configuration files in /usr/lib/ (distribution "
"packages) or /usr/local/lib/ (local installs)  \\&\\s-2\\u[1]\\d\\s+2\\&. "
"Files in /etc/ are reserved for the local administrator, who may use this "
"logic to override the configuration files installed by vendor packages\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid ""
"It is recommended to prefix all filenames with a two-digit number and a dash "
"to simplify the ordering\\&. It is recommended to use the range 10-40 for "
"configuration files in /usr/ and the range 60-90 for configuration files in /"
"etc/ and /run/, to make sure that local and transient configuration files "
"will always take priority over configuration files shipped by the OS "
"vendor\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"If the administrator wants to disable a configuration file supplied by the "
"vendor, the recommended way is to place a symlink to /dev/null in the "
"configuration directory in /etc/, with the same filename as the vendor "
"configuration file\\&. If the vendor configuration file is included in the "
"initrd image, the image has to be regenerated\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<Example\\ \\&1.\\ \\&/etc/modules-load\\&.d/virtio-net\\&.conf example:>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid ""
"# Load virtio-net\\&.ko at boot\n"
"virtio-net\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd>(1), B<systemd-modules-load.service>(8), B<systemd-delta>(1), "
"B<modprobe>(8)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid ""
"💣💥🧨💥💥💣 Please note that those configuration files must be available at "
"all times. If /usr/local/ is a separate partition, it may not be available "
"during early boot, and must not be used for configuration."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Configuration files are read from directories in /etc/, /run/, /usr/local/"
"lib/, and /lib/, in order of precedence, as listed in the SYNOPSIS section "
"above\\&. Files must have the \"\\&.conf\" extension\\&. Files in /etc/ "
"override files with the same name in /run/, /usr/local/lib/, and /lib/\\&. "
"Files in /run/ override files with the same name under /usr/\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Packages should install their configuration files in /usr/lib/ (distribution "
"packages) or /usr/local/lib/ (local installs)\\&. Files in /etc/ are "
"reserved for the local administrator, who may use this logic to override the "
"configuration files installed by vendor packages\\&. It is recommended to "
"prefix all filenames with a two-digit number and a dash, to simplify the "
"ordering of the files\\&."
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""
