# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:08+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_fd"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/fd/ - file descriptors"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</fd/>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is a subdirectory containing one entry for each file which the process "
"has open, named by its file descriptor, and which is a symbolic link to the "
"actual file.  Thus, 0 is standard input, 1 standard output, 2 standard "
"error, and so on."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For file descriptors for pipes and sockets, the entries will be symbolic "
"links whose content is the file type with the inode.  A B<readlink>(2)  call "
"on this file returns a string in the format:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "type:[inode]\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example, I<socket:[2248868]> will be a socket and its inode is 2248868.  "
"For sockets, that inode can be used to find more information in one of the "
"files under I</proc/net/>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For file descriptors that have no corresponding inode (e.g., file "
"descriptors produced by B<bpf>(2), B<epoll_create>(2), B<eventfd>(2), "
"B<inotify_init>(2), B<perf_event_open>(2), B<signalfd>(2), "
"B<timerfd_create>(2), and B<userfaultfd>(2)), the entry will be a symbolic "
"link with contents of the form"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "anon_inode:I<file-type>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In many cases (but not all), the I<file-type> is surrounded by square "
"brackets."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example, an epoll file descriptor will have a symbolic link whose "
"content is the string I<anon_inode:[eventpoll]>."
msgstr ""

#. The following was still true as at kernel 2.6.13
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In a multithreaded process, the contents of this directory are not available "
"if the main thread has already terminated (typically by calling "
"B<pthread_exit>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Programs that take a filename as a command-line argument, but don't take "
"input from standard input if no argument is supplied, and programs that "
"write to a file named as a command-line argument, but don't send their "
"output to standard output if no argument is supplied, can nevertheless be "
"made to use standard input or standard output by using I</proc/>pidI</fd> "
"files as command-line arguments.  For example, assuming that I<-i> is the "
"flag designating an input file and I<-o> is the flag designating an output "
"file:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "$B< foobar -i /proc/self/fd/0 -o /proc/self/fd/1 ...>\n"
msgstr ""

#.  The following is not true in my tests (MTK):
#.  Note that this will not work for
#.  programs that seek on their files, as the files in the fd directory
#.  are not seekable.
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "and you have a working filter."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I</proc/self/fd/N> is approximately the same as I</dev/fd/N> in some UNIX "
"and UNIX-like systems.  Most Linux MAKEDEV scripts symbolically link I</dev/"
"fd> to I</proc/self/fd>, in fact."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Most systems provide symbolic links I</dev/stdin>, I</dev/stdout>, and I</"
"dev/stderr>, which respectively link to the files I<0>, I<1>, and I<2> in I</"
"proc/self/fd>.  Thus the example command above could be written as:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "$B< foobar -i /dev/stdin -o /dev/stdout ...>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Permission to dereference or read (B<readlink>(2))  the symbolic links in "
"this directory is governed by a ptrace access mode "
"B<PTRACE_MODE_READ_FSCREDS> check; see B<ptrace>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that for file descriptors referring to inodes (pipes and sockets, see "
"above), those inodes still have permission bits and ownership information "
"distinct from those of the I</proc/>pidI</fd> entry, and that the owner may "
"differ from the user and group IDs of the process.  An unprivileged process "
"may lack permissions to open them, as in this example:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< echo test | sudo -u nobody cat>\n"
"test\n"
"$B< echo test | sudo -u nobody cat /proc/self/fd/0>\n"
"cat: /proc/self/fd/0: Permission denied\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"File descriptor 0 refers to the pipe created by the shell and owned by that "
"shell's user, which is not I<nobody>, so B<cat> does not have permission to "
"create a new file descriptor to read from that inode, even though it can "
"still read from its existing file descriptor 0."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
