# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:21+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT\\&.D"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "environment.d"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "environment.d - Definition of user service environment"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "~/\\&.config/environment\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/environment\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/run/environment\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "/usr/local/lib/environment\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/usr/lib/environment\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/environment"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Configuration files in the environment\\&.d/ directories contain lists of "
"environment variable assignments passed to services started by the systemd "
"user instance\\&.  B<systemd-environment-d-generator>(8)  parses them and "
"updates the environment exported by the systemd user instance\\&. See below "
"for an discussion of which processes inherit those variables\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It is recommended to use numerical prefixes for file names to simplify "
"ordering\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For backwards compatibility, a symlink to /etc/environment is installed, so "
"this file is also parsed\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Configuration files are read from directories in /etc/, /run/, /usr/local/"
"lib/, and /usr/lib/, in order of precedence, as listed in the SYNOPSIS "
"section above\\&. Files must have the \"\\&.conf\" extension\\&. Files in /"
"etc/ override files with the same name in /run/, /usr/local/lib/, and /usr/"
"lib/\\&. Files in /run/ override files with the same name under /usr/\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"All configuration files are sorted by their filename in lexicographic order, "
"regardless of which of the directories they reside in\\&. If multiple files "
"specify the same option, the entry in the file with the lexicographically "
"latest name will take precedence\\&. Thus, the configuration in a certain "
"file may either be replaced completely (by placing a file with the same name "
"in a directory with higher priority), or individual settings might be "
"changed (by specifying additional settings in a file with a different name "
"that is ordered later)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Packages should install their configuration files in /usr/lib/ (distribution "
"packages) or /usr/local/lib/ (local installs)  \\&\\s-2\\u[1]\\d\\s+2\\&. "
"Files in /etc/ are reserved for the local administrator, who may use this "
"logic to override the configuration files installed by vendor packages\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"It is recommended to prefix all filenames with a two-digit number and a dash "
"to simplify the ordering\\&. It is recommended to use the range 10-40 for "
"configuration files in /usr/ and the range 60-90 for configuration files in /"
"etc/ and /run/, to make sure that local and transient configuration files "
"will always take priority over configuration files shipped by the OS "
"vendor\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the administrator wants to disable a configuration file supplied by the "
"vendor, the recommended way is to place a symlink to /dev/null in the "
"configuration directory in /etc/, with the same filename as the vendor "
"configuration file\\&. If the vendor configuration file is included in the "
"initrd image, the image has to be regenerated\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION FORMAT"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The configuration files contain a list of \"I<KEY>=I<VALUE>\" environment "
"variable assignments, separated by newlines\\&. The right hand side of these "
"assignments may reference previously defined environment variables, using "
"the \"${OTHER_KEY}\" and \"$OTHER_KEY\" format\\&. It is also possible to "
"use \"${I<FOO>:-I<DEFAULT_VALUE>}\" to expand in the same way as \"${I<FOO>}"
"\" unless the expansion would be empty, in which case it expands to "
"I<DEFAULT_VALUE>, and use \"${I<FOO>:+I<ALTERNATE_VALUE>}\" to expand to "
"I<ALTERNATE_VALUE> as long as \"${I<FOO>}\" would have expanded to a non-"
"empty value\\&. No other elements of shell syntax are supported\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each I<KEY> must be a valid variable name\\&. Empty lines and lines "
"beginning with the comment character \"#\" are ignored\\&."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Example"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<Example\\ \\&1.\\ \\&Setup environment to allow access to a program "
"installed in /opt/foo>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/environment\\&.d/60-foo\\&.conf:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"        FOO_DEBUG=force-software-gl,log-verbose\n"
"        PATH=/opt/foo/bin:$PATH\n"
"        LD_LIBRARY_PATH=/opt/foo/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}\n"
"        XDG_DATA_DIRS=/opt/foo/share:${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "APPLICABILITY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Environment variables exported by the user service manager (B<systemd --"
"user> instance started in the user@I<uid>\\&.service system service) are "
"passed to any services started by that service manager\\&. In particular, "
"this may include services which run user shells\\&. For example in the GNOME "
"environment, the graphical terminal emulator runs as the gnome-terminal-"
"server\\&.service user unit, which in turn runs the user shell, so that "
"shell will inherit environment variables exported by the user manager\\&. "
"For other instances of the shell, not launched by the user service manager, "
"the environment they inherit is defined by the program that starts them\\&. "
"Hint: in general, B<systemd.service>(5)  units contain programs launched by "
"systemd, and B<systemd.scope>(5)  units contain programs launched by "
"something else\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that these files do not affect the environment block of the service "
"manager itself, but exclusively the environment blocks passed to the "
"services it manages\\&. Environment variables set that way thus cannot be "
"used to influence behaviour of the service manager\\&. In order to make "
"changes to the service manager\\*(Aqs environment block the environment must "
"be modified before the user\\*(Aqs service manager is invoked, for example "
"from the system service manager or via a PAM module\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifically, for ssh logins, the B<sshd>(8)  service builds an environment "
"that is a combination of variables forwarded from the remote system and "
"defined by B<sshd>, see the discussion in B<ssh>(1)\\&. A graphical display "
"session will have an analogous mechanism to define the environment\\&. Note "
"that some managers query the systemd user instance for the exported "
"environment and inject this configuration into programs they start, using "
"B<systemctl show-environment> or the underlying D-Bus call\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd-environment-d-generator>(8), B<systemd.environment-"
"generator>(7)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"💣💥🧨💥💥💣 Please note that those configuration files must be available at "
"all times. If /usr/local/ is a separate partition, it may not be available "
"during early boot, and must not be used for configuration."
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Configuration files are read from directories in /etc/, /run/, /usr/local/"
"lib/, and /lib/, in order of precedence, as listed in the SYNOPSIS section "
"above\\&. Files must have the \"\\&.conf\" extension\\&. Files in /etc/ "
"override files with the same name in /run/, /usr/local/lib/, and /lib/\\&. "
"Files in /run/ override files with the same name under /usr/\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Packages should install their configuration files in /usr/lib/ (distribution "
"packages) or /usr/local/lib/ (local installs)\\&. Files in /etc/ are "
"reserved for the local administrator, who may use this logic to override the "
"configuration files installed by vendor packages\\&. It is recommended to "
"prefix all filenames with a two-digit number and a dash, to simplify the "
"ordering of the files\\&."
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Packages should install their configuration files in /usr/lib/ (distribution "
"packages) or /usr/local/lib/ (local installs)\\&. Files in /etc/ are "
"reserved for the local administrator, who may use this logic to override the "
"configuration files installed by vendor packages\\&. It is recommended to "
"prefix all filenames with a two-digit number and a dash, to simplify the "
"ordering of the files\\&. It is recommended to use the range 10-40 for "
"configuration files in /usr/ and the range 60-90 for configuration files in /"
"etc/ and /run/, to make sure that local and transient configuration files "
"will always take priority over configuration files shipped by the OS "
"vendor\\&."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 256.8"
msgstr ""
