# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SMBIOS-TYPE-11"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "smbios-type-11"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "smbios-type-11 - SMBIOS Type 11 strings"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "/sys/firmware/dmi/entries/11-*/raw"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"Various OS components process SMBIOS Type 11 vendor strings that a virtual "
"machine manager (VMM) may set and a virtual machine (VM) receives\\&. SMBIOS "
"Type 11 vendor strings may play a similar role as B<kernel-command-line>(7)  "
"parameters but generally are under control of the VMM rather than the boot "
"loader or UKI\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"For details on SMBIOS Type 11 see the \\m[blue]B<System Management BIOS>\\m[]"
"\\&\\s-2\\u[1]\\d\\s+2 specifications\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "STRINGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "The following strings are supported:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"I<io\\&.systemd\\&.credential:>I<CREDENTIAL=VALUE>, "
"I<io\\&.systemd\\&.credential\\&.binary:>I<CREDENTIAL=VALUE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"This allows passing additional system credentials into the system, in "
"textual or binary (Base64) form\\&. See B<systemd.exec>(5)  and "
"\\m[blue]B<System and Service Credentials>\\m[]\\&\\s-2\\u[2]\\d\\s+2 for "
"details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "Added in version 252\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "I<io\\&.systemd\\&.stub\\&.kernel-cmdline-extra=>I<CMDLINE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"This allows configuration of additional kernel command line options, and is "
"read by the kernel UEFI stub\\&. For details see B<systemd-stub>(7)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "Added in version 254\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "I<io\\&.systemd\\&.boot\\&.kernel-cmdline-extra=>I<CMDLINE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"This allows configuration of additional kernel command line options for Boot "
"Loader Specification Type 1 entries, and is read by B<systemd-boot>\\&. For "
"details see B<systemd-boot>(7)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid "Added in version 256\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd>(1), B<kernel-command-line>(7), B<systemd.system-credentials>(7)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "System Management BIOS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "\\%https://www.dmtf.org/standards/smbios/"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "System and Service Credentials"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron
msgid "\\%https://systemd.io/CREDENTIALS"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 mageia-cauldron
msgid ""
"Various OS components process SMBIOS Type 11 vendor strings that a virtual "
"machine manager (VMM) may set and a virtual machine (VM) receives\\&. SMBIOS "
"Type 11 vendor strings may play a similar role as B<kernel-command-line>(1)  "
"parameters but generally are under control of the VMM rather than the boot "
"loader or UKI\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 mageia-cauldron
msgid ""
"This allows configuration of additional kernel command line options, and is "
"read by the kernel UEFI stub\\&. For details see B<systemd-stub>(1)\\&."
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr ""

#. type: Plain text
#: fedora-41
msgid ""
"This allows configuration of additional kernel command line options for Boot "
"Loader Specification Type 1 entries, and is read by B<systemd-boot>\\&. For "
"details see B<systemd-boot>(1)\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""
