# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:10+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_mutexattr_setkind_np"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-19"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pthread_mutexattr_setkind_np, pthread_mutexattr_getkind_np - deprecated "
"mutex creation attributes"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_mutexattr_setkind_np(pthread_mutexattr_t *>I<attr>B<, int >I<kind>B<);>\n"
"B<int pthread_mutexattr_getkind_np(const pthread_mutexattr_t *>I<attr>B<,>\n"
"B<                                 int *>I<kind>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are deprecated, use B<pthread_mutexattr_settype>(3)  and "
"B<pthread_mutexattr_gettype>(3)  instead."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_mutexattr_getkind_np> always returns 0."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_mutexattr_setkind_np> returns 0 on success and a non-zero error "
"code on error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On error, B<pthread_mutexattr_setkind_np> returns the following error code:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<kind> is neither B<PTHREAD_MUTEX_FAST_NP> nor "
"B<PTHREAD_MUTEX_RECURSIVE_NP> nor B<PTHREAD_MUTEX_ERRORCHECK_NP>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_mutexattr_settype>(3), B<pthread_mutexattr_gettype>(3)."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PTHREAD_MUTEXATTR_SETKIND_NP"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LinuxThreads"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "B<#include E<lt>pthread.hE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<int pthread_mutexattr_setkind_np(pthread_mutexattr_t *>I<attr>B<, int "
">I<kind>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<int pthread_mutexattr_getkind_np(const pthread_mutexattr_t *>I<attr>B<, "
"int *>I<kind>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are deprecated, use B<pthread_mutexattr_settype>(3)  and "
"B<pthread_mutexattr_gettype>(3) instead."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"I<kind> is neither B<PTHREAD_MUTEX_FAST_NP> nor "
"B<PTHREAD_MUTEX_RECURSIVE_NP> nor B<PTHREAD_MUTEX_ERRORCHECK_NP>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"B<int pthread_mutexattr_setkind_np(pthread_mutexattr_t *>I<attr>B<, int "
">I<kind>B<);> B<int pthread_mutexattr_getkind_np(const pthread_mutexattr_t "
"*>I<attr>B<, int *>I<kind>B<);>"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
