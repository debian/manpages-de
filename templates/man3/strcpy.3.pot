# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strcpy"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "stpcpy, strcpy, strcat - copy or catenate a string"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *stpcpy(char *restrict >I<dst>B<, const char *restrict >I<src>B<);>\n"
"B<char *strcpy(char *restrict >I<dst>B<, const char *restrict >I<src>B<);>\n"
"B<char *strcat(char *restrict >I<dst>B<, const char *restrict >I<src>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<stpcpy>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<stpcpy>()"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strcpy>()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions copy the string pointed to by I<src>, into a string at the "
"buffer pointed to by I<dst>.  The programmer is responsible for allocating a "
"destination buffer large enough, that is, I<strlen(src) + 1>.  For the "
"difference between the two functions, see RETURN VALUE."
msgstr ""

#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strcat>()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This function catenates the string pointed to by I<src>, after the string "
"pointed to by I<dst> (overwriting its terminating null byte).  The "
"programmer is responsible for allocating a destination buffer large enough, "
"that is, I<strlen(dst) + strlen(src) + 1>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An implementation of these functions might be:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"char *\n"
"stpcpy(char *restrict dst, const char *restrict src)\n"
"{\n"
"    char  *p;\n"
"\\&\n"
"    p = mempcpy(dst, src, strlen(src));\n"
"    *p = \\[aq]\\[rs]0\\[aq];\n"
"\\&\n"
"    return p;\n"
"}\n"
"\\&\n"
"char *\n"
"strcpy(char *restrict dst, const char *restrict src)\n"
"{\n"
"    stpcpy(dst, src);\n"
"    return dst;\n"
"}\n"
"\\&\n"
"char *\n"
"strcat(char *restrict dst, const char *restrict src)\n"
"{\n"
"    stpcpy(dst + strlen(dst), src);\n"
"    return dst;\n"
"}\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This function returns a pointer to the terminating null byte of the copied "
"string."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions return I<dst>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<stpcpy>(),\n"
"B<strcpy>(),\n"
"B<strcat>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89, SVr4, 4.3BSD."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The strings I<src> and I<dst> may not overlap."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the destination buffer is not large enough, the behavior is undefined.  "
"See B<_FORTIFY_SOURCE> in B<feature_test_macros>(7)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<strcat>()  can be very inefficient.  Read about E<.UR https://"
"www.joelonsoftware.com/\\:2001/12/11/\\:back-to-basics/> Shlemiel the "
"painter E<.UE .>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    char    *p;\n"
"    char    *buf1;\n"
"    char    *buf2;\n"
"    size_t  len, maxsize;\n"
"\\&\n"
"    maxsize = strlen(\"Hello \") + strlen(\"world\") + strlen(\"!\") + 1;\n"
"    buf1 = malloc(sizeof(*buf1) * maxsize);\n"
"    if (buf1 == NULL)\n"
"        err(EXIT_FAILURE, \"malloc()\");\n"
"    buf2 = malloc(sizeof(*buf2) * maxsize);\n"
"    if (buf2 == NULL)\n"
"        err(EXIT_FAILURE, \"malloc()\");\n"
"\\&\n"
"    p = buf1;\n"
"    p = stpcpy(p, \"Hello \");\n"
"    p = stpcpy(p, \"world\");\n"
"    p = stpcpy(p, \"!\");\n"
"    len = p - buf1;\n"
"\\&\n"
"    printf(\"[len = %zu]: \", len);\n"
"    puts(buf1);  // \"Hello world!\"\n"
"    free(buf1);\n"
"\\&\n"
"    strcpy(buf2, \"Hello \");\n"
"    strcat(buf2, \"world\");\n"
"    strcat(buf2, \"!\");\n"
"    len = strlen(buf2);\n"
"\\&\n"
"    printf(\"[len = %zu]: \", len);\n"
"    puts(buf2);  // \"Hello world!\"\n"
"    free(buf2);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strdup>(3), B<string>(3), B<wcscpy>(3), B<string_copying>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"char *\n"
"stpcpy(char *restrict dst, const char *restrict src)\n"
"{\n"
"    char  *p;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    p = mempcpy(dst, src, strlen(src));\n"
"    *p = \\[aq]\\e0\\[aq];\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    return p;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"char *\n"
"strcpy(char *restrict dst, const char *restrict src)\n"
"{\n"
"    stpcpy(dst, src);\n"
"    return dst;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"char *\n"
"strcat(char *restrict dst, const char *restrict src)\n"
"{\n"
"    stpcpy(dst + strlen(dst), src);\n"
"    return dst;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<strcat>()  can be very inefficient.  Read about E<.UR https://"
"www.joelonsoftware.com/\\:2001/12/11/\\:back-to-basics/> Shlemiel "
"the painter E<.UE .>"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    char    *p;\n"
"    char    *buf1;\n"
"    char    *buf2;\n"
"    size_t  len, maxsize;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    maxsize = strlen(\"Hello \") + strlen(\"world\") + strlen(\"!\") + 1;\n"
"    buf1 = malloc(sizeof(*buf1) * maxsize);\n"
"    if (buf1 == NULL)\n"
"        err(EXIT_FAILURE, \"malloc()\");\n"
"    buf2 = malloc(sizeof(*buf2) * maxsize);\n"
"    if (buf2 == NULL)\n"
"        err(EXIT_FAILURE, \"malloc()\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    p = buf1;\n"
"    p = stpcpy(p, \"Hello \");\n"
"    p = stpcpy(p, \"world\");\n"
"    p = stpcpy(p, \"!\");\n"
"    len = p - buf1;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"[len = %zu]: \", len);\n"
"    puts(buf1);  // \"Hello world!\"\n"
"    free(buf1);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    strcpy(buf2, \"Hello \");\n"
"    strcat(buf2, \"world\");\n"
"    strcat(buf2, \"!\");\n"
"    len = strlen(buf2);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"[len = %zu]: \", len);\n"
"    puts(buf2);  // \"Hello world!\"\n"
"    free(buf2);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"char *\n"
"stpcpy(char *restrict dst, const char *restrict src)\n"
"{\n"
"    char  *p;\n"
"\\&\n"
"    p = mempcpy(dst, src, strlen(src));\n"
"    *p = \\[aq]\\e0\\[aq];\n"
"\\&\n"
"    return p;\n"
"}\n"
"\\&\n"
"char *\n"
"strcpy(char *restrict dst, const char *restrict src)\n"
"{\n"
"    stpcpy(dst, src);\n"
"    return dst;\n"
"}\n"
"\\&\n"
"char *\n"
"strcat(char *restrict dst, const char *restrict src)\n"
"{\n"
"    stpcpy(dst + strlen(dst), src);\n"
"    return dst;\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
