# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 17:55+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "bsearch"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "bsearch - binary search of a sorted array"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void *bsearch(const void >I<key>B<[.>I<size>B<], const void >I<base>B<[.>I<size>B< * .>I<nmemb>B<],>\n"
"B<              size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<              int (*>I<compar>B<)(const void [.>I<size>B<], const void [.>I<size>B<]));>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<bsearch>()  function searches an array of I<nmemb> objects, the "
"initial member of which is pointed to by I<base>, for a member that matches "
"the object pointed to by I<key>.  The size of each member of the array is "
"specified by I<size>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The contents of the array should be in ascending sorted order according to "
"the comparison function referenced by I<compar>.  The I<compar> routine is "
"expected to have two arguments which point to the I<key> object and to an "
"array member, in that order, and should return an integer less than, equal "
"to, or greater than zero if the I<key> object is found, respectively, to be "
"less than, to match, or be greater than the array member."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<bsearch>()  function returns a pointer to a matching member of the "
"array, or NULL if no match is found.  If there are multiple elements that "
"match the key, the element returned is unspecified."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<bsearch>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89, C99, SVr4, 4.3BSD."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The example below first sorts an array of structures using B<qsort>(3), then "
"retrieves desired elements using B<bsearch>()."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"
"\\&\n"
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"
"\\&\n"
"static struct mi  months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"
"\\&\n"
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"
"\\&\n"
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"
"\\&\n"
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: unknown month\\[rs]n\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\[rs]n\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<hsearch>(3), B<lsearch>(3), B<qsort>(3), B<tsearch>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static struct mi  months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: unknown month\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"
"\\&\n"
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"
"\\&\n"
"static struct mi  months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"
"\\&\n"
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"
"\\&\n"
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"
"\\&\n"
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: unknown month\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
