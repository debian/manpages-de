# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "intro"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "July 1997"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux DDI"
msgstr ""

#
#
#
#.  Copyright 1997 Stephen Williams
#.  Permission is granted to make and distribute verbatim copies of this
#.  manual provided the copyright notice and this permission notice are
#.  preserved on all copies.
#.  Permission is granted to copy and distribute modified versions of this
#.  manual under the conditions for verbatim copying, provided that the
#.  entire resulting derived work is distributed under the terms of a
#.  permission notice identical to this one
#.  Since the Linux kernel and libraries are constantly changing, this
#.  manual page may be incorrect or out-of-date.  The author(s) assume no
#.  responsibility for errors or omissions, or for damages resulting from
#.  the use of the information contained herein.  The author(s) may not
#.  have taken the same level of care in the production of this manual,
#.  which is licensed free of charge, as they might when working
#.  professionally.
#.  Formatted or processed versions of this manual, if unaccompanied by
#.  the source, must acknowledge the copyright and authors of this work.
#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "intro - Introduction to kernel interface"
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "B<#include E<lt>linux/version.hE<gt>>"
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"This section documents the functions available to device driver writers and "
"kernel level modules. The functions are of interest mainly to device driver "
"writers, although anyone considering running code in linux kernel mode may "
"need to be familiar with these interfaces."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Some of the functions of the DDI exist only in certain versions of the "
"kernel. Use the B<LINUX_VERSION_CODE> macro to test for specific versions of "
"the kernel. For example, to use a feature that is new to 2.1, say:"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#if LINUX_VERSION_CODE E<gt>= 0x020100\n"
"    ... use new stuff ...\n"
"#else\n"
"    ... do it the old way ...\n"
"#endif\n"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The following is a list of the man pages, divided roughly into function "
"groups."
msgstr ""

#. type: SS
#: mageia-cauldron
#, no-wrap
msgid "Kernel Functions"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "These are general kernel functions."
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "MAJOR"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "MOD_INC_USE_COUNT"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "cli"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "init_bh"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "init_module"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "kmalloc"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "poll_wait"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "printk"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "probe_irq_on"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "register_chrdev"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "register_console"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "request_irq"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "save_flags"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "sleep_on"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "wake_up"
msgstr ""

#. type: SS
#: mageia-cauldron
#, no-wrap
msgid "/proc functions"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "These functions relate to manipulation of the B</proc> filesystem."
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "proc_dir_entry"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "proc_net_register"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "proc_scsi_register"
msgstr ""

#. type: SS
#: mageia-cauldron
#, no-wrap
msgid "BIOS32 functions"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "These are specific to PCI (BIOS32) support."
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_find_class"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_present"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_read_config_byte"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_read_config_dword"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_read_config_word"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_strerror"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_write_config_byte"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_write_config_dword"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "pcibios_write_config_word"
msgstr ""

#. type: SS
#: mageia-cauldron
#, no-wrap
msgid "VM functions"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"These are functions that support manipulating the virtual memory subsystem."
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "MAP_NR"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "mem_map_reserve"
msgstr ""

#. type: SS
#: mageia-cauldron
#, no-wrap
msgid "Network Functions"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_dequeue"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_insert"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_peek"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_queue_empty"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_queue_head"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_queue_head_init"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_queue_len"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_queue_tail"
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "skb_unlink"
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Each man page attempts to list the kernel versions where the function is "
"available. If the form of the function changes, this section tells when the "
"described form applies."
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"This section lists other man pages that may be of interest. Also, "
"interesting source files in the linux kernel may be listed here."
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Each man page has a section like this one that lists the author(s) who "
"contributed significantly to that page. Other unnamed individuals may also "
"have contributed corrections, editorial, etc."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Major contributors are (in alphabetical order)  Cyrus Durgin "
"E<lt>cider@speakeasy.orgE<gt>, Niel Moore E<lt>amethyst@maxwell.ml.orgE<gt>, "
"Keith Owens E<lt>kaos@ocs.com.auE<gt>, Kirk Petersen "
"E<lt>kirk@speakeasy.orgE<gt>, and Stephen Williams "
"E<lt>steve@icarus.comE<gt>."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Editorial, and this intro page, were done by Stephen Williams "
"E<lt>steve@icarus.comE<gt>."
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The living linux kernel is a moving target, and the kernel functions are "
"unique to linux. Therefore, although the editor and contributers make a good "
"effort to be as accurate as possible, errors may exist. The source codes of "
"the linux kernel are the ultimate authority on the behavior of any function "
"and should be considered the final word."
msgstr ""
