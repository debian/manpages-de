# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-09-23 08:51+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "May 31, 1993"
msgstr ""

#. type: Dt
#: debian-bookworm debian-unstable
#, no-wrap
msgid "ADVENTURE 6"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm adventure>"
msgstr ""

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "an exploration game"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm> E<.Op saved-file>"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The object of the game is to locate and explore Colossal Cave, find the "
"treasures hidden there, and bring them back to the building with you.  The "
"program is self-descriptive to a point, but part of the game is to discover "
"its rules."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"To terminate a game, enter E<.Dq quit>; to save a game for later resumption, "
"enter E<.Dq suspend>."
msgstr ""
