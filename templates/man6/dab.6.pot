# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:20+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Dd
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "December 24, 2003"
msgstr ""

#. type: Dt
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DAB 6"
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "E<.Nm dab>"
msgstr ""

#. type: Nd
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Dots and Boxes game"
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "E<.Nm> E<.Op Ar level>"
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"E<.Nm> is a game where each player tries to complete the most boxes. A turn "
"consists of putting one border of a box; the player setting the fourth and "
"final border of a box gets the point for the box and has another turn."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The keys used are the vi keys: E<.Ic k> for up, E<.Ic j> for down, E<.Ic h> "
"for left, and E<.Ic l> for right.  E<.Aq Ic space> sets a new border, E<.Ic "
"q> quits the game."
msgstr ""

#. type: Plain text
#: archlinux
msgid "Support option is: E<.Ar level> sets the starting level."
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"E<.An Christos Zoulas> E<.Aq christos@NetBSD.org> E<.An Mike Sharov> E<.Aq "
"msharov@users.sourceforge.net>"
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"E<.Rs> E<.%A Elwyn R. Berlekamp> E<.%T The Dots and Boxes Game: "
"Sophisticated Child's Play> E<.%D 2000> E<.%I A K Peters> E<.%O http://"
"www.akpeters.com/book.asp?bID=111> E<.Re>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"E<.Nm> E<.Op Fl aw> E<.Op Fl n Ar ngames> E<.Op Fl p Ao Ar c|h Ac Ns Ao Ar c|"
"h Ac> E<.Op Ar xdim Oo Ar ydim Oc>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"E<.Nm> is a game where each player tries to complete the most boxes.  A turn "
"consists of putting one border of a box; the player setting the fourth and "
"final border of a box gets the point for the box and has another turn."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The keys used are the vi keys: E<.Ic k> for up, E<.Ic j> for down, E<.Ic h> "
"for left, and E<.Ic l> for right.  To switch between even and odd rows, use "
"one of the following keys: E<.Ic u> E<.Pq diagonal right up>, E<.Ic y> E<.Pq "
"diagonal left up>, E<.Ic b> E<.Pq diagonal left down>, E<.Ic n> E<.Pq "
"diagonal right down>; E<.Aq Ic space> sets a new border, E<.Ic CTRL-L> and "
"E<.Ic CTRL-R> redraw the screen, and E<.Ic q> quits the game."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Support options are:"
msgstr ""

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Fl a"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Don't use the alternate character set."
msgstr ""

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Fl n Ar ngames"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"E<.Ar ngames> games will be played.  E<.Pq Especially useful in Fl p Ar cc "
"No mode.>"
msgstr ""

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Fl p Ao Ar c|h Ac Ns Ao Ar c|h Ac"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Select which of the two players is a human or a computer.  The first "
"argument is the first player; E<.Ic c> stands for computer and E<.Ic h> for "
"human."
msgstr ""

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Fl w"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Wait for a character press between games."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"E<.Ar xdim> and E<.Ar ydim> define the size of the board in the x and y "
"dimensions."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "E<.An Christos Zoulas> E<.Aq christos@NetBSD.org>"
msgstr ""
