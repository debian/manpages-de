# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-06 18:25+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Dd
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "May 31, 1993"
msgstr ""

#. type: Dt
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "RAIN 6"
msgstr ""

#. type: Sh
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "E<.Nm rain>"
msgstr ""

#. type: Nd
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "animated raindrops display"
msgstr ""

#. type: Sh
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "E<.Nm> E<.Op Fl d Ar delay>"
msgstr ""

#. type: Sh
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The output of E<.Nm> is modeled after the E<.Tn VAX/VMS> program of the same "
"name.  To obtain the proper effect, either the terminal must be set for 9600 "
"baud or the E<.Fl d> option must be used to specify a delay, in "
"milliseconds, between each update.  The default delay is 120."
msgstr ""

#. type: Sh
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "E<.An Eric P. Scott>"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"The output of E<.Nm> is modeled after the E<.Tn VAX/VMS> program of the same "
"name.  To obtain the proper effect, either the terminal must be set for 9600 "
"baud or the E<.Fl d> option must be used to specify a delay, in "
"milliseconds, between each update.  A reasonable delay is 120; the default "
"is 0."
msgstr ""
