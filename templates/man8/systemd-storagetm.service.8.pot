# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-STORAGETM\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd-storagetm.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"systemd-storagetm.service, systemd-storagetm - Exposes all local block "
"devices as NVMe-TCP mass storage devices"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "systemd-storagetm\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B</usr/lib/systemd/systemd-storagetm> [OPTIONS...] [I<DEVICE>]"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"systemd-storagetm\\&.service is a service that exposes all local block "
"devices as NVMe-TCP mass storage devices\\&. Its primary use-case is to be "
"invoked by the storage-target-mode\\&.target unit that can be booted into\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "B<Warning>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"The NVMe disks are currently exposed without authentication or encryption, "
"in read/write mode\\&. This means network peers may read from and write to "
"the device without any restrictions\\&. This functionality should hence only "
"be used in a local setup\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Note that to function properly networking must be configured too\\&. The "
"recommended mechanism to boot into a storage target mode is by adding "
"\"rd\\&.systemd\\&.unit=storage-target-mode\\&.target ip=link-local\" on the "
"kernel command line\\&. Note that \"ip=link-local\" only configures link-"
"local IP, i\\&.e\\&. IPv4LL and IPv6LL, which means non-routable "
"addresses\\&. This is done for security reasons, so that only systems on the "
"local link can access the devices\\&. Use \"ip=dhcp\" to assign routable "
"addresses too\\&. For further details see B<systemd-network-"
"generator.service>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Unless the B<--all> switch is used expects one or more block devices or "
"regular files to expose via NVMe-TCP as argument\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--nqn=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Takes a string\\&. If specified configures the NVMe Qualified Name to use "
"for the exposed NVMe-TCP mass storage devices\\&. The NQN should follow the "
"syntax described in \\m[blue]B<NVM Express Base Specification 2\\&.0c>\\m[]"
"\\&\\s-2\\u[1]\\d\\s+2, section 4\\&.5 \"NVMe Qualified Names\"\\&. Note "
"that the NQN specified here will be suffixed with a dot and the block device "
"name before it is exposed on the NVMe target\\&. If not specified defaults "
"to \"nqn\\&.2023-10\\&.io\\&.systemd:storagetm\\&.I<ID>\", where ID is "
"replaced by a 128bit ID derived from B<machine-id>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 255\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--all>, B<-a>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If specified exposes all local block devices via NVMe-TCP, current and "
"future (i\\&.e\\&. it watches block devices come and go and updates the NVMe-"
"TCP list as needed)\\&. Note that by default any block devices that "
"originate on the same block device as the block device backing the current "
"root file system are excluded\\&. If the switch is specified twice this "
"safety mechanism is disabled\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "B<--list-devices>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"Show a list of candidate block devices this command may operate on\\&. "
"Specifically, this enumerates block devices currently present, and shows "
"their device node paths along with any of their symlinks\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "Added in version 257\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<systemd>(1), B<systemd.special>(7)"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "NVM Express Base Specification 2.0c"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"\\%https://nvmexpress.org/wp-content/uploads/NVM-Express-Base-"
"Specification-2.0c-2022.10.04-Ratified.pdf"
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Warning: the NVMe disks are currently exposed without authentication or "
"encryption, in read/write mode\\&. This means network peers may read from "
"and write to the device without any restrictions\\&. This functionality "
"should hence only be used in a local setup\\&."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 256.8"
msgstr ""
