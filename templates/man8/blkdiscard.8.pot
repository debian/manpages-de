# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:36+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "BLKDISCARD"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "blkdiscard - discard sectors on a device"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<blkdiscard> [options] [B<-o> I<offset>] [B<-l> I<length>] I<device>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<blkdiscard> is used to discard device sectors. This is useful for solid-"
"state drivers (SSDs) and thinly-provisioned storage. Unlike B<fstrim>(8), "
"this command is used directly on the block device."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, B<blkdiscard> will discard all blocks on the device. Options may "
"be used to modify this behavior based on range or size, as explained below."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The I<device> argument is the pathname of the block device."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<WARNING: All data in the discarded region on the device will be lost!>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<offset> and I<length> arguments may be followed by the multiplicative "
"suffixes KiB (=1024), MiB (=1024*1024), and so on for GiB, TiB, PiB, EiB, "
"ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the same meaning as "
"\"KiB\") or the suffixes KB (=1000), MB (=1000*1000), and so on for GB, TB, "
"PB, EB, ZB and YB."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-f>, B<--force>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Disable all checking. Since v2.36 the block device is open in exclusive mode "
"(B<O_EXCL>) by default to avoid collision with mounted filesystem or another "
"kernel subsystem. The B<--force> option disables the exclusive access mode."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--offset> I<offset>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Byte offset into the device from which to start discarding. The provided "
"value must be aligned to the device sector size. The default value is zero."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--length> I<length>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The number of bytes to discard (counting from the starting point). The "
"provided value must be aligned to the device sector size. If the specified "
"value extends past the end of the device, B<blkdiscard> will stop at the "
"device size boundary. The default value extends to the end of the device."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--step> I<length>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The number of bytes to discard within one iteration. The default is to "
"discard all by one ioctl call."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--secure>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Perform a secure discard. A secure discard is the same as a regular discard "
"except that all copies of the discarded blocks that were possibly created by "
"garbage collection must also be erased. This requires support from the "
"device."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-z>, B<--zeroout>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Zero-fill rather than discard."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display the aligned values of I<offset> and I<length>. If the B<--step> "
"option is specified, it prints the discard progress every second."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<fstrim>(8)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<blkdiscard> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
