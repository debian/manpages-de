# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:33+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "PAM_SYSTEMD_LOADKEY"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "pam_systemd_loadkey"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"pam_systemd_loadkey - Read password from kernel keyring and set it as PAM "
"authtok"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "pam_systemd_loadkey\\&.so"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
msgid ""
"B<pam_systemd_loadkey> reads a NUL-separated password list from the kernel "
"keyring, and sets the last password in the list as the PAM authtok, which "
"can be used by e\\&.g\\&.  B<pam_get_authtok>(3)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The password list is supposed to be stored in the \"user\" keyring of the "
"root user, by an earlier call to B<systemd-ask-password>(1)  with B<--"
"keyname=>\\&. You can pass the keyname to B<pam_systemd_loadkey> via the "
"B<keyname=> option\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "I<keyname=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Takes a string argument which sets the keyname to read\\&. The default is "
"\"cryptsetup\"\\&. During boot, B<systemd-cryptsetup@.service>(8)  stores a "
"passphrase or PIN in the keyring\\&. The LUKS2 volume key can also be used, "
"via the B<link-volume-key> option in B<crypttab>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "B<Table\\ \\&1.\\ \\& Possible values for >I<keyname>\\&."
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ".T&"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "l l"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "l l."
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "cryptsetup"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Passphrase or recovery key"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "fido2-pin"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Security token PIN"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "luks2-pin"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "LUKS2 token PIN"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "tpm2-pin"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "TPM2 PIN"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Added in version 255\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "I<debug>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The module will log debugging information as it operates\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This module is intended to be used when you use LUKS with a passphrase, "
"enable autologin in the display manager, and want to unlock Gnome Keyring / "
"KDE KWallet automatically\\&. So in total, you only enter one password "
"during boot\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"You need to set the password of your Gnome Keyring/KWallet to the same as "
"your LUKS passphrase\\&. Then add the following lines to your display "
"manager\\*(Aqs PAM config under /etc/pam\\&.d/ (e\\&.g\\&.  sddm-autologin):"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"-auth       optional    pam_systemd_loadkey\\&.so\n"
"-auth       optional    pam_gnome_keyring\\&.so\n"
"-session    optional    pam_gnome_keyring\\&.so auto_start\n"
"-session    optional    pam_kwallet5\\&.so auto_start\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"And add the following lines to your display manager\\*(Aqs systemd service "
"file, so it can access root\\*(Aqs keyring:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"[Service]\n"
"KeyringMode=inherit\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
msgid ""
"In this setup, early during the boot process, B<systemd-"
"cryptsetup@.service>(8)  will ask for the passphrase and store it in the "
"kernel keyring with the keyname \"cryptsetup\"\\&. Then when the display "
"manager does the autologin, B<pam_systemd_loadkey> will read the passphrase "
"from the kernel keyring, set it as the PAM authtok, and then "
"B<pam_gnome_keyring> and B<pam_kwallet5> will unlock with the same "
"passphrase\\&."
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr ""

#. type: Plain text
#: fedora-41 mageia-cauldron
msgid ""
"B<pam_systemd_loadkey> reads a NUL-separated password list from the kernel "
"keyring, and sets the last password in the list as the PAM authtok\\&."
msgstr ""

#. type: Plain text
#: fedora-41 mageia-cauldron
msgid ""
"In this setup, early during the boot process, B<systemd-"
"cryptsetup@.service>(8)  will ask for the passphrase and store it in the "
"kernel keyring with the keyname \"cryptsetup\"\\&. Then when the display "
"manager does the autologin, pam_systemd_loadkey will read the passphrase "
"from the kernel keyring, set it as the PAM authtok, and then "
"pam_gnome_keyring and pam_kwallet5 will unlock with the same passphrase\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Takes a string argument which sets the keyname to read\\&. The default is "
"\"cryptsetup\", which is used by B<systemd-cryptsetup@.service>(8)  to store "
"LUKS passphrase during boot\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"-auth       optional    pam_systemd_loadkey\\&.so\n"
"-session    optional    pam_gnome_keyring\\&.so auto_start\n"
"-session    optional    pam_kwallet5\\&.so auto_start\n"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 256.8"
msgstr ""
