# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-06 18:26+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "RPMGRAPH"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "30 June 2002"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "rpmgraph - Display RPM Package Dependency Graph"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\\f[B]rpmgraph\\f[R] \\f[I]PACKAGE_FILE ...\\fR"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\f[B]rpmgraph\\f[R] uses \\f[I]PACKAGE_FILE\\f[R] arguments to generate a "
"package dependency graph.  Each \\f[I]PACKAGE_FILE\\f[R] argument is read "
"and added to an rpm transaction set.  The elements of the transaction set "
"are partially ordered using a topological sort.  The partially ordered "
"elements are then printed to standard output.\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Nodes in the dependency graph are package names, and edges in the directed "
"graph point to the parent of each node.  The parent node is defined as the "
"last predecessor of a package when partially ordered using the package "
"dependencies as a relation.  That means that the parent of a given package "
"is the package\\[aq]s last prerequisite."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The output is in \\f[B]dot\\f[R](1) directed graph format, and can be "
"displayed or printed using the \\f[B]dotty\\f[R] graph editor from the "
"\\f[B]graphviz\\f[R] package.  There are no \\f[B]rpmgraph\\f[R] specific "
"options, only common \\f[B]rpm\\f[R] options.  See the \\f[B]rpmgraph\\f[R] "
"usage message for what is currently implemented.\\fR"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "\\f[B]dot\\f[R](1), \\f[B]dotty\\f[R](1)\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"\\f[B]http://www.graphviz.org/ E<lt>URL:http://www.graphviz.org/E<gt>\\fR"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Jeff Johnson E<lt>jbj\\[at]redhat.comE<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "\\f[B]dot\\f[R](1),\\fR"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "\\f[B]dotty\\f[R](1),\\fR"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "** http://www.graphviz.org/ E<lt>URL:http://www.graphviz.org/E<gt>**"
msgstr ""
