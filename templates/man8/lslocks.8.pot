# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LSLOCKS"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "lslocks - list local system locks"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<lslocks> [options]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lslocks> lists information about all the currently held file locks in a "
"Linux system."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that lslocks also lists OFD (Open File Description) locks, these locks "
"are not associated with any process (PID is -1). OFD locks are associated "
"with the open file description on which they are acquired. This lock type is "
"available since Linux 3.15, see B<fcntl>(2) for more details."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-i>, B<--noinaccessible>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Ignore lock files which are inaccessible for the current user."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use JSON output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g., B<lslocks -o +BLOCKER>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Output all available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--pid> I<pid>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display only the locks held by the process with this I<pid>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use the raw output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-u>, B<--notruncate>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not truncate text in columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OUTPUT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "COMMAND"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The command name of the process holding the lock."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "PID"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The process ID of the process which holds the lock or -1 for OFDLCK."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "TYPE"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The type of lock; can be FLOCK (created with B<flock>(2)), POSIX (created "
"with B<fcntl>(2) and B<lockf>(3)) or OFDLCK (created with B<fcntl>(2))."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "SIZE"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Size of the locked file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "MODE"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The lock\\(cqs access permissions (read, write). If the process is blocked "
"and waiting for the lock, then the mode is postfixed with an \\(aq*\\(aq "
"(asterisk)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "M"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Whether the lock is mandatory; 0 means no (meaning the lock is only "
"advisory), 1 means yes. (See B<fcntl>(2).)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "START"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Relative byte offset of the lock."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "END"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Ending offset of the lock."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "PATH"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Full path of the lock. If none is found, or there are no permissions to read "
"the path, it will fall back to the device\\(cqs mountpoint and \"...\" is "
"appended to the path. The path might be truncated; use B<--notruncate> to "
"get the full path."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "BLOCKER"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The PID of the process which blocks the lock."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lslocks> command is meant to replace the B<lslk>(8) command, "
"originally written by"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "and unmaintained since 2001."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<flock>(1), B<fcntl>(2), B<lockf>(3)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lslocks> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
