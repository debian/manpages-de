# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:19+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BTRFS"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Dec 01, 2024"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "btrfs - a toolbox to manage btrfs filesystems"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<btrfs> [global] E<lt>groupE<gt> [E<lt>groupE<gt>...] E<lt>commandE<gt> "
"[E<lt>argsE<gt>]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<btrfs> utility is a toolbox for managing btrfs filesystems.  There are "
"command groups to work with subvolumes, devices, for whole filesystem or "
"other specific actions. See section I<\\%COMMANDS>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There are also standalone tools for some tasks like I<\\%btrfs-convert(8)> "
"or I<\\%btrfstune(8)> that were separate historically and/or haven\\(aqt "
"been merged to the main utility. See section I<\\%STANDALONE TOOLS> for more "
"details."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For other topics (mount options, etc) please refer to the separate manual "
"page I<\\%btrfs(5)>\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMMAND SYNTAX"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Any command name can be shortened so long as the shortened form is "
"unambiguous, however, it is recommended to use full command names in "
"scripts.  All command groups have their manual page named B<btrfs-"
"E<lt>groupE<gt>>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example: it is possible to run B<btrfs sub snaps> instead of B<btrfs "
"subvolume snapshot>\\&.  But B<btrfs file s> is not allowed, because B<file "
"s> may be interpreted both as B<filesystem show> and as B<filesystem "
"sync>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the command name is ambiguous, the list of conflicting options is printed."
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide
msgid ""
"I<Sizes>, both upon input and output, can be expressed in either SI or IEC-I "
"units (see \\X'tty: link https://man7.org/linux/man-pages/man1/"
"numfmt.1.html'I<\\%numfmt(1)>\\X'tty: link')  with the suffix I<B> "
"appended.  All numbers will be formatted according to the rules of the I<C> "
"locale (ignoring the shell locale, see \\X'tty: link https://man7.org/linux/"
"man-pages/man7/locale.7.html'I<\\%locale(7)>\\X'tty: link')."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an overview of a given command use B<btrfs command --help> or B<btrfs "
"[command...] --help --full> to print all available options."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There are global options that are passed between I<btrfs> and the I<group> "
"name and affect behaviour not specific to the command, e.g. verbosity or the "
"type of the output."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--format>I<\\ E<lt>formatE<gt>>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"if supported by the command, print subcommand output in that format (text, "
"json)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v|--verbose>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "increase verbosity of the subcommand"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-q|--quiet>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print only errors"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--log>I<\\ E<lt>levelE<gt>>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "set log level (default, info, verbose, debug, quiet)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The remaining options are relevant only for the main tool:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print condensed help for all subcommands"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print version string"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<balance>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Balance btrfs filesystem chunks across single or several devices.  See I<\\"
"%btrfs-balance(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<check>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Do off-line check on a btrfs filesystem.  See I<\\%btrfs-check(8)> for "
"details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<device>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Manage devices managed by btrfs, including add/delete/scan and so on.  See "
"I<\\%btrfs-device(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<filesystem>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Manage a btrfs filesystem, including label setting/sync and so on.  See I<\\"
"%btrfs-filesystem(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<inspect-internal>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Debug tools for developers/hackers.  See I<\\%btrfs-inspect-internal(8)> for "
"details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<property>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Get/set a property from/to a btrfs object.  See I<\\%btrfs-property(8)> for "
"details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<qgroup>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Manage quota group(qgroup) for btrfs filesystem.  See I<\\%btrfs-qgroup(8)> "
"for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<quota>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Manage quota on btrfs filesystem like enabling/rescan and etc.  See I<\\"
"%btrfs-quota(8)> and I<\\%btrfs-qgroup(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<receive>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Receive subvolume data from stdin/file for restore and etc.  See I<\\%btrfs-"
"receive(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<replace>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Replace btrfs devices.  See I<\\%btrfs-replace(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<rescue>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Try to rescue damaged btrfs filesystem.  See I<\\%btrfs-rescue(8)> for "
"details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<restore>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Try to restore files from a damaged btrfs filesystem.  See I<\\%btrfs-"
"restore(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<scrub>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Scrub a btrfs filesystem.  See I<\\%btrfs-scrub(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<send>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Send subvolume data to stdout/file for backup and etc.  See I<\\%btrfs-"
"send(8)> for details."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<subvolume>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Create/delete/list/manage btrfs subvolume.  See I<\\%btrfs-subvolume(8)> for "
"details."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDALONE TOOLS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"New functionality could be provided using a standalone tool. If the "
"functionality proves to be useful, then the standalone tool is declared "
"obsolete and its functionality is copied to the main tool. Obsolete tools "
"are removed after a long (years) depreciation period."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Tools that are still in active use without an equivalent in B<btrfs>:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<btrfs-convert>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "in-place conversion from ext2/3/4 filesystems to btrfs"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<btrfstune>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "tweak some filesystem properties on a unmounted filesystem"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<btrfs-select-super>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "rescue tool to overwrite primary superblock from a spare copy"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<btrfs-find-root>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "rescue helper to find tree roots in a filesystem"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For space-constrained environments, it\\(aqs possible to build a single "
"binary with functionality of several standalone tools. This is following the "
"concept of busybox where the file name selects the functionality. This works "
"for symlinks or hardlinks. The full list can be obtained by B<btrfs help --"
"box>\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<btrfs> returns a zero exit status if it succeeds. Non zero is returned in "
"case of failure."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide
msgid ""
"B<btrfs> is part of btrfs-progs.  Please refer to the documentation at "
"\\X'tty: link https://btrfs.readthedocs.io'I<\\%https://"
"btrfs.readthedocs.io>\\X'tty: link'\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<\\%btrfs(5)>, I<\\%btrfs-balance(8)>, I<\\%btrfs-check(8)>, I<\\%btrfs-"
"convert(8)>, I<\\%btrfs-device(8)>, I<\\%btrfs-filesystem(8)>, I<\\%btrfs-"
"inspect-internal(8)>, I<\\%btrfs-property(8)>, I<\\%btrfs-qgroup(8)>, I<\\"
"%btrfs-quota(8)>, I<\\%btrfs-receive(8)>, I<\\%btrfs-replace(8)>, I<\\%btrfs-"
"rescue(8)>, I<\\%btrfs-restore(8)>, I<\\%btrfs-scrub(8)>, I<\\%btrfs-"
"send(8)>, I<\\%btrfs-subvolume(8)>, I<\\%btrfstune(8)>, I<\\%mkfs.btrfs(8)>"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Jul 16, 2024"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "6.6.3"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Sizes>, both upon input and output, can be expressed in either SI or IEC-I "
"units (see I<\\%numfmt(1)>)  with the suffix I<B> appended.  All numbers "
"will be formatted according to the rules of the I<C> locale (ignoring the "
"shell locale, see I<\\%locale(7)>)."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<btrfs> is part of btrfs-progs.  Please refer to the documentation at I<\\"
"%https://btrfs.readthedocs.io>\\&."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "Oct 28, 2024"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"I<Sizes>, both upon input and output, can be expressed in either SI or IEC-I "
"units (see \\X'tty: link https://www.man7.org/linux/man-pages/man1/"
"numfmt.1.html'I<\\%numfmt(1)>\\X'tty: link')  with the suffix I<B> "
"appended.  All numbers will be formatted according to the rules of the I<C> "
"locale (ignoring the shell locale, see \\X'tty: link https://man7.org/linux/"
"man-pages/man7/locale.7.html'I<\\%locale(7)>\\X'tty: link')."
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "Sep 17, 2024"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "6.11"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "Nov 29, 2024"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Mar 10, 2024"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "6.7.1"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "May 02, 2024"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "6.8.1"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Aug 15, 2024"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "6.10.1"
msgstr ""
