# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-06 18:26+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "RPMSPEC"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "29 October 2010"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "rpmspec - RPM Spec Tool"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "QUERYING SPEC FILES:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\f[B]rpmspec\\f[R] {\\f[B]-q|--query\\f[R]} [\\f[B]select-options\\f[R]] "
"[\\f[B]query-options\\f[R]] \\f[I]SPEC_FILE ...\\fR"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "PARSING SPEC FILES TO STDOUT:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\\f[B]rpmspec\\f[R] {\\f[B]-P|--parse\\f[R]} \\f[I]SPEC_FILE ...\\fR"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "INVOKING MACRO SHELL:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\f[B]rpmspec\\f[R] {\\f[B]\\[en]shell\\f[R]} [\\f[I]SPEC_FILE ...\\f[R]]\\fR"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\f[B]rpmspec\\f[R] is a tool for querying a spec file.  More specifically "
"for querying hypothetical packages which would be created from the given "
"spec file.  So querying a spec file with \\f[B]rpmspec\\f[R] is similar to "
"querying a package built from that spec file.  But is is not identical.  "
"With \\f[B]rpmspec\\f[R] you can\\[aq]t query all fields which you can query "
"from a built package.  E.  g.  you can\\[aq]t query BUILDTIME with "
"\\f[B]rpmspec\\f[R] for obvious reasons.  You also cannot query other fields "
"automatically generated during a build of a package like auto generated "
"dependencies.\\fR"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "select-options"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[\\f[B]--rpms\\f[R]] [\\f[B]--srpm\\f[R]]\\fR"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "query-options"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"[\\f[B]--qf,--queryformat \\f[I]QUERYFMT\\f[R]] [\\f[B]--target "
"\\f[I]TARGET_PLATFORM\\f[R]]\\fR"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "QUERY OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "The general form of an rpm spec query command is"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\f[B]rpm\\f[R] {\\f[B]-q|--query\\f[R]} [\\f[B]select-options\\f[R]] "
"[\\f[B]query-options\\f[R]]\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"You may specify the format that the information should be printed in.  To do "
"this, you use the"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\\f[B]--qf|--queryformat\\f[R] \\f[I]QUERYFMT\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"option, followed by the \\f[I]QUERYFMT\\f[R] format string.  See "
"\\f[B]rpm(8)\\f[R] for details.\\fR"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SELECT OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\f[B]--rpms\\f[R] Operate on the all binary package headers generated from "
"spec.  \\f[B]--builtrpms\\f[R] Operate only on the binary package headers of "
"packages which would be built from spec.  That means ignoring package "
"headers of packages that won\\[aq]t be built from spec i.  e.  ignoring "
"package headers of packages without file section.  \\f[B]--srpm\\f[R] "
"Operate on the source package header(s) generated from spec.\\fR"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Get list of binary packages which would be generated from the rpm spec file:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
" $ rpmspec -q rpm.spec\n"
" rpm-4.11.3-3.fc20.x86_64\n"
" rpm-libs-4.11.3-3.fc20.x86_64\n"
" rpm-build-libs-4.11.3-3.fc20.x86_64\n"
" ...\\fR\n"
"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Get summary infos for single binary packages generated from the rpm spec "
"file:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
" $ rpmspec -q --qf \\[dq]%{name}: %{summary}\\[rs]n\\[dq] rpm.spec\n"
" rpm: The RPM package management system\n"
" rpm-libs: Libraries for manipulating RPM packages\n"
" rpm-build-libs: Libraries for building and signing RPM packages\n"
" ...\\fR\n"
"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Get the source package which would be generated from the rpm spec file:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
" $ rpmspec -q --srpm rpm.spec\n"
" rpm-4.11.3-3.fc20.x86_64\\fR\n"
"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Parse the rpm spec file to stdout:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
" $ rpmspec -P rpm.spec\n"
" Summary: The RPM package management system\n"
" Name: rpm\n"
" Version: 4.14.0\n"
" ...\\fR\n"
"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Run interactive macro shell for debugging macros:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
" $ rpmspec --shell\n"
" E<gt> %define foo bar\n"
" E<gt> %foo\n"
" bar\n"
" E<gt> %(date)\n"
" Tue Apr 13 03:55:37 PM EEST 2021\n"
" E<gt> %getncpus\n"
" 8\\fR\n"
"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Run interactive macros shell in spec context:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
" $ rpmspec --shell popt.spec\n"
" %name\n"
" popt\n"
" %version\n"
" 1.18\\fR\n"
"\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"\\f[B]popt\\f[R](3), \\f[B]rpm\\f[R](8), \\f[B]rpmdb\\f[R](8), "
"\\f[B]rpmkeys\\f[R](8), \\f[B]rpmsign\\f[R](8), \\f[B]rpm2cpio\\f[R](8), "
"\\f[B]rpmbuild\\f[R](8)\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\f[B]rpmspec --help\\f[R] - as rpm supports customizing the options via "
"popt aliases it\\[aq]s impossible to guarantee that what\\[aq]s described in "
"the manual matches what\\[aq]s available.\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\\f[B]http://www.rpm.org/ E<lt>URL:http://www.rpm.org/E<gt>\\fR"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
"Marc Ewing E<lt>marc\\[at]redhat.comE<gt>\n"
"Jeff Johnson E<lt>jbj\\[at]redhat.comE<gt>\n"
"Erik Troan E<lt>ewt\\[at]redhat.comE<gt>\n"
"Panu Matilainen E<lt>pmatilai\\[at]redhat.comE<gt>\\fR\n"
"\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid ""
"\\f[C]\n"
"popt(3),\n"
"rpm(8),\n"
"rpmdb(8),\n"
"rpmkeys(8),\n"
"rpmsign(8),\n"
"rpm2cpio(8),\n"
"rpmbuild(8),\\fR\n"
"\n"
msgstr ""
