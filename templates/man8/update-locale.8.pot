# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 20:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "UPDATE-LOCALE"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "April 2006"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Debian GNU/Linux"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "update-locale - Modify global locale settings"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNTAX"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "update-locale [I<OPTIONS>] [I<var>=I<locale> | I<var>] [...]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"This program can be called by maintainer scripts when Debian packages are "
"installed or removed, it updates the I</etc/default/locale> file to reflect "
"changes in system configuration related to global locale settings.  When "
"variables have no value assigned, they are removed from the locale file.  "
"Some basic checks are performed to ensure that requested settings are valid."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Display an help message and exit."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--reset>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Variables which are not set on command-line are cleared out."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--locale-file>I< FILE>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Define file containing locale variables.  (Default: I</etc/default/locale>)"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--no-checks>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Do not perform sanity checks on locale variables."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid "The command\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<        update-locale LANG=en_CA.UTF-8 LANGUAGE>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid "sets B<LANG> to B<en_CA.UTF-8> and removes definitions for B<LANGUAGE>.\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "I</etc/default/locale>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "File where global locale settings are stored."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Denis Barbier E<lt>barbier@linuxfr.orgE<gt>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<locale-gen>(8), B<locale>(1)"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"This program can be called by maintainer scripts when Debian packages are "
"installed or removed, it updates the I</etc/locale.conf> file to reflect "
"changes in system configuration related to global locale settings.  When "
"variables have no value assigned, they are removed from the locale file.  "
"Some basic checks are performed to ensure that requested settings are valid."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"Define file containing locale variables.  (Default: I</etc/locale.conf>)"
msgstr ""

#. type: TP
#: debian-unstable
#, no-wrap
msgid "I</etc/locale.conf>"
msgstr ""
