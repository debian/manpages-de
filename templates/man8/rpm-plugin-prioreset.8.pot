# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-18 20:37+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "RPM-PRIORESET"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "28 Jan 2020"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"rpm-plugin-prioreset - Plugin for the RPM Package Manager to fix issues with "
"priorities of deamons on SysV init"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"In general scriptlets run with the same priority as rpm itself.  However on "
"legacy SysV init systems, properties of the parent process can be inherited "
"by the actual daemons on restart.  As a result daemons may end up with "
"unwanted nice or ionice values.  This plugin resets the scriptlet process "
"priorities after forking, and can be used to counter that effect.  Should "
"not be used with systemd because it\\[aq]s not needed there, and the effect "
"is counter-productive."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Configuration"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"There are currently no options for this plugin in particular.  See \\f[B]rpm-"
"plugins\\f[R](8) on how to control plugins in general.\\fR"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "\\f[B]rpm\\f[R](8), \\f[B]rpm-plugins\\f[R](8)\\fR"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "\\f[I]rpm\\f[R](8) \\f[I]rpm-plugins\\f[R](8)\\fR"
msgstr ""
