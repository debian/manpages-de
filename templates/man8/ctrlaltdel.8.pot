# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:38+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "CTRLALTDEL"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "ctrlaltdel - set the function of the Ctrl-Alt-Del combination"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<ctrlaltdel> B<hard>|B<soft>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Based on examination of the I<linux/kernel/reboot.c> code, it is clear that "
"there are two supported functions that the E<lt>Ctrl-Alt-DelE<gt> sequence "
"can perform."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<hard>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Immediately reboot the computer without calling B<sync>(2) and without any "
"other preparation. This is the default."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<soft>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Make the kernel send the B<SIGINT> (interrupt) signal to the B<init> process "
"(this is always the process with PID 1). If this option is used, the "
"B<init>(8) program must support this feature. Since there are now several "
"B<init>(8) programs in the Linux community, please consult the documentation "
"for the version that you are currently using."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When the command is run without any argument, it will display the current "
"setting."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The function of B<ctrlaltdel> is usually set in the I</etc/rc.local> file."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I</etc/rc.local>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<init>(8), B<systemd>(1)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<ctrlaltdel> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
