# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:43+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-INITCTL\\&.SERVICE"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "systemd-initctl.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-initctl.service, systemd-initctl.socket, systemd-initctl - /dev/"
"initctl compatibility"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-initctl\\&.service"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-initctl\\&.socket"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-initctl"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-initctl is a system service that implements compatibility with the /"
"dev/initctl FIFO file system object, as implemented by the SysV init "
"system\\&.  systemd-initctl is automatically activated on request and "
"terminates itself when it is unused\\&."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd>(1)"
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/usr/lib/systemd/systemd-initctl"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 256.8"
msgstr ""
