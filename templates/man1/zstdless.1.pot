# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-06 18:36+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ZSTDLESS"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "March 2024"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "zstd 1.5.6"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<zstdless> - view zstandard-compressed files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<zstdless> [I<flags>] [I<file> ...]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<zstdless> runs B<less>(1) on files or stdin, if no I<file> argument is "
"given, after decompressing them with B<zstdcat>(1)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<zstd>(1)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2023"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "zstd 1.5.4"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<zstdless> [I<flags>] [I<file> \\|.\\|.\\|.]"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "March 2023"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "zstd 1.5.5"
msgstr ""
