# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-11-28 09:39+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Pamundice User Manual"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "26 April 2020"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "pamundice - combine grid of images (tiles) into one"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"CW<\n"
"    $ pamdice myimage.ppm -outstem=myimage_part -width=10 -height=8\n"
"    $ pamundice myimage_part_%1d_%1a.ppm -across=10 -down=8 E<gt>myimage.ppm>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"CW<    $ pamundice myimage.ppm myimage_part_%2a -across=13 -hoverlap=9>\n"
"\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<pamundice>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[B<-across=>I<n>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[B<-down=>I<n>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[B<-hoverlap=>I<pixels>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[B<-voverlap=>I<pixels>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[B<-verbose>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "{I<input_filename_pattern>, B<-listfile=>I<filename>}"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"You can use the minimum unique abbreviation of the options.  You can use two "
"hyphens instead of one.  You can separate an option name from its value with "
"white space instead of an equals sign."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<pamundice> reads a bunch of Netpbm images as input and combines them as a "
"grid of tiles into a single output image of the same kind on Standard Output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "You can optionally make the pieces overlap."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"The images can either be in files whose names indicate where they go in the\n"
"  output (e.g. 'myimage_part_03_04' could be the image for Row 3,\n"
"  Column 4 - see the I<input_filename_pattern> argument) or listed in a\n"
"  file, with a B<-listfile> option.\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The input images must all have the same format (PAM, PPM, etc.)  and maxval "
"and for PAM must have the same depth and tuple type.  All the images in a "
"rank (horizontal row of tiles) must have the same height.  All the images in "
"a file (vertical column of tiles)  must have the same width.  But it is not "
"required that every rank have the same height or every file have the same "
"width."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<pamdice> is the inverse of B<pamundice>.  You can use B<pamundice> to "
"reassemble an image sliced up by B<pamdice>.  You can use B<pamdice> to "
"recreate the tiles of an image created by B<pamundice>, but to do this, the "
"original ranks must all have been the same height except for the bottom one "
"and the original files must all have been the same width except the right "
"one."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"One use for this is to process an image in pieces when the whole image is "
"too large to process.  For example, you might have an image so large that an "
"image editor can't read it all into memory or processes it very slowly.  You "
"can split it into smaller pieces with B<pamdice>, edit one at a time, and "
"then reassemble them with B<pamundice>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Of course, you can also use B<pamundice> to compose various kinds of "
"checkerboard images, for example, you could write a program to render a "
"chessboard by computing an image of each square, then using B<pamundice> to "
"assemble them into a board."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 fedora-rawhide
msgid ""
"An alternative to join images in a single direction (i.e. a single rank or a "
"single file) is B<pnmcat>.  B<pnmcat> gives you more flexibility than "
"B<pamundice> in identifying the input images: you can supply them on "
"Standard Input or as a list of arbitrarily named files."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"To join piecewise photographs, use B<pnmstitch> instead of B<pamundice>, "
"because it figures out where the pieces overlap, even if they don't overlap "
"exactly vertically or horizontally."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"To create an image of the same tile repeated in a grid, that's B<pnmtile>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<pnmindex> does a similar thing to B<pamundice>: it combines a bunch of "
"small images in a grid into a big one.  But its purpose is to produce a an "
"index image of the input images.  So it leaves space between them and has "
"labels for them, for example."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "ARGUMENTS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Unless you use a B<-listfile> option,, there is one non-option argument, and "
"it is mandatory: I<input_filename_pattern>.  This tells B<pamundice> what "
"files contain the input tiles."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<pamundice> reads the input images from files which are named with a "
"pattern that indicates their positions in the combined image.  For example, "
"B<tile_00_05.ppm> could be the 6th tile over in the 1st rank, while "
"B<tile_04_01> is the 2nd tile over in the 5th rank."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"You cannot supply any of the data on Standard Input, and the files must be "
"the kind that B<pamundice> can close and reopen and read the same image a "
"second time (e.g. a regular file is fine; a named pipe is probably not)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<input_filename_pattern> is a printf-style pattern.  (See the standard C "
"library B<printf> subroutine).  For the example above, it would be "
"B<tile_%2d_%2a.ppm>.  The only possible conversion specifiers are:"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<d>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\"down\": The rank (row) number, starting with 0."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<a>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\"across\": The file (column) number, starting with 0."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<%>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "The per cent character (%)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number between the % and the conversion specifier is the precision and "
"is required.  It says how many characters of the file name are described by "
"that conversion.  The rank or file number is filled with leading zeroes as "
"necessary."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"So the example B<tile_%2d_%2a.ppm> means to get the name of the file that "
"contains the tile at Rank 0, File 5, you:"
msgstr ""

#. type: IP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"replace the \"%2d\" with the rank number, as a 2 digit decimal number: \"00\""
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Replace the \"%2a\" with the file number, as a 2 digit decimal number: \"05\""
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Note that this pattern describes file names that B<pamdice> produces, except "
"that the precision may be more or less.  (B<pamdice> uses however many "
"digits are required for the highest numbered image)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"In addition to the options common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<pamundice> recognizes the following\n"
"command line options:\n"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-across=>I<N>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This is the number of tiles across in the grid, i.e. the number of tiles in "
"each rank, or the number of files."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Default is 1."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-down=>I<N>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This is the number of tiles up and down in the grid, i.e. the number of "
"tiles in each file, or the number of ranks."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-hoverlap=>I<pixels>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This is the amount in pixels to overlap the tiles horizontally.  "
"B<pamundice> clips this much off the right edge of every tile before joining "
"it to the adjacent image to the right.  The tiles along the right edge "
"remain whole."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "There must not be any input image narrower than this."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Note that this undoes the effect of the same B<-hoverlap> option of "
"B<pamdice>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Default is zero -- no overlap."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-voverlap=>I<pixels>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This is analogous to B<-hoverlap>, but B<pamundice> clips the bottom edge of "
"each image before joining it to the one below."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-listfile=>I<filename>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This option names a file that contains the names of all the input files.  "
"This is an alternative to specifying a file name pattern as an argument."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"The named file contains file name, one per line.  Each file contains the\n"
"  image for one tile, in row-major order, top to bottom, left to right.  So\n"
"  the first file is the upper left tile, the second is the one to right of\n"
"  that, etc.  The number of lines in the file must be equal to the number of\n"
"  tiles in the output, the product of the B<-across> and B<-down>\n"
"  values.\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"The file names have no meaning to B<pamundice>.  You can use the same\n"
"  file multiple times to have identical tiles in the output.\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "This option was new in Netpbm 10.90 (March 2020)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Print information about the processing to Standard Error."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 fedora-rawhide
msgid ""
"B<pamundice> was new in Netpbm 10.39 (June 2007).  Before that, B<pnmcat> is "
"the best substitute."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-41 fedora-rawhide
msgid ""
"B<pamdice>(1)  \\&, B<pnmcat>(1)  \\&, B<pnmindex>(1)  \\&, B<pnmtile>(1)  "
"\\&, B<pnm>(1)  \\& B<pam>(1)  \\&"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/pamundice.html>"
msgstr ""

#. type: Plain text
#: debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An alternative to join images in a single direction (i.e. a single rank or a "
"single file) is B<pamcat>.  B<pamcat> gives you more flexibility than "
"B<pamundice> in identifying the input images: you can supply them on "
"Standard Input or as a list of arbitrarily named files."
msgstr ""

#. type: Plain text
#: debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamundice> was new in Netpbm 10.39 (June 2007).  Before that, B<pamcat> is "
"the best substitute."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"B<pamdice>(1)  \\&, B<pamcat>(1)  \\&, B<pnmindex>(1)  \\&, B<pnmtile>(1)  "
"\\&, B<pnm>(1)  \\& B<pam>(1)  \\&"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pamdice>(1)  \\&, B<pamcat>(1)  \\&, B<pnmindex>(1)  \\&, B<pnmtile>(1)  "
"\\&, B<pnm>(5)  \\& B<pam>(5)  \\&"
msgstr ""
