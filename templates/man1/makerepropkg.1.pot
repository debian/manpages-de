# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-10-01 02:42-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "MAKEREPROPKG"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-06-18"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "makerepropkg - Rebuild a package to see if it is reproducible"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "makerepropkg [OPTIONS] [E<lt>package_file|pkgnameE<gt>...]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Given the path to a built pacman package(s), attempt to rebuild it using the "
"PKGBUILD in the current directory. The package will be built in an "
"environment as closely matching the environment of the initial package as "
"possible, by building up a chroot to match the information exposed in the "
"package\\(cqs BUILDINFO(5) manifest. On success, the resulting package will "
"be compared to the input package, and makerepropkg will report whether the "
"artifacts are identical."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"When given multiple packages, additional package files are assumed to be "
"split packages and will be treated as additional artifacts to compare during "
"the verification step."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"A valid target(s) for pacman -S can be specified instead, and makerepropkg "
"will download it to the cache if needed. This is mostly useful to specify "
"which repository to retrieve from. If no positional arguments are specified, "
"the targets will be sourced from the PKGBUILD."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"In either case, the package name will be converted to a filename from the "
"cache, and makerepropkg will proceed as though this filename was initially "
"specified."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"This implements a verifier for pacman/libalpm packages in accordance with the"
msgstr ""

#. type: Plain text
#: archlinux
msgid "project."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-d>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "If packages are not reproducible, compare them using diffoscope."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-n>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Do not run the check() function in the PKGBUILD."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-c>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set the pacman cache directory."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-M> E<lt>fileE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Location of a makepkg config file."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-l> E<lt>chrootE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The directory name to use as the chroot namespace Useful for maintaining "
"multiple copies Default: $USER"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show this usage message"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
