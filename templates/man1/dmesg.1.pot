# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:21+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "DMESG"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "dmesg - print or control the kernel ring buffer"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg> [options]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg> B<--clear>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg> B<--read-clear> [options]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg> B<--console-level> I<level>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg> B<--console-on>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg> B<--console-off>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg> is used to examine or control the kernel ring buffer."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default action is to display all messages from the kernel ring buffer."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<--clear>, B<--read-clear>, B<--console-on>, B<--console-off>, and B<--"
"console-level> options are mutually exclusive."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-C>, B<--clear>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Clear the ring buffer."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--read-clear>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Clear the ring buffer after first printing its contents."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-D>, B<--console-off>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Disable the printing of messages to the console."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-d>, B<--show-delta>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display the timestamp and the time delta spent between messages. If used "
"together with B<--notime> then only the time delta without the timestamp is "
"printed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-E>, B<--console-on>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Enable printing messages to the console."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--reltime>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display the local time and the delta in human-readable format. Be aware that "
"conversion to the local time could be inaccurate (see B<-T> for more "
"details)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-F>, B<--file> I<file>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Read the syslog messages from the given I<file>. Note that B<-F> does not "
"support messages in kmsg format. The old syslog format is supported only."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-f>, B<--facility> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Restrict output to the given (comma-separated) I<list> of facilities. For "
"example:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg --facility=daemon>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"will print messages from system daemons only. For all supported facilities "
"see the B<--help> output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-H>, B<--human>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Enable human-readable output. See also B<--color>, B<--reltime> and B<--"
"nopager>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Use JSON output format. The time output format is in \"sec.usec\" format "
"only, log priority level is not decoded by default (use B<--decode> to split "
"into facility and priority), the other options to control the output format "
"or time format are silently ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-k>, B<--kernel>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print kernel messages."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-L>, B<--color>[=I<when>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Colorize the output. The optional argument I<when> can be B<auto>, B<never> "
"or B<always>. If the I<when> argument is omitted, it defaults to B<auto>. "
"The colors can be disabled; for the current built-in default see the B<--"
"help> output. See also the B<COLORS> section below."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--level> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Restrict output to the given (comma-separated) I<list> of levels. For "
"example:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg --level=err,warn>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"will print error and warning messages only. For all supported levels see the "
"B<--help> output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--console-level> I<level>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Set the I<level> at which printing of messages is done to the console. The "
"I<level> is a level number or abbreviation of the level name. For all "
"supported levels see the B<--help> output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"For example, B<-n 1> or B<-n emerg> prevents all messages, except emergency "
"(panic) messages, from appearing on the console. All levels of messages are "
"still written to I</proc/kmsg>, so B<syslogd>(8) can still be used to "
"control exactly where kernel messages appear. When the B<-n> option is used, "
"B<dmesg> will I<not> print or clear the kernel ring buffer."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--noescape>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The unprintable and potentially unsafe characters (e.g., broken multi-byte "
"sequences, terminal controlling chars, etc.) are escaped in format \\"
"(rsxE<lt>hexE<gt> for security reason by default. This option disables this "
"feature at all. It\\(cqs usable for example for debugging purpose together "
"with B<--raw>. Be careful and don\\(cqt use it by default."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-P>, B<--nopager>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Do not pipe output into a pager. A pager is enabled by default for B<--"
"human> output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--force-prefix>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Add facility, level or timestamp information to each line of a multi-line "
"message."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print the raw message buffer, i.e., do not strip the log-level prefixes, but "
"all unprintable characters are still escaped (see also B<--noescape>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that the real raw format depends on the method how B<dmesg> reads "
"kernel messages. The I</dev/kmsg> device uses a different format than "
"B<syslog>(2). For backward compatibility, B<dmesg> returns data always in "
"the B<syslog>(2) format. It is possible to read the real raw data from I</"
"dev/kmsg> by, for example, the command \\(aqdd if=/dev/kmsg iflag=nonblock\\"
"(aq."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-S>, B<--syslog>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Force B<dmesg> to use the B<syslog>(2) kernel interface to read kernel "
"messages. The default is to use I</dev/kmsg> rather than B<syslog>(2) since "
"kernel 3.5.0."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--buffer-size> I<size>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Use a buffer of I<size> to query the kernel ring buffer. This is 16392 by "
"default. (The default kernel syslog buffer size was 4096 at first, 8192 "
"since 1.3.54, 16384 since 2.1.113.) If you have set the kernel buffer to be "
"larger than the default, then this option can be used to view the entire "
"buffer."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-T>, B<--ctime>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print human-readable timestamps."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<Be aware that the timestamp could be inaccurate!> The B<time> source used "
"for the logs is B<not updated after> system B<SUSPEND>/B<RESUME>. Timestamps "
"are adjusted according to current delta between boottime and monotonic "
"clocks, this works only for messages printed after last resume."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--since> I<time>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display record since the specified time. The time is possible to specify in "
"absolute way as well as by relative notation (e.g. \\(aq1 hour ago\\(aq). Be "
"aware that the timestamp could be inaccurate and see B<--ctime> for more "
"details."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--until> I<time>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display record until the specified time. The time is possible to specify in "
"absolute way as well as by relative notation (e.g. \\(aq1 hour ago\\(aq). Be "
"aware that the timestamp could be inaccurate and see B<--ctime> for more "
"details."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--notime>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not print kernel\\(cqs timestamps."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--time-format> I<format>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print timestamps using the given I<format>, which can be B<ctime>, "
"B<reltime>, B<delta> or B<iso>. The first three formats are aliases of the "
"time-format-specific options. The B<iso> format is a B<dmesg> implementation "
"of the ISO-8601 timestamp format. The purpose of this format is to make the "
"comparing of timestamps between two systems, and any other parsing, easy. "
"The definition of the B<iso> timestamp is: YYYY-MM-"
"DDE<lt>TE<gt>HH:MM:SS,E<lt>microsecondsE<gt>\\(E<lt>-+E<gt>E<lt>timezone "
"offset from UTCE<gt>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<iso> format has the same issue as B<ctime>: the time may be inaccurate "
"when a system is suspended and resumed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-u>, B<--userspace>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print userspace messages."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-w>, B<--follow>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Wait for new messages. This feature is supported only on systems with a "
"readable I</dev/kmsg> (since kernel 3.5.0)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-W>, B<--follow-new>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Wait and print only new messages."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-x>, B<--decode>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Decode facility and level (priority) numbers to human-readable prefixes."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "COLORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The output colorization is implemented by B<terminal-colors.d>(5) "
"functionality.  Implicit coloring can be disabled by an empty file"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I</etc/terminal-colors.d/dmesg.disable>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "for the B<dmesg> command or for all tools by"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I</etc/terminal-colors.d/disable>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The user-specific I<$XDG_CONFIG_HOME/terminal-colors.d> or I<$HOME/.config/"
"terminal-colors.d> overrides the global setting."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that the output colorization may be enabled by default, and in this "
"case I<terminal-colors.d> directories do not have to exist yet."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The logical color names supported by B<dmesg> are:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<subsys>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The message sub-system prefix (e.g., \"ACPI:\")."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<time>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The message timestamp."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<timebreak>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The message timestamp in short ctime format in B<--reltime> or B<--human> "
"output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<alert>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The text of the message with the alert log priority."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<crit>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The text of the message with the critical log priority."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<err>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The text of the message with the error log priority."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<warn>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The text of the message with the warning log priority."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<segfault>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The text of the message that inform about segmentation fault."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<dmesg> can fail reporting permission denied error. This is usually caused "
"by B<dmesg_restrict> kernel setting, please see B<syslog>(2) for more "
"details."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<dmesg> was originally written by"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<terminal-colors.d>(5), B<syslogd>(8)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<dmesg> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
