# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:33+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#.  ========================================================================
#. type: IX
#: archlinux
#, no-wrap
msgid "Title"
msgstr ""

#.  ========================================================================
#. type: IX
#: archlinux
#, no-wrap
msgid "PERLBOOK 1perl"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "PERLBOOK"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-09-01"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "perl v5.40.0"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Perl Programmers Reference Guide"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "perlbook - Books about and related to Perl"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Header"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"There are many books on Perl and Perl-related. A few of these are good, some "
"are OK, but many aren't worth your money. There is a list of these books, "
"some with extensive reviews, at E<lt>https://www.perl.org/books/"
"library.htmlE<gt> . We list some of the books here, and while listing a book "
"implies our endorsement, don't think that not including a book means "
"anything."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Most of these books are available online through O'Reilly Online Learning "
"( E<lt>https://www.oreilly.comE<gt> )."
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "The most popular books"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Subsection"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The major reference book on Perl, written by the creator of Perl, is "
"\\&I<Programming Perl>:"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Programming Perl> (the \"Camel Book\"):"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Item"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Programming Perl (the \"Camel Book\"):"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Tom Christiansen, brian d foy, Larry Wall with Jon Orwant \\& ISBN "
"978-0-596-00492-7 [4th edition February 2012] \\& ISBN 978-1-4493-9890-3 "
"[ebook] \\& https://www.oreilly.com/library/view/-/9781449321451/"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The Ram is a cookbook with hundreds of examples of using Perl to accomplish "
"specific tasks:"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<The Perl Cookbook> (the \"Ram Book\"):"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "The Perl Cookbook (the \"Ram Book\"):"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Tom Christiansen and Nathan Torkington, \\& with Foreword by Larry "
"Wall \\& ISBN 978-0-596-00313-5 [2nd Edition August 2003] \\& ISBN "
"978-0-596-15888-0 [ebook] \\& https://www.oreilly.com/library/view/-/"
"0596003137/"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"If you want to learn the basics of Perl, you might start with the Llama "
"book, which assumes that you already know a little about programming:"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Learning Perl>  (the \"Llama Book\")"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Learning Perl (the \"Llama Book\")"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Randal L. Schwartz, Tom Phoenix, and brian d foy \\& ISBN "
"978-1-4920-9495-1 [8th edition August 2021] \\& ISBN 978-1-4920-9492-0 "
"[ebook] \\& https://www.learning-perl.com/"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The tutorial started in the Llama continues in the Alpaca, which introduces "
"the intermediate features of references, data structures, object-oriented "
"programming, and modules:"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Intermediate Perl> (the \"Alpaca Book\")"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Intermediate Perl (the \"Alpaca Book\")"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Randal L. Schwartz and brian d foy, with Tom Phoenix \\& foreword by "
"Damian Conway \\& ISBN 978-1-4493-9309-0 [2nd edition August 2012] \\& ISBN "
"978-1-4493-0459-1 [ebook] \\& https://www.intermediateperl.com/"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "References"
msgstr ""

#. type: Plain text
#: archlinux
msgid "You might want to keep these desktop references close by your keyboard:"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Perl 5 Pocket Reference>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Perl 5 Pocket Reference"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Johan Vromans \\& ISBN 978-1-4493-0370-9 [5th edition July 2011] \\& "
"ISBN 978-1-4493-0813-1 [ebook] \\& https://www.oreilly.com/library/view/-/"
"9781449311186/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Perl Debugger Pocket Reference>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Perl Debugger Pocket Reference"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Richard Foley \\& ISBN 978-0-596-00503-0 [1st edition January 2004] "
"\\& ISBN 978-0-596-55625-9 [ebook] \\& https://www.oreilly.com/library/"
"view/-/9781449311186/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Regular Expression Pocket Reference>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Regular Expression Pocket Reference"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Tony Stubblebine \\& ISBN 978-0-596-51427-3 [2nd edition July 2007] "
"\\& ISBN 978-0-596-55782-9 [ebook] \\& https://www.oreilly.com/library/"
"view/-/9780596514273/"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Tutorials"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Beginning Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Beginning Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid "(There are 2 books with this title)"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Curtis \\*(AqOvid\\*(Aq Poe \\& ISBN 978-1-118-01384-7 \\& https://"
"www.wiley.com/en-ie/Beginning+Perl-p-9781118235638 \\& \\& by James Lee \\& "
"ISBN 1-59059-391-X [3rd edition April 2010 & ebook] \\& https://"
"www.apress.com/9781430227939"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Learning Perl> (the \"Llama Book\")"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Randal L. Schwartz, Tom Phoenix, and brian d foy \\& ISBN "
"978-1-4493-0358-7 [6th edition June 2011] \\& ISBN 978-1-4493-0458-4 [ebook] "
"\\& https://www.learning-perl.com/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Mastering Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Mastering Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by brian d foy \\& ISBN 9978-1-4493-9311-3 [2st edition January 2014] "
"\\& ISBN 978-1-4493-6487-8 [ebook] \\& https://www.masteringperl.org/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Effective Perl Programming>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Effective Perl Programming"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Joseph N. Hall, Joshua A. McAdams, brian d foy \\& ISBN 0-321-49694-9 "
"[2nd edition 2010] \\& https://www.effectiveperlprogramming.com/"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Task-Oriented"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Writing Perl Modules for CPAN>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Writing Perl Modules for CPAN"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Sam Tregar \\& ISBN 1-59059-018-X [1st edition August 2002 & ebook] "
"\\& https://www.apress.com/9781590590188E<gt>"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<The Perl Cookbook>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "The Perl Cookbook"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Automating System Administration with Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Automating System Administration with Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by David N. Blank-Edelman \\& ISBN 978-0-596-00639-6 [2nd edition May "
"2009] \\& ISBN 978-0-596-80251-6 [ebook] \\& https://www.oreilly.com/library/"
"view/-/9780596801892/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Real World SQL Server Administration with Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Real World SQL Server Administration with Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Linchi Shea \\& ISBN 1-59059-097-X [1st edition July 2003 & ebook] "
"\\& https://www.apress.com/9781590590973"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Special Topics"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Regular Expressions Cookbook>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Regular Expressions Cookbook"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Jan Goyvaerts and Steven Levithan \\& ISBN 978-1-4493-1943-4 [2nd "
"edition August 2012] \\& ISBN 978-1-4493-2747-7 [ebook] \\& https://"
"shop.oreilly.com/product/0636920023630.do"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Programming the Perl DBI>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Programming the Perl DBI"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Tim Bunce and Alligator Descartes \\& ISBN 978-1-56592-699-8 "
"[February 2000] \\& ISBN 978-1-4493-8670-2 [ebook] \\& https://"
"www.oreilly.com/library/view/-/1565926994/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Perl Best Practices>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Perl Best Practices"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Damian Conway \\& ISBN 978-0-596-00173-5 [1st edition July 2005] \\& "
"ISBN 978-0-596-15900-9 [ebook] \\& https://www.oreilly.com/library/view/-/"
"0596001738/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Higher-Order Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Higher-Order Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Mark-Jason Dominus \\& ISBN 1-55860-701-3 [1st edition March 2005] "
"\\& free ebook https://hop.perl.plover.com/book/ \\& https://"
"hop.perl.plover.com/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Mastering Regular Expressions>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Mastering Regular Expressions"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Jeffrey E. F. Friedl \\& ISBN 978-0-596-52812-6 [3rd edition August "
"2006] \\& ISBN 978-0-596-55899-4 [ebook] \\& https://learning.oreilly.com/"
"library/view/-/0596528124/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Network Programming with Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Network Programming with Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Lincoln Stein \\& ISBN 0-201-61571-1 [1st edition 2001] \\& https://"
"www.oreilly.com/library/view/-/0201615711/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Perl Template Toolkit>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Perl Template Toolkit"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Darren Chamberlain, Dave Cross, and Andy Wardley \\& ISBN "
"978-0-596-00476-7 [December 2003] \\& ISBN 978-1-4493-8647-4 [ebook] \\& "
"https://www.oreilly.com/library/view/-/0596004761/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Object Oriented Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Object Oriented Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Damian Conway \\& with foreword by Randal L. Schwartz \\& ISBN "
"1-884777-79-1 [1st edition August 1999 & ebook] \\& https://www.manning.com/"
"conway/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Data Munging with Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Data Munging with Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Dave Cross \\& ISBN 1-930110-00-6 [1st edition 2001 & ebook] \\& "
"https://www.manning.com/crossE<gt>"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Mastering Perl/Tk>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Mastering Perl/Tk"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Steve Lidie and Nancy Walsh \\& ISBN 978-1-56592-716-2 [1st edition "
"January 2002] \\& ISBN 978-0-596-10344-6 [ebook] \\& https://www.oreilly.com/"
"library/view/-/1565927168/"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Extending and Embedding Perl>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Extending and Embedding Perl"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Tim Jenness and Simon Cozens \\& ISBN 1-930110-82-0 [1st edition "
"August 2002 & ebook] \\& https://www.manning.com/jenness"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Pro Perl Debugging>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Pro Perl Debugging"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& by Richard Foley with Andy Lester \\& ISBN 1-59059-454-1 [1st edition "
"July 2005 & ebook] \\& https://www.apress.com/9781590594544"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Free (as in beer) books"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Some of these books are available as free downloads."
msgstr ""

#. type: Plain text
#: archlinux
msgid "\\&I<Higher-Order Perl>: E<lt>https://hop.perl.plover.com/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "\\&I<Modern Perl>: E<lt>https://onyxneon.com/books/modern_perl/E<gt>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Other interesting, non-Perl books"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"You might notice several familiar Perl concepts in this collection of ACM "
"columns from Jon Bentley. The similarity to the title of the major Perl book "
"(which came later) is not completely accidental:"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<Programming Pearls>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Programming Pearls"
msgstr ""

#. type: Plain text
#: archlinux
msgid "\\& by Jon Bentley \\& ISBN 978-0-201-65788-3 [2 edition, October 1999]"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "I<More Programming Pearls>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "More Programming Pearls"
msgstr ""

#. type: Plain text
#: archlinux
msgid "\\& by Jon Bentley \\& ISBN 0-201-11889-0 [January 1988]"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "A note on freshness"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Each version of Perl comes with the documentation that was current at the "
"time of release. This poses a problem for content such as book lists. There "
"are probably very nice books published after this list was included in your "
"Perl release, and you can check the latest released version at E<lt>https://"
"perldoc.perl.org/perlbook.htmlE<gt> ."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Some of the books we've listed appear almost ancient in internet scale, but "
"we've included those books because they still describe the current way of "
"doing things. Not everything in Perl changes every day.  Many of the "
"beginner-level books, too, go over basic features and techniques that are "
"still valid today. In general though, we try to limit this list to books "
"published in the past five years."
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Get your book listed"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"If your Perl book isn't listed and you think it should be, let us know.  "
"E<lt>mailto:perl5-porters@perl.orgE<gt>"
msgstr ""
