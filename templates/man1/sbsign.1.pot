# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:39+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "SBSIGN"
msgstr ""

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "September 2024"
msgstr ""

#. type: TH
#: archlinux fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "sbsign 0.9.5"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid "sbsign - UEFI secure boot signing tool"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"B<sbsign> [I<\\,options\\/>] I<\\,--key E<lt>keyfileE<gt> --cert "
"E<lt>certfileE<gt> E<lt>efi-boot-imageE<gt>\\/>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid "Sign an EFI boot image for use with secure boot."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--engine> E<lt>engE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid "use the specified engine to load the key"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--key> E<lt>keyfileE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid "signing key (PEM-encoded RSA private key)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--cert> E<lt>certfileE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid "certificate (x509 certificate)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"B<--addcert> E<lt>addcertfileE<gt> additional intermediate certificates in a "
"file"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--detached>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid "write a detached signature, instead of a signed binary"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--output> E<lt>fileE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"write signed data to E<lt>fileE<gt> (default E<lt>efi-boot-"
"imageE<gt>.signed, or E<lt>efi-boot-imageE<gt>.pk7 for detached signatures)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "June 2022"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "sbsign 0.9.4"
msgstr ""

#. type: TH
#: fedora-41
#, no-wrap
msgid "August 2024"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "November 2024"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr ""
