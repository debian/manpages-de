# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:34+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PKGCTL-REPO"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-06-18"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl-repo - Manage Git packaging repositories and their configuration"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo [OPTIONS] [SUBCOMMAND]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Manage Git packaging repositories and helps with their configuration "
"according to distro specs."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Git author information and the used signing key is set up from "
"I<makepkg.conf> read from any valid location like I</etc> or "
"I<XDG_CONFIG_HOME>.  The configure command can be used to synchronize the "
"distro specs and makepkg.conf settings for previously cloned repositories."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The unprivileged option can be used for cloning packaging repositories "
"without SSH access using read-only HTTPS."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show a help text"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SUBCOMMANDS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo clean"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Remove untracked files from the working tree"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo clone"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Clone a package repository"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo configure"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Configure a clone according to distro specs"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo create"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Create a new GitLab package repository"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo switch"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Switch a package repository to a specified version"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo web"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Open the packaging repository\\(cqs website"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"pkgctl-repo-clean(1)  pkgctl-repo-clone(1)  pkgctl-repo-configure(1)  pkgctl-"
"repo-create(1)  pkgctl-repo-switch(1)  pkgctl-repo-web(1)"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
