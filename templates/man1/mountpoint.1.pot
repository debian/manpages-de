# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:45+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "MOUNTPOINT"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "mountpoint - see if a directory or file is a mountpoint"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<mountpoint> [B<-d>|B<-q>] I<directory>|I<file>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<mountpoint> B<-x> I<device>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<mountpoint> checks whether the given I<directory> or I<file> is mentioned "
"in the I</proc/self/mountinfo> file."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-d>, B<--fs-devno>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Show the major/minor numbers of the device that is mounted on the given "
"directory."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-q>, B<--quiet>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Be quiet - don\\(cqt print anything."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--nofollow>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Do not follow symbolic link if it the last element of the I<directory> path."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-x>, B<--devno>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Show the major/minor numbers of the given blockdevice on standard output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<mountpoint> has the following exit status values:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<0>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"success; the directory is a mountpoint, or device is block device on B<--"
"devno>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<1>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "failure; incorrect invocation, permissions or system error"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<32>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"failure; the directory is not a mountpoint, or device is not a block device "
"on B<--devno>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "enables libmount debug output."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The util-linux B<mountpoint> implementation was written from scratch for "
"libmount. The original version for sysvinit suite was written by Miquel van "
"Smoorenburg."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<mount>(8)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<mountpoint> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
