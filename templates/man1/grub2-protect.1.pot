# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 18:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB-PROTECT"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "October 2024"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "grub-protect - protect a disk key with a key protector"
msgstr ""

#. type: SH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<grub-protect> [I<\\,OPTION\\/>...]"
msgstr ""

#. type: SH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"grub-protect helps to pretect a disk encryption key with a specified key "
"protector."
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Protect a cleartext key using a GRUB key protector that can retrieve the key "
"during boot to unlock fully-encrypted disks automatically."
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--action>=I<\\,add\\/>|remove"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Add or remove a key protector to or from a key."
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--protector>=I<\\,tpm2\\/>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Key protector to use (only tpm2 is currently supported)."
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2-asymmetric>=I<\\,TYPE\\/> The type of SRK: RSA (RSA2048), RSA3072, RSA4096,"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "and ECC (ECC_NIST_P256). (default: ECC)"
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2-bank>=I<\\,ALG\\/>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Bank of PCRs used to authorize key release: SHA1, SHA256, SHA384, or SHA512. "
"(default: SHA256)"
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2-device>=I<\\,FILE\\/>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Path to the TPM2 device. (default: I<\\,/dev/tpm0\\/>)"
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2-evict>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Evict a previously persisted SRK from the TPM, if any."
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2-keyfile>=I<\\,FILE\\/>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Path to a file that contains the cleartext key to protect."
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2-outfile>=I<\\,FILE\\/>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Path to the file that will contain the key after sealing (must be accessible "
"to GRUB during boot)."
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2-pcrs>=I<\\,0[\\/>,1]..."
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Comma-separated list of PCRs used to authorize key release e.g., '7,11'. "
"Please be aware that PCR 0~7 are used by the firmware and the measurement "
"result may change after a firmware update (for baremetal systems) or a "
"package (OVMF/SeaBIOS/SLOF) update in the VM host. This may lead tothe "
"failure of key unsealing.  (default: 7)"
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2-srk>=I<\\,NUM\\/>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The SRK handle if the SRK is to be made persistent."
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--tpm2key>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Use TPM 2.0 Key File format instead of the raw format."
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "give this help list"
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "give a short usage message"
msgstr ""

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "print program version"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""

#. type: SH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""

#. type: SH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-protect> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-protect> programs are properly installed "
"at your site, the command"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info grub-protect>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "November 2024"
msgstr ""
