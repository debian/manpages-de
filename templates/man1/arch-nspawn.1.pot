# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-10-01 01:21-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "ARCH-NSPAWN"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-06-18"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "arch-nspawn - Run a command or OS in a light-weight namespace container"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "arch-nspawn [options] working-dir [systemd-nspawn arguments]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<arch-nspawn> is a wrapper around systemd-nspawn to run command or OS in a "
"namespace container such as a directory including base utilities of a OS.  "
"It is used to build package(s) in given clean and defined environment."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-C> E<lt>fileE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Location of a pacman config file"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-M> E<lt>fileE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Location of a makepkg config file"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-c> E<lt>dirE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Set pacman cache, if no directory is specified the passed pacman.conf\\(cqs "
"cachedir is used with a fallback to I</etc/pacman.conf>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-f> E<lt>srcE<gt>[:E<lt>dstE<gt>]"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Copy file from the host to the chroot.  If I<dst> is not provided, it "
"defaults to I<src> inside of the chroot."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-s>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Do not run setarch"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show this usage message"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
