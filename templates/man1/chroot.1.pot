# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-06 17:55+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "CHROOT"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2024"
msgstr ""

#. type: TH
#: archlinux fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr ""

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "chroot - run command or interactive shell with special root directory"
msgstr ""

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<chroot> [I<\\,OPTION\\/>] I<\\,NEWROOT \\/>[I<\\,COMMAND \\/>[I<\\,ARG\\/"
">]...]"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<chroot> I<\\,OPTION\\/>"
msgstr ""

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Run COMMAND with root directory set to NEWROOT."
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--groups>=I<\\,G_LIST\\/>"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "specify supplementary groups as g1,g2,..,gN"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--userspec>=I<\\,USER\\/>:GROUP"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "specify user and group (ID or name) to use"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--skip-chdir>"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "do not change working directory to '/'"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "output version information and exit"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If no command is given, run '\"$SHELL\" B<-i>' (default: '/bin/sh B<-i>')."
msgstr ""

#. type: SS
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "125"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "if the chroot command itself fails"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "126"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "if COMMAND is found but cannot be invoked"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "127"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "if COMMAND cannot be found"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "-"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "the exit status of COMMAND otherwise"
msgstr ""

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Written by Roland McGrath."
msgstr ""

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<chroot>(2)"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chrootE<gt>"
msgstr ""

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chroot invocation\\(aq"
msgstr ""

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "September 2024"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "November 2024"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr ""

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr ""

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr ""
