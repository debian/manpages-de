# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "IPCS"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "ipcs - show information on IPC facilities"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<ipcs> [options]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<ipcs> shows information on System V inter-process communication "
"facilities. By default it shows information about all three resources: "
"shared memory segments, message queues, and semaphore arrays."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-i>, B<--id> I<id>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Show full details on just the one resource element identified by I<id>. This "
"option needs to be combined with one of the three resource options: B<-m>, "
"B<-q> or B<-s>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Resource options"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-m>, B<--shmems>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Write information about active shared memory segments."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-q>, B<--queues>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Write information about active message queues."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--semaphores>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Write information about active semaphore sets."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Write information about all three resources (default)."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Output formats"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Of these options only one takes effect: the last one specified."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--creator>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Show creator and owner."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--limits>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Show resource limits."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--pid>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Show PIDs of creator and last operator."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--time>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Write time information. The time of the last control operation that changed "
"the access permissions for all facilities, the time of the last B<msgsnd>(2) "
"and B<msgrcv>(2) operations on message queues, the time of the last "
"B<shmat>(2) and B<shmdt>(2) operations on shared memory, and the time of the "
"last B<semop>(2) operation on semaphores."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-u>, B<--summary>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Show status summary."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Representation"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "These affect only the B<-l> (B<--limits>) option."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--human>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print sizes in human-readable format."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The Linux B<ipcs> utility is not fully compatible to the POSIX B<ipcs> "
"utility. The Linux version does not support the POSIX B<-a>, B<-b> and B<-o> "
"options, but does support the B<-l> and B<-u> options not defined by POSIX. "
"A portable application shall not use the B<-a>, B<-b>, B<-o>, B<-l>, and B<-"
"u> options."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The current implementation of B<ipcs> obtains information about available "
"IPC resources by parsing the files in I</proc/sysvipc>. Before util-linux "
"version v2.23, an alternate mechanism was used: the B<IPC_STAT> command of "
"B<msgctl>(2), B<semctl>(2), and B<shmctl>(2). This mechanism is also used in "
"later util-linux versions in the case where I</proc> is unavailable. A "
"limitation of the B<IPC_STAT> mechanism is that it can only be used to "
"retrieve information about IPC resources for which the user has read "
"permission."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<ipcmk>(1), B<ipcrm>(1), B<msgrcv>(2), B<msgsnd>(2), B<semget>(2), "
"B<semop>(2), B<shmat>(2), B<shmdt>(2), B<shmget>(2), B<sysvipc>(7)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<ipcs> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
