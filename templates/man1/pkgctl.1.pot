# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-06-29 05:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PKGCTL"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-06-18"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl - Unified command-line frontend for devtools"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl [SUBCOMMAND] [OPTIONS]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Command-line utility serving as a unified interface for multiple development "
"tools.  This tool aims to simplify and optimize interactions with devtools "
"by offering various subcommands for executing tasks related to package "
"management, repository management, version control, among others."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Utilizing pkgctl enables users to efficiently administer their development "
"workflows."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-V, --version>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show pkgctl version information"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show a help text"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SUBCOMMANDS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl aur"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Interact with the Arch User Repository"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl auth"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Authenticate with services like GitLab"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl build"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Build packages inside a clean chroot"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl db"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Pacman database modification for package update, move etc"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl diff"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Compare package files using different modes"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl release"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Release step to commit, tag and upload build artifacts"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Manage Git packaging repositories and their configuration"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl search"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Search for an expression across the GitLab packaging group"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl version"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Check and manage package versions against upstream"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"pkgctl-aur(1)  pkgctl-auth(1)  pkgctl-build(1)  pkgctl-db(1)  pkgctl-"
"diff(1)  pkgctl-release(1)  pkgctl-repo(1)  pkgctl-search(1)  pkgctl-"
"version(1)"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
