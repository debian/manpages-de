# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Darima Kogan <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-06 18:08+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_exe"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/exe - symbolic link to program pathname"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</exe>"
msgstr "I</proc/>pidI</exe>"

#.  The following was still true as at kernel 2.6.13
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Under Linux 2.2 and later, this file is a symbolic link containing the "
"actual pathname of the executed command.  This symbolic link can be "
"dereferenced normally; attempting to open it will open the executable.  You "
"can even type I</proc/>pidI</exe> to run another copy of the same executable "
"that is being run by process I<pid>.  If the pathname has been unlinked, the "
"symbolic link will contain the string \\[aq]\\ (deleted)\\[aq] appended to "
"the original pathname.  In a multithreaded process, the contents of this "
"symbolic link are not available if the main thread has already terminated "
"(typically by calling B<pthread_exit>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Permission to dereference or read (B<readlink>(2))  this symbolic link is "
"governed by a ptrace access mode B<PTRACE_MODE_READ_FSCREDS> check; see "
"B<ptrace>(2)."
msgstr ""
"Право разыменовывать или читать (B<readlink>(2)) эту символическую ссылку "
"определяется проверкой режима доступа ptrace B<PTRACE_MODE_READ_FSCREDS>; "
"смотрите B<ptrace>(2)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Under Linux 2.0 and earlier, I</proc/>pidI</exe> is a pointer to the binary "
"which was executed, and appears as a symbolic link.  A B<readlink>(2)  call "
"on this file under Linux 2.0 returns a string in the format:"
msgstr ""
"В ядрах Linux 2.0 и более ранних, I</proc/>pidI</exe> указывает на двоичный "
"файл, который был выполнен, и работает как символьная ссылка. Вызов "
"B<readlink>(2) над этим файлом в Linux 2.0 вернёт строку следующего вида:"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "[device]:inode\n"
msgstr "[device]:inode\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For example, [0301]:1502 would be inode 1502 on device major 03 (IDE, MFM, "
"etc. drives) minor 01 (first partition on the first drive)."
msgstr ""
"Например, [0301]:1502 указывает на inode 1502 на устройстве со старшим "
"номером устройства 03 (IDE, MFM, и т.п. диски), младшим номером 01 (первый "
"раздел на первом диске)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<find>(1)  with the I<-inum> option can be used to locate the file."
msgstr ""
"Для поиска файла можно воспользоваться B<find>(1) с параметром I<-inum>."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15 августа 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
