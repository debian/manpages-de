# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:05+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "mq_send"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mq_send, mq_timedsend - send a message to a message queue"
msgstr "mq_send, mq_timedsend - отправляет сообщение в очередь сообщений"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Real-time library (I<librt>, I<-lrt>)"
msgstr "Библиотека реального времени (I<librt>, I<-lrt>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>mqueue.hE<gt>>\n"
msgstr "B<#include E<lt>mqueue.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int mq_send(mqd_t >I<mqdes>B<, const char *>I<msg_ptr>B<,>\n"
#| "B<              size_t >I<msg_len>B<, unsigned int >I<msg_prio>B<);>\n"
msgid ""
"B<int mq_send(mqd_t >I<mqdes>B<, const char >I<msg_ptr>B<[.>I<msg_len>B<],>\n"
"B<              size_t >I<msg_len>B<, unsigned int >I<msg_prio>B<);>\n"
msgstr ""
"B<int mq_send(mqd_t >I<mqdes>B<, const char *>I<msg_ptr>B<,>\n"
"B<              size_t >I<msg_len>B<, unsigned int >I<msg_prio>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>time.hE<gt>>\n"
"B<#include E<lt>mqueue.hE<gt>>\n"
msgstr ""
"B<#include E<lt>time.hE<gt>>\n"
"B<#include E<lt>mqueue.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int mq_timedsend(mqd_t >I<mqdes>B<, const char *>I<msg_ptr>B<,>\n"
#| "B<              size_t >I<msg_len>B<, unsigned int >I<msg_prio>B<,>\n"
#| "B<              const struct timespec *>I<abs_timeout>B<);>\n"
msgid ""
"B<int mq_timedsend(mqd_t >I<mqdes>B<, const char >I<msg_ptr>B<[.>I<msg_len>B<],>\n"
"B<              size_t >I<msg_len>B<, unsigned int >I<msg_prio>B<,>\n"
"B<              const struct timespec *>I<abs_timeout>B<);>\n"
msgstr ""
"B<int mq_timedsend(mqd_t >I<mqdes>B<, const char *>I<msg_ptr>B<,>\n"
"B<              size_t >I<msg_len>B<, unsigned int >I<msg_prio>B<,>\n"
"B<              const struct timespec *>I<abs_timeout>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<mq_timedsend>():"
msgstr "B<mq_timedsend>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mq_send>()  adds the message pointed to by I<msg_ptr> to the message queue "
"referred to by the message queue descriptor I<mqdes>.  The I<msg_len> "
"argument specifies the length of the message pointed to by I<msg_ptr>; this "
"length must be less than or equal to the queue's I<mq_msgsize> attribute.  "
"Zero-length messages are allowed."
msgstr ""
"Функция B<mq_send>() добавляет сообщение, на которое указывает I<msg_ptr>, в "
"очередь сообщений, на которую ссылается дескриптор очереди сообщений "
"I<mqdes>. В аргументе I<msg_len> задаётся длина сообщения, на которое "
"указывает I<msg_ptr>; эта длина должна быть меньше или равно атрибуту "
"очереди I<mq_msgsize>. Допускаются сообщения нулевой длины."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<msg_prio> argument is a nonnegative integer that specifies the "
"priority of this message.  Messages are placed on the queue in decreasing "
"order of priority, with newer messages of the same priority being placed "
"after older messages with the same priority.  See B<mq_overview>(7)  for "
"details on the range for the message priority."
msgstr ""
"Значение аргумента I<msg_prio> представляет собой неотрицательное целое, "
"которым определяется приоритет этого сообщения. Сообщения помещаются в "
"очередь в порядке уменьшения приоритета, самые новые сообщения с одинаковым "
"приоритетом размещаются после старых с тем же приоритетом. Описание "
"диапазона приоритета сообщения смотрите в B<mq_overview>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the message queue is already full (i.e., the number of messages on the "
"queue equals the queue's I<mq_maxmsg> attribute), then, by default, "
"B<mq_send>()  blocks until sufficient space becomes available to allow the "
"message to be queued, or until the call is interrupted by a signal handler.  "
"If the B<O_NONBLOCK> flag is enabled for the message queue description, then "
"the call instead fails immediately with the error B<EAGAIN>."
msgstr ""
"Если очередь сообщений заполнена (т. е., количество сообщений в очереди "
"равно атрибуту очереди I<mq_maxmsg>), то по умолчанию B<mq_send>() "
"блокируется до появления места для записи сообщения, или пока вызов не будет "
"прерван обработчиком сигнала. Если в описании очереди сообщений включён флаг "
"B<O_NONBLOCK>, то вместо этого вызов сразу завершается с ошибкой B<EAGAIN>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<mq_timedsend>()  behaves just like B<mq_send>(), except that if the "
#| "queue is full and the B<O_NONBLOCK> flag is not enabled for the message "
#| "queue description, then I<abs_timeout> points to a structure which "
#| "specifies how long the call will block.  This value is an absolute "
#| "timeout in seconds and nanoseconds since the Epoch, 1970-01-01 00:00:00 "
#| "+0000 (UTC), specified in the following structure:"
msgid ""
"B<mq_timedsend>()  behaves just like B<mq_send>(), except that if the queue "
"is full and the B<O_NONBLOCK> flag is not enabled for the message queue "
"description, then I<abs_timeout> points to a structure which specifies how "
"long the call will block.  This value is an absolute timeout in seconds and "
"nanoseconds since the Epoch, 1970-01-01 00:00:00 +0000 (UTC), specified in a "
"B<timespec>(3)  structure."
msgstr ""
"Функция B<mq_timedsend>() действует подобно B<mq_send>(), за исключением "
"того, что если очередь пуста и в описании очереди сообщений не установлен "
"флаг B<O_NONBLOCK>, то I<abs_timeout> указывает на структуру, в которой "
"задаётся длительность блокировки вызова. Это абсолютное значение задаётся в "
"секундах и наносекундах начиная с Эпохи, 1970-01-01 00:00:00 +0000 (UTC), в "
"структуре вида:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the message queue is full, and the timeout has already expired by the "
"time of the call, B<mq_timedsend>()  returns immediately."
msgstr ""
"Если очередь сообщений полна и вышло время ожидания на момент вызова, то "
"B<mq_timedsend>() сразу же завершается."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<mq_send>()  and B<mq_timedsend>()  return zero; on error, -1 "
"is returned, with I<errno> set to indicate the error."
msgstr ""
"При успешном выполнении B<mq_send>() и B<mq_timedsend>() возвращается ноль; "
"при ошибке возвращается -1, а в I<errno> помещается код ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The queue was full, and the B<O_NONBLOCK> flag was set for the message queue "
"description referred to by I<mqdes>."
msgstr ""
"Очередь была полна и в описании очереди сообщений, на которое ссылается "
"I<mqdes>, указан флаг B<O_NONBLOCK>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The descriptor specified in I<mqdes> was invalid or not opened for writing."
msgstr "В I<mqdes> указан некорректный или не открытый на запись дескриптор."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The call was interrupted by a signal handler; see B<signal>(7)."
msgstr "Вызов был прерван обработчиком сигнала; смотрите B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The call would have blocked, and I<abs_timeout> was invalid, either because "
"I<tv_sec> was less than zero, or because I<tv_nsec> was less than zero or "
"greater than 1000 million."
msgstr ""
"Вызов бы заблокировался и в I<abs_timeout> указано некорректное значение, "
"так как I<tv_sec> меньше нуля или I<tv_nsec> меньше нуля или более 1000 "
"миллионов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EMSGSIZE>"
msgstr "B<EMSGSIZE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<msg_len> was greater than the I<mq_msgsize> attribute of the message queue."
msgstr "I<msg_len> больше, чем свойство очереди сообщений I<mq_msgsize>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ETIMEDOUT>"
msgstr "B<ETIMEDOUT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The call timed out before a message could be transferred."
msgstr ""
"Истёк период ожидания в вызове, раньше появления возможности передачи "
"сообщения."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mq_send>(),\n"
"B<mq_timedsend>()"
msgstr ""
"B<mq_send>(),\n"
"B<mq_timedsend>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Linux, B<mq_timedsend>()  is a system call, and B<mq_send>()  is a "
"library function layered on top of that system call."
msgstr ""
"В Linux B<mq_timedsend>() является системным вызовом, а B<mq_send>() — "
"библиотечной функцией, реализованной на основе этого системного вызова:"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<mq_close>(3), B<mq_getattr>(3), B<mq_notify>(3), B<mq_open>(3), "
#| "B<mq_receive>(3), B<mq_unlink>(3), B<mq_overview>(7), B<time>(7)"
msgid ""
"B<mq_close>(3), B<mq_getattr>(3), B<mq_notify>(3), B<mq_open>(3), "
"B<mq_receive>(3), B<mq_unlink>(3), B<timespec>(3), B<mq_overview>(7), "
"B<time>(7)"
msgstr ""
"B<mq_close>(3), B<mq_getattr>(3), B<mq_notify>(3), B<mq_open>(3), "
"B<mq_receive>(3), B<mq_unlink>(3), B<mq_overview>(7), B<time>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
