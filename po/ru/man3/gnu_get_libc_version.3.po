# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:00+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "gnu_get_libc_version"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"gnu_get_libc_version, gnu_get_libc_release - get glibc version and release"
msgstr ""
"gnu_get_libc_version, gnu_get_libc_release - возвращают идентификатор версии "
"и выпуска glibc"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>gnu/libc-version.hE<gt>>\n"
msgstr "B<#include E<lt>gnu/libc-version.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<const char *gnu_get_libc_version(void);>\n"
"B<const char *gnu_get_libc_release(void);>\n"
msgstr ""
"B<const char *gnu_get_libc_version(void);>\n"
"B<const char *gnu_get_libc_release(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<gnu_get_libc_version>()  returns a string that identifies the "
"glibc version available on the system."
msgstr ""
"Функция B<gnu_get_libc_version>() возвращает строку, которая описывает "
"версию glibc, установленную в системе."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<gnu_get_libc_release>()  returns a string indicates the "
"release status of the glibc version available on the system.  This will be a "
"string such as I<stable>."
msgstr ""
"Функция B<gnu_get_libc_release>() возвращает строку, которая описывает "
"состояние выпуска версии glibc, установленной в системе. Обычно, "
"возвращается строка I<stable>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<gnu_get_libc_version>(),\n"
"B<gnu_get_libc_release>()"
msgstr ""
"B<gnu_get_libc_version>(),\n"
"B<gnu_get_libc_release>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.1."
msgstr "glibc 2.1."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "When run, the program below will produce output such as the following:"
msgstr "Нижеследующая программа выдает такие результаты:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out>\n"
"GNU libc version: 2.8\n"
"GNU libc release: stable\n"
msgstr ""
"$B< ./a.out>\n"
"Версия GNU libc: 2.8\n"
"Выпуск GNU libc: stable\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Исходный код программы"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(int argc, char *argv[])\n"
#| "{\n"
#| "    printf(\"GNU libc version: %s\\en\", gnu_get_libc_version());\n"
#| "    printf(\"GNU libc release: %s\\en\", gnu_get_libc_release());\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#include E<lt>gnu/libc-version.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    printf(\"GNU libc version: %s\\[rs]n\", gnu_get_libc_version());\n"
"    printf(\"GNU libc release: %s\\[rs]n\", gnu_get_libc_release());\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    printf(\"Версия GNU libc: %s\\en\", gnu_get_libc_version());\n"
"    printf(\"Выпуск GNU libc: %s\\en\", gnu_get_libc_release());\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<confstr>(3)"
msgstr "B<confstr>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid "These functions first appeared in glibc 2.1."
msgstr "Эти функции впервые появились в glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "These functions are glibc-specific."
msgstr "Эти функции есть только в glibc."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>gnu/libc-version.hE<gt>>\n"
msgid "#include E<lt>gnu/libc-version.hE<gt>\n"
msgstr "B<#include E<lt>gnu/libc-version.hE<gt>>\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(int argc, char *argv[])\n"
#| "{\n"
#| "    printf(\"GNU libc version: %s\\en\", gnu_get_libc_version());\n"
#| "    printf(\"GNU libc release: %s\\en\", gnu_get_libc_release());\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    printf(\"GNU libc version: %s\\en\", gnu_get_libc_version());\n"
"    printf(\"GNU libc release: %s\\en\", gnu_get_libc_release());\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    printf(\"Версия GNU libc: %s\\en\", gnu_get_libc_version());\n"
"    printf(\"Выпуск GNU libc: %s\\en\", gnu_get_libc_release());\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(int argc, char *argv[])\n"
#| "{\n"
#| "    printf(\"GNU libc version: %s\\en\", gnu_get_libc_version());\n"
#| "    printf(\"GNU libc release: %s\\en\", gnu_get_libc_release());\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#include E<lt>gnu/libc-version.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    printf(\"GNU libc version: %s\\en\", gnu_get_libc_version());\n"
"    printf(\"GNU libc release: %s\\en\", gnu_get_libc_release());\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    printf(\"Версия GNU libc: %s\\en\", gnu_get_libc_version());\n"
"    printf(\"Выпуск GNU libc: %s\\en\", gnu_get_libc_release());\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
