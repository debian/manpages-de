# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:20+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "[To be documented]"
msgid "undocumented"
msgstr "[Будет описано]"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "undocumented - undocumented library functions"
msgstr "undocumented - недокументированные библиотечные функции"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Undocumented library functions"
msgid "Undocumented library functions\n"
msgstr "Недокументированные библиотечные функции"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This man page mentions those library functions which are implemented in the "
"standard libraries but not yet documented in man pages."
msgstr ""
"В этой справочной странице перечислены библиотечные функции, реализованные в "
"стандартных библиотеках, но для которых пока нет справочных страниц."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Solicitation"
msgstr "Содействие"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you have information about these functions, please look in the source "
"code, write a man page (using a style similar to that of the other Linux "
"section 3 man pages), and send it to B<mtk.manpages@gmail.com> for inclusion "
"in the next man page release."
msgstr ""
"Если вы располагаете информацией об этих функциях, посмотрите исходный код, "
"напишите справочную страницу (в том же стиле, что и остальные документы "
"Linux в справочном разделе 3) и отправьте её на адрес B<mtk.manpages@gmail."
"com> для того, чтобы она вошла в следующий выпуск страниц."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "The list"
msgstr "Список"

#.  .BR chflags (3),
#.  .BR fattach (3),
#.  .BR fchflags (3),
#.  .BR fclean (3),
#.  .BR fdetach (3),
#.  .BR obstack stuff (3),
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<authdes_create>(3), B<authdes_getucred>(3), B<authdes_pk_create>(3), "
#| "B<clntunix_create>(3), B<creat64>(3), B<dn_skipname>(3), B<fcrypt>(3), "
#| "B<fp_nquery>(3), B<fp_query>(3), B<fp_resstat>(3), B<freading>(3), "
#| "B<freopen64>(3), B<fseeko64>(3), B<ftello64>(3), B<ftw64>(3), "
#| "B<fwscanf>(3), B<get_avphys_pages>(3), B<getdirentries64>(3), "
#| "B<getmsg>(3), B<getnetname>(3), B<get_phys_pages>(3), B<getpublickey>(3), "
#| "B<getsecretkey>(3), B<h_errlist>(3), B<host2netname>(3), B<hostalias>(3), "
#| "B<inet_nsap_addr>(3), B<inet_nsap_ntoa>(3), B<init_des>(3), "
#| "B<libc_nls_init>(3), B<mstats>(3), B<netname2host>(3), "
#| "B<netname2user>(3), B<nlist>(3), B<obstack_free>(3), "
#| "B<parse_printf_format>(3), B<p_cdname>(3), B<p_cdnname>(3), "
#| "B<p_class>(3), B<p_fqname>(3), B<p_option>(3), B<p_query>(3), "
#| "B<printf_size>(3), B<printf_size_info>(3), B<p_rr>(3), B<p_time>(3), "
#| "B<p_type>(3), B<putlong>(3), B<putshort>(3), B<re_compile_fastmap>(3), "
#| "B<re_compile_pattern>(3), B<register_printf_function>(3), B<re_match>(3), "
#| "B<re_match_2>(3), B<re_rx_search>(3), B<re_search>(3), B<re_search_2>(3), "
#| "B<re_set_registers>(3), B<re_set_syntax>(3), B<res_send_setqhook>(3), "
#| "B<res_send_setrhook>(3), B<ruserpass>(3), B<setfileno>(3), "
#| "B<sethostfile>(3), B<svc_exit>(3), B<svcudp_enablecache>(3), B<tell>(3), "
#| "B<tr_break>(3), B<tzsetwall>(3), B<ufc_dofinalperm>(3), B<ufc_doit>(3), "
#| "B<user2netname>(3), B<wcschrnul>(3), B<wcsftime>(3), B<wscanf>(3), "
#| "B<xdr_authdes_cred>(3), B<xdr_authdes_verf>(3), B<xdr_cryptkeyarg>(3), "
#| "B<xdr_cryptkeyres>(3), B<xdr_datum>(3), B<xdr_des_block>(3), "
#| "B<xdr_domainname>(3), B<xdr_getcredres>(3), B<xdr_keybuf>(3), "
#| "B<xdr_keystatus>(3), B<xdr_mapname>(3), B<xdr_netnamestr>(3), "
#| "B<xdr_netobj>(3), B<xdr_passwd>(3), B<xdr_peername>(3), "
#| "B<xdr_rmtcall_args>(3), B<xdr_rmtcallres>(3), B<xdr_unixcred>(3), "
#| "B<xdr_yp_buf>(3), B<xdr_yp_inaddr>(3), B<xdr_ypbind_binding>(3), "
#| "B<xdr_ypbind_resp>(3), B<xdr_ypbind_resptype>(3), "
#| "B<xdr_ypbind_setdom>(3), B<xdr_ypdelete_args>(3), B<xdr_ypmaplist>(3), "
#| "B<xdr_ypmaplist_str>(3), B<xdr_yppasswd>(3), B<xdr_ypreq_key>(3), "
#| "B<xdr_ypreq_nokey>(3), B<xdr_ypresp_all>(3), B<xdr_ypresp_all_seq>(3), "
#| "B<xdr_ypresp_key_val>(3), B<xdr_ypresp_maplist>(3), "
#| "B<xdr_ypresp_master>(3), B<xdr_ypresp_order>(3), B<xdr_ypresp_val>(3), "
#| "B<xdr_ypstat>(3), B<xdr_ypupdate_args>(3), B<yp_all>(3), B<yp_bind>(3), "
#| "B<yperr_string>(3), B<yp_first>(3), B<yp_get_default_domain>(3), "
#| "B<yp_maplist>(3), B<yp_master>(3), B<yp_match>(3), B<yp_next>(3), "
#| "B<yp_order>(3), B<ypprot_err>(3), B<yp_unbind>(3), B<yp_update>(3)"
msgid ""
"B<authdes_create>(3), B<authdes_getucred>(3), B<authdes_pk_create>(3), "
"B<clntunix_create>(3), B<creat64>(3), B<dn_skipname>(3), B<fcrypt>(3), "
"B<fp_nquery>(3), B<fp_query>(3), B<fp_resstat>(3), B<freading>(3), "
"B<freopen64>(3), B<fseeko64>(3), B<ftello64>(3), B<ftw64>(3), B<fwscanf>(3), "
"B<get_avphys_pages>(3), B<getdirentries64>(3), B<getmsg>(3), "
"B<getnetname>(3), B<get_phys_pages>(3), B<getpublickey>(3), "
"B<getsecretkey>(3), B<h_errlist>(3), B<host2netname>(3), B<hostalias>(3), "
"B<inet_nsap_addr>(3), B<inet_nsap_ntoa>(3), B<init_des>(3), "
"B<libc_nls_init>(3), B<mstats>(3), B<netname2host>(3), B<netname2user>(3), "
"B<nlist>(3), B<obstack_free>(3), B<parse_printf_format>(3), B<p_cdname>(3), "
"B<p_cdnname>(3), B<p_class>(3), B<p_fqname>(3), B<p_option>(3), "
"B<p_query>(3), B<printf_size>(3), B<printf_size_info>(3), B<p_rr>(3), "
"B<p_time>(3), B<p_type>(3), B<putlong>(3), B<putshort>(3), "
"B<re_compile_fastmap>(3), B<re_compile_pattern>(3), "
"B<register_printf_function>(3), B<re_match>(3), B<re_match_2>(3), "
"B<re_rx_search>(3), B<re_search>(3), B<re_search_2>(3), "
"B<re_set_registers>(3), B<re_set_syntax>(3), B<res_send_setqhook>(3), "
"B<res_send_setrhook>(3), B<ruserpass>(3), B<setfileno>(3), "
"B<sethostfile>(3), B<svc_exit>(3), B<svcudp_enablecache>(3), B<tell>(3), "
"B<thrd_create>(3), B<thrd_current>(3), B<thrd_equal>(3), B<thrd_sleep>(3), "
"B<thrd_yield>(3), B<tr_break>(3), B<tzsetwall>(3), B<ufc_dofinalperm>(3), "
"B<ufc_doit>(3), B<user2netname>(3), B<wcschrnul>(3), B<wcsftime>(3), "
"B<wscanf>(3), B<xdr_authdes_cred>(3), B<xdr_authdes_verf>(3), "
"B<xdr_cryptkeyarg>(3), B<xdr_cryptkeyres>(3), B<xdr_datum>(3), "
"B<xdr_des_block>(3), B<xdr_domainname>(3), B<xdr_getcredres>(3), "
"B<xdr_keybuf>(3), B<xdr_keystatus>(3), B<xdr_mapname>(3), "
"B<xdr_netnamestr>(3), B<xdr_netobj>(3), B<xdr_passwd>(3), "
"B<xdr_peername>(3), B<xdr_rmtcall_args>(3), B<xdr_rmtcallres>(3), "
"B<xdr_unixcred>(3), B<xdr_yp_buf>(3), B<xdr_yp_inaddr>(3), "
"B<xdr_ypbind_binding>(3), B<xdr_ypbind_resp>(3), B<xdr_ypbind_resptype>(3), "
"B<xdr_ypbind_setdom>(3), B<xdr_ypdelete_args>(3), B<xdr_ypmaplist>(3), "
"B<xdr_ypmaplist_str>(3), B<xdr_yppasswd>(3), B<xdr_ypreq_key>(3), "
"B<xdr_ypreq_nokey>(3), B<xdr_ypresp_all>(3), B<xdr_ypresp_all_seq>(3), "
"B<xdr_ypresp_key_val>(3), B<xdr_ypresp_maplist>(3), B<xdr_ypresp_master>(3), "
"B<xdr_ypresp_order>(3), B<xdr_ypresp_val>(3), B<xdr_ypstat>(3), "
"B<xdr_ypupdate_args>(3), B<yp_all>(3), B<yp_bind>(3), B<yperr_string>(3), "
"B<yp_first>(3), B<yp_get_default_domain>(3), B<yp_maplist>(3), "
"B<yp_master>(3), B<yp_match>(3), B<yp_next>(3), B<yp_order>(3), "
"B<ypprot_err>(3), B<yp_unbind>(3), B<yp_update>(3)"
msgstr ""
"B<authdes_create>(3), B<authdes_getucred>(3), B<authdes_pk_create>(3), "
"B<clntunix_create>(3), B<creat64>(3), B<dn_skipname>(3), B<fcrypt>(3), "
"B<fp_nquery>(3), B<fp_query>(3), B<fp_resstat>(3), B<freading>(3), "
"B<freopen64>(3), B<fseeko64>(3), B<ftello64>(3), B<ftw64>(3), B<fwscanf>(3), "
"B<get_avphys_pages>(3), B<getdirentries64>(3), B<getmsg>(3), "
"B<getnetname>(3), B<get_phys_pages>(3), B<getpublickey>(3), "
"B<getsecretkey>(3), B<h_errlist>(3), B<host2netname>(3), B<hostalias>(3), "
"B<inet_nsap_addr>(3), B<inet_nsap_ntoa>(3), B<init_des>(3), "
"B<libc_nls_init>(3), B<mstats>(3), B<netname2host>(3), B<netname2user>(3), "
"B<nlist>(3), B<obstack_free>(3), B<parse_printf_format>(3), B<p_cdname>(3), "
"B<p_cdnname>(3), B<p_class>(3), B<p_fqname>(3), B<p_option>(3), "
"B<p_query>(3), B<printf_size>(3), B<printf_size_info>(3), B<p_rr>(3), "
"B<p_time>(3), B<p_type>(3), B<putlong>(3), B<putshort>(3), "
"B<re_compile_fastmap>(3), B<re_compile_pattern>(3), "
"B<register_printf_function>(3), B<re_match>(3), B<re_match_2>(3), "
"B<re_rx_search>(3), B<re_search>(3), B<re_search_2>(3), "
"B<re_set_registers>(3), B<re_set_syntax>(3), B<res_send_setqhook>(3), "
"B<res_send_setrhook>(3), B<ruserpass>(3), B<setfileno>(3), "
"B<sethostfile>(3), B<svc_exit>(3), B<svcudp_enablecache>(3), B<tell>(3), "
"B<tr_break>(3), B<tzsetwall>(3), B<ufc_dofinalperm>(3), B<ufc_doit>(3), "
"B<user2netname>(3), B<wcschrnul>(3), B<wcsftime>(3), B<wscanf>(3), "
"B<xdr_authdes_cred>(3), B<xdr_authdes_verf>(3), B<xdr_cryptkeyarg>(3), "
"B<xdr_cryptkeyres>(3), B<xdr_datum>(3), B<xdr_des_block>(3), "
"B<xdr_domainname>(3), B<xdr_getcredres>(3), B<xdr_keybuf>(3), "
"B<xdr_keystatus>(3), B<xdr_mapname>(3), B<xdr_netnamestr>(3), "
"B<xdr_netobj>(3), B<xdr_passwd>(3), B<xdr_peername>(3), "
"B<xdr_rmtcall_args>(3), B<xdr_rmtcallres>(3), B<xdr_unixcred>(3), "
"B<xdr_yp_buf>(3), B<xdr_yp_inaddr>(3), B<xdr_ypbind_binding>(3), "
"B<xdr_ypbind_resp>(3), B<xdr_ypbind_resptype>(3), B<xdr_ypbind_setdom>(3), "
"B<xdr_ypdelete_args>(3), B<xdr_ypmaplist>(3), B<xdr_ypmaplist_str>(3), "
"B<xdr_yppasswd>(3), B<xdr_ypreq_key>(3), B<xdr_ypreq_nokey>(3), "
"B<xdr_ypresp_all>(3), B<xdr_ypresp_all_seq>(3), B<xdr_ypresp_key_val>(3), "
"B<xdr_ypresp_maplist>(3), B<xdr_ypresp_master>(3), B<xdr_ypresp_order>(3), "
"B<xdr_ypresp_val>(3), B<xdr_ypstat>(3), B<xdr_ypupdate_args>(3), "
"B<yp_all>(3), B<yp_bind>(3), B<yperr_string>(3), B<yp_first>(3), "
"B<yp_get_default_domain>(3), B<yp_maplist>(3), B<yp_master>(3), "
"B<yp_match>(3), B<yp_next>(3), B<yp_order>(3), B<ypprot_err>(3), "
"B<yp_unbind>(3), B<yp_update>(3)"

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
