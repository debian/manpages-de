# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:16+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<system>()"
msgid "system"
msgstr "B<system>()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "system - execute a shell command"
msgstr "system - выполняет команду оболочки (shell)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int system(const char *>I<command>B<);>\n"
msgstr "B<int system(const char *>I<command>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<system>()  library function uses B<fork>(2)  to create a child "
#| "process that executes the shell command specified in I<command> using "
#| "B<execl>(3)  as follows:"
msgid ""
"The B<system>()  library function behaves as if it used B<fork>(2)  to "
"create a child process that executed the shell command specified in "
"I<command> using B<execl>(3)  as follows:"
msgstr ""
"В библиотечной функции B<system>() используется B<fork>(2) для создания "
"процесса-потомка, в котором посредством B<execl>(3) запускается команда "
"оболочки, указанная в I<command>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "execl(\"/bin/sh\", \"sh\", \"-c\", command, (char *) NULL);\n"
msgstr "execl(\"/bin/sh\", \"sh\", \"-c\", command, (char *) NULL);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<system>()  returns after the command has been completed."
msgstr ""
"Функция B<system>() возвращает результат после завершения работы команды."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"During execution of the command, B<SIGCHLD> will be blocked, and B<SIGINT> "
"and B<SIGQUIT> will be ignored, in the process that calls B<system>().  "
"(These signals will be handled according to their defaults inside the child "
"process that executes I<command>.)"
msgstr ""
"На время выполнения команды B<SIGCHLD> блокируется, а B<SIGINT> и B<SIGQUIT> "
"игнорируются в процессе, который вызвал B<system>() (эти сигналы будут "
"обработаны их действиями по умолчанию внутри процесса-потомка, который "
"выполняет I<command>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<command> is NULL, then B<system>()  returns a status indicating whether "
"a shell is available on the system."
msgstr ""
"Если значение I<command> равно NULL, то B<system>() возвращает состояние, "
"показывающее доступна ли оболочка в системе."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The return value of B<system>()  is one of the following:"
msgstr "Возвращаемым значением B<system>() может быть одно из:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<command> is NULL, then a nonzero value if a shell is available, or 0 if "
"no shell is available."
msgstr ""
"Если значение I<command> равно NULL, то возвращается ненулевое значение, "
"если оболочка присутствует в системе, или 0, если оболочка недоступна."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If a child process could not be created, or its status could not be "
"retrieved, the return value is -1 and I<errno> is set to indicate the error."
msgstr ""
"Если процесс-потомок не может быть создан или его состояние невозможно "
"вернуть, то возвращается значение -1, а I<errno> присваивается код ошибки."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If a shell could not be executed in the child process, then the return value "
"is as though the child shell terminated by calling B<_exit>(2)  with the "
"status 127."
msgstr ""
"Если оболочка не может выполниться в процессе-потомке, то возвращается "
"значение будет таким же как если бы оболочка-потомок завершилась вызовом "
"B<_exit>(2) с состоянием 127."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If all system calls succeed, then the return value is the termination status "
"of the child shell used to execute I<command>.  (The termination status of a "
"shell is the termination status of the last command it executes.)"
msgstr ""
"Если все системные вызовы выполнены без ошибок, то возвращается значение "
"состояния завершения процесса-потомка, использовавшегося для выполнения "
"I<command> (состояние завершения оболочки — это состояние завершения "
"последней выполнявшейся команды)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the last two cases, the return value is a \"wait status\" that can be "
"examined using the macros described in B<waitpid>(2).  (i.e., "
"B<WIFEXITED>(), B<WEXITSTATUS>(), and so on)."
msgstr ""
"В последних двух случаях возвращаемое значение — это «состояние ожидания», "
"которое можно определить с помощью макроса описанного в B<waitpid>(2) (т. "
"е., B<WIFEXITED>(), B<WEXITSTATUS>() и т. п.)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<system>()  does not affect the wait status of any other children."
msgstr "Функция B<system>() не отражает состояние ожидание других потомков."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<system>()  can fail with any of the same errors as B<fork>(2)."
msgstr ""
"Функция B<system>() может завершиться  с теми же ошибками что и B<fork>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<system>()"
msgstr "B<system>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89."
msgstr "POSIX.1-2001, C89."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<system>()  provides simplicity and convenience: it handles all of the "
"details of calling B<fork>(2), B<execl>(3), and B<waitpid>(2), as well as "
"the necessary manipulations of signals; in addition, the shell performs the "
"usual substitutions and I/O redirections for I<command>.  The main cost of "
"B<system>()  is inefficiency: additional system calls are required to create "
"the process that runs the shell and to execute the shell."
msgstr ""
"Функция B<system>() проста и удобна: она позаботится обо всём для вызовов "
"B<fork>(2), B<execl>(3) и B<waitpid>(2), а также необходимых действиях с "
"сигналами; также оболочка выполнит обычные подстановки и перенаправления "
"ввода-вывода I<command>. Но B<system>() не эффективна: для создания процесса "
"требуются дополнительные системные вызовы, которые запускают оболочку и "
"выполняют команду."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the B<_XOPEN_SOURCE> feature test macro is defined (before including "
"I<any> header files), then the macros described in B<waitpid>(2)  "
"(B<WEXITSTATUS>(), etc.) are made available when including I<E<lt>stdlib."
"hE<gt>>."
msgstr ""
"Если определён макрос тестирования свойств B<_XOPEN_SOURCE> (до включения "
"I<всех> заголовочных файлов), то при включении I<E<lt>stdlib.hE<gt>> "
"становятся доступны макросы, описанные в B<waitpid>(2) (B<WEXITSTATUS>() и "
"т. п.)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"As mentioned, B<system>()  ignores B<SIGINT> and B<SIGQUIT>.  This may make "
"programs that call it from a loop uninterruptible, unless they take care "
"themselves to check the exit status of the child.  For example:"
msgstr ""
"Как уже упоминалось,  функция B<system>() игнорирует B<SIGINT> и B<SIGQUIT>. "
"Это может привести к тому, что программы вызывающие её из цикла станут не "
"прерываемыми, пока сами не проверят условия выхода для своих дочерних "
"процессов. Пример:"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    if (WIFSIGNALED(ret) &&\n"
#| "        (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))\n"
#| "            break;\n"
#| "}\n"
msgid ""
"while (something) {\n"
"    int ret = system(\"foo\");\n"
"\\&\n"
"    if (WIFSIGNALED(ret) &&\n"
"        (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))\n"
"            break;\n"
"}\n"
msgstr ""
"    if (WIFSIGNALED(ret) &&\n"
"        (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))\n"
"            break;\n"
"}\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"According to POSIX.1, it is unspecified whether handlers registered using "
"B<pthread_atfork>(3)  are called during the execution of B<system>().  In "
"the glibc implementation, such handlers are not called."
msgstr ""
"В POSIX.1 не определено, вызываются ли обработчики, зарегистрированные "
"B<pthread_atfork>(3), при выполнении B<system>(). В реализации glibc такие "
"обработчики не вызываются."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In versions of glibc before 2.1.3, the check for the availability of I</"
#| "bin/sh> was not actually performed if I<command> was NULL; instead it was "
#| "always assumed to be available, and B<system>()  always returned 1 in "
#| "this case.  Since glibc 2.1.3, this check is performed because, even "
#| "though POSIX.1-2001 requires a conforming implementation to provide a "
#| "shell, that shell may not be available or executable if the calling "
#| "program has previously called B<chroot>(2)  (which is not specified by "
#| "POSIX.1-2001)."
msgid ""
"Before glibc 2.1.3, the check for the availability of I</bin/sh> was not "
"actually performed if I<command> was NULL; instead it was always assumed to "
"be available, and B<system>()  always returned 1 in this case.  Since glibc "
"2.1.3, this check is performed because, even though POSIX.1-2001 requires a "
"conforming implementation to provide a shell, that shell may not be "
"available or executable if the calling program has previously called "
"B<chroot>(2)  (which is not specified by POSIX.1-2001)."
msgstr ""
"В версиях glibc до 2.1.3 проверка доступности I</bin/sh> в действительности "
"не выполнялась, если значение I<command> равно NULL; вместо этого всегда "
"предполагалось наличие, и в этом случае B<system>() всегда возвращала 1. "
"Начиная с glibc 2.1.3, эта проверка выполняется, так как несмотря на "
"требование POSIX.1-2001   a conforming implementation to provide a shell, "
"that shell may not be available or executable if the calling program has "
"previously called B<chroot>(2)  (which is not specified by POSIX.1-2001)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It is possible for the shell command to terminate with a status of 127, "
"which yields a B<system>()  return value that is indistinguishable from the "
"case where a shell could not be executed in the child process."
msgstr ""
"Существует вероятность, что команда оболочки возвратит значение 127, которое "
"также является и возвращаемым значением самой B<system>(); в этом случае "
"нельзя понять, что невозможно выполнить оболочку в процессе-потомке."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Caveats"
msgstr "Предостережения"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Do not use B<system>()  from a privileged program (a set-user-ID or set-"
"group-ID program, or a program with capabilities)  because strange values "
"for some environment variables might be used to subvert system integrity.  "
"For example, B<PATH> could be manipulated so that an arbitrary program is "
"executed with privilege.  Use the B<exec>(3)  family of functions instead, "
"but not B<execlp>(3)  or B<execvp>(3)  (which also use the B<PATH> "
"environment variable to search for an executable)."
msgstr ""
"Не используйте B<system>() в привилегированных программах (программы с set-"
"user-ID или set-group-ID, или программы с мандатами), так как могут "
"использоваться странные значения некоторых переменных окружения для "
"разрушения целостности системы. Например, B<PATH> может быть изменена так, "
"что произвольная программа выполняется с расширенными правами. Вместо этого "
"используйте семейство функций B<exec>(3), но не B<execlp>(3) или "
"B<execvp>(3) (которые также используют переменную окружения B<PATH> для "
"поиска исполняемого файла)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<system>()  will not, in fact, work properly from programs with set-user-ID "
"or set-group-ID privileges on systems on which I</bin/sh> is bash version 2: "
"as a security measure, bash 2 drops privileges on startup.  (Debian uses a "
"different shell, B<dash>(1), which does not do this when invoked as B<sh>.)"
msgstr ""
"Функция B<system>(), фактически, работает неправильно для программ с "
"привилегиями set-user-ID или set-group-ID в системах, где I</bin/sh> это "
"bash версии 2: для безопасности bash 2 при запуске убирает привилегии (в "
"Debian используется другая оболочка, B<dash>(1), которая не делает этого, "
"если вызывается как B<sh>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Any user input that is employed as part of I<command> should be I<carefully> "
"sanitized, to ensure that unexpected shell commands or command options are "
"not executed.  Such risks are especially grave when using B<system>()  from "
"a privileged program."
msgstr ""
"Любой введённые пользователем данные, выполняемые как часть I<команды>, "
"должны быть I<внимательно> отцензурированы так, чтобы не выполнились "
"неожидаемые команды оболочки или параметры команды. Эти риски особенно "
"серьезны при использовании B<system>() из привилегированной программы."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#.  [BUG 211029](https://bugzilla.kernel.org/show_bug.cgi?id=211029)
#.  [glibc bug](https://sourceware.org/bugzilla/show_bug.cgi?id=27143)
#.  [POSIX bug](https://www.austingroupbugs.net/view.php?id=1440)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the command name starts with a hyphen, B<sh>(1)  interprets the command "
"name as an option, and the behavior is undefined.  (See the B<-c> option to "
"B<sh>(1).)  To work around this problem, prepend the command with a space as "
"in the following call:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    system(\" -unfortunate-command-name\");\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sh>(1), B<execve>(2), B<fork>(2), B<sigaction>(2), B<sigprocmask>(2), "
"B<wait>(2), B<exec>(3), B<signal>(7)"
msgstr ""
"B<sh>(1), B<execve>(2), B<fork>(2), B<sigaction>(2), B<sigprocmask>(2), "
"B<wait>(2), B<exec>(3), B<signal>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"while (something) {\n"
"    int ret = system(\"foo\");\n"
msgstr ""
"while (something) {\n"
"    int ret = system(\"foo\");\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (WIFSIGNALED(ret) &&\n"
"        (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))\n"
"            break;\n"
"}\n"
msgstr ""
"    if (WIFSIGNALED(ret) &&\n"
"        (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))\n"
"            break;\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
