# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:59+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getnameinfo>()"
msgid "getnameinfo"
msgstr "B<getnameinfo>()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"getnameinfo - address-to-name translation in protocol-independent manner"
msgstr "getnameinfo - перевод адреса в имя не зависящим от протокола способом"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/socket.hE<gt>>\n"
"B<#include E<lt>netdb.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/socket.hE<gt>>\n"
"B<#include E<lt>netdb.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int getnameinfo(const struct sockaddr *>I<addr>B<, socklen_t >I<addrlen>B<,>\n"
#| "B<                char *>I<host>B<, socklen_t >I<hostlen>B<,>\n"
#| "B<                char *>I<serv>B<, socklen_t >I<servlen>B<, int >I<flags>B<);>\n"
msgid ""
"B<int getnameinfo(const struct sockaddr *restrict >I<addr>B<, socklen_t >I<addrlen>B<,>\n"
"B<                char >I<host>B<[_Nullable restrict .>I<hostlen>B<],>\n"
"B<                socklen_t >I<hostlen>B<,>\n"
"B<                char >I<serv>B<[_Nullable restrict .>I<servlen>B<],>\n"
"B<                socklen_t >I<servlen>B<,>\n"
"B<                int >I<flags>B<);>\n"
msgstr ""
"B<int getnameinfo(const struct sockaddr *>I<addr>B<, socklen_t >I<addrlen>B<,>\n"
"B<                char *>I<host>B<, socklen_t >I<hostlen>B<,>\n"
"B<                char *>I<serv>B<, socklen_t >I<servlen>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "B<getnameinfo>()"
msgid "B<getnameinfo>():"
msgstr "B<getnameinfo>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.22:\n"
"        _POSIX_C_SOURCE E<gt>= 200112L\n"
"    glibc 2.21 and earlier:\n"
"        _POSIX_C_SOURCE\n"
msgstr ""
"    Начиная с glibc 2.22:\n"
"        _POSIX_C_SOURCE E<gt>= 200112L\n"
"    glibc 2.21 и ранее:\n"
"        _POSIX_C_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<getnameinfo>()  function is the inverse of B<getaddrinfo>(3): it "
"converts a socket address to a corresponding host and service, in a protocol-"
"independent manner.  It combines the functionality of B<gethostbyaddr>(3)  "
"and B<getservbyport>(3), but unlike those functions, B<getnameinfo>()  is "
"reentrant and allows programs to eliminate IPv4-versus-IPv6 dependencies."
msgstr ""
"Функция B<getnameinfo>() выполняет операцию, обратную B<getaddrinfo>(3); она "
"преобразует адрес сокета в соответствующие узел и службу, способом, который "
"не зависит от протокола. Она сочетает в себе действия функций "
"B<gethostbyaddr>(3) и B<getservbyport>(3), но в отличии от этих функций "
"B<getnameinfo>() реентерабельна и позволяет программам не зависеть от типа "
"IPv4 и IPv6."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<addr> argument is a pointer to a generic socket address structure (of "
"type I<sockaddr_in> or I<sockaddr_in6>)  of size I<addrlen> that holds the "
"input IP address and port number.  The arguments I<host> and I<serv> are "
"pointers to caller-allocated buffers (of size I<hostlen> and I<servlen> "
"respectively) into which B<getnameinfo>()  places null-terminated strings "
"containing the host and service names respectively."
msgstr ""
"Аргумент I<addr> — это указатель на обобщённую структуру адреса сокета (типа "
"I<sockaddr_in> или I<sockaddr_in6>) размером I<addrlen>, которая содержит IP-"
"адрес и номер порта. Аргументы I<host> и I<serv> указывают на выделенные "
"вызывающим буферы (размером I<hostlen> и I<servlen>, соответственно), в "
"которые B<getnameinfo>() помещает строки (заканчивающееся null), содержащие "
"имя узла и службы, соответственно."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The caller can specify that no hostname (or no service name)  is required by "
"providing a NULL I<host> (or I<serv>)  argument or a zero I<hostlen> (or "
"I<servlen>)  argument.  However, at least one of hostname or service name "
"must be requested."
msgstr ""
"Вызывающий может указать, что имя узла (или службы) не требуется, указав в "
"аргументе I<host> (или I<serv>) NULL или в I<hostlen> (или I<servlen>) "
"значение 0. Однако, по крайней мере один параметр, имя узла или службы, "
"должно быть запрошено."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<flags> argument modifies the behavior of B<getnameinfo>()  as follows:"
msgstr ""
"Аргумент I<flags> меняет поведение функции B<getnameinfo>() следующим "
"образом:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NI_NAMEREQD>"
msgstr "B<NI_NAMEREQD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If set, then an error is returned if the hostname cannot be determined."
msgstr ""
"Если этот флаг установлен, то возвращается ошибка, если имя машины не может "
"быть определено."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NI_DGRAM>"
msgstr "B<NI_DGRAM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If set, then the service is datagram (UDP) based rather than stream (TCP) "
#| "based.  This is required for the few ports (512\\(en514)  that have "
#| "different services for UDP and TCP."
msgid ""
"If set, then the service is datagram (UDP) based rather than stream (TCP) "
"based.  This is required for the few ports (512\\[en]514)  that have "
"different services for UDP and TCP."
msgstr ""
"Если этот флаг установлен, то сначала используется имя службы на основе "
"дейтаграмм (UDP), а не потоков (TCP). Это требуется для немногих портов "
"(512\\(en514), которые имеют различные службы для UDP и TCP."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NI_NOFQDN>"
msgstr "B<NI_NOFQDN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If set, return only the hostname part of the fully qualified domain name for "
"local hosts."
msgstr ""
"Если этот флаг установлен, то возвращается только часть имени машины от "
"полностью определённого доменного имени (FQDN) для локальных машин."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NI_NUMERICHOST>"
msgstr "B<NI_NUMERICHOST>"

#.  For example, by calling
#.  .BR inet_ntop ()
#.  instead of
#.  .BR gethostbyaddr ().
#.  POSIX.1-2001 TC1 has NI_NUMERICSCOPE, but glibc doesn't have it.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If set, then the numeric form of the hostname is returned.  (When not set, "
"this will still happen in case the node's name cannot be determined.)"
msgstr ""
"Если этот флаг установлен, то имя узла возвращается в числовой форме (если "
"этот флаг не установлен, то это также произойдёт в случае, когда имя узла "
"невозможно определить)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NI_NUMERICSERV>"
msgstr "B<NI_NUMERICSERV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If set, then the numeric form of the service address is returned.  (When not "
"set, this will still happen in case the service's name cannot be determined.)"
msgstr ""
"Если этот флаг установлен, тогда имя службы возвращается в числовой форме "
"(если этот флаг не установлен, то это также произойдёт в случае, когда имя "
"узла невозможно определить)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Extensions to getnameinfo() for Internationalized Domain Names"
msgstr "Расширения getnameinfo() для интернациональных доменных имён"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Starting with glibc 2.3.4, B<getnameinfo>()  has been extended to "
"selectively allow hostnames to be transparently converted to and from the "
"Internationalized Domain Name (IDN) format (see RFC 3490, "
"I<Internationalizing Domain Names in Applications (IDNA)>).  Three new flags "
"are defined:"
msgstr ""
"Начиная с glibc 2.3.4, B<getnameinfo>() была расширена для выборочного "
"прозрачного разрешения имён для формата интернациональных доменных имён "
"(IDN) (смотрите RFC 3490, I<Internationalizing Domain Names in Applications "
"(IDNA)>). Было определено четыре новых флага:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NI_IDN>"
msgstr "B<NI_IDN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If this flag is used, then the name found in the lookup process is converted "
"from IDN format to the locale's encoding if necessary.  ASCII-only names are "
"not affected by the conversion, which makes this flag usable in existing "
"programs and environments."
msgstr ""
"Если этот флаг установлен, то при необходимости искомое имя преобразуется из "
"формата IDN в кодировку локали. Имена из только ASCI-символов не меняются "
"при преобразовании, из-за чего данный флаг можно использовать в существующих "
"программах и средах."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<NI_IDN_ALLOW_UNASSIGNED>, B<NI_IDN_USE_STD3_ASCII_RULES>"
msgid "B<NI_IDN_ALLOW_UNASSIGNED>"
msgstr "B<NI_IDN_ALLOW_UNASSIGNED>, B<NI_IDN_USE_STD3_ASCII_RULES>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<NI_IDN_ALLOW_UNASSIGNED>, B<NI_IDN_USE_STD3_ASCII_RULES>"
msgid "B<NI_IDN_USE_STD3_ASCII_RULES>"
msgstr "B<NI_IDN_ALLOW_UNASSIGNED>, B<NI_IDN_USE_STD3_ASCII_RULES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Setting these flags will enable the IDNA_ALLOW_UNASSIGNED (allow unassigned "
"Unicode code points) and IDNA_USE_STD3_ASCII_RULES (check output to make "
"sure it is a STD3 conforming hostname)  flags respectively to be used in the "
"IDNA handling."
msgstr ""
"Установка этих флагов включает IDNA_ALLOW_UNASSIGNED (разрешать не "
"назначенные кодовые точки Юникода) и IDNA_USE_STD3_ASCII_RULES (проверять "
"вывод на соответствие имени узла STD3) соответственно для возможности работы "
"с IDNA."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#.  FIXME glibc defines the following additional errors, some which
#.  can probably be returned by getnameinfo(); they need to
#.  be documented.
#.      #ifdef __USE_GNU
#.      #define EAI_INPROGRESS  -100  /* Processing request in progress.  */
#.      #define EAI_CANCELED    -101  /* Request canceled.  */
#.      #define EAI_NOTCANCELED -102  /* Request not canceled.  */
#.      #define EAI_ALLDONE     -103  /* All requests done.  */
#.      #define EAI_INTR        -104  /* Interrupted by a signal.  */
#.      #define EAI_IDN_ENCODE  -105  /* IDN encoding failed.  */
#.      #endif
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, 0 is returned, and node and service names, if requested, are "
"filled with null-terminated strings, possibly truncated to fit the specified "
"buffer lengths.  On error, one of the following nonzero error codes is "
"returned:"
msgstr ""
"При успешном выполнении возвращается 0, а строки (оканчивающееся null) имени "
"узла и службы (если запрашивались) записываются в соответствующий буфер "
"заданной длины. При ошибке возвращается одно из следующих ненулевых значений "
"ошибки:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAI_AGAIN>"
msgstr "B<EAI_AGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The name could not be resolved at this time.  Try again later."
msgstr ""
"Имя не может быть определено в настоящий момент. Попробуйте повторить "
"попытку позже."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAI_BADFLAGS>"
msgstr "B<EAI_BADFLAGS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<flags> argument has an invalid value."
msgstr "Параметр I<flags> имеет неверное значение."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAI_FAIL>"
msgstr "B<EAI_FAIL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A nonrecoverable error occurred."
msgstr "Произошла неисправимая ошибка."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAI_FAMILY>"
msgstr "B<EAI_FAMILY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The address family was not recognized, or the address length was invalid for "
"the specified family."
msgstr ""
"Не распознано семейство адресов, или для данного семейства была указана "
"неверно длина адреса."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAI_MEMORY>"
msgstr "B<EAI_MEMORY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Out of memory."
msgstr "Не хватает памяти."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAI_NONAME>"
msgstr "B<EAI_NONAME>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The name does not resolve for the supplied arguments.  B<NI_NAMEREQD> is set "
"and the host's name cannot be located, or neither hostname nor service name "
"were requested."
msgstr ""
"Имя не может быть определено для указанных параметров. Установлен флаг "
"B<NI_NAMEREQD> и имя машины не может быть определено, или не было запрошено "
"не имя машины и не имя службы."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAI_OVERFLOW>"
msgstr "B<EAI_OVERFLOW>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The buffer pointed to by I<host> or I<serv> was too small."
msgstr "Размер буфера, на который указывает I<host> или I<serv> слишком мал."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAI_SYSTEM>"
msgstr "B<EAI_SYSTEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A system error occurred.  The error code can be found in I<errno>."
msgstr ""
"Произошла системная ошибка. Код системной ошибки можно найти в переменной "
"I<errno>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<gai_strerror>(3)  function translates these error codes to a human "
"readable string, suitable for error reporting."
msgstr ""
"Функция B<gai_strerror>(3) транслирует эти коды ошибок в читаемый формат, "
"подходящий для сообщений об ошибке."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</etc/hosts>"
msgstr "I</etc/hosts>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</etc/nsswitch.conf>"
msgstr "I</etc/nsswitch.conf>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</etc/resolv.conf>"
msgstr "I</etc/resolv.conf>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getnameinfo>()"
msgstr "B<getnameinfo>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe env locale"
msgstr "MT-Safe env locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, POSIX.1-2008, RFC\\ 2553."
msgid "POSIX.1-2008.  RFC\\ 2553."
msgstr "POSIX.1-2001, POSIX.1-2008, RFC\\ 2553."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.1.  POSIX.1-2001."
msgstr "glibc 2.1.  POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Before glibc version 2.2, the I<hostlen> and I<servlen> arguments were "
#| "typed as I<size_t>."
msgid ""
"Before glibc 2.2, the I<hostlen> and I<servlen> arguments were typed as "
"I<size_t>."
msgstr ""
"В glibc до версии 2.2 аргументы I<hostlen> и I<servlen> имели тип I<size_t>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In order to assist the programmer in choosing reasonable sizes for the "
"supplied buffers, I<E<lt>netdb.hE<gt>> defines the constants"
msgstr ""
"Чтобы помочь программисту выбрать нужный размер буферов в I<E<lt>netdb."
"hE<gt>> определены константы"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"#define NI_MAXHOST      1025\n"
"#define NI_MAXSERV      32\n"
msgstr ""
"#define NI_MAXHOST      1025\n"
"#define NI_MAXSERV      32\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since glibc 2.8, these definitions are exposed only if suitable feature test "
"macros are defined, namely: B<_GNU_SOURCE>, B<_DEFAULT_SOURCE> (since glibc "
"2.19), or (in glibc versions up to and including 2.19)  B<_BSD_SOURCE> or "
"B<_SVID_SOURCE>."
msgstr ""
"Начиная с glibc 2.8, эти определения доступны только, если определён "
"подходящий макрос тестирования свойств: B<_GNU_SOURCE>, B<_DEFAULT_SOURCE> "
"(начиная с glibc 2.19) или (в версиях glibc с 2.19 включительно) "
"B<_BSD_SOURCE> или B<_SVID_SOURCE>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The former is the constant B<MAXDNAME> in recent versions of BIND's "
"I<E<lt>arpa/nameser.hE<gt>> header file.  The latter is a guess based on the "
"services listed in the current Assigned Numbers RFC."
msgstr ""
"Первая — это константа B<MAXDNAME> из новых версий заголовочного файла "
"I<E<lt>arpa/nameser.hE<gt>>  BIND. Последняя — вычислена на основе служб, "
"перечисленных в текущем RFC «Assigned Numbers»."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following code tries to get the numeric hostname and service name, for a "
"given socket address.  Note that there is no hardcoded reference to a "
"particular address family."
msgstr ""
"Следующий код пытается получить имя машины и службы в числовой форме для "
"указанного адреса сокета. Обратите внимание, что здесь нет прямых упоминаний "
"определённого семейства адресов."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,\n"
#| "            sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)\n"
#| "    printf(\"host=%s, serv=%s\\en\", hbuf, sbuf);\n"
msgid ""
"struct sockaddr *addr;     /* input */\n"
"socklen_t addrlen;         /* input */\n"
"char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];\n"
"\\&\n"
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,\n"
"            sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)\n"
"    printf(\"host=%s, serv=%s\\[rs]n\", hbuf, sbuf);\n"
msgstr ""
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,\n"
"            sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)\n"
"    printf(\"host=%s, serv=%s\\en\", hbuf, sbuf);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following version checks if the socket address has a reverse address "
"mapping."
msgstr ""
"Следующая версия проверяет, имеет ли адрес сокета обратное отображение "
"адреса."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf),\n"
#| "            NULL, 0, NI_NAMEREQD))\n"
#| "    printf(\"could not resolve hostname\");\n"
#| "else\n"
#| "    printf(\"host=%s\\en\", hbuf);\n"
msgid ""
"struct sockaddr *addr;     /* input */\n"
"socklen_t addrlen;         /* input */\n"
"char hbuf[NI_MAXHOST];\n"
"\\&\n"
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf),\n"
"            NULL, 0, NI_NAMEREQD))\n"
"    printf(\"could not resolve hostname\");\n"
"else\n"
"    printf(\"host=%s\\[rs]n\", hbuf);\n"
msgstr ""
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf),\n"
"            NULL, 0, NI_NAMEREQD))\n"
"    printf(\"не удалось определить имя узла\");\n"
"else\n"
"    printf(\"host=%s\\en\", hbuf);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An example program using B<getnameinfo>()  can be found in B<getaddrinfo>(3)."
msgstr ""
"Пример программы, использующей B<getnameinfo>(), можно найти в "
"B<getaddrinfo>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<accept>(2), B<getpeername>(2), B<getsockname>(2), B<recvfrom>(2), "
"B<socket>(2), B<getaddrinfo>(3), B<gethostbyaddr>(3), B<getservbyname>(3), "
"B<getservbyport>(3), B<inet_ntop>(3), B<hosts>(5), B<services>(5), "
"B<hostname>(7), B<named>(8)"
msgstr ""
"B<accept>(2), B<getpeername>(2), B<getsockname>(2), B<recvfrom>(2), "
"B<socket>(2), B<getaddrinfo>(3), B<gethostbyaddr>(3), B<getservbyname>(3), "
"B<getservbyport>(3), B<inet_ntop>(3), B<hosts>(5), B<services>(5), "
"B<hostname>(7), B<named>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"R.\\& Gilligan, S.\\& Thomson, J.\\& Bound and W.\\& Stevens, I<Basic Socket "
"Interface Extensions for IPv6>, RFC\\ 2553, March 1999."
msgstr ""
"R.\\& Gilligan, S.\\& Thomson, J.\\& Bound and W.\\& Stevens, I<Basic Socket "
"Interface Extensions for IPv6>, RFC\\ 2553, March 1999."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Tatsuya Jinmei and Atsushi Onoe, I<An Extension of Format for IPv6 Scoped "
"Addresses>, internet draft, work in progress E<.UR ftp://ftp.ietf.org\\:/"
"internet-drafts\\:/draft-ietf-ipngwg-scopedaddr-format-02.txt> E<.UE .>"
msgstr ""
"Tatsuya Jinmei and Atsushi Onoe, I<An Extension of Format for IPv6 Scoped "
"Addresses>, черновик E<.UR ftp://ftp.ietf.org\\:/internet-drafts\\:/draft-"
"ietf-ipngwg-scopedaddr-format-02.txt> E<.UE .>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Craig Metz, I<Protocol Independence Using the Sockets API>, Proceedings of "
"the freenix track: 2000 USENIX annual technical conference, June 2000"
msgstr ""
"Craig Metz, I<Protocol Independence Using the Sockets API>, Продолжение темы "
"freenix: 2000 USENIX ежегодной технической конференции, июнь 2000"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"E<.UR http://www.usenix.org\\:/publications\\:/library\\:/proceedings\\:/"
"usenix2000\\:/freenix\\:/metzprotocol.html> E<.UE .>"
msgstr ""
"E<.UR http://www.usenix.org\\:/publications\\:/library\\:/proceedings\\:/"
"usenix2000\\:/freenix\\:/metzprotocol.html> E<.UE .>"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<NI_IDN_ALLOW_UNASSIGNED>, B<NI_IDN_USE_STD3_ASCII_RULES>"
msgstr "B<NI_IDN_ALLOW_UNASSIGNED>, B<NI_IDN_USE_STD3_ASCII_RULES>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<getnameinfo>()  is provided in glibc since version 2.1."
msgid "B<getnameinfo>()  is provided since glibc 2.1."
msgstr "Функция B<getnameinfo>() появилась в glibc начиная с версии 2.1."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, RFC\\ 2553."
msgstr "POSIX.1-2001, POSIX.1-2008, RFC\\ 2553."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"struct sockaddr *addr;     /* input */\n"
"socklen_t addrlen;         /* input */\n"
"char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];\n"
msgstr ""
"struct sockaddr *addr;     /* входные */\n"
"socklen_t addrlen;         /* входные */\n"
"char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,\n"
"            sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)\n"
"    printf(\"host=%s, serv=%s\\en\", hbuf, sbuf);\n"
msgstr ""
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,\n"
"            sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)\n"
"    printf(\"host=%s, serv=%s\\en\", hbuf, sbuf);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"struct sockaddr *addr;     /* input */\n"
"socklen_t addrlen;         /* input */\n"
"char hbuf[NI_MAXHOST];\n"
msgstr ""
"struct sockaddr *addr;     /* входные */\n"
"socklen_t addrlen;         /* входные */\n"
"char hbuf[NI_MAXHOST];\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf),\n"
"            NULL, 0, NI_NAMEREQD))\n"
"    printf(\"could not resolve hostname\");\n"
"else\n"
"    printf(\"host=%s\\en\", hbuf);\n"
msgstr ""
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf),\n"
"            NULL, 0, NI_NAMEREQD))\n"
"    printf(\"не удалось определить имя узла\");\n"
"else\n"
"    printf(\"host=%s\\en\", hbuf);\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,\n"
#| "            sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)\n"
#| "    printf(\"host=%s, serv=%s\\en\", hbuf, sbuf);\n"
msgid ""
"struct sockaddr *addr;     /* input */\n"
"socklen_t addrlen;         /* input */\n"
"char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];\n"
"\\&\n"
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,\n"
"            sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)\n"
"    printf(\"host=%s, serv=%s\\en\", hbuf, sbuf);\n"
msgstr ""
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf), sbuf,\n"
"            sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) == 0)\n"
"    printf(\"host=%s, serv=%s\\en\", hbuf, sbuf);\n"

#. type: Plain text
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf),\n"
#| "            NULL, 0, NI_NAMEREQD))\n"
#| "    printf(\"could not resolve hostname\");\n"
#| "else\n"
#| "    printf(\"host=%s\\en\", hbuf);\n"
msgid ""
"struct sockaddr *addr;     /* input */\n"
"socklen_t addrlen;         /* input */\n"
"char hbuf[NI_MAXHOST];\n"
"\\&\n"
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf),\n"
"            NULL, 0, NI_NAMEREQD))\n"
"    printf(\"could not resolve hostname\");\n"
"else\n"
"    printf(\"host=%s\\en\", hbuf);\n"
msgstr ""
"if (getnameinfo(addr, addrlen, hbuf, sizeof(hbuf),\n"
"            NULL, 0, NI_NAMEREQD))\n"
"    printf(\"не удалось определить имя узла\");\n"
"else\n"
"    printf(\"host=%s\\en\", hbuf);\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
