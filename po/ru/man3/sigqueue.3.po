# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:14+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<sigqueue>()"
msgid "sigqueue"
msgstr "B<sigqueue>()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sigqueue - queue a signal and data to a process"
msgstr "sigqueue - вставляет сигнал и данные в очередь процесса"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int sigqueue(pid_t >I<pid>B<, int >I<sig>B<, const union sigval >I<value>B<);>"
msgid "B<int sigqueue(pid_t >I<pid>B<, int >I<sig>B<, const union sigval >I<value>B<);>\n"
msgstr "B<int sigqueue(pid_t >I<pid>B<, int >I<sig>B<, const union sigval >I<value>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "B<sigqueue>()"
msgid "B<sigqueue>():"
msgstr "B<sigqueue>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 199309L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 199309L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sigqueue>()  sends the signal specified in I<sig> to the process whose PID "
"is given in I<pid>.  The permissions required to send a signal are the same "
"as for B<kill>(2).  As with B<kill>(2), the null signal (0) can be used to "
"check if a process with a given PID exists."
msgstr ""
"Вызов B<sigqueue>() отправляет сигнал, указанный в I<sig>, процессу с "
"идентификатором PID, определённом в I<pid>. Требуются определённые права для "
"отправки сигнала, такие же как для B<kill>(2). Как и в случае с B<kill>(2), "
"пустой сигнал (0) может использоваться для проверки того, что заданный PID "
"вообще существует."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<value> argument is used to specify an accompanying item of data "
"(either an integer or a pointer value) to be sent with the signal, and has "
"the following type:"
msgstr ""
"Аргумент I<value> используется для указания сопутствующего элемента данных "
"(либо целого, либо указателя), отправляемых сигналу, и имеет следующий тип:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"union sigval {\n"
"    int   sival_int;\n"
"    void *sival_ptr;\n"
"};\n"
msgstr ""
"union sigval {\n"
"    int   sival_int;\n"
"    void *sival_ptr;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the receiving process has installed a handler for this signal using the "
"B<SA_SIGINFO> flag to B<sigaction>(2), then it can obtain this data via the "
"I<si_value> field of the I<siginfo_t> structure passed as the second "
"argument to the handler.  Furthermore, the I<si_code> field of that "
"structure will be set to B<SI_QUEUE>."
msgstr ""
"Если у процесса, принимающего сигнал, установлен обработчик посредством "
"B<sigaction>(2) с флагом B<SA_SIGINFO>, то он может получить данные через "
"поле I<si_value> структуры I<siginfo_t>, передаваемой как второй аргумент "
"для обработчика. Кроме этого, значение поля I<si_code> этой структуры будет "
"установлено в B<SI_QUEUE>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<sigqueue>()  returns 0, indicating that the signal was "
"successfully queued to the receiving process.  Otherwise, -1 is returned and "
"I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<sigqueue>() возвращается 0, что означает, что "
"сигнал попал в очередь принимающего процесса. При ошибке возвращается -1 и в "
"I<errno> содержится код ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The limit of signals which may be queued has been reached.  (See "
"B<signal>(7)  for further information.)"
msgstr ""
"Достигнуто ограничение на количество сигналов в очереди (подробней об этом "
"смотрите в B<signal>(7))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<sig> was invalid."
msgstr "Значение I<sig> некорректно."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The process does not have permission to send the signal to the receiving "
"process.  For the required permissions, see B<kill>(2)."
msgstr ""
"Процесс не имеет прав для отправки сигнала принимающему процессу. Требуемые "
"права смотрите в B<kill>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No process has a PID matching I<pid>."
msgstr "Нет процесса с идентификатором PID, соответствующем указанному I<pid>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<sigqueue>()"
msgstr "B<sigqueue>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Отличия между библиотекой C и ядром"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Linux, B<sigqueue>()  is implemented using the B<rt_sigqueueinfo>(2)  "
"system call.  The system call differs in its third argument, which is the "
"I<siginfo_t> structure that will be supplied to the receiving process's "
"signal handler or returned by the receiving process's B<sigtimedwait>(2)  "
"call.  Inside the glibc B<sigqueue>()  wrapper, this argument, I<uinfo>, is "
"initialized as follows:"
msgstr ""
"В Linux B<sigqueue>() реализована через системный вызов "
"B<rt_sigqueueinfo>(2). Данный системный вызов отличается от неё третьим "
"аргументом: структура I<siginfo_t>, которая будет предоставляться "
"обработчику сигнала принимающего процесса или возвращаться вызовом "
"B<sigtimedwait>(2) из принимающего процесса. В обёрточной функции glibc "
"B<sigqueue>() этот аргумент, I<uinfo>, инициализируется следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"uinfo.si_signo = sig;      /* Argument supplied to sigqueue() */\n"
"uinfo.si_code = SI_QUEUE;\n"
"uinfo.si_pid = getpid();   /* Process ID of sender */\n"
"uinfo.si_uid = getuid();   /* Real UID of sender */\n"
"uinfo.si_value = val;      /* Argument supplied to sigqueue() */\n"
msgstr ""
"uinfo.si_signo = sig;      /* аргумент, передаваемый в sigqueue() */\n"
"uinfo.si_code = SI_QUEUE;\n"
"uinfo.si_pid = getpid();   /* ID процесса отправителя */\n"
"uinfo.si_uid = getuid();   /* реальный UID отправителя */\n"
"uinfo.si_value = val;      /* аргумент, передаваемый в sigqueue() */\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "glibc 2.1.  C99, POSIX.1-2001."
msgid "Linux 2.2.  POSIX.1-2001."
msgstr "glibc 2.1.  C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If this function results in the sending of a signal to the process that "
"invoked it, and that signal was not blocked by the calling thread, and no "
"other threads were willing to handle this signal (either by having it "
"unblocked, or by waiting for it using B<sigwait>(3)), then at least some "
"signal must be delivered to this thread before this function returns."
msgstr ""
"Если этот вызов приводит к отправке сигнала процессу, который его вызвал, и "
"этот сигнал не заблокирован вызывающей нитью, и никакие другие нити не "
"желают обрабатывать сигнал (либо он для них не заблокирован, либо они "
"ожидали его с помощью B<sigwait>(3), то, по меньшей мере, сигнал должен быть "
"доставлен этой нити до завершения этого вызова."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<kill>(2), B<rt_sigqueueinfo>(2), B<sigaction>(2), B<signal>(2), "
"B<pthread_sigqueue>(3), B<sigwait>(3), B<signal>(7)"
msgstr ""
"B<kill>(2), B<rt_sigqueueinfo>(2), B<sigaction>(2), B<signal>(2), "
"B<pthread_sigqueue>(3), B<sigwait>(3), B<signal>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<sigqueue>()  and the underlying B<rt_sigqueueinfo>()  system call first "
#| "appeared in Linux 2.2."
msgid ""
"B<sigqueue>()  and the underlying B<rt_sigqueueinfo>(2)  system call first "
"appeared in Linux 2.2."
msgstr ""
"Функция B<sigqueue>() и используемый ей системный вызов B<rt_sigqueueinfo>() "
"впервые появились в Linux 2.2."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
