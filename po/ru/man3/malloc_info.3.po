# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:04+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<malloc_info>()"
msgid "malloc_info"
msgstr "B<malloc_info>()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "malloc_info - export malloc state to a stream"
msgstr "malloc_info - экспортирует состояние malloc в поток"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>malloc.hE<gt>>\n"
msgstr "B<#include E<lt>malloc.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int malloc_info(int >I<options>B<, FILE *>I<stream>B<);>\n"
msgstr "B<int malloc_info(int >I<options>B<, FILE *>I<stream>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<malloc_info>()  function exports an XML string that describes the "
"current state of the memory-allocation implementation in the caller.  The "
"string is printed on the file stream I<stream>.  The exported string "
"includes information about all arenas (see B<malloc>(3))."
msgstr ""
"Функция B<malloc_info>() экспортирует строку XML, описывающую текущее "
"состояние реализации выделения памяти вызывающего. Строка печатается в "
"файловый поток I<stream>. В экспортируемой строке содержится информация о "
"всех областях (arenas) (смотрите B<malloc>(3))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "As currently implemented, I<options> must be zero."
msgstr "В текущей реализации значение I<options> должно быть равно нулю."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<malloc_info>()  returns 0; on error, it returns -1, with "
#| "I<errno> set to indicate the cause."
msgid ""
"On success, B<malloc_info>()  returns 0.  On failure, it returns -1, and "
"I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<malloc_info>() возвращается 0; при ошибке "
"возвращается -1, а в I<errno> помещается код ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<options> was nonzero."
msgstr "Значение I<options> не равно."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<malloc_info>()"
msgstr "B<malloc_info>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "Since glibc 2.10:"
msgid "glibc 2.10."
msgstr "Начиная с glibc 2.10:"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The memory-allocation information is provided as an XML string (rather than "
"a C structure)  because the information may change over time (according to "
"changes in the underlying implementation).  The output XML string includes a "
"version field."
msgstr ""
"Информация о выделении памяти предоставляется в виде строки XML (а не в "
"структуре C), так как структура со временем может меняться (при изменении в "
"реализации). Возвращаемая строка XML содержит поле версии."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<open_memstream>(3)  function can be used to send the output of "
"B<malloc_info>()  directly into a buffer in memory, rather than to a file."
msgstr ""
"Для отправки вывода B<malloc_info>() в буфер памяти, а не в файл можно "
"использовать функцию B<open_memstream>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<malloc_info>()  function is designed to address deficiencies in "
"B<malloc_stats>(3)  and B<mallinfo>(3)."
msgstr ""
"Функция B<malloc_info>() разработана для компенсации нехватки данных из "
"B<malloc_stats>(3) и B<mallinfo>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program below takes up to four command-line arguments, of which the "
"first three are mandatory.  The first argument specifies the number of "
"threads that the program should create.  All of the threads, including the "
"main thread, allocate the number of blocks of memory specified by the second "
"argument.  The third argument controls the size of the blocks to be "
"allocated.  The main thread creates blocks of this size, the second thread "
"created by the program allocates blocks of twice this size, the third thread "
"allocates blocks of three times this size, and so on."
msgstr ""
"Программа, представленная ниже, принимает до четырёх параметров командной "
"строки, три из которых обязательны. В первом параметре задаётся количество "
"нитей, которые должна создать программа. Все нити, включая главную нить, "
"выделяют количество блоков памяти, заданное в втором параметре. В третьем "
"параметре задаётся размер выделяемых блоков. Главная нить создает блоки "
"этого размера, вторая нить создаваемая программой, выделяет блоки "
"двукратного размера, третья нить выделяет блоки трёхкратного размера и так "
"далее."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program calls B<malloc_info>()  twice to display the memory-allocation "
"state.  The first call takes place before any threads are created or memory "
"allocated.  The second call is performed after all threads have allocated "
"memory."
msgstr ""
"Чтобы показать состояние выделения памяти программа дважды вызывает "
"B<malloc_info>(). Первый раз вызов делается до создания нитей и выделения "
"памяти. Второй вызов выполняется после того, как все нити выделят память."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the following example, the command-line arguments specify the creation of "
"one additional thread, and both the main thread and the additional thread "
"allocate 10000 blocks of memory.  After the blocks of memory have been "
"allocated, B<malloc_info>()  shows the state of two allocation arenas."
msgstr ""
"В следующем примере аргументами командной строки задаётся создание одной "
"дополнительной нити и что главная и дополнительная нить выделяют 10000 "
"блоков памяти. После того, как блоки памяти выделены, B<malloc_info>() "
"показывает состояние двух областей выделения."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "============ After allocating blocks ============\n"
#| "E<lt>malloc version=\"1\"E<gt>\n"
#| "E<lt>heap nr=\"0\"E<gt>\n"
#| "E<lt>sizesE<gt>\n"
#| "E<lt>/sizesE<gt>\n"
#| "E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
#| "E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
#| "E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
#| "E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
#| "E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
#| "E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
#| "E<lt>/heapE<gt>\n"
#| "E<lt>heap nr=\"1\"E<gt>\n"
#| "E<lt>sizesE<gt>\n"
#| "E<lt>/sizesE<gt>\n"
#| "E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
#| "E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
#| "E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
#| "E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
#| "E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
#| "E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
#| "E<lt>/heapE<gt>\n"
#| "E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
#| "E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
#| "E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
#| "E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
#| "E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
#| "E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
#| "E<lt>/mallocE<gt>\n"
msgid ""
"$ B<getconf GNU_LIBC_VERSION>\n"
"glibc 2.13\n"
"$ B<./a.out 1 10000 100>\n"
"============ Before allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
"\\&\n"
"============ After allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>heap nr=\"1\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
msgstr ""
"============ после выделения блоков ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>heap nr=\"1\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Исходный код программы"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"static size_t        blockSize;\n"
"static size_t        numThreads;\n"
"static unsigned int  numBlocks;\n"
"\\&\n"
"static void *\n"
"thread_func(void *arg)\n"
"{\n"
"    int tn = (int) arg;\n"
"\\&\n"
"    /* The multiplier \\[aq](2 + tn)\\[aq] ensures that each thread (including\n"
"       the main thread) allocates a different amount of memory. */\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize * (2 + tn)) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc-thread\");\n"
"\\&\n"
"    sleep(100);         /* Sleep until main thread terminates. */\n"
"    return NULL;\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int        sleepTime;\n"
"    pthread_t  *thr;\n"
"\\&\n"
"    if (argc E<lt> 4) {\n"
"        fprintf(stderr,\n"
"                \"%s num-threads num-blocks block-size [sleep-time]\\[rs]n\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    numThreads = atoi(argv[1]);\n"
"    numBlocks = atoi(argv[2]);\n"
"    blockSize = atoi(argv[3]);\n"
"    sleepTime = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"
"\\&\n"
"    thr = calloc(numThreads, sizeof(*thr));\n"
"    if (thr == NULL)\n"
"        err(EXIT_FAILURE, \"calloc\");\n"
"\\&\n"
"    printf(\"============ Before allocating blocks ============\\[rs]n\");\n"
"    malloc_info(0, stdout);\n"
"\\&\n"
"    /* Create threads that allocate different amounts of memory. */\n"
"\\&\n"
"    for (size_t tn = 0; tn E<lt> numThreads; tn++) {\n"
"        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
"                               (void *) tn);\n"
"        if (errno != 0)\n"
"            err(EXIT_FAILURE, \"pthread_create\");\n"
"\\&\n"
"        /* If we add a sleep interval after the start-up of each\n"
"           thread, the threads likely won\\[aq]t contend for malloc\n"
"           mutexes, and therefore additional arenas won\\[aq]t be\n"
"           allocated (see malloc(3)). */\n"
"\\&\n"
"        if (sleepTime E<gt> 0)\n"
"            sleep(sleepTime);\n"
"    }\n"
"\\&\n"
"    /* The main thread also allocates some memory. */\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc\");\n"
"\\&\n"
"    sleep(2);           /* Give all threads a chance to\n"
"                           complete allocations. */\n"
"\\&\n"
"    printf(\"\\[rs]n============ After allocating blocks ============\\[rs]n\");\n"
"    malloc_info(0, stdout);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mallinfo>(3), B<malloc>(3), B<malloc_stats>(3), B<mallopt>(3), "
"B<open_memstream>(3)"
msgstr ""
"B<mallinfo>(3), B<malloc>(3), B<malloc_stats>(3), B<mallopt>(3), "
"B<open_memstream>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<malloc_info>()  was added to glibc in version 2.10."
msgid "B<malloc_info>()  was added in glibc 2.10."
msgstr "Функция B<malloc_info>() впервые появилась в glibc 2.10."

#. type: Plain text
#: debian-bookworm
msgid "This function is a GNU extension."
msgstr "Эта функция является расширением GNU."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"$ B<getconf GNU_LIBC_VERSION>\n"
"glibc 2.13\n"
"$ B<./a.out 1 10000 100>\n"
"============ Before allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
msgstr ""
"$ B<getconf GNU_LIBC_VERSION>\n"
"glibc 2.13\n"
"$ B<./a.out 1 10000 100>\n"
"============ до выделения блоков ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"============ After allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>heap nr=\"1\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
msgstr ""
"============ после выделения блоков ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>heap nr=\"1\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>mqueue.hE<gt>\n"
#| "#include E<lt>sys/stat.hE<gt>\n"
#| "#include E<lt>fcntl.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>unistd.hE<gt>\n"
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>mqueue.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "static size_t blockSize;\n"
#| "static int numThreads, numBlocks;\n"
msgid ""
"static size_t        blockSize;\n"
"static size_t        numThreads;\n"
"static unsigned int  numBlocks;\n"
msgstr ""
"static size_t blockSize;\n"
"static int numThreads, numBlocks;\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "static void *\n"
#| "thread_func(void *arg)\n"
#| "{\n"
#| "    int j;\n"
#| "    int tn = (int) arg;\n"
msgid ""
"static void *\n"
"thread_func(void *arg)\n"
"{\n"
"    int tn = (int) arg;\n"
msgstr ""
"static void *\n"
"thread_func(void *arg)\n"
"{\n"
"    int j;\n"
"    int tn = (int) arg;\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "    /* The multiplier \\(aq(2 + tn)\\(aq ensures that each thread (including\n"
#| "       the main thread) allocates a different amount of memory */\n"
msgid ""
"    /* The multiplier \\[aq](2 + tn)\\[aq] ensures that each thread (including\n"
"       the main thread) allocates a different amount of memory. */\n"
msgstr ""
"    /* Множитель \\(aq(2 + tn)\\(aq для обеспечения того, что каждая\n"
"       нить (включая главную) выделяет разное количество памяти */\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "    for (j = 0; j E<lt> numBlocks; j++)\n"
#| "        if (malloc(blockSize * (2 + tn)) == NULL)\n"
#| "            errExit(\"malloc-thread\");\n"
msgid ""
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize * (2 + tn)) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc-thread\");\n"
msgstr ""
"    for (j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize * (2 + tn)) == NULL)\n"
"            errExit(\"malloc-thread\");\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "    sleep(100);         /* Sleep until main thread terminates */\n"
#| "    return NULL;\n"
#| "}\n"
msgid ""
"    sleep(100);         /* Sleep until main thread terminates. */\n"
"    return NULL;\n"
"}\n"
msgstr ""
"    sleep(100);         /* Спим, пока главная нить не завершит работу */\n"
"    return NULL;\n"
"}\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(int argc, char *argv[])\n"
#| "{\n"
#| "    int j, tn, sleepTime;\n"
#| "    pthread_t *thr;\n"
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int        sleepTime;\n"
"    pthread_t  *thr;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int j, tn, sleepTime;\n"
"    pthread_t *thr;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc E<lt> 4) {\n"
"        fprintf(stderr,\n"
"                \"%s num-threads num-blocks block-size [sleep-time]\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 4) {\n"
"        fprintf(stderr,\n"
"                \"%s num-threads num-blocks block-size [sleep-time]\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    numThreads = atoi(argv[1]);\n"
"    numBlocks = atoi(argv[2]);\n"
"    blockSize = atoi(argv[3]);\n"
"    sleepTime = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"
msgstr ""
"    numThreads = atoi(argv[1]);\n"
"    numBlocks = atoi(argv[2]);\n"
"    blockSize = atoi(argv[3]);\n"
"    sleepTime = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "    thr = calloc(numThreads, sizeof(pthread_t));\n"
#| "    if (thr == NULL)\n"
#| "        errExit(\"calloc\");\n"
msgid ""
"    thr = calloc(numThreads, sizeof(*thr));\n"
"    if (thr == NULL)\n"
"        err(EXIT_FAILURE, \"calloc\");\n"
msgstr ""
"    thr = calloc(numThreads, sizeof(pthread_t));\n"
"    if (thr == NULL)\n"
"        errExit(\"calloc\");\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"============ Before allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
msgstr ""
"    printf(\"============ до выделения блоков ============\\en\");\n"
"    malloc_info(0, stdout);\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid "    /* Create threads that allocate different amounts of memory */\n"
msgid "    /* Create threads that allocate different amounts of memory. */\n"
msgstr "    /* Создаём нити, которые выделяют разное количество памяти  */\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "    for (tn = 0; tn E<lt> numThreads; tn++) {\n"
#| "        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
#| "                               (void *) tn);\n"
#| "        if (errno != 0)\n"
#| "            errExit(\"pthread_create\");\n"
msgid ""
"    for (size_t tn = 0; tn E<lt> numThreads; tn++) {\n"
"        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
"                               (void *) tn);\n"
"        if (errno != 0)\n"
"            err(EXIT_FAILURE, \"pthread_create\");\n"
msgstr ""
"    for (tn = 0; tn E<lt> numThreads; tn++) {\n"
"        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
"                               (void *) tn);\n"
"        if (errno != 0)\n"
"            errExit(\"pthread_create\");\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "        /* If we add a sleep interval after the start-up of each\n"
#| "           thread, the threads likely won\\(aqt contend for malloc\n"
#| "           mutexes, and therefore additional arenas won\\(aqt be\n"
#| "           allocated (see malloc(3)). */\n"
msgid ""
"        /* If we add a sleep interval after the start-up of each\n"
"           thread, the threads likely won\\[aq]t contend for malloc\n"
"           mutexes, and therefore additional arenas won\\[aq]t be\n"
"           allocated (see malloc(3)). */\n"
msgstr ""
"        /* если мы добавим задержку после запуска каждой нити,\n"
"           то нити, вероятно,  не будут бороться за мьютекс malloc,\n"
"           и поэтому дополнительные области выделены\n"
"           не будут (смотрите malloc(3)) */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        if (sleepTime E<gt> 0)\n"
"            sleep(sleepTime);\n"
"    }\n"
msgstr ""
"        if (sleepTime E<gt> 0)\n"
"            sleep(sleepTime);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid "    /* The main thread also allocates some memory */\n"
msgid "    /* The main thread also allocates some memory. */\n"
msgstr "    /* главная нить также выделяет память */\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "    for (j = 0; j E<lt> numBlocks; j++)\n"
#| "        if (malloc(blockSize) == NULL)\n"
#| "            errExit(\"malloc\");\n"
msgid ""
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc\");\n"
msgstr ""
"    for (j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize) == NULL)\n"
"            errExit(\"malloc\");\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "    sleep(2);           /* Give all threads a chance to\n"
#| "                           complete allocations */\n"
msgid ""
"    sleep(2);           /* Give all threads a chance to\n"
"                           complete allocations. */\n"
msgstr ""
"    sleep(2);           /* ждём, чтобы потоки успели\n"
"                           выделить память */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"\\en============ After allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
msgstr ""
"    printf(\"\\en============ после выделения блоков ============\\en\");\n"
"    malloc_info(0, stdout);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"static size_t        blockSize;\n"
"static size_t        numThreads;\n"
"static unsigned int  numBlocks;\n"
"\\&\n"
"static void *\n"
"thread_func(void *arg)\n"
"{\n"
"    int tn = (int) arg;\n"
"\\&\n"
"    /* The multiplier \\[aq](2 + tn)\\[aq] ensures that each thread (including\n"
"       the main thread) allocates a different amount of memory. */\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize * (2 + tn)) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc-thread\");\n"
"\\&\n"
"    sleep(100);         /* Sleep until main thread terminates. */\n"
"    return NULL;\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int        sleepTime;\n"
"    pthread_t  *thr;\n"
"\\&\n"
"    if (argc E<lt> 4) {\n"
"        fprintf(stderr,\n"
"                \"%s num-threads num-blocks block-size [sleep-time]\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    numThreads = atoi(argv[1]);\n"
"    numBlocks = atoi(argv[2]);\n"
"    blockSize = atoi(argv[3]);\n"
"    sleepTime = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"
"\\&\n"
"    thr = calloc(numThreads, sizeof(*thr));\n"
"    if (thr == NULL)\n"
"        err(EXIT_FAILURE, \"calloc\");\n"
"\\&\n"
"    printf(\"============ Before allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
"\\&\n"
"    /* Create threads that allocate different amounts of memory. */\n"
"\\&\n"
"    for (size_t tn = 0; tn E<lt> numThreads; tn++) {\n"
"        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
"                               (void *) tn);\n"
"        if (errno != 0)\n"
"            err(EXIT_FAILURE, \"pthread_create\");\n"
"\\&\n"
"        /* If we add a sleep interval after the start-up of each\n"
"           thread, the threads likely won\\[aq]t contend for malloc\n"
"           mutexes, and therefore additional arenas won\\[aq]t be\n"
"           allocated (see malloc(3)). */\n"
"\\&\n"
"        if (sleepTime E<gt> 0)\n"
"            sleep(sleepTime);\n"
"    }\n"
"\\&\n"
"    /* The main thread also allocates some memory. */\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc\");\n"
"\\&\n"
"    sleep(2);           /* Give all threads a chance to\n"
"                           complete allocations. */\n"
"\\&\n"
"    printf(\"\\en============ After allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
