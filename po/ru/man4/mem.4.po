# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:04+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "mem"
msgstr "mem"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mem, kmem, port - system memory, kernel memory and system ports"
msgstr "mem, kmem, port - системная память, память ядра и порты системы"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I</dev/mem> is a character device file that is an image of the main memory "
"of the computer.  It may be used, for example, to examine (and even patch) "
"the system."
msgstr ""
"Файл I</dev/mem> — это файл символьного устройства, представляющий образ "
"физической памяти компьютера. Этот файл может быть использован для "
"исследования системы (и даже для внесения в неё исправлений)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Byte addresses in I</dev/mem> are interpreted as physical memory addresses.  "
"References to nonexistent locations cause errors to be returned."
msgstr ""
"Адреса байтов в I</dev/mem> рассматриваются как адреса физической памяти. "
"При ссылках на несуществующие адреса возвращаются ошибки."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Examining and patching is likely to lead to unexpected results when read-"
"only or write-only bits are present."
msgstr ""
"Исследование системы или внесение в неё исправлений иногда приводят к "
"непредвиденным результатам в том случае, если есть биты, которые разрешается "
"только читать или только записывать."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.26, and depending on the architecture, the "
"B<CONFIG_STRICT_DEVMEM> kernel configuration option limits the areas which "
"can be accessed through this file.  For example: on x86, RAM access is not "
"allowed but accessing memory-mapped PCI regions is."
msgstr ""
"Начиная с версии Linux 2.6.26 и в зависимости от архитектуры, параметр "
"настройки ядра B<CONFIG_STRICT_DEVMEM> ограничивает области, к которым можно "
"получить доступ через этот файл. Например, на архитектуре x86 доступ к ОЗУ "
"не разрешен, однако к отображенной в области PCI памяти доступ есть."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "It is typically created by:"
msgstr "Обычно, этот файл создается так:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 660 /dev/mem c 1 1\n"
"chown root:kmem /dev/mem\n"
msgstr ""
"mknod -m 660 /dev/mem c 1 1\n"
"chown root:kmem /dev/mem\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The file I</dev/kmem> is the same as I</dev/mem>, except that the kernel "
"virtual memory rather than physical memory is accessed.  Since Linux 2.6.26, "
"this file is available only if the B<CONFIG_DEVKMEM> kernel configuration "
"option is enabled."
msgstr ""
"Файл I</dev/kmem> идентичен файлу I</dev/mem> за исключением того, что "
"обеспечивает доступ к виртуальной памяти ядра, а не к физической. Начиная с "
"версии Linux 2.6.26, этот файл становится доступен только после включения "
"параметра настройки ядра B<CONFIG_DEVKMEM>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 640 /dev/kmem c 1 2\n"
"chown root:kmem /dev/kmem\n"
msgstr ""
"mknod -m 640 /dev/kmem c 1 2\n"
"chown root:kmem /dev/kmem\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/port> is similar to I</dev/mem>, but the I/O ports are accessed."
msgstr ""
"Файл I</dev/port> похож на I</dev/mem>, но предоставляет доступ к портам "
"ввода-вывода."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 660 /dev/port c 1 4\n"
"chown root:kmem /dev/port\n"
msgstr ""
"mknod -m 660 /dev/port c 1 4\n"
"chown root:kmem /dev/port\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/mem>"
msgstr "I</dev/mem>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/kmem>"
msgstr "I</dev/kmem>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/port>"
msgstr "I</dev/port>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chown>(1), B<mknod>(1), B<ioperm>(2)"
msgstr "B<chown>(1), B<mknod>(1), B<ioperm>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
