# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2016.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Lockal <lockalsash@gmail.com>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Баринов Владимир, 2016.
# Иван Павлов <pavia00@gmail.com>, 2017,2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:10+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "readdir"
msgstr "readdir"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "readdir - read directory entry"
msgstr "readdir - читает элемент каталога"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* определения констант B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_readdir, unsigned int >I<fd>B<,>\n"
"B<            struct old_linux_dirent *>I<dirp>B<, unsigned int >I<count>B<);>\n"
msgstr ""
"B<int syscall(SYS_readdir, unsigned int >I<fd>B<,>\n"
"B<            struct old_linux_dirent *>I<dirp>B<, unsigned int >I<count>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgid ""
"I<Note>: There is no definition of B<struct old_linux_dirent>; see NOTES."
msgstr ""
"I<Замечание>: В glibc нет обёрточной функции для данного системного вызова; "
"смотрите ЗАМЕЧАНИЯ."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is not the function you are interested in.  Look at B<readdir>(3)  for "
"the POSIX conforming C library interface.  This page documents the bare "
"kernel system call interface, which is superseded by B<getdents>(2)."
msgstr ""
"Эта не та функция, которая должна представлять для вас интерес. Смотрите "
"описание функции B<readdir>(3), которая является интерфейсом библиотеки "
"языка C, соответствующим стандарту POSIX. В этой странице описан минимальный "
"интерфейс системного вызова ядра, который заменён на B<getdents>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<readdir>()  reads one I<old_linux_dirent> structure from the directory "
"referred to by the file descriptor I<fd> into the buffer pointed to by "
"I<dirp>.  The argument I<count> is ignored; at most one I<old_linux_dirent> "
"structure is read."
msgstr ""
"Вызов B<readdir>() читает структуру I<old_linux_dirent> из каталога, "
"заданного файловым дескриптором I<fd>, в буфер, указываемый в I<dirp>. "
"Аргумент I<count> игнорируется; всегда считывается только одна структура "
"I<old_linux_dirent>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<old_linux_dirent> structure is declared (privately in Linux kernel "
"file B<fs/readdir.c>)  as follows:"
msgstr ""
"Структура I<old_linux_dirent> определена (в файле ядра Linux B<fs/readdir."
"c>, недоступна извне) следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct old_linux_dirent {\n"
"    unsigned long d_ino;     /* inode number */\n"
"    unsigned long d_offset;  /* offset to this I<old_linux_dirent> */\n"
"    unsigned short d_namlen; /* length of this I<d_name> */\n"
"    char  d_name[1];         /* filename (null-terminated) */\n"
"}\n"
msgstr ""
"struct old_linux_dirent {\n"
"    unsigned long d_ino;     /* номер иноды */\n"
"    unsigned long d_offset;  /* смещение на данную I<old_linux_dirent> */\n"
"    unsigned short d_namlen; /* длина данной I<d_name> */\n"
"    char  d_name[1];         /* имя файла (с null в конце) */\n"
"}\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<d_ino> is an inode number.  I<d_offset> is the distance from the start "
#| "of the directory to this I<old_linux_dirent>.  I<d_reclen> is the size of "
#| "I<d_name>, not counting the terminating null byte (\\(aq\\e0\\(aq).  "
#| "I<d_name> is a null-terminated filename."
msgid ""
"I<d_ino> is an inode number.  I<d_offset> is the distance from the start of "
"the directory to this I<old_linux_dirent>.  I<d_reclen> is the size of "
"I<d_name>, not counting the terminating null byte (\\[aq]\\[rs]0\\[aq]).  "
"I<d_name> is a null-terminated filename."
msgstr ""
"Значением I<d_ino> является номер иноды. В I<d_offset> задаётся смещение "
"данной I<old_linux_dirent> от начала каталога. В I<d_reclen> задаётся размер "
"I<d_name> без учета завершающего байта null (\\(aq\\e0\\(aq). В I<d_name> "
"указывается имя файла, завершающееся null."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, 1 is returned.  On end of directory, 0 is returned.  On "
#| "error, -1 is returned, and I<errno> is set appropriately."
msgid ""
"On success, 1 is returned.  On end of directory, 0 is returned.  On error, "
"-1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении возвращается 1. Если каталог закончился возвращается "
"0. При ошибке возвращается -1, а переменной I<errno> присваивается номер "
"ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Invalid file descriptor I<fd>."
msgstr "Неверный файловый дескриптор I<fd>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Argument points outside the calling process's address space."
msgstr ""
"Аргумент указывает за пределы адресного пространства вызывающего процесса."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Result buffer is too small."
msgstr "Буфер результата слишком мал."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No such directory."
msgstr "Заданный каталог не существует."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "File descriptor does not refer to a directory."
msgstr "Файловый дескриптор указывает не на каталог."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You will need to define the I<old_linux_dirent> structure yourself.  "
"However, probably you should use B<readdir>(3)  instead."
msgstr ""
"Структуру I<old_linux_dirent> нужно определить самостоятельно. Однако лучше "
"использовать B<readdir>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This system call does not exist on x86-64."
msgstr "Этот системный вызов отсутствует на x86-64."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getdents>(2), B<readdir>(3)"
msgstr "B<getdents>(2), B<readdir>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "I<d_ino> is an inode number.  I<d_offset> is the distance from the start "
#| "of the directory to this I<old_linux_dirent>.  I<d_reclen> is the size of "
#| "I<d_name>, not counting the terminating null byte (\\(aq\\e0\\(aq).  "
#| "I<d_name> is a null-terminated filename."
msgid ""
"I<d_ino> is an inode number.  I<d_offset> is the distance from the start of "
"the directory to this I<old_linux_dirent>.  I<d_reclen> is the size of "
"I<d_name>, not counting the terminating null byte (\\[aq]\\e0\\[aq]).  "
"I<d_name> is a null-terminated filename."
msgstr ""
"Значением I<d_ino> является номер иноды. В I<d_offset> задаётся смещение "
"данной I<old_linux_dirent> от начала каталога. В I<d_reclen> задаётся размер "
"I<d_name> без учета завершающего байта null (\\(aq\\e0\\(aq). В I<d_name> "
"указывается имя файла, завершающееся null."

#. type: Plain text
#: debian-bookworm
msgid "This system call is Linux-specific."
msgstr "Данный вызов есть только в Linux."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
