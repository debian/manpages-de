# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:59+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "futimesat"
msgstr "futimesat"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"futimesat - change timestamps of a file relative to a directory file "
"descriptor"
msgstr ""
"futimesat - изменяет временные отметки файла, определяемого относительно "
"файлового дескриптора каталога"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>>            /* Definition of B<AT_*> constants */\n"
"B<#include E<lt>sys/time.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>>            /* определения констант B<AT_*> */\n"
"B<#include E<lt>sys/time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int futimesat(int >I<dirfd>B<, const char *>I<pathname>B<,>\n"
#| "B<              const struct timeval >I<times>B<[2]);>\n"
msgid ""
"B<[[deprecated]] int futimesat(int >I<dirfd>B<, const char *>I<pathname>B<,>\n"
"B<                             const struct timeval >I<times>B<[2]);>\n"
msgstr ""
"B<int futimesat(int >I<dirfd>B<, const char *>I<pathname>B<,>\n"
"B<              const struct timeval >I<times>B<[2]);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<futimesat>():"
msgstr "B<futimesat>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This system call is obsolete.  Use B<utimensat>(2)  instead."
msgstr ""
"Данный системный вызов устарел. Используйте вместо него B<utimensat>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<futimesat>()  system call operates in exactly the same way as "
"B<utimes>(2), except for the differences described in this manual page."
msgstr ""
"Системный вызов B<futimesat>() работает также как системный вызов "
"B<utimes>(2), за исключением случаев, описанных в данной справочной странице."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<utimes>(2)  for a relative pathname)."
msgstr ""
"Если в I<pathname> задан относительный путь, то он считается относительно "
"каталога, на который ссылается файловый дескриптор I<dirfd> (а не "
"относительно текущего рабочего каталога вызывающего процесса, как это "
"делается в B<utimes>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pathname> is relative and I<dirfd> is the special value B<AT_FDCWD>, "
"then I<pathname> is interpreted relative to the current working directory of "
"the calling process (like B<utimes>(2))."
msgstr ""
"Если в I<pathname> задан относительный путь и I<dirfd> равно специальному "
"значению B<AT_FDCWD>, то I<pathname> рассматривается относительно текущего "
"рабочего каталога вызывающего процесса (как B<utimes>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pathname> is absolute, then I<dirfd> is ignored.  (See B<openat>(2)  "
"for an explanation of why the I<dirfd> argument is useful.)"
msgstr ""
"Если I<pathname> является абсолютным, то I<dirfd> игнорируется. (См. "
"B<openat>(2) для объяснения того, почему аргумент I<dirfd> полезен.)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<futimesat>()  returns a 0.  On error, -1 is returned and "
"I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<futimesat>() возвращает 0; при ошибке \\(em -1, а "
"в I<errno> задаётся причина ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The same errors that occur for B<utimes>(2)  can also occur for "
"B<futimesat>().  The following additional errors can occur for "
"B<futimesat>():"
msgstr ""
"В B<futimesat>() могут возникнуть те же ошибки, что и в B<utimes>(2). Также, "
"в B<futimesat>() могут возникнуть следующие ошибки:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<pathname> is relative but I<dirfd> is neither B<AT_FDCWD> nor a valid file "
"descriptor."
msgstr ""
"В I<pathname> содержится относительный путь, но значение I<dirfd> не равно "
"B<AT_FDCWD> и не является правильным файловым дескриптором."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<pathname> is relative and I<dirfd> is a file descriptor referring to a "
"file other than a directory."
msgstr ""
"Значение I<pathname> содержит относительный путь и I<dirfd> содержит "
"файловый дескриптор, указывающий на файл, а не на каталог."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: SS
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "glibc"
msgstr "glibc"

#.  The Solaris futimesat() also has this strangeness.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pathname> is NULL, then the glibc B<futimesat>()  wrapper function "
"updates the times for the file referred to by I<dirfd>."
msgstr ""
"Если I<pathname> равно NULL, то обёрточная функция glibc B<futimesat>() "
"обновляет временные метки файла, указанного в I<dirfd>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Отсутствуют."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 2.6.16, glibc 2.4."
msgstr "Linux 2.6.16, glibc 2.4."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This system call is nonstandard.  It was implemented from a specification "
#| "that was proposed for POSIX.1, but that specification was replaced by the "
#| "one for B<utimensat>(2)."
msgid ""
"It was implemented from a specification that was proposed for POSIX.1, but "
"that specification was replaced by the one for B<utimensat>(2)."
msgstr ""
"Данный системный вызов не является стандартным. Он был реализован по "
"спецификации POSIX.1, но она была заменена на B<utimensat>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A similar system call exists on Solaris."
msgstr "Подобный системный вызов есть в Solaris."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<stat>(2), B<utimensat>(2), B<utimes>(2), B<futimes>(3), "
"B<path_resolution>(7)"
msgstr ""
"B<stat>(2), B<utimensat>(2), B<utimes>(2), B<futimes>(3), "
"B<path_resolution>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<futimesat>()  was added to Linux in kernel 2.6.16; library support was "
#| "added to glibc in version 2.4."
msgid ""
"B<futimesat>()  was added in Linux 2.6.16; library support was added in "
"glibc 2.4."
msgstr ""
"Вызов B<futimesat>() был добавлен в ядро Linux версии 2.6.16; поддержка в "
"glibc доступна с версии 2.4."

#. type: Plain text
#: debian-bookworm
msgid ""
"This system call is nonstandard.  It was implemented from a specification "
"that was proposed for POSIX.1, but that specification was replaced by the "
"one for B<utimensat>(2)."
msgstr ""
"Данный системный вызов не является стандартным. Он был реализован по "
"спецификации POSIX.1, но она была заменена на B<utimensat>(2)."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "glibc notes"
msgstr "Замечания по glibc"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
