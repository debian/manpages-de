# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Artyom Kunyov <artkun@guitarplayer.ru>, 2012.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Konstantin Shvaykovskiy <kot.shv@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "lseek"
msgstr "lseek"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "lseek - reposition read/write file offset"
msgstr "lseek - изменяет файловое смещение, используемое при чтении/записи"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<off_t lseek(int >I<fd>B<, off_t >I<offset>B<, int >I<whence>B<);>\n"
msgstr "B<off_t lseek(int >I<fd>B<, off_t >I<offset>B<, int >I<whence>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<lseek>()  repositions the file offset of the open file description "
"associated with the file descriptor I<fd> to the argument I<offset> "
"according to the directive I<whence> as follows:"
msgstr ""
"Вызов B<lseek>() изменяет файловое смещение в описании открытого файла, "
"связанного с файловым дескрипторов I<fd>, на значение аргумента I<offset> в "
"соответствии с директивой I<whence>, которая может принимать одно из "
"следующих значений:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SEEK_SET>"
msgstr "B<SEEK_SET>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file offset is set to I<offset> bytes."
msgstr "Установить файловое смещение равным I<offset> (в байтах)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SEEK_CUR>"
msgstr "B<SEEK_CUR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file offset is set to its current location plus I<offset> bytes."
msgstr ""
"Установить файловое смещение равным текущему положению плюс I<offset> (в "
"байтах)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SEEK_END>"
msgstr "B<SEEK_END>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file offset is set to the size of the file plus I<offset> bytes."
msgstr ""
"Установить файловое смещение равным размеру файла плюс I<offset> (в байтах)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<lseek>()  allows the file offset to be set beyond the end of the file "
#| "(but this does not change the size of the file).  If data is later "
#| "written at this point, subsequent reads of the data in the gap (a "
#| "\"hole\") return null bytes (\\(aq\\e0\\(aq) until data is actually "
#| "written into the gap."
msgid ""
"B<lseek>()  allows the file offset to be set beyond the end of the file (but "
"this does not change the size of the file).  If data is later written at "
"this point, subsequent reads of the data in the gap (a \"hole\") return null "
"bytes (\\[aq]\\[rs]0\\[aq]) until data is actually written into the gap."
msgstr ""
"Вызов B<lseek>() позволяет задавать смещение, которое будет находиться за "
"существующим концом файла (но это не изменяет размер файла). Если позднее по "
"этому смещению будут записаны данные, то последующее чтение в промежутке "
"(«дырке») от конца файла до этого смещения, будет возвращать нулевые байты "
"(\\(aq\\e0\\(aq), пока в этот промежуток действительно не будут записаны "
"данные."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Seeking file data and holes"
msgstr "Перемещения по данным файла и промежутки"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Since version 3.1, Linux supports the following additional values for "
#| "I<whence>:"
msgid ""
"Since Linux 3.1, Linux supports the following additional values for "
"I<whence>:"
msgstr ""
"Начиная с версии 3.1, в Linux поддерживаются следующие дополнительные "
"значения I<whence>:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SEEK_DATA>"
msgstr "B<SEEK_DATA>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Adjust the file offset to the next location in the file greater than or "
"equal to I<offset> containing data.  If I<offset> points to data, then the "
"file offset is set to I<offset>."
msgstr ""
"Подогнать файловое смещение к следующему расположению, большему или равному "
"значению I<offset>, по которому в файле есть данные. Если значение I<offset> "
"указывает на данные, то файловое смещение устанавливается в I<offset>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SEEK_HOLE>"
msgstr "B<SEEK_HOLE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Adjust the file offset to the next hole in the file greater than or equal to "
"I<offset>.  If I<offset> points into the middle of a hole, then the file "
"offset is set to I<offset>.  If there is no hole past I<offset>, then the "
"file offset is adjusted to the end of the file (i.e., there is an implicit "
"hole at the end of any file)."
msgstr ""
"Подогнать файловое смещение к следующему промежутку, большему или равному "
"значению I<offset>. Если значение I<offset> указывает в середину промежутка, "
"то файловое смещение устанавливается в I<offset>. Если перед I<offset> нет "
"промежутка, то файловое смещение подгоняется к концу файла (т.е., это "
"скрытый промежуток, который есть в конце любого файла)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In both of the above cases, B<lseek>()  fails if I<offset> points past the "
"end of the file."
msgstr ""
"В обоих, показанных выше, случаях, B<lseek>() завершится с ошибкой, если "
"I<offset> указывает за конец файла."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These operations allow applications to map holes in a sparsely allocated "
"file.  This can be useful for applications such as file backup tools, which "
"can save space when creating backups and preserve holes, if they have a "
"mechanism for discovering holes."
msgstr ""
"Эти операции позволяют приложениям отображать промежутки в разреженно "
"выделенном файле. Это может быть полезно для таких приложений, как "
"инструменты резервного копирования файлов, которые могут выиграть в месте "
"при создании резервных копий и сохранить промежутки, если у них есть "
"механизм их обнаружения."

#.  https://lkml.org/lkml/2011/4/22/79
#.  http://lwn.net/Articles/440255/
#.  http://blogs.oracle.com/bonwick/entry/seek_hole_and_seek_data
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For the purposes of these operations, a hole is a sequence of zeros that "
"(normally) has not been allocated in the underlying file storage.  However, "
"a filesystem is not obliged to report holes, so these operations are not a "
"guaranteed mechanism for mapping the storage space actually allocated to a "
"file.  (Furthermore, a sequence of zeros that actually has been written to "
"the underlying storage may not be reported as a hole.)  In the simplest "
"implementation, a filesystem can support the operations by making "
"B<SEEK_HOLE> always return the offset of the end of the file, and making "
"B<SEEK_DATA> always return I<offset> (i.e., even if the location referred to "
"by I<offset> is a hole, it can be considered to consist of data that is a "
"sequence of zeros)."
msgstr ""
"Для поддержки этих операций промежуток представляется последовательностью "
"нулей, которые (обычно) физически не занимают места на носителе. Однако "
"файловая система может не сообщать о промежутках, поэтому эти операции — не "
"гарантируемый механизм отображения пространства носителя в файл (более того, "
"последовательность нулей, которая на самом деле была записана на носитель, "
"может не посчитаться промежутком). В простейшей реализации, файловая система "
"может поддержать эти операции так: при B<SEEK_HOLE> всегда возвращать "
"смещение конца файла, а при B<SEEK_DATA> всегда возвращать значение "
"I<offset> (т.е., даже если расположение, указанное I<offset>, является "
"промежутком, это можно считать данными, состоящими из последовательности "
"нулей)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<_GNU_SOURCE> feature test macro must be defined in order to obtain the "
"definitions of B<SEEK_DATA> and B<SEEK_HOLE> from I<E<lt>unistd.hE<gt>>."
msgstr ""
"Чтобы получить определения B<SEEK_DATA> и B<SEEK_HOLE> из I<E<lt>unistd."
"hE<gt>>, нужно задать макрос тестирования свойств B<_GNU_SOURCE>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<SEEK_HOLE> and B<SEEK_DATA> operations are supported for the following "
"filesystems:"
msgstr ""
"Операции B<SEEK_HOLE> и B<SEEK_DATA> поддерживаются следующими файловыми "
"системами:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Btrfs (since Linux 3.1)"
msgstr "Btrfs (начиная с Linux 3.1)"

#.  commit 93862d5e1ab875664c6cc95254fc365028a48bb1
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "OCFS (since Linux 3.2)"
msgstr "OCFS (начиная с Linux 3.2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "XFS (since Linux 3.5)"
msgstr "XFS (начиная с Linux 3.5)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ext4 (since Linux 3.8)"
msgstr "ext4 (начиная с Linux 3.8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<tmpfs>(5)  (since Linux 3.8)"
msgstr "B<tmpfs>(5)  (начиная с Linux 3.8)"

#.  commit 1c6dcbe5ceff81c2cf8d929646af675cd59fe7c0
#.  commit 24bab491220faa446d945624086d838af41d616c
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "NFS (since Linux 3.18)"
msgstr "NFS (начиная с Linux 3.18)"

#.  commit 0b5da8db145bfd44266ac964a2636a0cf8d7c286
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "FUSE (since Linux 4.5)"
msgstr "FUSE (начиная с Linux 4.5)"

#.  commit 3a27411cb4bc3ce31db228e3569ad01b462a4310
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "GFS2 (since Linux 4.15)"
msgstr "GFS2 (начиная с Linux 4.15)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Upon successful completion, B<lseek>()  returns the resulting offset "
"location as measured in bytes from the beginning of the file.  On error, the "
"value I<(off_t)\\ -1> is returned and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<lseek>() возвращает получившееся в результате "
"смещение в байтах от начала файла. При ошибке возвращается значение "
"I<(off_t)\\ -1> и в I<errno> записывается код ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<fd> is not an open file descriptor."
msgstr "I<fd> не является открытым файловым дескриптором."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#.  Some systems may allow negative offsets for character devices
#.  and/or for remote filesystems.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<whence> is not valid.  Or: the resulting file offset would be negative, or "
"beyond the end of a seekable device."
msgstr ""
"Неправильное значение I<whence>. Получается, что возвращаемое файловое "
"смещение стало бы отрицательным или указывало бы за конец поверхности "
"носителя."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENXIO>"
msgstr "B<ENXIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<whence> is B<SEEK_DATA> or B<SEEK_HOLE>, and the file offset is beyond "
#| "the end of the file."
msgid ""
"I<whence> is B<SEEK_DATA> or B<SEEK_HOLE>, and I<offset> is beyond the end "
"of the file, or I<whence> is B<SEEK_DATA> and I<offset> is within a hole at "
"the end of the file."
msgstr ""
"Значение I<whence> равно B<SEEK_DATA> или B<SEEK_HOLE>, а файловое смещение "
"указывает за конец файла."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EOVERFLOW>"
msgstr "B<EOVERFLOW>"

#.  HP-UX 11 says EINVAL for this case (but POSIX.1 says EOVERFLOW)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The resulting file offset cannot be represented in an I<off_t>."
msgstr ""
"Результирующие файловое смещение не может быть представлено типом I<off_t>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ESPIPE>"
msgstr "B<ESPIPE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<fd> is associated with a pipe, socket, or FIFO."
msgstr "Значение I<fd> связано с каналом, сокетом или FIFO."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#.  Other systems return the number of written characters,
#.  using SEEK_SET to set the counter. (Of written characters.)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Linux, using B<lseek>()  on a terminal device fails with the error "
"B<ESPIPE>."
msgstr ""
"В Linux при использовании B<lseek>() на устройствах терминалов возвращается "
"ошибка B<ESPIPE>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#.  FIXME . Review http://austingroupbugs.net/view.php?id=415 in the future
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<SEEK_DATA> and B<SEEK_HOLE> are nonstandard extensions also present in "
"Solaris, FreeBSD, and DragonFly BSD; they are proposed for inclusion in the "
"next POSIX revision (Issue 8)."
msgstr ""
"Значения B<SEEK_DATA> и B<SEEK_HOLE> являются нестандартными расширениями, "
"которые также есть в Solaris, FreeBSD и DragonFly BSD; их предложили "
"включить в следующую редакцию POSIX (выпуск 8)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<open>(2)  for a discussion of the relationship between file "
"descriptors, open file descriptions, and files."
msgstr ""
"Описание взаимосвязи между файловыми дескрипторами, открытыми файловыми "
"описаниями и файлами смотрите в B<open>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the B<O_APPEND> file status flag is set on the open file description, "
"then a B<write>(2)  I<always> moves the file offset to the end of the file, "
"regardless of the use of B<lseek>()."
msgstr ""
"Если установлен флаг состояние файла B<O_APPEND> в открытом файловом "
"описании, то B<write>(2) I<всегда> перемещает файловое смещение в конец "
"файла, независимо от использования B<lseek>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some devices are incapable of seeking and POSIX does not specify which "
"devices must support B<lseek>()."
msgstr ""
"Некоторые устройства не могут выполнять смещения и в POSIX не указано какие "
"устройства должны поддерживать B<lseek>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<dup>(2), B<fallocate>(2), B<fork>(2), B<open>(2), B<fseek>(3), "
"B<lseek64>(3), B<posix_fallocate>(3)"
msgstr ""
"B<dup>(2), B<fallocate>(2), B<fork>(2), B<open>(2), B<fseek>(3), "
"B<lseek64>(3), B<posix_fallocate>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<lseek>()  allows the file offset to be set beyond the end of the file "
#| "(but this does not change the size of the file).  If data is later "
#| "written at this point, subsequent reads of the data in the gap (a "
#| "\"hole\") return null bytes (\\(aq\\e0\\(aq) until data is actually "
#| "written into the gap."
msgid ""
"B<lseek>()  allows the file offset to be set beyond the end of the file (but "
"this does not change the size of the file).  If data is later written at "
"this point, subsequent reads of the data in the gap (a \"hole\") return null "
"bytes (\\[aq]\\e0\\[aq]) until data is actually written into the gap."
msgstr ""
"Вызов B<lseek>() позволяет задавать смещение, которое будет находиться за "
"существующим концом файла (но это не изменяет размер файла). Если позднее по "
"этому смещению будут записаны данные, то последующее чтение в промежутке "
"(«дырке») от конца файла до этого смещения, будет возвращать нулевые байты "
"(\\(aq\\e0\\(aq), пока в этот промежуток действительно не будут записаны "
"данные."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<off_t> data type is a signed integer data type specified by POSIX.1."
msgstr ""
"Тип данных I<off_t> представляет собой знаковый целочисленный тип, определён "
"в POSIX.1."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
