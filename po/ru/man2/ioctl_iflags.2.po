# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:01+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "ioctl_iflags"
msgstr "ioctl_iflags"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "ioctl_iflags - ioctl() operations for inode flags"
msgstr "ioctl_iflags - операции ioctl() для флагов иноды"

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "Various Linux filesystems support the notion of I<inode "
#| "flags>\\(emattributes that modify the semantics of files and "
#| "directories.  These flags can be retrieved and modified using two "
#| "B<ioctl>(2)  operations:"
msgid ""
"Various Linux filesystems support the notion of I<inode "
"flags>\\[em]attributes that modify the semantics of files and directories.  "
"These flags can be retrieved and modified using two B<ioctl>(2)  operations:"
msgstr ""
"Различные файловые системы Linux поддерживают I<флаги иноды> — атрибуты, "
"которые изменяют смысл файлов и каталогов. Эти флаги можно запросить и "
"изменить с помощью двух операций B<ioctl>(2):"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int attr;\n"
"fd = open(\"pathname\", ...);\n"
msgstr ""
"int attr;\n"
"fd = open(\"pathname\", ...);\n"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid ""
#| "ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* Place current flags\n"
#| "                                       in \\(aqattr\\(aq */\n"
#| "attr |= FS_NOATIME_FL;              /* Tweak returned bit mask */\n"
#| "ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* Update flags for inode\n"
#| "                                       referred to by \\(aqfd\\(aq */\n"
msgid ""
"ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* Place current flags\n"
"                                       in \\[aq]attr\\[aq] */\n"
"attr |= FS_NOATIME_FL;              /* Tweak returned bit mask */\n"
"ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* Update flags for inode\n"
"                                       referred to by \\[aq]fd\\[aq] */\n"
msgstr ""
"ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* помещает текущие флаги\n"
"                                       в \\(aqattr\\(aq */\n"
"attr |= FS_NOATIME_FL;              /* изменяет полученную битовую маску */\n"
"ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* обновляет флаги иноды,\n"
"                                       на которую указывает \\(aqfd\\(aq */\n"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The B<lsattr>(1)  and B<chattr>(1)  shell commands provide interfaces to "
"these two operations, allowing a user to view and modify the inode flags "
"associated with a file."
msgstr ""
"Команды оболочки B<lsattr>(1) и B<chattr>(1) предоставляют интерфейс к этим "
"двум операциям, позволяя пользователю просматривать и изменять флаги иноды, "
"связанной с файлом."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The following flags are supported (shown along with the corresponding letter "
"used to indicate the flag by B<lsattr>(1)  and B<chattr>(1)):"
msgstr ""
"Поддерживаются следующие флаги (показаны вместе с соответствующей буквой, "
"которой обозначается флаг в B<lsattr>(1) и B<chattr>(1)):"

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_APPEND_FL> \\(aqa\\(aq"
msgid "B<FS_APPEND_FL> \\[aq]a\\[aq]"
msgstr "B<FS_APPEND_FL> \\(aqa\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The file can be opened only with the B<O_APPEND> flag.  (This restriction "
"applies even to the superuser.)  Only a privileged process "
"(B<CAP_LINUX_IMMUTABLE>)  can set or clear this attribute."
msgstr ""
"Файл можно открыть только с флагом B<O_APPEND> (это ограничение не обойти "
"даже суперпользователю). Только привилегированный процесс "
"(B<CAP_LINUX_IMMUTABLE>) может установить или сбросить этот атрибут."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_COMPR_FL> \\(aqc\\(aq"
msgid "B<FS_COMPR_FL> \\[aq]c\\[aq]"
msgstr "B<FS_COMPR_FL> \\(aqc\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Store the file in a compressed format on disk.  This flag is I<not> "
"supported by most of the mainstream filesystem implementations; one "
"exception is B<btrfs>(5)."
msgstr ""
"Хранить файл на диске в сжатом виде. Этот флаг I<не> поддерживается "
"реализациями большинства основных файловых систем; исключением является "
"B<btrfs>(5)."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_DIRSYNC_FL> \\(aqD\\(aq (since Linux 2.6.0)"
msgid "B<FS_DIRSYNC_FL> \\[aq]D\\[aq] (since Linux 2.6.0)"
msgstr "B<FS_DIRSYNC_FL> \\(aqD\\(aq (начиная с Linux 2.6.0)"

#.  .TP
#.  .BR FS_EXTENT_FL " \[aq]e\[aq]"
#.  FIXME Some support on ext4? (EXT4_EXTENTS_FL)
#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Write directory changes synchronously to disk.  This flag provides semantics "
"equivalent to the B<mount>(2)  B<MS_DIRSYNC> option, but on a per-directory "
"basis.  This flag can be applied only to directories."
msgstr ""
"Синхронно записывать изменения каталога на диск. Этот флаг предоставляет "
"эквивалент параметра B<MS_DIRSYNC> вызова B<mount>(2), но для каталога. Этот "
"флаг применим только для каталогов."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_IMMUTABLE_FL> \\(aqi\\(aq"
msgid "B<FS_IMMUTABLE_FL> \\[aq]i\\[aq]"
msgstr "B<FS_IMMUTABLE_FL> \\(aqi\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The file is immutable: no changes are permitted to the file contents or "
"metadata (permissions, timestamps, ownership, link count, and so on).  (This "
"restriction applies even to the superuser.)  Only a privileged process "
"(B<CAP_LINUX_IMMUTABLE>)  can set or clear this attribute."
msgstr ""
"Файл постоянный (immutable): не допускает изменение содержимого и метаданных "
"(прав, меток времени, владельцев, счётчика ссылок и т. п.) (это ограничение "
"не обойти даже суперпользователю). Только привилегированный процесс "
"(B<CAP_LINUX_IMMUTABLE>) может установить или сбросить этот атрибут."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_JOURNAL_DATA_FL> \\(aqj\\(aq"
msgid "B<FS_JOURNAL_DATA_FL> \\[aq]j\\[aq]"
msgstr "B<FS_JOURNAL_DATA_FL> \\(aqj\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Enable journaling of file data on B<ext3>(5)  and B<ext4>(5)  filesystems.  "
"On a filesystem that is journaling in I<ordered> or I<writeback> mode, a "
"privileged (B<CAP_SYS_RESOURCE>)  process can set this flag to enable "
"journaling of data updates on a per-file basis."
msgstr ""
"Включение журналирования данных файла в файловых системах B<ext3>(5) и "
"B<ext4>(5). В файловой системе с журналиванием в режиме I<ordered> или "
"I<writeback> привилегированный (B<CAP_SYS_RESOURCE>) процесс может "
"установить этот флаг для включения журналирования изменения данных пофайлово."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_NOATIME_FL> \\(aqA\\(aq"
msgid "B<FS_NOATIME_FL> \\[aq]A\\[aq]"
msgstr "B<FS_NOATIME_FL> \\(aqA\\(aq"

#.  .TP
#.  .BR FS_NOCOMP_FL " \[aq]\[aq]"
#.  FIXME Support for FS_NOCOMP_FL on Btrfs?
#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Don't update the file last access time when the file is accessed.  This can "
"provide I/O performance benefits for applications that do not care about the "
"accuracy of this timestamp.  This flag provides functionality similar to the "
"B<mount>(2)  B<MS_NOATIME> flag, but on a per-file basis."
msgstr ""
"Не изменять время последнего доступа к файлу при доступе. Это может "
"увеличить произвольность ввода-вывода в приложениях, которым не нужна "
"точность этой метки времени. Данный флаг предоставляет функциональность "
"флага B<MS_NOATIME> вызова B<mount>(2), но пофайлово."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_NOCOW_FL> \\(aqC\\(aq (since Linux 2.6.39)"
msgid "B<FS_NOCOW_FL> \\[aq]C\\[aq] (since Linux 2.6.39)"
msgstr "B<FS_NOCOW_FL> \\(aqC\\(aq (начиная с Linux 2.6.39)"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The file will not be subject to copy-on-write updates.  This flag has an "
"effect only on filesystems that support copy-on-write semantics, such as "
"Btrfs.  See B<chattr>(1)  and B<btrfs>(5)."
msgstr ""
"Файл не будет подвергаться обновлениям копирования-при-записи. Этот флаг "
"работает только в файловых системах с поддержкой копирования-при-записи, "
"таких как Btrfs. Смотрите B<chattr>(1) и B<btrfs>(5)."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_NODUMP_FL> \\(aqd\\(aq"
msgid "B<FS_NODUMP_FL> \\[aq]d\\[aq]"
msgstr "B<FS_NODUMP_FL> \\(aqd\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Don't include this file in backups made using B<dump>(8)."
msgstr "Не включать этот файл в резервную копию, создаваемую B<dump>(8)."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_NOTAIL_FL> \\(aqt\\(aq"
msgid "B<FS_NOTAIL_FL> \\[aq]t\\[aq]"
msgstr "B<FS_NOTAIL_FL> \\(aqt\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"This flag is supported only on Reiserfs.  It disables the Reiserfs tail-"
"packing feature, which tries to pack small files (and the final fragment of "
"larger files)  into the same disk block as the file metadata."
msgstr ""
"Этот флаг поддерживается только в Reiserfs. Он отключает свойство Reiserfs "
"упаковки хвостов, которая пытается упаковывать маленькие файлы (и конечные "
"фрагменты больших файлов) в в дисковые блоки метаданных файла."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_PROJINHERIT_FL> \\(aqP\\(aq (since Linux 4.5)"
msgid "B<FS_PROJINHERIT_FL> \\[aq]P\\[aq] (since Linux 4.5)"
msgstr "B<FS_PROJINHERIT_FL> \\(aqP\\(aq (начиная с Linux 4.5)"

#.  commit 040cb3786d9b25293b8b0b05b90da0f871e1eb9b
#.  Flag name was added in Linux 4.4
#.  FIXME Not currently supported because not in FS_FL_USER_MODIFIABLE?
#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Inherit the quota project ID.  Files and subdirectories will inherit the "
"project ID of the directory.  This flag can be applied only to directories."
msgstr ""
"Наследовать ID квоты проекта. Файлы и подкаталоги будут наследовать ID "
"проекта каталога. Этот флаг можно применять только для каталогов."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_SECRM_FL> \\(aqs\\(aq"
msgid "B<FS_SECRM_FL> \\[aq]s\\[aq]"
msgstr "B<FS_SECRM_FL> \\(aqs\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Mark the file for secure deletion.  This feature is not implemented by any "
"filesystem, since the task of securely erasing a file from a recording "
"medium is surprisingly difficult."
msgstr ""
"Помечать файл для безопасного удаления. Это свойство не реализовано во всех "
"файловых системах, так как задача безопасного стирания файла с хранящего "
"носителя на удивление сложна."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_SYNC_FL> \\(aqS\\(aq"
msgid "B<FS_SYNC_FL> \\[aq]S\\[aq]"
msgstr "B<FS_SYNC_FL> \\(aqS\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Make file updates synchronous.  For files, this makes all writes synchronous "
"(as though all opens of the file were with the B<O_SYNC> flag).  For "
"directories, this has the same effect as the B<FS_DIRSYNC_FL> flag."
msgstr ""
"Выполнять обновления файла синхронно. Для файлов, это приводит к выполнению "
"синхронной записи (как если бы все файлы открывались с флагом B<O_SYNC>). "
"Для каталогов, это приводит к тому эффекту что и флаг B<FS_DIRSYNC_FL>."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_TOPDIR_FL> \\(aqT\\(aq"
msgid "B<FS_TOPDIR_FL> \\[aq]T\\[aq]"
msgstr "B<FS_TOPDIR_FL> \\(aqT\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Mark a directory for special treatment under the Orlov block-allocation "
"strategy.  See B<chattr>(1)  for details.  This flag can be applied only to "
"directories and has an effect only for ext2, ext3, and ext4."
msgstr ""
"Пометить каталог для специальной обработки по стратегии выделения блоков "
"Орлова. Подробности смотрите в B<chattr>(1). Этот флаг можно применять "
"только к каталогам и он действует только в ext2, ext3 и ext4."

#. type: TP
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<FS_UNRM_FL> \\(aqu\\(aq"
msgid "B<FS_UNRM_FL> \\[aq]u\\[aq]"
msgstr "B<FS_UNRM_FL> \\(aqu\\(aq"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Allow the file to be undeleted if it is deleted.  This feature is not "
"implemented by any filesystem, since it is possible to implement file-"
"recovery mechanisms outside the kernel."
msgstr ""
"Разрешить файлу быть не удалённым, если он удалён. Это свойство реализовано "
"во всех файловых системах, так как возможно реализовать механизм "
"восстановления файла вне ядра."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"In most cases, when any of the above flags is set on a directory, the flag "
"is inherited by files and subdirectories created inside that directory.  "
"Exceptions include B<FS_TOPDIR_FL>, which is not inheritable, and "
"B<FS_DIRSYNC_FL>, which is inherited only by subdirectories."
msgstr ""
"В большинстве случаев, когда какой-либо из флагов установлен на каталоге, "
"этот флаг наследуется  файлами и подкаталогами, созданными внутри каталога. "
"Исключением является B<FS_TOPDIR_FL>, который не наследуется и "
"B<FS_DIRSYNC_FL>, который наследуется только подкаталогами."

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: debian-bookworm
msgid "Inode flags are a nonstandard Linux extension."
msgstr "Флаги иноды являются нестандартным расширением Linux."

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"In order to change the inode flags of a file using the B<FS_IOC_SETFLAGS> "
"operation, the effective user ID of the caller must match the owner of the "
"file, or the caller must have the B<CAP_FOWNER> capability."
msgstr ""
"Для изменения флагов иноды файла используется операция B<FS_IOC_SETFLAGS>; "
"эффективный идентификатор пользователя вызывающего должен совпадать с "
"владельцем файла или вызывающий должен иметь мандат B<CAP_FOWNER>."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The type of the argument given to the B<FS_IOC_GETFLAGS> and "
"B<FS_IOC_SETFLAGS> operations is I<int\\ *>, notwithstanding the implication "
"in the kernel source file I<include/uapi/linux/fs.h> that the argument is "
"I<long\\ *>."
msgstr ""

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"B<chattr>(1), B<lsattr>(1), B<mount>(2), B<btrfs>(5), B<ext4>(5), B<xfs>(5), "
"B<xattr>(7), B<mount>(8)"
msgstr ""
"B<chattr>(1), B<lsattr>(1), B<mount>(2), B<btrfs>(5), B<ext4>(5), B<xfs>(5), "
"B<xattr>(7), B<mount>(8)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid ""
#| "ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* Place current flags\n"
#| "                                       in \\(aqattr\\(aq */\n"
#| "attr |= FS_NOATIME_FL;              /* Tweak returned bit mask */\n"
#| "ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* Update flags for inode\n"
#| "                                       referred to by \\(aqfd\\(aq */\n"
msgid ""
"int attr;\n"
"fd = open(\"pathname\", ...);\n"
"\\&\n"
"ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* Place current flags\n"
"                                       in \\[aq]attr\\[aq] */\n"
"attr |= FS_NOATIME_FL;              /* Tweak returned bit mask */\n"
"ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* Update flags for inode\n"
"                                       referred to by \\[aq]fd\\[aq] */\n"
msgstr ""
"ioctl(fd, FS_IOC_GETFLAGS, &attr);  /* помещает текущие флаги\n"
"                                       в \\(aqattr\\(aq */\n"
"attr |= FS_NOATIME_FL;              /* изменяет полученную битовую маску */\n"
"ioctl(fd, FS_IOC_SETFLAGS, &attr);  /* обновляет флаги иноды,\n"
"                                       на которую указывает \\(aqfd\\(aq */\n"

#. type: Plain text
#: mageia-cauldron
msgid "Linux."
msgstr "Linux."
