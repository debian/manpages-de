# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:57+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "execveat"
msgstr "execveat"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "execveat - execute program relative to a directory file descriptor"
msgstr ""
"execveat - выполняет программу, определяемую относительно файлового "
"дескриптора каталога"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/fcntl.hE<gt>>      /* Definition of B<AT_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/fcntl.hE<gt>>      /* определения констант B<AT_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int execveat(int >I<dirfd>B<, const char *>I<pathname>B<,>\n"
"B<             char *const _Nullable >I<argv>B<[],>\n"
"B<             char *const _Nullable >I<envp>B<[],>\n"
"B<             int >I<flags>B<);>\n"
msgstr ""
"B<int execveat(int >I<dirfd>B<, const char *>I<pathname>B<,>\n"
"B<             char *const _Nullable >I<argv>B<[],>\n"
"B<             char *const _Nullable >I<envp>B<[],>\n"
"B<             int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#.  commit 51f39a1f0cea1cacf8c787f652f26dfee9611874
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<execveat>()  system call executes the program referred to by the "
"combination of I<dirfd> and I<pathname>.  It operates in exactly the same "
"way as B<execve>(2), except for the differences described in this manual "
"page."
msgstr ""
"Системный вызов B<execveat>() выполняет программу, на которую ссылается "
"комбинация I<dirfd> и I<pathname>. Он работает также как системный вызов "
"B<execve>(2), за исключением случаев, описанных в данной справочной странице."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<execve>(2)  for a relative pathname)."
msgstr ""
"Если в I<pathname> задан относительный путь, то он считается относительно "
"каталога, на который ссылается файловый дескриптор I<dirfd> (а не "
"относительно текущего рабочего каталога вызывающего процесса, как это "
"делается в B<execve>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pathname> is relative and I<dirfd> is the special value B<AT_FDCWD>, "
"then I<pathname> is interpreted relative to the current working directory of "
"the calling process (like B<execve>(2))."
msgstr ""
"Если в I<pathname> задан относительный путь и I<dirfd> равно специальному "
"значению B<AT_FDCWD>, то I<pathname> рассматривается относительно текущего "
"рабочего каталога вызывающего процесса (как B<execve>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<pathname> is absolute, then I<dirfd> is ignored."
msgstr "Если в I<pathname> задан абсолютный путь, то I<dirfd> игнорируется."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pathname> is an empty string and the B<AT_EMPTY_PATH> flag is "
"specified, then the file descriptor I<dirfd> specifies the file to be "
"executed (i.e., I<dirfd> refers to an executable file, rather than a "
"directory)."
msgstr ""
"Если I<pathname> — пустая строка и указан флаг B<AT_EMPTY_PATH>, то файловым "
"дескриптором I<dirfd> задаётся выполняемый файл (т. е., I<dirfd> ссылается "
"на исполняемый файл, а не на каталог)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<flags> argument is a bit mask that can include zero or more of the "
"following flags:"
msgstr ""
"Аргумент I<flags> является битовой маской, которая может включать ноль или "
"более следующих флагов:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<AT_EMPTY_PATH>"
msgstr "B<AT_EMPTY_PATH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<pathname> is an empty string, operate on the file referred to by "
"I<dirfd> (which may have been obtained using the B<open>(2)  B<O_PATH> flag)."
msgstr ""
"Если значение I<pathname> равно пустой строке, то вызов выполняет действие с "
"файлом, на который ссылается I<dirfd> (может быть получен с помощью "
"B<open>(2) с флагом B<O_PATH>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<AT_SYMLINK_NOFOLLOW>"
msgstr "B<AT_SYMLINK_NOFOLLOW>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the file identified by I<dirfd> and a non-NULL I<pathname> is a symbolic "
"link, then the call fails with the error B<ELOOP>."
msgstr ""
"Если файл задаётся I<dirfd> и I<pathname> — символическая ссылка (не NULL), "
"то вызов завершается с ошибкой B<ELOOP>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<execveat>()  does not return.  On error, -1 is returned, "
#| "and I<errno> is set appropriately."
msgid ""
"On success, B<execveat>()  does not return.  On error, -1 is returned, and "
"I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<execveat>() не возвращает управление. В случае "
"ошибки возвращается -1, а I<errno> устанавливается в соответствующее "
"значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The same errors that occur for B<execve>(2)  can also occur for "
"B<execveat>().  The following additional errors can occur for B<execveat>():"
msgstr ""
"В B<execveat>() могут возникнуть те же ошибки, что и в B<execve>(). Также, в "
"B<execveat>() могут возникнуть следующие ошибки:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<pathname>"
msgstr "I<pathname>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is relative but I<dirfd> is neither B<AT_FDCWD> nor a valid file descriptor."
msgstr ""
"является относительным, но I<dirfd> не является ни B<AT_FDCWD>, ни "
"допустимым файловым дескриптором."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Invalid flag specified in I<flags>."
msgstr "Указано неверное значение в I<flags>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<flags> includes B<AT_SYMLINK_NOFOLLOW> and the file identified by I<dirfd> "
"and a non-NULL I<pathname> is a symbolic link."
msgstr ""
"Значение I<flags> содержит B<AT_SYMLINK_NOFOLLOW> и файл задаётся I<dirfd>, "
"а I<pathname> — символическая ссылка (не NULL)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program identified by I<dirfd> and I<pathname> requires the use of an "
"interpreter program (such as a script starting with \"#!\"), but the file "
"descriptor I<dirfd> was opened with the B<O_CLOEXEC> flag, with the result "
"that the program file is inaccessible to the launched interpreter.  See BUGS."
msgstr ""
"Программа задаётся I<dirfd> и по I<pathname> требуется использовать "
"интерпретирующую программу (то есть сценарий, начинающийся с «#!»), но "
"файловый дескриптор I<dirfd> открыт с флагом B<O_CLOEXEC>, что приводит к "
"недоступности файла программы запускаемому интерпретатору. Смотрите ДЕФЕКТЫ."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<pathname> is relative and I<dirfd> is a file descriptor referring to a "
"file other than a directory."
msgstr ""
"Значение I<pathname> содержит относительный путь и I<dirfd> содержит "
"файловый дескриптор, указывающий на файл, а не на каталог."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 3.19, glibc 2.34."
msgstr "Linux 3.19, glibc 2.34."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In addition to the reasons explained in B<openat>(2), the B<execveat>()  "
"system call is also needed to allow B<fexecve>(3)  to be implemented on "
"systems that do not have the I</proc> filesystem mounted."
msgstr ""
"В дополнении к причинам, описанным в B<openat>(2), системному вызову "
"B<execveat>() также требуется разрешить B<fexecve>(3) для реализации в "
"системах, у которых не смонтированной файловой системы I</proc>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When asked to execute a script file, the I<argv[0]> that is passed to the "
"script interpreter is a string of the form I</dev/fd/N> or I</dev/fd/N/P>, "
"where I<N> is the number of the file descriptor passed via the I<dirfd> "
"argument.  A string of the first form occurs when B<AT_EMPTY_PATH> is "
"employed.  A string of the second form occurs when the script is specified "
"via both I<dirfd> and I<pathname>; in this case, I<P> is the value given in "
"I<pathname>."
msgstr ""
"При запросе запуска файла сценария, значение I<argv[0]>, передаваемое в "
"интерпретатор сценарий, является строкой в виде I</dev/fd/N> или I</dev/fd/N/"
"P>, где I<N> — номер файлового дескриптора, передаваемого через аргумент "
"I<dirfd>. Строка в первом формате встречается, когда указан "
"B<AT_EMPTY_PATH>. Строка во втором формате встречается, когда сценарий "
"задаётся сразу через I<dirfd> и I<pathname>; в этом случае I<P> — это "
"значение, указанное в I<pathname>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For the same reasons described in B<fexecve>(3), the natural idiom when "
"using B<execveat>()  is to set the close-on-exec flag on I<dirfd>.  (But see "
"BUGS.)"
msgstr ""
"По причинам, описанным в B<fexecve>(3), естественным подходом является "
"использование B<execveat>() с установленным флагом close-on-exec у I<dirfd> "
"(но смотрите ДЕФЕКТЫ)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<ENOENT> error described above means that it is not possible to set the "
"close-on-exec flag on the file descriptor given to a call of the form:"
msgstr ""
"Ошибка B<ENOENT>, описанная выше, означает, что невозможно установить флаг "
"close-on-exec у файлового дескриптора, переданного вызову в виде:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "execveat(fd, \"\", argv, envp, AT_EMPTY_PATH);\n"
msgstr "execveat(fd, \"\", argv, envp, AT_EMPTY_PATH);\n"

#.  For an example, see Michael Kerrisk's 2015-01-10 reply in this LKML
#.  thread (http://thread.gmane.org/gmane.linux.kernel/1836105/focus=20229):
#.      Subject: [PATCHv10 man-pages 5/5] execveat.2: initial man page.\"                        for execveat(2
#.      Date: Mon, 24 Nov 2014 11:53:59 +0000
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"However, the inability to set the close-on-exec flag means that a file "
"descriptor referring to the script leaks through to the script itself.  As "
"well as wasting a file descriptor, this leakage can lead to file-descriptor "
"exhaustion in scenarios where scripts recursively employ B<execveat>()."
msgstr ""
"Однако неспособность установить флаг close-on-exec означает утечку файловых "
"дескрипторов, через ссылку сценария на самого себя. Помимо траты файлового "
"дескриптора, это может привести к исчерпанию файловых дескрипторов, если "
"сценарии рекурсивно вызывают B<execveat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<execve>(2), B<openat>(2), B<fexecve>(3)"
msgstr "B<execve>(2), B<openat>(2), B<fexecve>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-02"
msgstr "2 января 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<execveat>()  was added in Linux 3.19.  Library support was added in glibc "
"2.34."
msgstr ""
"B<execveat>() был добавлен в Linux 3.19. Поддержка библиотеки была добавлен "
"в glibc 2.34."

#. type: Plain text
#: debian-bookworm
msgid "The B<execveat>()  system call is Linux-specific."
msgstr "Системный вызов B<execveat>() есть только в Linux."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
