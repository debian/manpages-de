# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-10-01 01:30-0300\n"
"PO-Revision-Date: 2024-11-06 01:30+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.4\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "CHECKPKG"
msgstr "CHECKPKG"

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-06-18"
msgstr "18 iunie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr "\\ \" "

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr "\\ \""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid ""
"checkpkg - Compare the current build package with the repository version"
msgstr ""
"checkpkg - compară pachetul de compilare curent cu versiunea din depozit"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux
msgid "checkpkg"
msgstr "checkpkg"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid ""
"Searches for a locally built package corresponding to the PKGBUILD, and "
"downloads the last version of that package from the Pacman repositories. It "
"then compares the list of .so files provided by each version of the package "
"and outputs if there are soname differences for the new package. A directory "
"is also created using mktemp with files containing a file list for both "
"packages and a library list for both packages."
msgstr ""
"Caută un pachet construit local corespunzător PKGBUILD și descarcă ultima "
"versiune a acelui pachet din depozitele Pacman. Apoi compară lista de "
"fișiere .so furnizate de fiecare versiune a pachetului și iese dacă există "
"diferențe de soname pentru noul pachet. De asemenea, se creează un director "
"utilizând mktemp cu fișiere care conțin o listă de fișiere pentru ambele "
"pachete și o listă de biblioteci pentru ambele pachete."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: archlinux
msgid "B<-r, --rmdir>"
msgstr "B<-r, --rmdir>"

#. type: Plain text
#: archlinux
msgid ""
"Remove the temporary directory created to contain the file and library list "
"of both packages."
msgstr ""
"Elimină directorul temporar creat pentru a conține lista de fișiere și "
"biblioteci a ambelor pachete."

#. type: Plain text
#: archlinux
msgid "B<-w, --warn>"
msgstr "B<-w, --warn>"

#. type: Plain text
#: archlinux
msgid ""
"Print a warning instead of a regular message in case of soname differences."
msgstr ""
"Afișează un avertisment în loc de un mesaj obișnuit în cazul diferențelor de "
"nume de fișier."

#. type: Plain text
#: archlinux
msgid "B<-M, --makepkg-config>"
msgstr "B<-M, --makepkg-config>"

#. type: Plain text
#: archlinux
msgid "Set an alternate makepkg configuration file."
msgstr "Definește un fișier alternativ de configurare makepkg."

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: archlinux
msgid "Show a help text"
msgstr "Afișează mesajul de ajutor"

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux
msgid "find-libprovides(1)"
msgstr "find-libprovides(1)"

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr "PAGINA WEB PRINCIPALĂ"

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
"I<Vă rugăm să raportați erorile și solicitările de caracteristici în "
"sistemul de urmărire a problemelor. Vă rugăm să faceți tot posibilul pentru "
"a furniza un caz de testare reproductibil pentru erori.>"
