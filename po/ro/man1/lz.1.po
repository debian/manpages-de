# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-06-27 19:36+0200\n"
"PO-Revision-Date: 2023-05-19 23:34+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "LZ"
msgstr "LZ"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Wed Feb 23 00:00:00 EET 2000"
msgstr "miercuri, 23 februarie 2000 00:00:00 Ora Europei de Est"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Mtools Users Manual"
msgstr "Manualul utilizatorului Mtools"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "lz - gunzips and shows a listing of a gzip'd tar'd archive"
msgstr ""
"lz - dezarhivează și afișează o listă a conținutului unei arhive tar "
"comprimate cu gzip"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#.  The command line
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<lz> I<file>"
msgstr "B<lz> I<fișier>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<lz> provides a listing of a gzip'd tar'd archive, that is a B<tar>(1) "
"archive compressed with the B<gzip>(1) utility.  It is not strictly "
"necessary on Debian GNU/Linux, because the GNU B<tar>(1) program provides "
"the same capability with the command"
msgstr ""
"B<lz> oferă o listă a unei arhive tar comprimate cu gzip, adică o arhivă "
"B<tar>(1) comprimată cu ajutorul utilitarului B<gzip>(1).  Nu este strict "
"necesar pe Debian GNU/Linux, deoarece programul GNU B<tar>(1) oferă aceeași "
"capacitate cu ajutorul comenzii"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<tar -tzf> I<file>"
msgstr "B<tar -tzf> I<fișier>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"but this utility is provided in the mtools package for other platforms and "
"is retained here for completeness."
msgstr ""
"B<lz> oferă o listă a unei arhive tar comprimate cu gzip, adică o arhivă "
"B<tar>(1) comprimată cu ajutorul utilitarului B<gzip>(1).  Nu este strict "
"necesar pe Debian GNU/Linux, deoarece programul GNU B<tar>(1) oferă aceeași "
"capacitate cu ajutorul comenzii miauu, dar această utilitate este furnizată "
"în pachetul mtools pentru alte platforme și este păstrată aici pentru ca "
"acesta să fie complet."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Robert King (Robert.King@mailbox.gu.edu.au) wrote this page for the I<Debian/"
"GNU> mtools package."
msgstr ""
"Robert King (Robert.King@mailbox.gu.edu.au) a scris această pagină pentru "
"pachetul I<Debian/GNU> mtools."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<mtools>(1), B<gzip>(1), B<tar>(1), B<uz>(1)."
msgstr "B<mtools>(1), B<gzip>(1), B<tar>(1), B<uz>(1)."
