# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-10-01 02:55-0300\n"
"PO-Revision-Date: 2024-11-06 10:48+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.4\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "MKARCHROOT"
msgstr "MKARCHROOT"

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-06-18"
msgstr "18 iunie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr "\\ \" "

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr "\\ \""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid ""
"mkarchroot - Creates an arch chroot in a specified location with a specified "
"set of packages"
msgstr ""
"mkarchroot - creează un chroot-archlinux într-o locație specificată cu un "
"set specificat de pachete"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux
msgid "mkarchroot [options] [location] [packages]"
msgstr "mkarchroot [opțiuni] [locație] [pachete]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid ""
"I<mkarchroot> is a script to create an Arch Linux chroot at a specified "
"location with specified packages. Typically used by I<makechrootpkg> to "
"create build chroots. Apart from installing specified packages the chroot is "
"created with an en_US.UTF-8 and de_DE.UTF-8 locale and a generated machine-"
"id."
msgstr ""
"I<mkarchroot> este un script pentru a crea un chroot Arch Linux într-o "
"locație specificată cu pachetele specificate. Utilizat de obicei de "
"I<makechrootpkg> pentru a crea chroots de compilare. În afară de instalarea "
"pachetelor specificate, chroot-ul este creat cu o configurație regională "
"en_US.UTF-8 și de_DE.UTF-8 și un ID de mașină generat."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: archlinux
msgid "B<-U>"
msgstr "B<-U>"

#. type: Plain text
#: archlinux
msgid "Use I<pacman -U> to install packages."
msgstr "Utilizează I<pacman -U> pentru a instala pachete."

#. type: Plain text
#: archlinux
msgid "B<-C> E<lt>fileE<gt>"
msgstr "B<-C> E<lt>fișierE<gt>"

#. type: Plain text
#: archlinux
msgid "Location of a pacman config file."
msgstr "Locația unui fișier de configurare pacman."

#. type: Plain text
#: archlinux
msgid "B<-M> E<lt>fileE<gt>"
msgstr "B<-M> E<lt>fișierE<gt>"

#. type: Plain text
#: archlinux
msgid "Location of a makepkg config file."
msgstr "Locația unui fișier de configurare makepkg."

#. type: Plain text
#: archlinux
msgid "B<-c> E<lt>dirE<gt>"
msgstr "B<-c> E<lt>directorE<gt>"

#. type: Plain text
#: archlinux
msgid "Set pacman cache."
msgstr "Stabilește cache-ul pacman."

#. type: Plain text
#: archlinux
msgid "B<-f> E<lt>srcE<gt>[:E<lt>dstE<gt>]"
msgstr "B<-f> E<lt>sursaE<gt>[:E<lt>destinațiaE<gt>]"

#. type: Plain text
#: archlinux
msgid ""
"Copy file from the host to the chroot.  If I<dst> is not provided, it "
"defaults to I<src> inside of the chroot."
msgstr ""
"Copiază fișierul din gazdă în chroot. În cazul în care I<destinația> nu este "
"furnizată, se va folosi ca valoare implicită I<sursa> din interiorul chroot-"
"ului."

#. type: Plain text
#: archlinux
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: archlinux
msgid "Do not run setarch."
msgstr "Nu rulează B<setarch>."

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux
msgid "Output command line options."
msgstr "Afișează opțiunile liniei de comandă\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux
msgid "pacman(1)"
msgstr "pacman(1)"

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr "PAGINA WEB PRINCIPALĂ"

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
"I<Vă rugăm să raportați erorile și solicitările de caracteristici în "
"sistemul de urmărire a problemelor. Vă rugăm să faceți tot posibilul pentru "
"a furniza un caz de testare reproductibil pentru erori.>"
