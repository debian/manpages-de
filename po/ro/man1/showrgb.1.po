# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-11-03 07:03+0100\n"
"PO-Revision-Date: 2023-06-28 12:17+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SHOWRGB"
msgstr "SHOWRGB"

#. type: TH
#: debian-bookworm fedora-41
#, no-wrap
msgid "rgb 1.0.6"
msgstr "rgb 1.0.6"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "X Version 11"
msgstr "X Versiunea 11"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "showrgb - display an rgb color-name database"
msgstr "showrgb - afișează o bază de date cu nume de culori rgb"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<showrgb> [ I<database> ]"
msgstr "B<showrgb> [ I<baza-de-date> ]"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<showrgb> program reads an rgb color-name database from a text file and "
"converts it back to source form, printing the result to standard output.  "
"The default database is the one that X was built with, and may be overridden "
"on the command line.  Specify the database name without the I<.txt>, I<.pag> "
"or I<.dir> suffix."
msgstr ""
"Programul I<showrgb> citește o bază de date cu nume de culori rgb dintr-un "
"fișier text și o convertește înapoi în format sursă, afișând rezultatul la "
"ieșirea standard.  Baza de date implicită este cea cu care a fost construit "
"mediul X și poate fi înlocuită în linia de comandă.  Specificați numele "
"bazei de date fără sufixul I<.txt>, I<.pag> sau I<.dir>."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/X11/rgb>"
msgstr "I</usr/share/X11/rgb>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "default database."
msgstr "baza de date implicită."

#. type: TH
#: debian-unstable fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "rgb 1.1.0"
msgstr "rgb 1.1.0"
