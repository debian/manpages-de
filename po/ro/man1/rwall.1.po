# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-09-06 18:26+0200\n"
"PO-Revision-Date: 2023-07-12 11:48+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: Dd
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "August 15, 1999"
msgstr "15 august 1999"

#. type: Dt
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "RWALL 1"
msgstr "RWALL 1"

#. type: Os
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux NetKit (0.17)"
msgstr "Linux NetKit (0.17)"

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "E<.Nm rwall>"
msgstr "E<.Nm rwall>"

#. type: Nd
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "send a message to users logged on a host"
msgstr "trimite un mesaj către utilizatorii conectați la o gazdă"

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm rwall> E<.Ar host> E<.Op Ar file>"
msgstr "E<.Nm rwall> E<.Ar gazda> E<.Op Ar fișier>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Nm rwall> command sends a message to the users logged into the "
"specified host. The message to be sent can be typed in and terminated with "
"EOF or it can be in a E<.Ar file>."
msgstr ""
"Comanda E<.Nm rwall> trimite un mesaj către utilizatorii conectați la gazda "
"specificată. Mesajul care urmează să fie trimis poate fi tastat și terminat "
"cu EOF sau poate fi într-un fișier E<.Ar>."

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "DIAGNOSTICS"
msgstr "DIAGNOSTICARE"

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "rwall: RPC: Program not registered"
msgstr "rwall: RPC: Programul nu este înregistrat"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The E<.Xr rpc.rwalld 8> daemon has not been started on the remote host."
msgstr "Demonul E<.Xr rpc.rwalld 8> nu a fost pornit pe gazda de la distanță."

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "rwall: RPC: Timed out"
msgstr "rwall: RPC: Timpul de așteptare a expirat"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"A communication error occurred.  Either the network is excessively "
"congested, or the E<.Xr rpc.rwalld 8> daemon has terminated on the remote "
"host."
msgstr ""
"S-a produs o eroare de comunicare.  Fie că rețeaua este excesiv de "
"aglomerată, fie că demonul E<.Xr rpc.rwalld 8> a fost terminat pe gazda de "
"la distanță."

#. type: It
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "rwall: RPC: Port mapper failure - RPC: Timed out"
msgstr "rwall: RPC :Eșecul cartografierii porturilor - RPC: Timpul de așteptare a expirat"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The remote host is not running the portmapper (see E<.Xr portmap 8 ),> and "
"cannot accommodate any RPC-based services.  The host may be down."
msgstr ""
"Gazda la distanță nu rulează «portmapper» (a se vedea E<.Xr portmap 8 ),> și "
"nu poate găzdui niciun serviciu bazat pe RPC.  Este posibil ca gazda să nu "
"funcționeze."

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "E<.Xr wall 1>, E<.Xr portmap 8>, E<.Xr rpc.rwalld 8>"
msgstr "E<.Xr wall 1>, E<.Xr portmap 8>, E<.Xr rpc.rwalld 8>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "The E<.Nm rwall> command appeared in E<.Tn SunOS>."
msgstr "Comanda E<.Nm rwall> a apărut în E<.Tn SunOS>."

#. type: Plain text
#: fedora-41 fedora-rawhide
msgid ""
"E<.Nm rwall> E<.Ar host> E<.Op Ar file> E<.Nm rwall> E<.Ar -n netgroup> "
"E<.Op Ar file>"
msgstr ""
"E<.Nm rwall> E<.Ar gazda> E<.Op Ar fișier> E<.Nm rwall> E<.Ar -n grupul-de-"
"rețea> E<.Op Ar fișier>"

#. type: Plain text
#: fedora-41 fedora-rawhide
msgid ""
"The E<.Nm rwall> command sends a message to the users logged into the "
"specified host or to users on all hosts in the specified netgroup. The "
"message to be sent can be typed in and terminated with EOF or it can be in a "
"E<.Ar file>."
msgstr ""
"Comanda E<.Nm rwall> trimite un mesaj către utilizatorii conectați la gazda "
"specificată sau către utilizatorii de pe toate gazdele din grupul de rețea "
"specificat. Mesajul care urmează să fie trimis poate fi tastat și terminat "
"cu EOF sau poate fi într-un fișier E<.Ar>."

#. type: Plain text
#: fedora-41 fedora-rawhide
msgid ""
"The remote host is not running the portmapper (see E<.Xr portmap 8 ),> and "
"cannot accomodate any RPC-based services.  The host may be down."
msgstr ""
"Gazda la distanță nu rulează «portmapper» (a se vedea E<.Xr portmap 8 ),> și "
"nu poate găzdui niciun serviciu bazat pe RPC. Este posibil ca gazda să nu "
"funcționeze (deconectată de la alimentare, sau de la rețeaua Internet)."
