# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-12-06 18:00+0100\n"
"PO-Revision-Date: 2024-03-02 00:53+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKNETDIR"
msgstr "GRUB-MKNETDIR"

#. type: TH
#: fedora-41 mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "October 2024"
msgstr "octombrie 2024"

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.12"
msgstr "GRUB 2.12"

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "grub-mknetdir - prepare a GRUB netboot directory."
msgstr "grub-mknetdir - pregătește un director GRUB de pornire din rețea."

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-mknetdir> [I<\\,OPTION\\/>...]"
msgstr "B<grub-mknetdir> [I<\\,OPȚIUNE\\/>...]"

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Prepares GRUB network boot images at net_directory/subdir assuming "
"net_directory being TFTP root."
msgstr ""
"Pregătește imaginile de pornire în rețea de GRUB în director_rețea/"
"subdirector, presupunând că director_rețea este rădăcina de TFTP."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--appended-signature-size>=I<\\,SIZE\\/>"
msgstr "B<--appended-signature-size>=I<\\,DIMENSIUNEA\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Add a note segment reserving SIZE bytes for an appended signature"
msgstr ""
"Adaugă un segment de notă care rezervă DIMENSIUNEA octeți pentru o semnătură "
"adăugată"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--compress>=I<\\,no\\/>|xz|gz|lzo"
msgstr "B<--compress>=I<\\,no\\/>|xz|gz|lzo"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "compress GRUB files [optional]"
msgstr "comprimă fișierele de GRUB [opțional]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "disable shim_lock verifier"
msgstr "dezactivează verificarea shim_lock"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--dtb>=I<\\,FILE\\/>"
msgstr "B<--dtb>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed a specific DTB"
msgstr "încorporează un DTB specific"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,DIRECTOR\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"utilizează imaginile și modulele din DIR [default=/usr/lib/grub/"
"E<lt>platformaE<gt>]]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--fonts>=I<\\,FONTS\\/>"
msgstr "B<--fonts>=I<\\,FONTURI\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install FONTS [default=unicode]"
msgstr "instalează FONTURI [implicit=unicode]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--install-modules>=I<\\,MODULES\\/>"
msgstr "B<--install-modules>=I<\\,MODULE\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install only MODULES and their dependencies [default=all]"
msgstr ""
"instalează numai MODULEle și dependențele acestora [implicit=all (toate)]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed FILE as public key for signature checking"
msgstr ""
"încorporează FIȘIERUL drept cheie publică pentru verificarea semnăturii"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--locale-directory>=I<\\,DIR\\/> use translations under DIR"
msgstr "B<--locale-directory>=I<\\,DIR\\/> utilizează traducerile din DIR"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[default=/usr/share/locale]"
msgstr "[default=/usr/share/locale]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--locales>=I<\\,LOCALES\\/>"
msgstr "B<--locales>=I<\\,LOCALES\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install only LOCALES [default=all]"
msgstr "instalează numai LOCALES [implicit=all (toate)]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--modules>=I<\\,MODULES\\/>"
msgstr "B<--modules>=I<\\,MODULE\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "pre-load specified modules MODULES"
msgstr "preîncarcă modulele MODULE specificate"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--sbat>=I<\\,FILE\\/>"
msgstr "B<--sbat>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "SBAT metadata"
msgstr "metadate SBAT"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--themes>=I<\\,THEMES\\/>"
msgstr "B<--themes>=I<\\,TEME\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install THEMES [default=starfield]"
msgstr "instalează TEME [implicită=starfield]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "afișează mesaje detaliate."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--x509key>=I<\\,FILE\\/>"
msgstr "B<-x>, B<--x509key>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed FILE as an x509 certificate for signature checking"
msgstr ""
"încorporează FIȘIERUL drept certificat x509 pentru verificarea semnăturii"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--core-compress>=I<\\,xz\\/>|none|auto"
msgstr "B<--core-compress>=I<\\,xz\\/>|none|auto"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "choose the compression to use for core image"
msgstr "alege comprimarea de utilizat pentru imaginea de bază"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--net-directory>=I<\\,DIR\\/>"
msgstr "B<--net-directory>=I<\\,DIR\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "root directory of TFTP server"
msgstr "directorul rădăcină al serverului TFTP"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--subdir>=I<\\,DIR\\/>"
msgstr "B<--subdir>=I<\\,DIR\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "relative subdirectory on network server"
msgstr "subdirector relativ pe serverul de rețea"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Raportați erorile la E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-mkimage>(1)"
msgstr "B<grub-mkimage>(1)"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mknetdir> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mknetdir> programs are properly installed "
"at your site, the command"
msgstr ""
"Documentația completă pentru B<grub-mknetdi> este menținută ca un manual "
"Texinfo. Dacă programele B<info> și B<grub-mknetdi> sunt instalate corect în "
"sistemul dvs., comanda"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<info grub-mknetdir>"
msgstr "B<info grub-mknetdir>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "November 2024"
msgstr "noiembrie 2024"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use images and modules under DIR [default=/usr/share/grub2/"
"E<lt>platformE<gt>]"
msgstr ""
"utilizează imaginile și modulele din DIR [default=/usr/lib/grub/"
"E<lt>platformaE<gt>]"

#. type: TP
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-cli>"
msgstr "B<--disable-cli>"

#. type: Plain text
#: opensuse-tumbleweed
msgid "disabled command line interface access"
msgstr "dezactivează accesul la interfața liniei de comandă"
