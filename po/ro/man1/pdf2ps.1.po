# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-12-06 18:07+0100\n"
"PO-Revision-Date: 2023-07-02 18:58+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "PDF2PS"
msgstr "PDF2PS"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "18 Sept 2024"
msgstr "18 septembrie 2024"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "10.04.0"
msgstr "10.04.0"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Ghostscript Tools"
msgstr "Instrumente Ghostscript"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "pdf2ps - Ghostscript PDF to PostScript translator"
msgstr "pdf2ps - convertor Ghostscript PDF în PostScript"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pdf2ps> [ I<options> ] I<input.pdf [output.ps]>"
msgstr "B<pdf2ps> [ I<opțiuni> ] I<fișier-intrare.pdf [fișier-ieșire.ps]>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pdf2ps> uses B<gs>(1) to convert the Adobe B<Portable Document Format> "
"(PDF) file \"input.pdf\" to B<PostScript>(tm) in \"output.ps\".  Normally "
"the output is allowed to use PostScript Level 2 (but not PostScript "
"LanguageLevel 3) constructs; the B<-dLanguageLevel=1> option restricts the "
"output to Level 1, while B<-dLanguageLevel=3> allows using LanguageLevel 3 "
"in the output."
msgstr ""
"B<pdf2ps> utilizează B<gs>(1) pentru a converti fișierul Adobe B<Portable "
"Document Format> (PDF) „fișier-intrare.pdf” în B<PostScript>(tm) în „fișier-"
"ieșire.ps”.  În mod normal, la ieșire este permisă utilizarea construcțiilor "
"PostScript Level 2 (dar nu și PostScript LanguageLevel 3); opțiunea B<-"
"dLanguageLevel=1> restricționează ieșirea la nivelul 1, în timp ce B<-"
"dLanguageLevel=3> permite utilizarea LanguageLevel 3 la ieșire."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Run \"B<gs -h>\" to find the location of Ghostscript documentation on your "
"system, from which you can get more details."
msgstr ""
"Rulați „B<gs -h>” pentru a găsi locația documentației Ghostscript în "
"sistemul dumneavoastră, de unde puteți obține mai multe detalii."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSION"
msgstr "VERSIUNEA"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "This document was last revised for Ghostscript version 10.04.0."
msgstr ""
"Acest document a fost revizuit ultima dată pentru versiunea Ghostscript "
"10.04.0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Artifex Software, Inc. are the primary maintainers of Ghostscript."
msgstr "Artifex Software, Inc. sunt principalii întreținători ai Ghostscript."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "21 September 2022"
msgstr "21 septembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10.00.0"
msgstr "10.00.0"

#. type: Plain text
#: debian-bookworm
msgid "This document was last revised for Ghostscript version 10.00.0."
msgstr ""
"Acest document a fost revizuit ultima dată pentru versiunea Ghostscript "
"10.00.0."

#. type: TH
#: fedora-41
#, no-wrap
msgid "06 May 2024"
msgstr "6 mai 2024"

#. type: TH
#: fedora-41
#, no-wrap
msgid "10.03.1"
msgstr "10.03.1"

#. type: Plain text
#: fedora-41
msgid "This document was last revised for Ghostscript version 10.03.1."
msgstr ""
"Acest document a fost revizuit ultima dată pentru versiunea Ghostscript "
"10.03.1."
