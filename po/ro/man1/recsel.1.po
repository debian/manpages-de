# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-09-06 18:26+0200\n"
"PO-Revision-Date: 2023-12-15 22:27+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "RECSEL"
msgstr "RECSEL"

#. type: TH
#: debian-bookworm fedora-41 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "aprilie 2022"

#. type: TH
#: debian-bookworm fedora-41 fedora-rawhide
#, no-wrap
msgid "recsel 1.9"
msgstr "recsel 1.9"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "recsel - print records from a recfile"
msgstr "recsel - afișează înregistrările dintr-un fișier rec."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"B<recsel> [I<\\,OPTION\\/>]... [I<\\,-t TYPE\\/>] [I<\\,-j FIELD\\/>] [I<\\,-"
"n INDEXES | -e RECORD_EXPR | -q STR | -m NUM\\/>] [I<\\,-c | (-p|-P) "
"FIELD_EXPR\\/>] [I<\\,FILE\\/>]..."
msgstr ""
"B<recsel> [I<\\,OPȚIUNE\\/>]... [I<\\,-t TIP\\/>] [I<\\,-j CÂMP\\/>] [I<\\,-"
"n INDEXURI | -e EXPRESIE_ÎNREGISTRARE | -q ȘIR | -m NUMĂR\\/>] [I<\\,-c | (-"
"p|-P) EXPRESIE_CÂMP\\/>] [I<\\,FIȘIER\\/>]..."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Select and print rec data."
msgstr "Selectați și tipăriți datele (rec) de înregistrare."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-d>, B<--include-descriptors>"
msgstr "B<-d>, B<--include-descriptors>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "print record descriptors along with the matched records."
msgstr ""
"imprimă descriptorii înregistrărilor împreună cu înregistrările care "
"corespund."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-C>, B<--collapse>"
msgstr "B<-C>, B<--collapse>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "do not section the result in records with newlines."
msgstr "nu secționează rezultatul din înregistrări cu linii noi."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-S>, B<--sort>=I<\\,FIELD\\/>,..."
msgstr "B<-S>, B<--sort>=I<\\,CÂMP\\/>,..."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "sort the output by the specified fields."
msgstr "sortează rezultatul în funcție de câmpurile specificate."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-G>, B<--group-by>=I<\\,FIELD\\/>,..."
msgstr "B<-G>, B<--group-by>=I<\\,CÂMP\\/>,..."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "group records by the specified fields."
msgstr "grupează înregistrările în funcție de câmpurile specificate."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-U>, B<--uniq>"
msgstr "B<-U>, B<--uniq>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "remove duplicated fields in the output records."
msgstr "elimină câmpurile duplicate din înregistrările de la ieșire."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-s>, B<--password>=I<\\,STR\\/>"
msgstr "B<-s>, B<--password>=I<\\,ȘIR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "decrypt confidential fields with the given password."
msgstr "decriptează câmpurile confidențiale cu parola dată."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "print a help message and exit."
msgstr "imprimă un mesaj de ajutor și iese."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "show version and exit."
msgstr "afișează versiunea și iese."

#. type: SS
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Record selection options:"
msgstr "Opțiuni de selectare a înregistrărilor:"

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-i>, B<--case-insensitive>"
msgstr "B<-i>, B<--case-insensitive>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "make strings case-insensitive in selection expressions."
msgstr ""
"face ca șirurile de caractere să nu țină cont de majuscule și minuscule în "
"expresiile de selecție."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,TIP\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "operate on records of the specified type only."
msgstr "operează numai asupra înregistrărilor de tipul specificat."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-e>, B<--expression>=I<\\,RECORD_EXPR\\/>"
msgstr "B<-e>, B<--expression>=I<\\,EXPR_ÎNREGISTRARE\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "selection expression."
msgstr "expresia de selecție."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-q>, B<--quick>=I<\\,STR\\/>"
msgstr "B<-q>, B<--quick>=I<\\,ȘIR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "select records with fields containing a string."
msgstr "selectează înregistrările cu câmpuri care conțin un șir de caractere."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-n>, B<--number>=I<\\,NUM\\/>,..."
msgstr "B<-n>, B<--number>=I<\\,NUMĂR\\/>,..."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "select specific records by position, with ranges."
msgstr "selectează înregistrări specifice în funcție de poziție, cu intervale."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-m>, B<--random>=I<\\,NUM\\/>"
msgstr "B<-m>, B<--random>=I<\\,NUMĂR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "select a given number of random records."
msgstr "selectează un număr dat de înregistrări aleatorii."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-j>, B<--join>=I<\\,FIELD\\/>"
msgstr "B<-j>, B<--join>=I<\\,CÂMP\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "perform an inner join using the specified field."
msgstr "efectuează o îmbinare internă utilizând câmpul specificat."

#. type: SS
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Output options:"
msgstr "Opțiuni de ieșire:"

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-p>, B<--print>=I<\\,FIELDS\\/>"
msgstr "B<-p>, B<--print>=I<\\,CÂMPURI\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "comma-separated list of fields to print for each matching record."
msgstr ""
"listă de câmpuri, separate prin virgule, care trebuie afișate pentru fiecare "
"înregistrare găsită."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-P>, B<--print-values>=I<\\,FIELDS\\/>"
msgstr "B<-P>, B<--print-values>=I<\\,CÂMPURI\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "as B<-p>, but print only the values of the selected fields."
msgstr "ca B<-p>, dar imprimă numai valorile câmpurilor selectate."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-R>, B<--print-row>=I<\\,FIELDS\\/>"
msgstr "B<-R>, B<--print-row>=I<\\,CÂMPURI\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "as B<-P>, but separate the values with spaces instead of newlines."
msgstr "ca B<-P>, dar separă valorile cu spații în loc de linii noi."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-c>, B<--count>"
msgstr "B<-c>, B<--count>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"print a count of the matching records instead of the records themselves."
msgstr ""
"afișează o numărătoare a înregistrărilor găsite în locul înregistrărilor în "
"sine."

#. type: SS
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Special options:"
msgstr "Opțiuni speciale:"

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--print-sexps>"
msgstr "B<--print-sexps>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "print the data in sexps instead of rec format."
msgstr "tipărește datele în format „sexps” în loc de „rec”."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Written by Jose E. Marchesi."
msgstr "Scris de Jose E. Marchesi."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Report bugs to: bug-recutils@gnu.org"
msgstr "Raportați erorile la: E<.MT bug-recutils@gnu.org>E<.ME>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"GNU recutils home page: E<lt>https://www.gnu.org/software/recutils/E<gt>"
msgstr ""
"Pagina principală a GNU recutils: E<lt>https://www.gnu.org/software/recutils/"
"E<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Ajutor general pentru utilizarea software-ului GNU: E<lt>http://www.gnu.org/"
"gethelp/E<gt>"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Copyright \\(co 2010-2020 Jose E. Marchesi.  License GPLv3+: GNU GPL version "
"3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2010-2020 Jose E. Marchesi. Licența GPLv3+: GNU GPL "
"versiunea 3 sau ulterioară E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"The full documentation for B<recsel> is maintained as a Texinfo manual.  If "
"the B<info> and B<recsel> programs are properly installed at your site, the "
"command"
msgstr ""
"Documentația completă pentru B<recsel> este menținută ca un manual Texinfo. "
"Dacă programele B<info> și B<recsel> sunt instalate corect în sistemul dvs., "
"comanda"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<info recutils>"
msgstr "B<info recutils>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "March 2024"
msgstr "martie 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GNU recutils 1.9"
msgstr "GNU recutils 1.9"
