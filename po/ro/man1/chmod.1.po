# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Laurențiu Buzdugan <lbuz@rolix.org>, 2004.
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.16\n"
"POT-Creation-Date: 2024-12-06 17:55+0100\n"
"PO-Revision-Date: 2024-06-02 10:46+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CHMOD"
msgstr "CHMOD"

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2024"
msgstr "august 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "chmod - change file mode bits"
msgstr ""
"chmod - schimbă permisiunile de acces ale fișierelor și dosarelor (biții de "
"mod de acces)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<chmod> [I<\\,OPTION\\/>]... I<\\,MODE\\/>[I<\\,,MODE\\/>]... I<\\,FILE\\/"
">..."
msgstr ""
"B<chmod> [I<\\,OPȚIUNE\\/>]... I<\\,MOD\\/>[I<\\,,MOD\\/>]... "
"I<\\,FIȘIER_sau_DOSAR\\/>..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chmod> [I<\\,OPTION\\/>]... I<\\,OCTAL-MODE FILE\\/>..."
msgstr "B<chmod> [I<\\,OPȚIUNE\\/>]... I<\\,MOD-OCTAL FIȘIER_sau_DOSAR\\/>..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chmod> [I<\\,OPTION\\/>]... I<\\,--reference=RFILE FILE\\/>..."
msgstr ""
"B<chmod> [I<\\,OPȚIUNE\\/>]... I<\\,--reference=FIȘIER_REF "
"FIȘIER_sau_DOSAR\\/>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<chmod>.  B<chmod> changes "
"the file mode bits of each given file according to I<mode>, which can be "
"either a symbolic representation of changes to make, or an octal number "
"representing the bit pattern for the new mode bits."
msgstr ""
"Această pagină de manual documentează versiunea GNU a B<chmod>. B<chmod> "
"schimbă permisiunile fiecărui I<fișier> furnizat ca argument, în "
"conformitate cu I<mod>, care este fie o reprezentare simbolică a "
"schimbărilor ce trebuie făcute, fie un număr octal reprezentând tiparul de "
"biți pentru noile permisiuni (biții de mod de acces)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The format of a symbolic mode is [B<ugoa>.\\|.\\|.][[B<-+=>][I<perms>.\\|."
"\\|.].\\|.\\|.], where I<perms> is either zero or more letters from the set "
"B<rwxXst>, or a single letter from the set B<ugo>.  Multiple symbolic modes "
"can be given, separated by commas."
msgstr ""
"Formatul unui mod simbolic este [B<ugoa>.\\|.\\|.][[B<-+=>][I<perms>.\\|."
"\\|.].\\|.\\|.], unde I<perms> este fie zero sau mai multe litere din setul "
"B<rwxXst>, fie o singură literă din setul B<ugo>. Se pot indica mai multe "
"moduri simbolice, separate prin virgule."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A combination of the letters B<ugoa> controls which users' access to the "
"file will be changed: the user who owns it (B<u>), other users in the file's "
"group (B<g>), other users not in the file's group (B<o>), or all users "
"(B<a>).  If none of these are given, the effect is as if (B<a>) were given, "
"but bits that are set in the umask are not affected."
msgstr ""
"O combinație a literelor B<ugoa> controlează ce utilizatori vor avea accesul "
"la fișierul/directorul modificat: utilizatorul care îl deține (B<u>), alți "
"utilizatori din grupul deținător al fișierului (B<g>), alți utilizatori care "
"nu sunt în grupul deținător al fișierului (B<o>) sau toți utilizatorii "
"(B<a>). Dacă niciunul dintre acestea nu este indicat, efectul este ca și cum "
"ar fi fost dat (B<a>), dar biții de mod de acces (permisiunile) care sunt "
"definiți în „umask” nu sunt afectați."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The operator B<+> causes the selected file mode bits to be added to the "
"existing file mode bits of each file; B<-> causes them to be removed; and "
"B<=> causes them to be added and causes unmentioned bits to be removed "
"except that a directory's unmentioned set user and group ID bits are not "
"affected."
msgstr ""
"Operatorul B<+> face ca biții de mod de fișier selectați să fie adăugați la "
"biții de mod de fișier existenți ai fiecărui fișier; operatorul B<-> face ca "
"aceștia să fie eliminați; iar operatorul B<=> face ca aceștia să fie "
"adăugați și face ca biții nemenționați să fie eliminați, cu excepția "
"faptului că biții de identificare de utilizator și de grup nemenționați ai "
"unui director nu sunt afectați."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The letters B<rwxXst> select file mode bits for the affected users: read "
"(B<r>), write (B<w>), execute (or search for directories)  (B<x>), execute/"
"search only if the file is a directory or already has execute permission for "
"some user (B<X>), set user or group ID on execution (B<s>), restricted "
"deletion flag or sticky bit (B<t>).  Instead of one or more of these "
"letters, you can specify exactly one of the letters B<ugo>: the permissions "
"granted to the user who owns the file (B<u>), the permissions granted to "
"other users who are members of the file's group (B<g>), and the permissions "
"granted to users that are in neither of the two preceding categories (B<o>)."
msgstr ""
"Literele B<rwxXst> selectează biții de mod de fișier pentru utilizatorii "
"afectați: citire (B<r>), scriere (B<w>), executare (sau căutare de "
"directoare) (B<x>), executare/căutare numai dacă fișierul este un director "
"sau dacă are deja permisiunea de execuție pentru un anumit utilizator "
"(B<X>), activează identificatorul utilizatorului (SETUID) sau grupului "
"(SETGID) la execuție (B<s>), indicatorul de ștergere restricționată sau "
"bitul lipicios „sticky” (B<t>). În loc de una sau mai multe dintre aceste "
"litere, puteți specifica exact una dintre literele B<ugo>: permisiunile "
"acordate utilizatorului care deține fișierul (B<u>), permisiunile acordate "
"altor utilizatori care sunt membri ai grupului fișierului (B<g>) și "
"permisiunile acordate utilizatorilor care nu fac parte din niciuna dintre "
"cele două categorii precedente (B<o>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A numeric mode is from one to four octal digits (0-7), derived by adding up "
"the bits with values 4, 2, and 1.  Omitted digits are assumed to be leading "
"zeros.  The first digit selects the set user ID (4) and set group ID (2) and "
"restricted deletion or sticky (1) attributes.  The second digit selects "
"permissions for the user who owns the file: read (4), write (2), and execute "
"(1); the third selects permissions for other users in the file's group, with "
"the same values; and the fourth for other users not in the file's group, "
"with the same values."
msgstr ""
"Un mod numeric este compus din una până la patru cifre octale (0-7), "
"obținute prin însumarea biților cu valorile 4, 2 și 1. Cifrele omise se "
"consideră a fi zerouri și se pun la începutul șirului. Prima cifră "
"selectează atributele „set user ID” (4), „set group ID” (2) și „ștergere "
"restricționată sau bitul lipicios (sticky)” (1). A doua cifră selectează "
"permisiunile pentru utilizatorul care deține fișierul: citire (4), scriere "
"(2) și execuție (1); a treia cifră selectează permisiunile pentru alți "
"utilizatori din grupul căruia îi aparține fișierul, cu aceleași valori; și a "
"patra cifră selectează permisiunile pentru alți utilizatori care nu fac "
"parte din grupul căruia îi aparține fișierul, cu aceleași valori."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<chmod> doesn't change the permissions of symbolic links; the B<chmod> "
"system call cannot change their permissions on most systems, and most "
"systems ignore permissions of symbolic links.  However, for each symbolic "
"link listed on the command line, B<chmod> changes the permissions of the "
"pointed-to file.  In contrast, B<chmod> ignores symbolic links encountered "
"during recursive directory traversals. Options that modify this behavior are "
"described in the B<OPTIONS> section."
msgstr ""
"B<chmod> nu modifică niciodată permisiunile legăturilor simbolice; apelul de "
"sistem B<chmod> nu poate modifica permisiunile acestora. Aceasta nu "
"reprezintă o problemă, deoarece permisiunile legăturilor simbolice nu sunt "
"niciodată utilizate. Cu toate acestea, pentru fiecare legătură simbolică "
"listată în linia de comandă, B<chmod> modifică permisiunile fișierului către "
"care indică legătura. În schimb, B<chmod> ignoră legăturile simbolice "
"întâlnite în timpul parcurgerii recursive a directoarelor. Opțiunile care "
"modifică acest comportament sunt descrise în secțiunea B<OPȚIUNI>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SETUID AND SETGID BITS"
msgstr "BIȚII SETUID ȘI SETGID"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<chmod> clears the set-group-ID bit of a regular file if the file's group "
"ID does not match the user's effective group ID or one of the user's "
"supplementary group IDs, unless the user has appropriate privileges.  "
"Additional restrictions may cause the set-user-ID and set-group-ID bits of "
"I<MODE> or I<RFILE> to be ignored.  This behavior depends on the policy and "
"functionality of the underlying B<chmod> system call.  When in doubt, check "
"the underlying system behavior."
msgstr ""
"B<chmod> șterge bitul set-group-ID al unui fișier obișnuit dacă "
"identificatorul de grup al fișierului nu se potrivește cu identificatorul de "
"grup efectiv al utilizatorului sau cu unul dintre identificatorii de grup "
"suplimentari ai utilizatorului, cu excepția cazului în care utilizatorul are "
"privilegiile corespunzătoare. Restricții suplimentare pot face ca biții set-"
"user-ID și set-group-ID din I<MOD> sau I<FIȘIER_REF> să fie ignorați. Acest "
"comportament depinde de politica și funcționalitatea apelului de sistem "
"B<chmod> care stă la baza acestuia. În caz de îndoială, verificați "
"comportamentul sistemului subiacent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For directories B<chmod> preserves set-user-ID and set-group-ID bits unless "
"you explicitly specify otherwise.  You can set or clear the bits with "
"symbolic modes like B<u+s> and B<g-s>.  To clear these bits for directories "
"with a numeric mode requires an additional leading zero like B<00755>, "
"leading minus like B<-6000>, or leading equals like B<=755>."
msgstr ""
"Pentru directoare, B<chmod> păstrează biții set-user-ID și set-group-ID, cu "
"excepția cazului în care se specifică în mod explicit altfel. Puteți activa "
"sau dezactiva biții cu moduri simbolice precum B<u+s> și B<g-s>. Pentru a "
"dezactiva acești biți pentru directoare, cu un mod numeric este necesar un "
"zero suplimentar în față, cum ar fi B<00755>, un minus în față, cum ar fi "
"B<-6000>, sau un egal în față, cum ar fi B<=755>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RESTRICTED DELETION FLAG OR STICKY BIT"
msgstr "INDICATORUL DE ȘTERGERE RESTRICȚIONATĂ SAU BITUL LIPICIOS (STICKY)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The restricted deletion flag or sticky bit is a single bit, whose "
"interpretation depends on the file type.  For directories, it prevents "
"unprivileged users from removing or renaming a file in the directory unless "
"they own the file or the directory; this is called the I<restricted deletion "
"flag> for the directory, and is commonly found on world-writable directories "
"like B</tmp>.  For regular files on some older systems, the bit saves the "
"program's text image on the swap device so it will load more quickly when "
"run; this is called the I<sticky bit>."
msgstr ""
"Indicatorul de ștergere restricționată sau bitul lipicios (sticky) este un "
"singur bit, a cărui interpretare depinde de tipul de fișier. În cazul "
"directoarelor, acesta împiedică utilizatorii neprivilegiați să elimine sau "
"să redenumească un fișier din director, cu excepția cazului în care sunt "
"proprietarii fișierului sau ai directorului; acesta se numește I<indicatorul "
"de ștergere restricționată> pentru directorul respectiv și se găsește în mod "
"obișnuit în directoare care pot fi scrise de oricine cum ar fi B</tmp>. "
"Pentru fișierele obișnuite de pe unele sisteme mai vechi, bitul salvează "
"imaginea de text a programului pe dispozitivul swap, astfel încât acesta se "
"va încărca mai repede atunci când este rulat; acest lucru se numește I<bitul "
"lipicios>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Change the mode of each FILE to MODE.  With B<--reference>, change the mode "
"of each FILE to that of RFILE."
msgstr ""
"Schimbă modul fiecărui FIȘIER la MOD. Cu B<--reference>, schimbă modul "
"fiecărui FIȘIER cu cel al FIȘIER_REF."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--changes>"
msgstr "B<-c>, B<--changes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "like verbose but report only when a change is made"
msgstr ""
"precum B<--verbose>, dar raportează numai atunci când se face o modificare"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--silent>, B<--quiet>"
msgstr "B<-f>, B<--silent>, B<--quiet>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "suppress most error messages"
msgstr "suprimă majoritatea mesajelor de eroare"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output a diagnostic for every file processed"
msgstr ""
"descrie în amănunt acțiunile sau non-acțiunile luate pentru fiecare fișier "
"sau dosar procesat"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--dereference>"
msgstr "B<--dereference>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"affect the referent of each symbolic link, rather than the symbolic link "
"itself"
msgstr ""
"afectează referentul fiecărei legături simbolice, mai degrabă decât legătura "
"simbolică în sine"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--no-dereference>"
msgstr "B<-h>, B<--no-dereference>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "affect each symbolic link, rather than the referent"
msgstr "afectează fiecare legătură simbolică, mai degrabă decât referentul"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-preserve-root>"
msgstr "B<--no-preserve-root>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "do not treat '/' specially (the default)"
msgstr "nu tratează „/” deosebit (valoarea implicită)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--preserve-root>"
msgstr "B<--preserve-root>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "fail to operate recursively on '/'"
msgstr "eșuează la operarea recursivă pe „/”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--reference>=I<\\,RFILE\\/>"
msgstr "B<--reference>=I<\\,FIȘIER_REF\\/>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use RFILE's mode instead of specifying MODE values.  RFILE is always "
"dereferenced if a symbolic link."
msgstr ""
"utilizează modul din FIȘIER_REF în loc de valorile specificate în MOD. "
"FIȘIER_REF este întotdeauna „dereferenced” dacă este o legătură simbolică."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--recursive>"
msgstr "B<-R>, B<--recursive>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "change files and directories recursively"
msgstr "schimbă fișierele și directoarele în mod recursiv"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"The following options modify how a hierarchy is traversed when the B<-R> "
"option is also specified.  If more than one is specified, only the final one "
"takes effect. '-H' is the default."
msgstr ""
"Următoarele opțiuni modifică modul de parcurgere a unei ierarhii atunci când "
"este specificată și opțiunea B<-R>. Dacă sunt specificate mai multe opțiuni, "
"numai cea finală are efect. „-H” este opțiunea implicită."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-H>"
msgstr "B<-H>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"if a command line argument is a symbolic link to a directory, traverse it"
msgstr ""
"dacă un argument din linia de comandă este o legătură simbolică la un dosar, "
"o urmărește"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-L>"
msgstr "B<-L>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "traverse every symbolic link to a directory encountered"
msgstr "urmărește fiecare legătură simbolică la un dosar, ce este întâlnită"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid "do not traverse any symbolic links"
msgstr "nu urmărește nicio legătură simbolică"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each MODE is of the form '[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+'."
msgstr ""
"Fiecare MOD este de forma „[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+”."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by David MacKenzie and Jim Meyering."
msgstr "Scris de David MacKenzie și Jim Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Ajutor online GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Raportați orice erori de traducere la: E<lt>https://translationproject.org/"
"team/ro.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor © 2024 Free Software Foundation, Inc. Licența GPLv3+: GNU "
"GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chmod>(2)"
msgstr "B<chmod>(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chmodE<gt>"
msgstr ""
"Documentația completă la E<lt>https://www.gnu.org/software/coreutils/"
"chmodE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chmod invocation\\(aq"
msgstr ""
"sau local rulând comanda: «info \\(aq(coreutils) chmod invocation\\(aq»"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "septembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0
msgid ""
"B<chmod> never changes the permissions of symbolic links; the B<chmod> "
"system call cannot change their permissions.  This is not a problem since "
"the permissions of symbolic links are never used.  However, for each "
"symbolic link listed on the command line, B<chmod> changes the permissions "
"of the pointed-to file.  In contrast, B<chmod> ignores symbolic links "
"encountered during recursive directory traversals."
msgstr ""
"B<chmod> nu modifică niciodată permisiunile legăturilor simbolice; apelul de "
"sistem B<chmod> nu poate modifica permisiunile acestora. Aceasta nu "
"reprezintă o problemă, deoarece permisiunile legăturilor simbolice nu sunt "
"niciodată utilizate. Cu toate acestea, pentru fiecare legătură simbolică "
"listată în linia de comandă, B<chmod> modifică permisiunile fișierului către "
"care indică legătura. În schimb, B<chmod> ignoră legăturile simbolice "
"întâlnite în timpul parcurgerii recursive a directoarelor."

#. type: Plain text
#: debian-bookworm
msgid "use RFILE's mode instead of MODE values"
msgstr "utilizează modul FIȘIER_REF în loc de valorile MOD"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor © 2022 Free Software Foundation, Inc. Licența GPLv3+: GNU "
"GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "octombrie 2024"

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "September 2024"
msgstr "septembrie 2024"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "November 2024"
msgstr "noiembrie 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "august 2023"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2023 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "ianuarie 2024"
