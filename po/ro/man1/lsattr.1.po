# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: 2024-06-02 11:20+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LSATTR"
msgstr "LSATTR"

#. type: TH
#: archlinux debian-bookworm fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "May 2024"
msgstr "mai 2024"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "E2fsprogs version 1.47.1"
msgstr "E2fsprogs versiunea 1.47.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "lsattr - list ext2/ext3/ext4 file attributes"
msgstr "lsattr - listează atributele fișierelor ext2/ext3/ext4"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<lsattr> [ B<-RVadlpv> ] [ I<files...> ]"
msgstr "B<lsattr> [ B<-RVadlpv> ] [ I<fișiere...> ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"B<lsattr> lists the file attributes on an ext2/ext3/ext4 file system.  See "
"B<chattr>(1)  for a description of the attributes and what they mean."
msgstr ""
"B<lsattr> listează atributele fișierelor pe un sistem de fișiere ext2/ext3/"
"ext4. A se vedea B<chattr>(1) pentru o descriere a atributelor și a "
"semnificației acestora."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>"
msgstr "B<-R>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Recursively list attributes of directories and their contents."
msgstr ""
"Listează în mod recurent atributele directoarelor și conținutul acestora."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Display the program version."
msgstr "Afișează versiunea programului, apoi iese."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "List all files in directories, including files that start with `.'."
msgstr ""
"Listează toate fișierele din directoare, inclusiv fișierele care încep cu "
"„.”."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "List directories like other files, rather than listing their contents."
msgstr ""
"Listează directoarele precum alte fișiere, în loc să listeze conținutul "
"acestora."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Print the options using long names instead of single character abbreviations."
msgstr ""
"Afișează opțiunile folosind nume lungi în loc de abrevieri cu un singur "
"caracter."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "List the file's project number."
msgstr "Afișează numărul de proiect al fișierului."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "List the file's version/generation number."
msgstr "Afișează numărul versiunii/generării fișierului."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<lsattr> was written by Remy Card E<lt>Remy.Card@linux.orgE<gt>.  It is "
"currently being maintained by Theodore Ts'o E<lt>tytso@alum.mit.eduE<gt>."
msgstr ""
"B<lsattr> a fost scris de Remy Card E<lt>Remy.Card@linux.orgE<gt>. În "
"prezent, este întreținut de Theodore Ts'o E<lt>tytso@alum.mit.eduE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "There are none :-)."
msgstr "Nu există niciuna :-)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITATE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<lsattr> is part of the e2fsprogs package and is available from http://"
"e2fsprogs.sourceforge.net."
msgstr ""
"B<lsattr> face parte din pachetul e2fsprogs și este disponibil la http://"
"e2fsprogs.sourceforge.net."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chattr>(1)"
msgstr "B<chattr>(1)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "E2fsprogs version 1.47.1-rc2"
msgstr "E2fsprogs versiunea 1.47.1-rc2"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
msgid "lsattr - list file attributes on a Linux second extended file system"
msgstr ""
"lsattr - listează atributele fișierelor pe un al doilea sistem de fișiere "
"extins Linux"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<lsattr> lists the file attributes on a second extended file system.  See "
"B<chattr>(1)  for a description of the attributes and what they mean."
msgstr ""
"B<lsattr> enumeră atributele fișierelor pe un al doilea sistem de fișiere "
"extins. A se vedea B<chattr>(1) pentru o descriere a atributelor și a "
"semnificației acestora."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "November 2024"
msgstr "noiembrie 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "E2fsprogs version 1.47.2-rc1"
msgstr "E2fsprogs versiunea 1.47.2-rc1"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "februarie 2023"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr "E2fsprogs versiunea 1.47.0"
