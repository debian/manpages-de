# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-11-03 06:42+0100\n"
"PO-Revision-Date: 2024-04-08 20:31+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BISON"
msgstr "BISON"

#. type: TH
#: archlinux fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "September 2021"
msgstr "septembrie 2021"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GNU Bison 3.8.2"
msgstr "GNU Bison 3.8.2"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "bison - GNU Project parser generator (yacc replacement)"
msgstr "bison - generator de analizator al proiectului GNU (înlocuitor yacc)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<bison> [I<\\,OPTION\\/>]... I<\\,FILE\\/>"
msgstr "B<bison> [I<\\,OPȚIUNE\\/>]... I<\\,FIȘIER\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Bison> is a parser generator in the style of I<yacc>(1).  It should be "
"upwardly compatible with input files designed for I<yacc>."
msgstr ""
"I<bison> este un generator de analizator în stilul I<yacc>(1). Acesta ar "
"trebui să fie compatibil cu fișierele de intrare concepute pentru I<yacc>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Input files should follow the I<yacc> convention of ending in B<.y>.  Unlike "
"I<yacc>, the generated files do not have fixed names, but instead use the "
"prefix of the input file.  Moreover, if you need to put I<C++> code in the "
"input file, you can end his name by a C++-like extension (.ypp or .y++), "
"then bison will follow your extension to name the output file (.cpp or .c+"
"+).  For instance, a grammar description file named B<parse.yxx> would "
"produce the generated parser in a file named B<parse.tab.cxx>, instead of "
"I<yacc>'s B<y.tab.c> or old I<Bison> version's B<parse.tab.c>."
msgstr ""
"Fișierele de intrare trebuie să respecte convenția I<yacc> de a se termina "
"cu B<.y>. Spre deosebire de I<yacc>, fișierele generate nu au nume fixe, ci "
"folosesc prefixul fișierului de intrare. În plus, dacă trebuie să puneți cod "
"I<C++> în fișierul de intrare, puteți termina numele său cu o extensie de "
"tip C++ (.ypp sau .y++), apoi bison va urma extensia dvs. pentru a denumi "
"fișierul de ieșire (.cpp sau .c++). De exemplu, un fișier de descriere a "
"gramaticii numit B<parse.yxx> va produce analizatorul generat într-un fișier "
"numit B<parse.tab.cxx>, în loc de I<y.tab.c> al lui B<yacc> sau "
"B<parse.tab.c> al vechii versiuni I<bison>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This description of the options that can be given to I<bison> is adapted "
"from the node B<Invocation> in the B<bison.texi> manual, which should be "
"taken as authoritative."
msgstr ""
"Această descriere a opțiunilor care pot fi date lui I<bison> este adaptată "
"de la nodul B<Invocation> din manualul B<bison.texi>, care ar trebui să fie "
"considerată ca fiind de referință."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Bison> supports both traditional single-letter options and mnemonic long "
"option names.  Long option names are indicated with B<--> instead of B<->.  "
"Abbreviations for option names are allowed as long as they are unique.  When "
"a long option takes an argument, like B<--file-prefix>, connect the option "
"name and the argument with B<=>."
msgstr ""
"I<bison> acceptă atât opțiunile tradiționale cu o singură literă, cât și "
"numele mnemonice lungi ale opțiunilor. Numele de opțiuni lungi sunt indicate "
"cu B<--> în loc de B<->. Sunt permise abrevierile pentru numele opțiunilor, "
"atât timp cât acestea sunt unice. Atunci când o opțiune lungă are un "
"argument, cum ar fi B<--file-prefix>, conectați numele opțiunii și "
"argumentul cu B<=>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Generate a deterministic LR or generalized LR (GLR) parser employing "
"LALR(1), IELR(1), or canonical LR(1) parser tables."
msgstr ""
"Generarea unui analizator determinist LR sau LR generalizat (GLR) care "
"utilizează tabelele de analizator LALR(1), IELR(1) sau LR(1) canonice."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too.  "
"The same is true for optional arguments."
msgstr ""
"Argumentele obligatorii pentru opțiunile lungi sunt obligatorii și pentru "
"opțiunile scurte. Similar pentru argumentele opționale."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Operation Modes:"
msgstr "Moduri de operare:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--print-localedir>"
msgstr "B<--print-localedir>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output directory containing locale-dependent data and exit"
msgstr ""
"afișează directorul care conține datele dependente de configurația regională "
"și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--print-datadir>"
msgstr "B<--print-datadir>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output directory containing skeletons and XSLT and exit"
msgstr "afișează directorul care conține scheletele și XSLT și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--update>"
msgstr "B<-u>, B<--update>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "apply fixes to the source grammar file and exit"
msgstr "aplică corecturile la fișierul sursă de gramatică și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--feature>[=I<\\,FEATURES\\/>]"
msgstr "B<-f>, B<--feature>[=I<\\,CARACTERISTICI\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "activate miscellaneous features"
msgstr "activează diverse caracteristici"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FEATURES is a list of comma separated words that can include:"
msgstr "CARACTERISTICI este o listă de cuvinte separate prin virgulă care pot include:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "caret, diagnostics-show-caret"
msgstr "caret, diagnostics-show-caret"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "show errors with carets"
msgstr "afișează erorile cu semne de cursor „‸”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "fixit, diagnostics-parseable-fixits"
msgstr "fixit, diagnostics-parseable-fixits"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "show machine-readable fixes"
msgstr "afișează corecturi care pot fi citite de mașină"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "syntax-only"
msgstr "syntax-only"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "do not generate any file"
msgstr "nu generează niciun fișier"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "all"
msgstr "all"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "all of the above"
msgstr "toate cele de mai sus"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "none"
msgstr "none"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "disable all of the above"
msgstr "dezactivează toate cele de mai sus"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Diagnostics:"
msgstr "Diagnosticare:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-W>, B<--warnings>[=I<\\,CATEGORY\\/>]"
msgstr "B<-W>, B<--warnings>[=I<\\,CATEGORIA\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "report the warnings falling in CATEGORY"
msgstr "raportează avertismentele care se încadrează în CATEGORIA"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--color>[=I<\\,WHEN\\/>]"
msgstr "B<--color>[=I<\\,CÂND\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "whether to colorize the diagnostics"
msgstr "dacă se dorește colorarea diagnosticelor"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--style>=I<\\,FILE\\/>"
msgstr "B<--style>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "specify the CSS FILE for colorizer diagnostics"
msgstr "specifică FIȘIERUL CSS pentru colorarea diagnosticelor"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Warning categories include:"
msgstr "Categoriile de avertismente includ:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "conflicts-sr"
msgstr "conflicts-sr"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "S/R conflicts (enabled by default)"
msgstr "Conflicte S/R (activată în mod implicit)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "conflicts-rr"
msgstr "conflicts-rr"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "R/R conflicts (enabled by default)"
msgstr "Conflicte R/R (activată în mod implicit)w"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "counterexamples, cex"
msgstr "counterexamples, cex"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "generate conflict counterexamples"
msgstr "generează contraexemple de conflict"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "dangling-alias"
msgstr "dangling-alias"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "string aliases not attached to a symbol"
msgstr "alias de șiruri care nu sunt atașate unui simbol"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "deprecated"
msgstr "deprecated"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "obsolete constructs"
msgstr "construcții obsolete"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "empty-rule"
msgstr "empty-rule"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "empty rules without %empty"
msgstr "reguli goale fără %empty"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "midrule-values"
msgstr "midrule-values"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "unset or unused midrule values"
msgstr "valori intermediare nedefinite sau neutilizate"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "precedence"
msgstr "precedence"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "useless precedence and associativity"
msgstr "precedență și asociativitate inutile"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "yacc"
msgstr "yacc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "incompatibilities with POSIX Yacc"
msgstr "incompatibilități cu POSIX Yacc"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "other"
msgstr "other"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "all other warnings (enabled by default)"
msgstr "toate celelalte avertismente (activate în mod implicit)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "all the warnings except 'counterexamples', 'dangling-alias' and 'yacc'"
msgstr ""
"toate avertismentele, cu excepția „counterexamples”, „dangling-alias” și "
"„yacc”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "no-CATEGORY"
msgstr "no-CATEGORY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "turn off warnings in CATEGORY"
msgstr "dezactivează avertismentele din CATEGORIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "turn off all the warnings"
msgstr "dezactivează toate avertismentele"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "error[=CATEGORY]"
msgstr "error[=CATEGORIE]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "treat warnings as errors"
msgstr "tratează avertismentele ca erori"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "WHEN can be one of the following:"
msgstr "CÂND poate fi unul dintre următoarele:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "always, yes"
msgstr "always, yes"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "colorize the output"
msgstr "colorează ieșirea"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "never, no"
msgstr "never, no"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "don't colorize the output"
msgstr "nu colorează ieșirea"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "auto, tty"
msgstr "auto, tty"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "colorize if the output device is a tty"
msgstr "colorează dacă dispozitivul de ieșire este un tty (terminal)"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Tuning the Parser:"
msgstr "Reglarea analizatorului:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--language>=I<\\,LANGUAGE\\/>"
msgstr "B<-L>, B<--language>=I<\\,LIMBAJ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "specify the output programming language"
msgstr "specifică limbajul de programare de ieșire"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--skeleton>=I<\\,FILE\\/>"
msgstr "B<-S>, B<--skeleton>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "specify the skeleton to use"
msgstr "specifică scheletul care trebuie utilizat"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--debug>"
msgstr "B<-t>, B<--debug>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "instrument the parser for tracing same as '-Dparse.trace'"
msgstr "instrumentează analizatorul pentru urmărire, la fel ca „-Dparse.trace”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--locations>"
msgstr "B<--locations>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "enable location support"
msgstr "activează suportul de locație"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--define=NAME>[=I<\\,VALUE\\/>]"
msgstr "B<-D>, B<--define=NUME>[=I<\\,VALOARE\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "similar to '%define NAME VALUE'"
msgstr "similar cu „%define NUME VALOARE”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>, B<--force-define=NAME>[=I<\\,VALUE\\/>]"
msgstr "B<-F>, B<--force-define=NUME>[=I<\\,VALOARE\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "override '%define NAME VALUE'"
msgstr "suprascrie „%define NUME VALOARE”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--name-prefix>=I<\\,PREFIX\\/>"
msgstr "B<-p>, B<--name-prefix>=I<\\,PREFIX\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"prepend PREFIX to the external symbols deprecated by '-Dapi.prefix={PREFIX}'"
msgstr ""
"antepune PREFIX la simbolurile externe depreciate de „-Dapi.prefix={PREFIX}”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--no-lines>"
msgstr "B<-l>, B<--no-lines>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "don't generate '#line' directives"
msgstr "nu generează directivele „#line”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--token-table>"
msgstr "B<-k>, B<--token-table>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "include a table of token names"
msgstr "include un tabel cu numele simbolurilor"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-y>, B<--yacc>"
msgstr "B<-y>, B<--yacc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "emulate POSIX Yacc"
msgstr "emulează POSIX Yacc"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Output Files:"
msgstr "Fișiere de ieșire:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--header>=I<\\,[FILE]\\/>"
msgstr "B<-H>, B<--header>=I<\\,[FIȘIER]\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "also produce a header file"
msgstr "produce, de asemenea, un fișier antet"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "likewise but cannot specify FILE (for POSIX Yacc)"
msgstr "la fel, dar nu poate specifica FIȘIER (pentru POSIX Yacc)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--report>=I<\\,THINGS\\/>"
msgstr "B<-r>, B<--report>=I<\\,LUCRURI\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "also produce details on the automaton"
msgstr "produce, de asemenea, detalii privind automatul"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--report-file>=I<\\,FILE\\/>"
msgstr "B<--report-file>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "write report to FILE"
msgstr "scrie raportul în FIȘIER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "same as '--report=state'"
msgstr "la fel ca „--report=state”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--file-prefix>=I<\\,PREFIX\\/>"
msgstr "B<-b>, B<--file-prefix>=I<\\,PREFIX\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "specify a PREFIX for output files"
msgstr "specifică un PREFIX pentru fișierele de ieșire"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "leave output to FILE"
msgstr "lasă ieșirea în FIȘIER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-g>, B<--graph>[=I<\\,FILE\\/>]"
msgstr "B<-g>, B<--graph>[=I<\\,FIȘIER\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "also output a graph of the automaton"
msgstr "produce, de asemenea, un grafic al automatului"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--html>[=I<\\,FILE\\/>]"
msgstr "B<--html>[=I<\\,FIȘIER\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "also output an HTML report of the automaton"
msgstr "produce, de asemenea, un raport HTML al automatului"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--xml>[=I<\\,FILE\\/>]"
msgstr "B<-x>, B<--xml>[=I<\\,FIȘIER\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "also output an XML report of the automaton"
msgstr "produce, de asemenea, un raport XML al automatului"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-M>, B<--file-prefix-map>=I<\\,OLD=NEW\\/> replace prefix OLD with NEW when writing file paths"
msgstr "B<-M>, B<--file-prefix-map>=I<\\,VECHI=NOU\\/> înlocuiește prefixul VECHI cu NOU la scrierea rutelor fișierelor"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "in output files"
msgstr "în fișierele de ieșire"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "THINGS is a list of comma separated words that can include:"
msgstr "LUCRURI este o listă de cuvinte separate prin virgulă, care poate include:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "states"
msgstr "states"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "describe the states"
msgstr "descrie stările"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "itemsets"
msgstr "itemsets"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "complete the core item sets with their closure"
msgstr "completează seturile de elemente de bază cu închiderea acestora"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "lookaheads"
msgstr "lookaheads"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "explicitly associate lookahead tokens to items"
msgstr "asociază în mod explicit simbolurile dinainte „lookahead” la elemente"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "solved"
msgstr "solved"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "describe shift/reduce conflicts solving"
msgstr "descrie rezolvarea conflictelor de deplasare/reducere"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "include all the above information"
msgstr "include toate informațiile de mai sus"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "disable the report"
msgstr "dezactivează raportul"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Robert Corbett and Richard Stallman."
msgstr "Scris de Robert Corbett și Richard Stallman."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-bison@gnu.orgE<gt>."
msgstr "Raportați erorile la: E<lt>bug-bison@gnu.orgE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU Bison home page: E<lt>https://www.gnu.org/software/bison/E<gt>."
msgstr ""
"Pagina principală a GNU Bison: E<lt>https://www.gnu.org/software/bison/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"General help using GNU software: E<lt>https://www.gnu.org/gethelp/E<gt>."
msgstr ""
"Ajutor general pentru utilizarea software-ului GNU: E<lt>https://www.gnu.org/"
"gethelp/E<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "For complete documentation, run: info bison."
msgstr "Pentru documentația completă, rulați: «info bison»."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Copyright \\(co 2021 Free Software Foundation, Inc."
msgstr "Drepturi de autor \\(co 2021 Free Software Foundation, Inc."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software; see the source for copying conditions.  There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
"Acesta este software liber; consultați sursa pentru condițiile de copiere. "
"NU există NICIO garanție; nici măcar pentru COMERCIALIZARE sau POTRIVIRE "
"PENTRU UN ANUMIT SCOP."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<lex>(1), B<flex>(1), B<yacc>(1)."
msgstr "B<lex>(1), B<flex>(1), B<yacc>(1)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The full documentation for B<bison> is maintained as a Texinfo manual.  If "
"the B<info> and B<bison> programs are properly installed at your site, the "
"command"
msgstr ""
"Documentația completă pentru B<bison> este menținută ca un manual Texinfo. "
"Dacă programele B<info> și B<bison> sunt instalate corect în sistemul dvs., "
"comanda"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info bison>"
msgstr "B<info bison>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "septembrie 2022"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Report translation bugs to E<lt>https://translationproject.org/team/E<gt>."
msgstr ""
"Raportați orice erori de traducere la: E<lt>https://translationproject.org/"
"team/ro.htmlE<gt>"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "octombrie 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "February 2023"
msgstr "februarie 2023"
