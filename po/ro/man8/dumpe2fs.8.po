# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-12-06 17:57+0100\n"
"PO-Revision-Date: 2023-10-29 16:22+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DUMPE2FS"
msgstr "DUMPE2FS"

#. type: TH
#: archlinux debian-bookworm fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "May 2024"
msgstr "mai 2024"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "E2fsprogs version 1.47.1"
msgstr "E2fsprogs versiunea 1.47.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "dumpe2fs - dump ext2/ext3/ext4 file system information"
msgstr ""
"dumpe2fs - afișează informații despre sistemul de fișiere ext2/ext3/ext4"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<dumpe2fs> [ B<-bfghixV> ] [ B<-o superblock=>I<superblock> ] [ B<-o "
"blocksize=>I<blocksize> ] I<device>"
msgstr ""
"B<dumpe2fs> [ B<-bfghixV> ] [ B<-o superblock=>I<super-bloc> ] [ B<-o "
"blocksize=>I<dimensiune-bloc> ] I<dispozitiv>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<dumpe2fs> prints the super block and blocks group information for the file "
"system present on I<device.>"
msgstr ""
"B<dumpe2fs> afișează informațiile despre super-blocul și grupul de blocuri "
"pentru sistemul de fișiere prezent pe I<dispozitiv.>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<Note:> When used with a mounted file system, the printed information may "
"be old or inconsistent."
msgstr ""
"B<Notă:> Atunci când se utilizează cu un sistem de fișiere montat, "
"informațiile afișate pot fi vechi sau incoerente."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print the blocks which are reserved as bad in the file system."
msgstr ""
"afișează blocurile care sunt rezervate ca fiind defectuoase în sistemul de "
"fișiere."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o superblock=>I<superblock>"
msgstr "B<-o superblock=>I<super-bloc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use the block I<superblock> when examining the file system.  This option is "
"not usually needed except by a file system wizard who is examining the "
"remains of a very badly corrupted file system."
msgstr ""
"utilizează blocul I<super-bloc> atunci când examinează sistemul de fișiere.  "
"Această opțiune nu este de obicei necesară decât pentru un expert în sisteme "
"de fișiere care examinează rămășițele unui sistem de fișiere foarte grav "
"corupt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o blocksize=>I<blocksize>"
msgstr "B<-o blocksize=>I<dimensiune-bloc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use blocks of I<blocksize> bytes when examining the file system.  This "
"option is not usually needed except by a file system wizard who is examining "
"the remains of a very badly corrupted file system."
msgstr ""
"utilizează blocuri de I<dimensiune-bloc> octeți atunci când examinează "
"sistemul de fișiere.  Această opțiune nu este de obicei necesară decât "
"pentru un expert în sisteme de fișiere care examinează rămășițele unui "
"sistem de fișiere foarte grav corupt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"force dumpe2fs to display a file system even though it may have some file "
"system feature flags which dumpe2fs may not understand (and which can cause "
"some of dumpe2fs's display to be suspect)."
msgstr ""
"forțează «dumpe2fs» să afișeze un sistem de fișiere, chiar dacă acesta poate "
"avea unele fanioane de caracteristici ale sistemului de fișiere pe care "
"«dumpe2fs» nu le înțelege (și care pot face ca unele dintre afișările lui "
"«dumpe2fs» să fie suspecte)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-g>"
msgstr "B<-g>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"display the group descriptor information in a machine readable colon-"
"separated value format.  The fields displayed are the group number; the "
"number of the first block in the group; the superblock location (or -1 if "
"not present); the range of blocks used by the group descriptors (or -1 if "
"not present); the block bitmap location; the inode bitmap location; and the "
"range of blocks used by the inode table."
msgstr ""
"afișează informațiile despre descriptorul de grup într-un format de valori "
"separate prin două puncte care poate fi citit de mașină.  Câmpurile afișate "
"sunt: numărul grupului; numărul primului bloc din grup; locația super-"
"blocului (sau -1 dacă nu este prezent); intervalul de blocuri utilizat de "
"descriptorii de grup (sau -1 dacă nu este prezent); locația hărții de biți a "
"blocurilor; locația hărții de biți a nodurilor-i; și intervalul de blocuri "
"utilizat de tabelul de noduri-i."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"only display the superblock information and not any of the block group "
"descriptor detail information."
msgstr ""
"afișează numai informațiile despre super-bloc și nu și informațiile "
"detaliate despre descriptorii de grupuri de blocuri."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"display the file system data from an image file created by B<e2image>, using "
"I<device> as the pathname to the image file."
msgstr ""
"afișează datele sistemului de fișiere dintr-un fișier imagine creat de "
"B<e2image>, utilizând I<dispozitiv> ca nume de rută către fișierul imagine."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the B<mmp> feature is enabled on the file system, check if I<device> is "
"in use by another node, see B<e2mmpstatus>(8)  for full details.  If used "
"together with the B<-i> option, only the MMP block information is printed."
msgstr ""
"Dacă caracteristica B<mmp> este activată pe sistemul de fișiere, verifică "
"dacă I<dispozitivul> este utilizat de un alt nod (mașină), consultați "
"B<e2mmpstatus>(8) pentru detalii complete.  Dacă se utilizează împreună cu "
"opțiunea B<-i>, se afișează numai informațiile despre blocul MMP."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>"
msgstr "B<-x>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"print the detailed group information block numbers in hexadecimal format"
msgstr ""
"afișează informații detaliate despre grupurile de blocuri cu numere în "
"format hexazecimal"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print the version number of B<dumpe2fs> and exit."
msgstr "afișează numărul versiunii de B<dumpe2fs> și iese."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXIT CODE"
msgstr "COD DE IEȘIRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<dumpe2fs> exits with a return code of 0 if the operation completed without "
"errors.  It will exit with a non-zero return code if there are any errors, "
"such as problems reading a valid superblock, bad checksums, or if the device "
"is in use by another node and B<-m> is specified."
msgstr ""
"B<dumpe2fs> iese cu un cod de ieșire 0 dacă operațiunea s-a încheiat fără "
"erori.  Va ieși cu un cod de ieșire diferit de zero în cazul în care există "
"erori, cum ar fi probleme la citirea unui super-bloc valid, sume de control "
"necorespunzătoare sau dacă dispozitivul este utilizat de un alt nod (mașină) "
"și este specificată opțiunea B<-m>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You may need to know the physical file system structure to understand the "
"output."
msgstr ""
"Este posibil să fie necesar să cunoașteți structura fizică a sistemului de "
"fișiere pentru a înțelege rezultatul."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<dumpe2fs> was written by Remy Card E<lt>Remy.Card@linux.orgE<gt>.  It is "
"currently being maintained by Theodore Ts'o E<lt>tytso@alum.mit.eduE<gt>."
msgstr ""
"B<dumpe2fs> a fost scris de Remy Card E<lt>Remy.Card@linux.orgE<gt>.  În "
"prezent, este întreținut de Theodore Ts'o E<lt>tytso@alum.mit.eduE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITATE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<dumpe2fs> is part of the e2fsprogs package and is available from http://"
"e2fsprogs.sourceforge.net."
msgstr ""
"B<e2label> face parte din pachetul „e2fsprogs” și este disponibil la http://"
"e2fsprogs.sourceforge.net."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<e2fsck>(8), B<e2mmpstatus>(8), B<mke2fs>(8), B<tune2fs>(8).  B<ext4>(5)"
msgstr ""
"B<e2fsck>(8), B<e2mmpstatus>(8), B<mke2fs>(8), B<tune2fs>(8).  B<ext4>(5)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "E2fsprogs version 1.47.1-rc2"
msgstr "E2fsprogs versiunea 1.47.1-rc2"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "November 2024"
msgstr "noiembrie 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "E2fsprogs version 1.47.2-rc1"
msgstr "E2fsprogs versiunea 1.47.2-rc1"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "februarie 2023"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr "E2fsprogs versiunea 1.47.0"
