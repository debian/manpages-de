# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-09-18 20:37+0200\n"
"PO-Revision-Date: 2024-11-06 12:52+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.4\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "RPM-PRIORESET"
msgstr "RPM-PRIORESET"

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "28 Jan 2020"
msgstr "28 ianuarie 2020"

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"rpm-plugin-prioreset - Plugin for the RPM Package Manager to fix issues with "
"priorities of deamons on SysV init"
msgstr ""
"rpm-plugin-prioreset - modul pentru gestionarul de pachete RPM pentru a "
"rezolva problemele cu prioritățile demonilor din init (inițierea, și "
"programul) SysV"

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Descriere"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"In general scriptlets run with the same priority as rpm itself.  However on "
"legacy SysV init systems, properties of the parent process can be inherited "
"by the actual daemons on restart.  As a result daemons may end up with "
"unwanted nice or ionice values.  This plugin resets the scriptlet process "
"priorities after forking, and can be used to counter that effect.  Should "
"not be used with systemd because it\\[aq]s not needed there, and the effect "
"is counter-productive."
msgstr ""
"În general, scripturile rulează cu aceeași prioritate ca rpm însuși. Cu "
"toate acestea, pe sistemele init-SysV moștenite, proprietățile procesului "
"părinte pot fi moștenite de către demonii reali la repornire. Ca urmare, "
"demonii pot ajunge să aibă valori nedorite „nice” sau „ionice”. Acest modul "
"reinițializează prioritățile procesului scriptului după bifurcare și poate "
"fi utilizat pentru a contracara acest efect. Nu ar trebui utilizat cu "
"systemd, deoarece nu este necesar acolo, iar efectul este contraproductiv."

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Configuration"
msgstr "Configurare"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"There are currently no options for this plugin in particular.  See \\f[B]rpm-"
"plugins\\f[R](8) on how to control plugins in general.\\fR"
msgstr ""
"În prezent nu există opțiuni pentru acest modul în special. Consultați "
"\\f[B]rpm-plugins\\f[R](8) despre cum să controlați modulele în general."

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "\\f[B]rpm\\f[R](8), \\f[B]rpm-plugins\\f[R](8)\\fR"
msgstr "\\f[B]rpm\\f[R](8), \\f[B]rpm-plugins\\f[R](8)\\fR"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "\\f[I]rpm\\f[R](8) \\f[I]rpm-plugins\\f[R](8)\\fR"
msgstr "\\f[I]rpm\\f[R](8) \\f[I]rpm-plugins\\f[R](8)\\fR"
