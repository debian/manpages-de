# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-09-06 18:28+0200\n"
"PO-Revision-Date: 2024-01-28 19:29+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sane-umax1220u"
msgstr "sane-umax1220u"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "14 Jul 2008"
msgstr "14 iulie 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sane-umax"
msgstr "sane-umax"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"sane-umax1220u - SANE backend for the UMAX Astra 1220U and similar scanners"
msgstr ""
"sane-umax1220u - controlor SANE pentru UMAX Astra 1220U și scanere similare"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sane-umax1220> library implements a SANE (Scanner Access Now Easy) "
"backend for the the UMAX Astra 1220U and similar scanners."
msgstr ""
"Biblioteca B<sane-umax1220> implementează un controlor SANE (Scanner Access "
"Now Easy) pentru scanerele UMAX Astra 1220U și altele similare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For more information on this backend, please visit I<http://umax1220u-"
"sane.sourceforge.net/>."
msgstr ""
"Pentru mai multe informații despre acest controlor, vă rugăm să vizitați "
"I<http://umax1220u-sane.sourceforge.net/>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "UMAX ASTRA 1600U/2000U/2100U SUPPORT"
msgstr "COMPATIBILITATE UMAX ASTRA 1600U/2000U/2100U"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This backend is also able to drive the UMAX Astra 1600U/2000U/2100U.  The "
"2100U is confirmed to work. For the other scanners no reports have been "
"received yet. Please contact us and tell us if your scanner works (I<sane-"
"devel@alioth-lists.debian.net>)."
msgstr ""
"Acest controlor este, de asemenea, capabil să controleze UMAX Astra 1600U/"
"2000U/2100U. S-a confirmat că funcționează și cu 2100U. Pentru celelalte "
"scanere nu au fost primite încă rapoarte. Vă rugăm să ne contactați și să ne "
"spuneți dacă scanerul dvs. funcționează (I<sane-devel@alioth-"
"lists.debian.net>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Usually, no manual configuration is necessary. The configuration file for "
"this backend resides in I</etc/sane.d/umax1220u.conf>."
msgstr ""
"De obicei, nu este necesară nicio configurare manuală. Fișierul de "
"configurare pentru acest controlor se află în I</etc/sane.d/umax1220u.conf>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Its content is a list of device names that correspond to UMAX Astra "
"scanners.  Empty lines and lines starting with a hash mark (#) are ignored. "
"A sample configuration file is shown below:"
msgstr ""
"Conținutul său este o listă de nume de dispozitive care corespund scanerelor "
"UMAX Astra.  Liniile goale și liniile care încep cu un simbol hash (#) sunt "
"ignorate. Un exemplu de fișier de configurare este prezentat mai jos:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
" #usb vendor product\n"
" usb 0x1606 0x0010\n"
" # Device list for non-linux systems\n"
" /dev/scanner\n"
" /dev/usb/scanner0\n"
msgstr ""
" #usb fabricant produs\n"
" usb 0x1606 0x0010\n"
" # Lista de dispozitive pentru sistemele non-linux\n"
" /dev/scanner\n"
" /dev/usb/scanner0\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<sane-usb>(5)  for information on how to set the access permissions on "
"the usb device files."
msgstr ""
"Consultați B<sane-usb>(5) pentru informații despre cum să stabiliți "
"permisiunile de acces la fișierele dispozitivului USB."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "The backend configuration file:"
msgstr "Fișierul de configurare al controlorului:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</etc/sane.d/umax1220u.conf>"
msgstr "I</etc/sane.d/umax1220u.conf>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "The static library implementing this backend:"
msgstr "Biblioteca statică care implementează acest controlor:"

#. type: Plain text
#: archlinux
msgid "I</usr/lib/sane/libsane-umax1220u.a>"
msgstr "I</usr/lib/sane/libsane-umax1220u.a>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "The shared library implementing this backend:"
msgstr "Biblioteca partajată care implementează acest controlor:"

#. type: Plain text
#: archlinux
msgid ""
"I</usr/lib/sane/libsane-umax1220u.so> (present on systems that support "
"dynamic loading)"
msgstr ""
"I</usr/lib/sane/libsane-umax1220u.so> (prezent pe sistemele care acceptă "
"încărcarea dinamică)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_UMAX1220U>"
msgstr "B<SANE_DEBUG_UMAX1220U>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend. E.g., a value of 128 "
"requests all debug output to be printed. Smaller levels reduce verbosity:"
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor. "
"De exemplu, o valoare de 128 solicită imprimarea tuturor datelor de "
"depanare. Nivelurile mai mici reduc gradul de detaliere al informațiilor:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<SANE_DEBUG_UMAX1220U> values:"
msgstr "Valorile B<SANE_DEBUG_UMAX1220U>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f(CRNumber  Remark\n"
" 1       print failures\n"
" 2       print information\n"
" 3       print high-level function calls\n"
" 4       print high-level function checkpoints\n"
" 9       print mid-level function calls\n"
" 10      print mid-level function checkpoints\n"
" 80      print protocol-level function entry\n"
" 90      print protocol-level function exit\\fR\n"
msgstr ""
"\\f(CRNumăr   Observații\n"
" 1       imprimă eșecurile\n"
" 2       imprimă informații\n"
" 3       imprimă apelurile de funcții de nivel înalt\n"
" 4       imprimă punctele de control ale funcțiilor de nivel înalt\n"
" 9       imprimă apelurile de funcții de nivel mediu\n"
" 10      imprimă punctele de control ale funcțiilor de nivel mediu\n"
" 80      imprimă intrarea funcției la nivel de protocol\n"
" 90      imprimă ieșirea funcției la nivel de protocol\\fR\n"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Example:"
msgstr "Exemplu:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "export SANE_DEBUG_UMAX1220U=10"
msgstr "export SANE_DEBUG_UMAX1220U=10"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "KNOWN BUGS"
msgstr "ERORI CUNOSCUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "600 dpi scanning may fail for large image sizes."
msgstr "Scanarea la 600 dpi poate eșua în cazul imaginilor de dimensiuni mari."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you keep getting I/O errors, try cycling the power on your scanner to "
"reset it."
msgstr ""
"Dacă primiți în continuare erori de In/Ieș, încercați să întrerupeți "
"alimentarea scanerului pentru a-l reinițializa."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There is no way to cancel a scan, since the driver ignores B<sane_cancel>()."
msgstr ""
"Nu există nicio modalitate de a anula o scanare, deoarece controlorul ignoră "
"B<sane_cancel>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you try scanning an image which is too small, you will get I/O errors. Be "
"sure to adjust the scan area before doing a scan, since by default, the scan "
"area is zero."
msgstr ""
"Dacă încercați să scanați o imagine care este prea mică, veți obține erori "
"de In/Ieș. Asigurați-vă că ajustați zona de scanare înainte de a efectua o "
"scanare, deoarece, în mod implicit, zona de scanare este zero."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sane>(7), B<sane-usb>(5)"
msgstr "B<sane>(7), B<sane-usb>(5)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "(Old) homepage:"
msgstr "Pagina principală (veche):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<http://umax1220u-sane.sourceforge.net/>"
msgstr "I<http://umax1220u-sane.sourceforge.net/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Marcio Luis Teixeira E<lt>I<marciot@users.sourceforge.net>E<gt>"
msgstr "Marcio Luis Teixeira E<lt>I<marciot@users.sourceforge.net>E<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EMAIL-CONTACT"
msgstr "ADRESA_DE_EMAIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<sane-devel@alioth-lists.debian.net>"
msgstr "I<sane-devel@alioth-lists.debian.net>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This backend isn't actively maintained. Nevertheless, bug reports and "
"comments should be sent to the sane-devel mailing list.  When reporting "
"bugs, please run the backend with B<SANE_DEBUG_UMAX1220U> set to 10 and "
"attach a copy of the log messages."
msgstr ""
"Acest controlor nu este întreținut în mod activ. Cu toate acestea, "
"rapoartele de erori și comentariile trebuie trimise la lista de discuții "
"sane-devel. Atunci când raportați erori, vă rugăm să rulați controlorul cu "
"B<SANE_DEBUG_UMAX1220U> stabilit la 10 și să atașați o copie a mesajelor de "
"jurnal."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-umax1220u.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-umax1220u.a>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I</usr/lib/x86_64-linux-gnu/sane/libsane-umax1220u.so> (present on systems "
"that support dynamic loading)"
msgstr ""
"I</usr/lib/x86_64-linux-gnu/sane/libsane-umax1220u.so> (prezent pe sistemele "
"care acceptă încărcarea dinamică)"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I</usr/lib64/sane/libsane-umax1220u.a>"
msgstr "I</usr/lib64/sane/libsane-umax1220u.a>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I</usr/lib64/sane/libsane-umax1220u.so> (present on systems that support "
"dynamic loading)"
msgstr ""
"I</usr/lib64/sane/libsane-umax1220u.so> (prezent pe sistemele care acceptă "
"încărcarea dinamică)"
