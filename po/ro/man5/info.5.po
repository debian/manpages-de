# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-09-06 18:16+0200\n"
"PO-Revision-Date: 2023-07-06 14:55+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "INFO"
msgstr "INFORMAȚII"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GNU Info"
msgstr "GNU Info"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FSF"
msgstr "FSF"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "info - readable online documentation"
msgstr "info - documentație online care poate fi citită"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Info file format is an easily-parsable representation for online "
"documents.  It can be read by B<emacs>(1)  and B<info>(1)  among other "
"programs."
msgstr ""
"Formatul de fișier Info este o reprezentare ușor de interpretat pentru "
"documentele online.  Acesta poate fi citit de B<emacs>(1) și B<info>(1), "
"printre alte programe."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Info files are usually created from B<texinfo>(5)  sources by "
"B<makeinfo>(1) , but can be created from scratch if so desired."
msgstr ""
"Fișierele Info sunt de obicei create din sursele B<texinfo>(5) de către "
"B<makeinfo>(1) , dar pot fi create de la zero dacă se dorește acest lucru."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For a full description of the Texinfo language and associated tools, please "
"see the Texinfo manual (written in Texinfo itself).  Most likely, running "
"this command from your shell:"
msgstr ""
"Pentru o descriere completă a limbajului Texinfo și a instrumentelor "
"asociate, vă rugăm să consultați manualul Texinfo (scris chiar în Texinfo). "
"Cel mai probabil, executând această comandă din shell:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "info texinfo\n"
msgstr "«info texinfo»\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or this key sequence from inside Emacs:"
msgstr "sau această secvență de taste în Emacs:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "M-x info RET m texinfo RET\n"
msgstr "M-x info RET m texinfo RET\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "will get you there."
msgstr "vă va duce acolo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITATE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "http://www.gnu.org/software/texinfo/"
msgstr "http://www.gnu.org/software/texinfo/"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Please send bug reports to bug-texinfo@gnu.org, general questions and "
"discussion to help-texinfo@gnu.org."
msgstr ""
"Trimiteți prin e-mail rapoartele de eroare la bug-texinfo@gnu.org, întrebări "
"generale și discuții la help-texinfo@gnu.org."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info>(1), B<install-info>(1), B<makeinfo>(1), B<texi2dvi>(1),"
msgstr "B<info>(1), B<install-info>(1), B<makeinfo>(1), B<texi2dvi>(1),"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<texindex>(1)."
msgstr "B<texindex>(1)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<emacs>(1), B<tex>(1)."
msgstr "B<emacs>(1), B<tex>(1)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<texinfo>(5)."
msgstr "B<texinfo>(5)."

#. type: Plain text
#: debian-bookworm
msgid ""
"The Info file format is an easily-parsable representation for online "
"documents.  It can be read by I<emacs(1)> and I<info(1)> among other "
"programs."
msgstr ""
"Formatul de fișier Info este o reprezentare ușor de interpretat pentru "
"documentele online.  Acesta poate fi citit de I<emacs(1)> și I<info(1)>, "
"printre alte programe."

#. type: Plain text
#: debian-bookworm
msgid ""
"Info files are usually created from I<texinfo(5)> sources by I<makeinfo(1)>, "
"but can be created from scratch if so desired."
msgstr ""
"Fișierele Info sunt de obicei create din sursele I<texinfo(5)> de către "
"I<makeinfo(1)>, dar pot fi create de la zero dacă se dorește acest lucru."

#. type: Plain text
#: debian-bookworm
msgid "info(1), install-info(1), makeinfo(1), texi2dvi(1),"
msgstr "info(1), install-info(1), makeinfo(1), texi2dvi(1),"

#. type: Plain text
#: debian-bookworm
msgid "texindex(1)."
msgstr "texindex(1)."

#. type: Plain text
#: debian-bookworm
msgid "emacs(1), tex(1)."
msgstr "emacs(1), tex(1)."

#. type: Plain text
#: debian-bookworm
msgid "texinfo(5)."
msgstr "texinfo(5)."
