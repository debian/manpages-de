# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-09-06 18:27+0200\n"
"PO-Revision-Date: 2023-12-18 09:38+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sane-mustek_usb2"
msgstr "sane-mustek_usb2"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "13 Jul 2008"
msgstr "13 iulie 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sane-mustek_usb2 - SANE backend for SQ113 based USB flatbed scanners"
msgstr ""
"sane-mustek_usb2 - controlor SANE pentru scanere plate USB bazate pe SQ113"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sane-mustek_usb2> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to USB flatbed scanners based on the Service & "
"Quality SQ113 chipset. At the moment, only the Mustek BearPaw 2448 TA Pro is "
"supported. It's planned to add support for other scanners that are based on "
"the SQ113 and maybe SQ11 chip. For more details, see the B<sane-mustek_usb2> "
"backend homepage: I<http://www.meier-geinitz.de/sane/mustek_usb2-backend/>."
msgstr ""
"Biblioteca B<sane-mustek_usb2> implementează un controlor SANE (Scanner "
"Access Now Easy) care oferă acces la scanerele plate USB bazate pe chipset-"
"ul SQ113 Service & Quality. Momentan, este acceptat doar Mustek BearPaw 2448 "
"TA Pro. Este planificat să se adauge suport pentru alte scanere care se "
"bazează pe cipul SQ113 și poate SQ11. Pentru mai multe detalii, consultați "
"pagina principală a controlorului B<sane-mustek_usb2>: I<http://www.meier-"
"geinitz.de/sane/mustek_usb2-backend/>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is BETA software. Especially if you test new or untested scanners, keep "
"your hand at the scanner's plug and unplug it, if the head bumps at the end "
"of the scan area."
msgstr ""
"Acesta este un software BETA. Mai ales dacă testați scanere noi sau "
"netestate, țineți mâna pe ștecherul scanerului și scoateți-l din priză, dacă "
"capul de citire se lovește la capătul zonei de scanare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you own a scanner other than the ones listed on the mustek_usb2 homepage "
"that works with this backend, please let me know this by sending the "
"scanner's exact model name and the USB vendor and device ids (e.g. from "
"B<sane-find-scanner>(1)  or syslog) to me. Even if the scanner's name is "
"only slightly different from the models already listed as supported, please "
"let me know."
msgstr ""
"Dacă dețineți un scaner, altul decât cele listate pe pagina principală a "
"mustek_usb2, care funcționează cu acest controlor, vă rog să-mi comunicați "
"acest lucru trimițându-mi numele exact al modelului de scaner și ID-urile "
"producătorului și dispozitivului USB (de exemplu, din B<sane-find-"
"scanner>(1) sau syslog). Chiar dacă numele scanerului este doar puțin "
"diferit de cel al modelelor deja listate ca fiind acceptate, vă rog să mă "
"anunțați."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBUSB ISSUES"
msgstr "PROBLEME DE LIBUSB"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Please use libusb-0.1.8 or later. Without libusb or with older libusb "
"versions all kinds of trouble can be expected. The scanner should be found "
"by B<sane-find-scanner>(1)  without further actions. For setting permissions "
"and general USB information, look at B<sane-usb>(5)."
msgstr ""
"Vă rugăm să utilizați libusb-0.1.8 sau o versiune ulterioară. Fără libusb "
"sau cu versiuni mai vechi de libusb se pot aștepta tot felul de probleme. "
"Scanerul ar trebui să fie găsit de B<sane-find-scanner>(1) fără alte "
"acțiuni. Pentru stabilirea permisiunilor și informații generale despre USB, "
"consultați B<sane-usb>(5)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-mustek_usb2.a>"
msgstr "I</usr/lib/sane/libsane-mustek_usb2.a>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Biblioteca statică care implementează acest controlor."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-mustek_usb2.so>"
msgstr "I</usr/lib/sane/libsane-mustek_usb2.so>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Biblioteca partajată care implementează acest controlor (prezentă pe "
"sistemele care acceptă încărcare dinamică)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_MUSTEK_USB2>"
msgstr "B<SANE_DEBUG_MUSTEK_USB2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor. "
"Nivelurile mai mari de depanare cresc cantitatea de detalii informative a "
"ieșirii."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Example: export SANE_DEBUG_MUSTEK_USB2=4"
msgstr "Exemplu: export SANE_DEBUG_MUSTEK_USB2=4"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sane>(7), B<sane-usb>(5), B<sane-plustek>(5), B<sane-ma1509>(5), B<sane-"
"mustek_usb>(5), B<sane-mustek>(5), B<sane-mustek_pp>(5), B<sane-find-"
"scanner>(1)"
msgstr ""
"B<sane>(7), B<sane-usb>(5), B<sane-plustek>(5), B<sane-ma1509>(5), B<sane-"
"mustek_usb>(5), B<sane-mustek>(5), B<sane-mustek_pp>(5), B<sane-find-"
"scanner>(1)"

#. type: Plain text
#: archlinux
msgid "I</usr/share/doc/sane/mustek_usb2/mustek_usb2.CHANGES>"
msgstr "I</usr/share/doc/sane/mustek_usb2/mustek_usb2.CHANGES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<http://www.meier-geinitz.de/sane/mustek_usb2-backend/>"
msgstr "I<http://www.meier-geinitz.de/sane/mustek_usb2-backend/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The driver has been written Roy Zhou, Jack Xu, and Vinci Cen from Mustek."
msgstr ""
"Controlorul a fost scris de Roy Zhou, Jack Xu și Vinci Cen de la Mustek."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Adjustments to SANE by Henning Meier-Geinitz."
msgstr "Ajustări la SANE de Henning Meier-Geinitz."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Please contact me if you find a bug or missing feature: E<lt>I<henning@meier-"
"geinitz.de>E<gt>."
msgstr ""
"Vă rog să mă contactați dacă găsiți o eroare sau o caracteristică lipsă: "
"E<lt>I<henning@meier-geinitz.de>E<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Please send a debug log if your scanner isn't detected correctly (see "
"B<SANE_DEBUG_MUSTEK_USB2> above)."
msgstr ""
"Vă rugăm să trimiteți un jurnal de depanare dacă scanerul dvs. nu este "
"detectat corect (a se vedea B<SANE_DEBUG_MUSTEK_USB2> de mai sus)."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-mustek_usb2.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-mustek_usb2.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-mustek_usb2.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-mustek_usb2.so>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "I</usr/share/doc/libsane/mustek_usb2/mustek_usb2.CHANGES>"
msgstr "I</usr/share/doc/libsane/mustek_usb2/mustek_usb2.CHANGES>"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-mustek_usb2.a>"
msgstr "I</usr/lib64/sane/libsane-mustek_usb2.a>"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-mustek_usb2.so>"
msgstr "I</usr/lib64/sane/libsane-mustek_usb2.so>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "I</usr/share/doc/sane-backends/mustek_usb2/mustek_usb2.CHANGES>"
msgstr "I</usr/share/doc/sane-backends/mustek_usb2/mustek_usb2.CHANGES>"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I</usr/share/doc/packages/sane-backends/mustek_usb2/mustek_usb2.CHANGES>"
msgstr ""
"I</usr/share/doc/packages/sane-backends/mustek_usb2/mustek_usb2.CHANGES>"
