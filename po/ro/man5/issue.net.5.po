# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-06-27 19:33+0200\n"
"PO-Revision-Date: 2023-05-23 02:26+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "May 22, 1994"
msgstr "22 mai 1994"

#. type: Dt
#: debian-bookworm debian-unstable
#, no-wrap
msgid "ISSUE.NET 5"
msgstr "ISSUE.NET 5"

#. type: Os
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Linux NetKit (0.17)"
msgstr "Linux NetKit (0.17)"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm issue.net >"
msgstr "E<.Nm issue.net >"

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "identification file for telnet sessions"
msgstr "fișier de identificare pentru sesiunile telnet"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The file E<.Pa /etc/issue.net> is a text file which contains a message or "
"system identification to be printed before the login prompt of a telnet "
"session. It may contain various `%\\&\\-char' sequences. The following "
"sequences are supported by E<.Ic telnetd>:"
msgstr ""
"Fișierul E<.Pa /etc/issue.net> este un fișier text care conține un mesaj sau "
"o identificare a sistemului care urmează să fie imprimată înainte de "
"promptul de conectare a unei sesiuni telnet. Acesta poate conține diverse "
"secvențe „%\\&\\-char”. Următoarele secvențe sunt acceptate de E<.Ic "
"telnetd>:"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&t"
msgstr "%\\&t"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- show the current tty"
msgstr "- afișează tty-ul curent"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&h"
msgstr "%\\&h"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- show the system node name (FQDN)"
msgstr "- afișează numele nodului de sistem (FQDN)"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&D"
msgstr "%\\&D"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- show the name of the NIS domain"
msgstr "- afișează numele domeniului NIS"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&d"
msgstr "%\\&d"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- show the current time and date"
msgstr "- afișează data și ora curentă"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&s"
msgstr "%\\&s"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- show the name of the operating system"
msgstr "- afișează numele sistemului de operare"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&m"
msgstr "%\\&m"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- show the machine (hardware) type"
msgstr "- afișează tipul mașinii (echipamentul)"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&r"
msgstr "%\\&r"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- show the operating system release"
msgstr "- afișează numărul de lansare al sistemului de operare"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&v"
msgstr "%\\&v"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- show the operating system version"
msgstr "- afișează versiunea sistemului de operare"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "%\\&%"
msgstr "%\\&%"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "- display a single '%' character"
msgstr "- afișează un singur caracter „%”"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Pa /etc/issue.net>"
msgstr "E<.Pa /etc/issue.net>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Xr in.telnetd 8>"
msgstr "E<.Xr in.telnetd 8>"
