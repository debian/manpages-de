# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:04+0100\n"
"PO-Revision-Date: 2024-07-01 17:30+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "__malloc_hook"
msgstr "__malloc_hook"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 iunie 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"__malloc_hook, __malloc_initialize_hook, __memalign_hook, __free_hook, "
"__realloc_hook, __after_morecore_hook - malloc debugging variables "
"(DEPRECATED)"
msgstr ""
"__malloc_hook, __malloc_initialize_hook, __memalign_hook, __free_hook, "
"__realloc_hook, __after_morecore_hook - variabile de depanare malloc "
"(DEPRECIATE)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>malloc.hE<gt>>\n"
msgstr "B<#include E<lt>malloc.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void *(*volatile __malloc_hook)(size_t >I<size>B<, const void *>I<caller>B<);>\n"
msgstr "B<void *(*volatile __malloc_hook)(size_t >I<size>B<, const void *>I<caller>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void *(*volatile __realloc_hook)(void *>I<ptr>B<, size_t >I<size>B<,>\n"
"B<                         const void *>I<caller>B<);>\n"
msgstr ""
"B<void *(*volatile __realloc_hook)(void *>I<ptr>B<, size_t >I<size>B<,>\n"
"B<                         const void *>I<caller>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void *(*volatile __memalign_hook)(size_t >I<alignment>B<, size_t >I<size>B<,>\n"
"B<                         const void *>I<caller>B<);>\n"
msgstr ""
"B<void *(*volatile __memalign_hook)(size_t >I<alignment>B<, size_t >I<size>B<,>\n"
"B<                         const void *>I<caller>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void (*volatile __free_hook)(void *>I<ptr>B<, const void *>I<caller>B<);>\n"
msgstr "B<void (*volatile __free_hook)(void *>I<ptr>B<, const void *>I<caller>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void (*__malloc_initialize_hook)(void);>\n"
msgstr "B<void (*__malloc_initialize_hook)(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void (*volatile __after_morecore_hook)(void);>\n"
msgstr "B<void (*volatile __after_morecore_hook)(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The GNU C library lets you modify the behavior of B<malloc>(3), "
"B<realloc>(3), and B<free>(3)  by specifying appropriate hook functions.  "
"You can use these hooks to help you debug programs that use dynamic memory "
"allocation, for example."
msgstr ""
"Biblioteca GNU C vă permite să modificați comportamentul lui B<malloc>(3), "
"B<realloc>(3) și B<free>(3) prin specificarea unor funcții cârlig "
"corespunzătoare. Puteți utiliza aceste cârlige pentru a vă ajuta la "
"depanarea programelor care utilizează alocarea dinamică a memoriei, de "
"exemplu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The variable B<__malloc_initialize_hook> points at a function that is called "
"once when the malloc implementation is initialized.  This is a weak "
"variable, so it can be overridden in the application with a definition like "
"the following:"
msgstr ""
"Variabila B<__malloc_initialize_hook> indică o funcție care este apelată o "
"singură dată atunci când este inițializată implementarea malloc. Aceasta "
"este o variabilă slabă, astfel încât poate fi suprascrisă în aplicație cu o "
"definiție ca cea de mai jos:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
msgstr "void (*__malloc_initialize_hook)(void) = my_init_hook;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Now the function I<my_init_hook>()  can do the initialization of all hooks."
msgstr ""
"Acum, funcția I<my_init_hook>() poate face inițializarea tuturor cârligelor."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The four functions pointed to by B<__malloc_hook>, B<__realloc_hook>, "
"B<__memalign_hook>, B<__free_hook> have a prototype like the functions "
"B<malloc>(3), B<realloc>(3), B<memalign>(3), B<free>(3), respectively, "
"except that they have a final argument I<caller> that gives the address of "
"the caller of B<malloc>(3), etc."
msgstr ""
"Cele patru funcții indicate de B<__malloc_hook>, B<__realloc_hook>, "
"B<__memalign_hook>, B<__free_hook> au un prototip asemănător funcțiilor "
"B<malloc>(3), B<realloc>(3), B<memalign>(3), B<free>(3), respectiv "
"B<memalign>(3), cu excepția faptului că acestea au un argument final "
"I<caller> care oferă adresa apelantului lui B<malloc>(3), etc."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The variable B<__after_morecore_hook> points at a function that is called "
"each time after B<sbrk>(2)  was asked for more memory."
msgstr ""
"Variabila B<__after_morecore_hook> indică o funcție care este apelată de "
"fiecare dată după ce lui B<sbrk>(2) i s-a cerut mai multă memorie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#.  https://bugzilla.redhat.com/show_bug.cgi?id=450187
#.  http://sourceware.org/bugzilla/show_bug.cgi?id=9957
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The use of these hook functions is not safe in multithreaded programs, and "
"they are now deprecated.  From glibc 2.24 onwards, the "
"B<__malloc_initialize_hook> variable has been removed from the API, and from "
"glibc 2.34 onwards, all the hook variables have been removed from the API.  "
"Programmers should instead preempt calls to the relevant functions by "
"defining and exporting B<malloc>(), B<free>(), B<realloc>(), and B<calloc>()."
msgstr ""
"Utilizarea acestor funcții cârlig nu este sigură în programele cu mai multe "
"fire, iar acum acestea sunt depreciate. Începând cu glibc 2.24, variabila "
"B<__malloc_initialize_hook> a fost eliminată din API, iar începând cu glibc "
"2.34, toate variabilele cârlig au fost eliminate din API. În schimb, "
"programatorii ar trebui să preîntâmpine apelurile către funcțiile relevante "
"prin definirea și exportul B<malloc>(), B<free>(), B<realloc>() și "
"B<calloc>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Here is a short example of how to use these variables."
msgstr "Iată un scurt exemplu de utilizare a acestor variabile."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"\\&\n"
"/* Prototypes for our hooks */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
"\\&\n"
"/* Variables to save original hooks */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"
"\\&\n"
"/* Override initializing hook from the C library */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
"\\&\n"
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
"\\&\n"
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
"\\&\n"
"    /* Restore all old hooks */\n"
"    __malloc_hook = old_malloc_hook;\n"
"\\&\n"
"    /* Call recursively */\n"
"    result = malloc(size);\n"
"\\&\n"
"    /* Save underlying hooks */\n"
"    old_malloc_hook = __malloc_hook;\n"
"\\&\n"
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) called from %p returns %p\\[rs]n\",\n"
"            size, caller, result);\n"
"\\&\n"
"    /* Restore our own hooks */\n"
"    __malloc_hook = my_malloc_hook;\n"
"\\&\n"
"    return result;\n"
"}\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"\\&\n"
"/* Prototipurile pentru cârligele noastre */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
"\\&\n"
"/* Variabile pentru a salva cârligele originale */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"
"\\&\n"
"/* Suprascrie cârligul de inițializare din biblioteca C */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
"\\&\n"
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
"\\&\n"
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
"\\&\n"
"    /* Restabilește toate cârligele vechi */\n"
"    __malloc_hook = old_malloc_hook;\n"
"\\&\n"
"    /* Apel recursiv */\n"
"    result = malloc(size);\n"
"\\&\n"
"    /* Salvează cârligele de bază */\n"
"    old_malloc_hook = __malloc_hook;\n"
"\\&\n"
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) apelat din %p returnează %p\\[rs]n\",\n"
"            size, caller, result);\n"
"\\&\n"
"    /* Restabilește propriile noastre cârlige*/\n"
"    __malloc_hook = my_malloc_hook;\n"
"\\&\n"
"    return result;\n"
"}\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<mallinfo>(3), B<malloc>(3), B<mcheck>(3), B<mtrace>(3)"
msgstr "B<mallinfo>(3), B<malloc>(3), B<mcheck>(3), B<mtrace>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr "7 ianuarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "These functions are GNU extensions."
msgstr "Aceste funcții sunt extensii GNU."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* Prototypes for our hooks */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
msgstr ""
"/* Prototipurile pentru cârligele noastre */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* Variables to save original hooks */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"
msgstr ""
"/* Variabile pentru a salva cârligele originale */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* Override initializing hook from the C library */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
msgstr ""
"/* Suprascrie cârligul de inițializare din biblioteca C */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
msgstr ""
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
msgstr ""
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Restore all old hooks */\n"
"    __malloc_hook = old_malloc_hook;\n"
msgstr ""
"    /* Restabilește toate cârligele vechi */\n"
"    __malloc_hook = old_malloc_hook;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Call recursively */\n"
"    result = malloc(size);\n"
msgstr ""
"    /* Apel recursiv */\n"
"    result = malloc(size);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Save underlying hooks */\n"
"    old_malloc_hook = __malloc_hook;\n"
msgstr ""
"    /* Salvează cârligele de bază */\n"
"    old_malloc_hook = __malloc_hook;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) called from %p returns %p\\en\",\n"
"            size, caller, result);\n"
msgstr ""
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) apelat din %p returnează %p\\en\",\n"
"            size, caller, result);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Restore our own hooks */\n"
"    __malloc_hook = my_malloc_hook;\n"
msgstr ""
"    /* Restabilește propriile noastre cârlige */\n"
"    __malloc_hook = my_malloc_hook;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    return result;\n"
"}\n"
msgstr ""
"    return result;\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"\\&\n"
"/* Prototypes for our hooks */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
"\\&\n"
"/* Variables to save original hooks */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"
"\\&\n"
"/* Override initializing hook from the C library */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
"\\&\n"
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
"\\&\n"
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
"\\&\n"
"    /* Restore all old hooks */\n"
"    __malloc_hook = old_malloc_hook;\n"
"\\&\n"
"    /* Call recursively */\n"
"    result = malloc(size);\n"
"\\&\n"
"    /* Save underlying hooks */\n"
"    old_malloc_hook = __malloc_hook;\n"
"\\&\n"
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) called from %p returns %p\\en\",\n"
"            size, caller, result);\n"
"\\&\n"
"    /* Restore our own hooks */\n"
"    __malloc_hook = my_malloc_hook;\n"
"\\&\n"
"    return result;\n"
"}\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"\\&\n"
"/* Prototipurile pentru cârligele noastre */\n"
"static void my_init_hook(void);\n"
"static void *my_malloc_hook(size_t, const void *);\n"
"\\&\n"
"/* Variabile pentru a salva cârligele originale */\n"
"static void *(*old_malloc_hook)(size_t, const void *);\n"
"\\&\n"
"/* Suprascrie cârligul de inițializare din biblioteca C */\n"
"void (*__malloc_initialize_hook)(void) = my_init_hook;\n"
"\\&\n"
"static void\n"
"my_init_hook(void)\n"
"{\n"
"    old_malloc_hook = __malloc_hook;\n"
"    __malloc_hook = my_malloc_hook;\n"
"}\n"
"\\&\n"
"static void *\n"
"my_malloc_hook(size_t size, const void *caller)\n"
"{\n"
"    void *result;\n"
"\\&\n"
"    /* Restabilește toate cârligele vechi */\n"
"    __malloc_hook = old_malloc_hook;\n"
"\\&\n"
"    /* Apel recursiv */\n"
"    result = malloc(size);\n"
"\\&\n"
"    /* Salvează cârligele de bază */\n"
"    old_malloc_hook = __malloc_hook;\n"
"\\&\n"
"    /* printf() might call malloc(), so protect it too */\n"
"    printf(\"malloc(%zu) apelat din %p returnează %p\\en\",\n"
"            size, caller, result);\n"
"\\&\n"
"    /* Restabilește propriile noastre cârlige*/\n"
"    __malloc_hook = my_malloc_hook;\n"
"\\&\n"
"    return result;\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
