# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 17:59+0100\n"
"PO-Revision-Date: 2024-07-01 17:04+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "getgrent_r"
msgstr "getgrent_r"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 iunie 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "getgrent_r, fgetgrent_r - get group file entry reentrantly"
msgstr ""
"getgrent_r, fgetgrent_r - obține intrarea fișierului de grup în mod reentrant"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>grp.hE<gt>>\n"
msgstr "B<#include E<lt>grp.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getgrent_r(struct group *restrict >I<gbuf>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct group **restrict >I<gbufp>B<);>\n"
"B<int fgetgrent_r(FILE *restrict >I<stream>B<, struct group *restrict >I<gbuf>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct group **restrict >I<gbufp>B<);>\n"
msgstr ""
"B<int getgrent_r(struct group *restrict >I<gbuf>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct group **restrict >I<gbufp>B<);>\n"
"B<int fgetgrent_r(FILE *restrict >I<stream>B<, struct group *restrict >I<gbuf>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct group **restrict >I<gbufp>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getgrent_r>():"
msgstr "B<getgrent_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<fgetgrent_r>():"
msgstr "B<fgetgrent_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _SVID_SOURCE\n"
msgstr ""
"    Începând cu glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 și versiunile anterioare:\n"
"        _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The functions B<getgrent_r>()  and B<fgetgrent_r>()  are the reentrant "
"versions of B<getgrent>(3)  and B<fgetgrent>(3).  The former reads the next "
"group entry from the stream initialized by B<setgrent>(3).  The latter reads "
"the next group entry from I<stream>."
msgstr ""
"Funcțiile B<getgrent_r>() și B<fgetgrent_r>() sunt versiunile reentrante ale "
"funcțiilor B<getgrent>(3) și B<fgetgrent>(3). Prima citește următoarea "
"intrare de grup din fluxul inițializat de B<setgrent>(3). Cea de-a doua "
"citește următoarea intrare de grup din I<stream>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<group> structure is defined in I<E<lt>grp.hE<gt>> as follows:"
msgstr ""
"Structura I<group> este definită în I<E<lt>grp.hE<gt>> după cum urmează:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct group {\n"
"    char   *gr_name;        /* group name */\n"
"    char   *gr_passwd;      /* group password */\n"
"    gid_t   gr_gid;         /* group ID */\n"
"    char  **gr_mem;         /* NULL-terminated array of pointers\n"
"                               to names of group members */\n"
"};\n"
msgstr ""
"struct group {\n"
"    char   *gr_name;        /* numele grupului */\n"
"    char   *gr_passwd;      /* parola grupului */\n"
"    gid_t   gr_gid;         /* identificatorul grupului */\n"
"    char  **gr_mem;         /* vector de indicatori cu terminație NULL \n"
"                               la numele membrilor grupului */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For more information about the fields of this structure, see B<group>(5)."
msgstr ""
"Pentru mai multe informații despre câmpurile acestei structuri, a se vedea "
"B<group>(5)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The nonreentrant functions return a pointer to static storage, where this "
"static storage contains further pointers to group name, password, and "
"members.  The reentrant functions described here return all of that in "
"caller-provided buffers.  First of all there is the buffer I<gbuf> that can "
"hold a I<struct group>.  And next the buffer I<buf> of size I<buflen> that "
"can hold additional strings.  The result of these functions, the I<struct "
"group> read from the stream, is stored in the provided buffer I<*gbuf>, and "
"a pointer to this I<struct group> is returned in I<*gbufp>."
msgstr ""
"Funcțiile care nu sunt reentrante returnează un indicator către stocarea "
"statică, unde această stocare statică conține alți indicatori către numele "
"grupului, parola și membrii. Funcțiile reentrante descrise aici returnează "
"toate acestea în memoriile tampon furnizate de apelant. În primul rând, "
"există o memorie tampon I<gbuf> care poate conține o I<structură de grup>. "
"Și apoi tamponul I<buf> de dimensiune I<buflen> care poate conține șiruri de "
"caractere suplimentare. Rezultatul acestor funcții, I<structura de grup> "
"citită din flux, este stocată în memoria tampon furnizată I<*gbuf>, iar un "
"indicator la această I<structură de grup> este returnat în I<*gbufp>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these functions return 0 and I<*gbufp> is a pointer to the "
"I<struct group>.  On error, these functions return an error value and "
"I<*gbufp> is NULL."
msgstr ""
"În caz de succes, aceste funcții returnează 0, iar I<*gbufp> este un "
"indicator la I<structura de grup>. În caz de eroare, aceste funcții "
"returnează o valoare de eroare, iar I<*gbufp> este NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No more entries."
msgstr "Nu mai există alte intrări."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient buffer space supplied.  Try again with larger buffer."
msgstr ""
"Spațiu de memorie tampon furnizat insuficient. Încercați din nou cu o "
"memorie tampon mai mare."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getgrent_r>()"
msgstr "B<getgrent_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:grent locale"
msgstr "MT-Unsafe race:grent locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<fgetgrent_r>()"
msgstr "B<fgetgrent_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the above table, I<grent> in I<race:grent> signifies that if any of the "
"functions B<setgrent>(3), B<getgrent>(3), B<endgrent>(3), or "
"B<getgrent_r>()  are used in parallel in different threads of a program, "
"then data races could occur."
msgstr ""
"În tabelul de mai sus, I<grent> din I<race:grent> semnifică faptul că, dacă "
"oricare dintre funcțiile B<setgrent>(3), B<getgrent>(3), B<endgrent>(3) sau "
"B<getgrent_r>() sunt utilizate în paralel în diferite fire de execuție ale "
"unui program, pot apărea competiții de date."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Other systems use the prototype"
msgstr "Alte sisteme utilizează prototipul"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct group *getgrent_r(struct group *grp, char *buf,\n"
"                         int buflen);\n"
msgstr ""
"struct group *getgrent_r(struct group *grp, char *buf,\n"
"                         int buflen);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or, better,"
msgstr "sau, mai bine zis,"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"int getgrent_r(struct group *grp, char *buf, int buflen,\n"
"               FILE **gr_fp);\n"
msgstr ""
"int getgrent_r(struct group *grp, char *buf, int buflen,\n"
"               FILE **gr_fp);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are done in a style resembling the POSIX version of "
"functions like B<getpwnam_r>(3)."
msgstr ""
"Aceste funcții sunt realizate într-un stil asemănător cu versiunea POSIX a "
"unor funcții precum B<getpwnam_r>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<getgrent_r>()  is not really reentrant since it shares the "
"reading position in the stream with all other threads."
msgstr ""
"Funcția B<getgrent_r>() nu este cu adevărat reentrantă, deoarece împarte "
"poziția de citire în flux cu toate celelalte fire de execuție."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct group grp;\n"
"    struct group *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, sizeof(buf), &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd):\", grpp-E<gt>gr_name, (intmax_t) grpp-E<gt>gr_gid);\n"
"        for (size_t j = 0; ; j++) {\n"
"            if (grpp-E<gt>gr_mem[j] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[j]);\n"
"        }\n"
"        printf(\"\\[rs]n\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct group grp;\n"
"    struct group *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, sizeof(buf), &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd):\", grpp-E<gt>gr_name, (intmax_t) grpp-E<gt>gr_gid);\n"
"        for (size_t j = 0; ; j++) {\n"
"            if (grpp-E<gt>gr_mem[j] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[j]);\n"
"        }\n"
"        printf(\"\\[rs]n\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getgrent_r: %s", strerror(i));
#.                exit(EXIT_FAILURE);
#.          }
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<fgetgrent>(3), B<getgrent>(3), B<getgrgid>(3), B<getgrnam>(3), "
"B<putgrent>(3), B<group>(5)"
msgstr ""
"B<fgetgrent>(3), B<getgrent>(3), B<getgrgid>(3), B<getgrnam>(3), "
"B<putgrent>(3), B<group>(5)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are GNU extensions, done in a style resembling the POSIX "
"version of functions like B<getpwnam_r>(3).  Other systems use the prototype"
msgstr ""
"Aceste funcții sunt extensii GNU, realizate într-un stil asemănător cu "
"versiunea POSIX a unor funcții precum B<getpwnam_r>(3). Alte sisteme "
"utilizează prototipul"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct group grp;\n"
"    struct group *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    struct group grp;\n"
"    struct group *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, sizeof(buf), &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd):\", grpp-E<gt>gr_name, (intmax_t) grpp-E<gt>gr_gid);\n"
"        for (size_t j = 0; ; j++) {\n"
"            if (grpp-E<gt>gr_mem[j] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[j]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, sizeof(buf), &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd):\", grpp-E<gt>gr_name, (intmax_t) grpp-E<gt>gr_gid);\n"
"        for (size_t j = 0; ; j++) {\n"
"            if (grpp-E<gt>gr_mem[j] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[j]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct group grp;\n"
"    struct group *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, sizeof(buf), &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd):\", grpp-E<gt>gr_name, (intmax_t) grpp-E<gt>gr_gid);\n"
"        for (size_t j = 0; ; j++) {\n"
"            if (grpp-E<gt>gr_mem[j] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[j]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct group grp;\n"
"    struct group *grpp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setgrent();\n"
"    while (1) {\n"
"        i = getgrent_r(&grp, buf, sizeof(buf), &grpp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd):\", grpp-E<gt>gr_name, (intmax_t) grpp-E<gt>gr_gid);\n"
"        for (size_t j = 0; ; j++) {\n"
"            if (grpp-E<gt>gr_mem[j] == NULL)\n"
"                break;\n"
"            printf(\" %s\", grpp-E<gt>gr_mem[j]);\n"
"        }\n"
"        printf(\"\\en\");\n"
"    }\n"
"    endgrent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
