# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:16+0100\n"
"PO-Revision-Date: 2024-07-01 18:47+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strtod"
msgstr "strtod"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-16"
msgstr "16 iunie 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "strtod, strtof, strtold - convert ASCII string to floating-point number"
msgstr ""
"strtod, strtof, strtold - convertesc un șir ASCII în număr în virgulă mobilă"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double strtod(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"
"B<float strtof(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"
"B<long double strtold(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"
msgstr ""
"B<double strtod(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"
"B<float strtof(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"
"B<long double strtold(const char *restrict >I<nptr>B<,>\n"
"B<              char **_Nullable restrict >I<endptr>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strtof>(), B<strtold>():"
msgstr "B<strtof>(), B<strtold>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strtod>(), B<strtof>(), and B<strtold>()  functions convert the "
"initial portion of the string pointed to by I<nptr> to I<double>, I<float>, "
"and I<long double> representation, respectively."
msgstr ""
"Funcțiile B<strtod>(), B<strtof>() și B<strtold>() convertesc porțiunea "
"inițială a șirului indicat de I<nptr> în reprezentarea I<double>, I<float> "
"și, respectiv, I<long double>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The expected form of the (initial portion of the) string is optional leading "
"white space as recognized by B<isspace>(3), an optional plus (\\[aq]+\\[aq]) "
"or minus sign (\\[aq]-\\[aq]) and then either (i) a decimal number, or (ii) "
"a hexadecimal number, or (iii) an infinity, or (iv) a NAN (not-a-number)."
msgstr ""
"Forma așteptată a șirului (porțiunii inițiale a șirului) este un spațiu alb "
"de început opțional, astfel cum este recunoscut de B<isspace>(3), un semn "
"plus („+”) sau minus („-”) opțional și apoi fie (i) un număr zecimal, fie "
"(ii) un număr hexazecimal, fie (iii) un infinit, fie (iv) un NAN (not-a-"
"number)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A I<decimal number> consists of a nonempty sequence of decimal digits "
"possibly containing a radix character (decimal point, locale-dependent, "
"usually \\[aq].\\[aq]), optionally followed by a decimal exponent.  A "
"decimal exponent consists of an \\[aq]E\\[aq] or \\[aq]e\\[aq], followed by "
"an optional plus or minus sign, followed by a nonempty sequence of decimal "
"digits, and indicates multiplication by a power of 10."
msgstr ""
"Un I<număr zecimal> constă dintr-o secvență nevidă de cifre zecimale care "
"poate conține un caracter drept separator zecimal (semn zecimal, în funcție "
"de configurația regională, de obicei „.”), urmat, opțional, de un exponent "
"zecimal. Un exponent zecimal constă dintr-un „E” sau „e”, urmat de un semn "
"plus sau minus opțional, urmat de o secvență nevidă de cifre zecimale, și "
"indică înmulțirea cu o putere de 10."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A I<hexadecimal number> consists of a \"0x\" or \"0X\" followed by a "
"nonempty sequence of hexadecimal digits possibly containing a radix "
"character, optionally followed by a binary exponent.  A binary exponent "
"consists of a \\[aq]P\\[aq] or \\[aq]p\\[aq], followed by an optional plus "
"or minus sign, followed by a nonempty sequence of decimal digits, and "
"indicates multiplication by a power of 2.  At least one of radix character "
"and binary exponent must be present."
msgstr ""
"Un I<număr hexazecimal> este format dintr-un „0x” sau „0X” urmat de o "
"secvență nevidă de cifre hexazecimale care poate conține un caracter drept "
"separator, urmat, opțional, de un exponent binar. Un exponent binar constă "
"dintr-un „P” sau „p”, urmat de un semn plus sau minus opțional, urmat de o "
"secvență nevidă de cifre zecimale și indică înmulțirea cu o putere de 2. "
"Trebuie să fie prezent (cel puțin unul dintre ele) fie caracterul cu rol "
"separator, fie exponentul binar."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "An I<infinity> is either \"INF\" or \"INFINITY\", disregarding case."
msgstr ""
"Un I<infinit> este fie „INF”, fie „INFINITY”, fără a lua în considerare "
"diferențele dintre ele."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"A I<NAN> is \"NAN\" (disregarding case) optionally followed by a string, "
"I<(n-char-sequence)>, where I<n-char-sequence> specifies in an "
"implementation-dependent way the type of NAN (see VERSIONS)."
msgstr ""
"Un I<NAN> este „NAN” (fără a lua în considerare majusculele și minusculele) "
"urmat, opțional, de un șir de caractere, I<(n-char-sequence)>, unde I<n-char-"
"sequence> specifică tipul de NAN într-un mod care depinde de implementare (a "
"se vedea secțiunea VERSIUNI)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions return the converted value, if any."
msgstr "Aceste funcții returnează valoarea convertită, dacă există."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<endptr> is not NULL, a pointer to the character after the last "
"character used in the conversion is stored in the location referenced by "
"I<endptr>."
msgstr ""
"Dacă I<endptr> nu este NULL, în locația la care face referire I<endptr> se "
"stochează un indicator la caracterul de după ultimul caracter utilizat în "
"conversie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If no conversion is performed, zero is returned and (unless I<endptr> is "
"null) the value of I<nptr> is stored in the location referenced by I<endptr>."
msgstr ""
"Dacă nu se efectuează nicio conversie, se returnează zero și (cu excepția "
"cazului în care I<endptr> este nul) valoarea lui I<nptr> este stocată în "
"locația la care face referire I<endptr>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the correct value would cause overflow, plus or minus B<HUGE_VAL>, "
"B<HUGE_VALF>, or B<HUGE_VALL> is returned (according to the return type and "
"sign of the value), and B<ERANGE> is stored in I<errno>."
msgstr ""
"În cazul în care valoarea corectă ar provoca o depășire, se returnează plus "
"sau minus B<HUGE_VAL>, B<HUGE_VALF> sau B<HUGE_VALL> (în funcție de tipul de "
"returnare și de semnul valorii), iar B<ERANGE> este stocat în I<errno>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the correct value would cause underflow, a value with magnitude no larger "
"than B<DBL_MIN>, B<FLT_MIN>, or B<LDBL_MIN> is returned and B<ERANGE> is "
"stored in I<errno>."
msgstr ""
"În cazul în care valoarea corectă ar provoca o depășire a limitei minime (ar "
"fi sub valoarea minimă), se returnează o valoare cu magnitudinea nu mai mare "
"decât B<DBL_MIN>, B<FLT_MIN> sau B<LDBL_MIN>, iar B<ERANGE> este stocat în "
"I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Overflow or underflow occurred."
msgstr "A apărut o depășire a limitei superioare/inferioare."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strtod>(),\n"
"B<strtof>(),\n"
"B<strtold>()"
msgstr ""
"B<strtod>(),\n"
"B<strtof>(),\n"
"B<strtold>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#.  From glibc 2.8's stdlib/strtod_l.c:
#.      We expect it to be a number which is put in the
#.      mantissa of the number.
#.  It looks as though at least FreeBSD (according to the manual) does
#.  something similar.
#.  C11 says: "An implementation may use the n-char sequence to determine
#. 	extra information to be represented in the NaN's significant."
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the glibc implementation, the I<n-char-sequence> that optionally follows "
"\"NAN\" is interpreted as an integer number (with an optional '0' or '0x' "
"prefix to select base 8 or 16)  that is to be placed in the mantissa "
"component of the returned value."
msgstr ""
"În implementarea glibc, secvența I<n-char-sequence> care urmează opțional "
"după „NAN” este interpretată ca un număr întreg (cu un prefix opțional „0” "
"sau „0x” pentru a selecta baza 8 sau 16) care urmează să fie plasat în "
"componenta mantisă a valorii returnate."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strtod>()"
msgstr "B<strtod>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr "C89, POSIX.1-2001."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strtof>()"
msgstr "B<strtof>()"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<strtold>()"
msgstr "B<strtold>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr "PRECAUȚII"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since 0 can legitimately be returned on both success and failure, the "
"calling program should set I<errno> to 0 before the call, and then determine "
"if an error occurred by checking whether I<errno> has a nonzero value after "
"the call."
msgstr ""
"Deoarece 0 poate fi returnat în mod legitim atât în caz de succes, cât și în "
"caz de eșec, programul apelant ar trebui să stabilească I<errno> la 0 "
"înainte de apel, iar apoi să determine dacă s-a produs o eroare verificând "
"dacă I<errno> are o valoare diferită de zero după apel."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See the example on the B<strtol>(3)  manual page; the use of the functions "
"described in this manual page is similar."
msgstr ""
"A se vedea exemplul de pe pagina de manual B<strtol>(3); utilizarea "
"funcțiilor descrise în această pagină de manual este similară."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<nan>(3), B<nanf>(3), B<nanl>(3), "
"B<strfromd>(3), B<strtol>(3), B<strtoul>(3)"
msgstr ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<nan>(3), B<nanf>(3), B<nanl>(3), "
"B<strfromd>(3), B<strtol>(3), B<strtoul>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid ""
"B<double strtod(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<float strtof(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<long double strtold(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
msgstr ""
"B<double strtod(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<float strtof(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<long double strtold(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"A I<NAN> is \"NAN\" (disregarding case) optionally followed by a string, "
"I<(n-char-sequence)>, where I<n-char-sequence> specifies in an "
"implementation-dependent way the type of NAN (see NOTES)."
msgstr ""
"Un I<NAN> este „NAN” (fără a lua în considerare majusculele și minusculele) "
"urmat, opțional, de un șir de caractere, I<(n-char-sequence)>, unde I<n-char-"
"sequence> specifică tipul de NAN într-un mod care depinde de implementare (a "
"se vedea secțiunea NOTE)."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: SH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
