# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: 2024-01-27 15:57+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_atfork"
msgstr "pthread_atfork"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "pthread_atfork - register fork handlers"
msgstr "pthread_atfork - înregistrează gestionarii de bifurcări"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "Biblioteca de fire de execuție POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)(void),>\n"
"B<                   void (*>I<child>B<)(void));>\n"
msgstr ""
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)(void),>\n"
"B<                   void (*>I<child>B<)(void));>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_atfork>()  function registers fork handlers that are to be "
"executed when B<fork>(2)  is called by any thread in a process.  The "
"handlers are executed in the context of the thread that calls B<fork>(2)."
msgstr ""
"Funcția B<pthread_atfork>() înregistrează gestionarii de bifurcare care "
"urmează să fie executați atunci când B<fork>(2) este apelat de orice fir "
"dintr-un proces. Gestionatorii sunt executați în contextul firului care "
"apelează B<fork>(2)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Three kinds of handler can be registered:"
msgstr "Se pot înregistra trei tipuri de gestionari:"

#. type: IP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<prepare> specifies a handler that is executed in the parent process before "
"B<fork>(2)  processing starts."
msgstr ""
"I<prepare> specifică un gestionar care este executat în procesul părinte "
"înainte de începerea procesării B<fork>(2)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<parent> specifies a handler that is executed in the parent process after "
"B<fork>(2)  processing completes."
msgstr ""
"I<parent> specifică un gestionar care este executat în procesul părinte după "
"terminarea procesării B<fork>(2)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<child> specifies a handler that is executed in the child process after "
"B<fork>(2)  processing completes."
msgstr ""
"I<child> specifică un gestionar care este executat în procesul-copil după "
"terminarea procesării B<fork>(2)."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Any of the three arguments may be NULL if no handler is needed in the "
"corresponding phase of B<fork>(2)  processing."
msgstr ""
"Oricare dintre cele trei argumente poate fi NULL în cazul în care nu este "
"necesar nici un gestionar în faza corespunzătoare a procesării B<fork>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<pthread_atfork>()  returns zero.  On error, it returns an "
"error number.  B<pthread_atfork>()  may be called multiple times by a "
"process to register additional handlers.  The handlers for each phase are "
"called in a specified order: the I<prepare> handlers are called in reverse "
"order of registration; the I<parent> and I<child> handlers are called in the "
"order of registration."
msgstr ""
"În caz de succes, B<pthread_atfork>() returnează zero. În caz de eroare, "
"returnează un număr de eroare. B<pthread_atfork>() poate fi apelat de mai "
"multe ori de către un proces pentru a înregistra gestionari suplimentari. "
"Operatorii pentru fiecare fază sunt apelați într-o ordine specificată: "
"operatorii I<prepare> sunt apelați în ordinea inversă a înregistrării; "
"operatorii I<parent> și I<child> sunt apelați în ordinea înregistrării."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Could not allocate memory to record the fork handler list entry."
msgstr ""
"Nu s-a putut aloca memorie pentru a înregistra intrarea în lista de "
"gestionare a bifurcărilor."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When B<fork>(2)  is called in a multithreaded process, only the calling "
"thread is duplicated in the child process.  The original intention of "
"B<pthread_atfork>()  was to allow the child process to be returned to a "
"consistent state.  For example, at the time of the call to B<fork>(2), other "
"threads may have locked mutexes that are visible in the user-space memory "
"duplicated in the child.  Such mutexes would never be unlocked, since the "
"threads that placed the locks are not duplicated in the child.  The intent "
"of B<pthread_atfork>()  was to provide a mechanism whereby the application "
"(or a library)  could ensure that mutexes and other process and thread state "
"would be restored to a consistent state.  In practice, this task is "
"generally too difficult to be practicable."
msgstr ""
"Atunci când B<fork>(2) este apelat într-un proces cu mai multe fire de "
"execuție, numai firul de execuție care face apelul este duplicat în procesul-"
"copil. Intenția inițială a B<pthread_atfork>() a fost de a permite revenirea "
"procesului-copil la o stare coerentă. De exemplu, în momentul apelului la "
"B<fork>(2), este posibil ca alte fire să aibă mutex-uri blocate care sunt "
"vizibile în memoria din spațiul utilizatorului duplicată în procesul-copil. "
"Astfel de mutex-uri nu vor fi niciodată deblocate, deoarece firele care au "
"plasat blocajele nu sunt duplicate în copil. Intenția lui "
"B<pthread_atfork>() a fost de a oferi un mecanism prin care aplicația (sau o "
"bibliotecă) să se asigure că starea mutex-urilor și a altor procese și fire "
"de execuție va fi readusă la o stare coerentă. În practică, această sarcină "
"este, în general, prea dificilă pentru a fi realizabilă."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"After a B<fork>(2)  in a multithreaded process returns in the child, the "
"child should call only async-signal-safe functions (see B<signal-"
"safety>(7))  until such time as it calls B<execve>(2)  to execute a new "
"program."
msgstr ""
"După ce un B<fork>(2) într-un proces cu mai multe fire de execuție "
"returnează în copil, copilul ar trebui să apeleze numai funcții asincrone "
"sigure pentru semnale (a se vedea B<signal-safety>(7)) până în momentul în "
"care apelează B<execve>(2) pentru a executa un nou program."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 specifies that B<pthread_atfork>()  shall not fail with the error "
"B<EINTR>."
msgstr ""
"POSIX.1 specifică faptul că B<pthread_atfork>() nu trebuie să eșueze cu "
"eroarea B<EINTR>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<fork>(2), B<atexit>(3), B<pthreads>(7)"
msgstr "B<fork>(2), B<atexit>(3), B<pthreads>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PTHREAD_ATFORK"
msgstr "PTHREAD_ATFORK"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LinuxThreads"
msgstr "LinuxThreads"

#. type: Plain text
#: debian-bookworm
msgid "pthread_atfork - register handlers to be called at fork(2) time"
msgstr ""
"pthread_atfork - înregistrează gestionarii care vor fi apelați la momentul "
"executării lui fork(2)"

#. type: Plain text
#: debian-bookworm
msgid "B<#include E<lt>pthread.hE<gt>>"
msgstr "B<#include E<lt>pthread.hE<gt>>"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)"
"(void), void (*>I<child>B<)(void));>"
msgstr ""
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)"
"(void), void (*>I<child>B<)(void));>"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_atfork> registers handler functions to be called just before and "
"just after a new process is created with B<fork>(2). The I<prepare> handler "
"will be called from the parent process, just before the new process is "
"created. The I<parent> handler will be called from the parent process, just "
"before B<fork>(2) returns. The I<child> handler will be called from the "
"child process, just before B<fork>(2) returns."
msgstr ""
"B<pthread_atfork> înregistrează funcțiile de gestionare pentru a fi apelate "
"chiar înainte și imediat după ce un nou proces este creat cu B<fork>(2). "
"Gestionarul I<prepare> va fi apelat din procesul părinte, chiar înainte de "
"crearea noului proces. Gestionarul I<parent> va fi apelat din procesul "
"părinte, chiar înainte ca B<fork>(2) să returneze. Gestionatorul I<child> va "
"fi apelat de către procesul copil, chiar înainte ca B<fork>(2) să returneze."

#. type: Plain text
#: debian-bookworm
msgid ""
"One or several of the three handlers I<prepare>, I<parent> and I<child> can "
"be given as B<NULL>, meaning that no handler needs to be called at the "
"corresponding point."
msgstr ""
"Unul sau mai mulți dintre cei trei gestionari I<prepare>, I<parent> și "
"I<child> pot fi indicați ca B<NULL>, ceea ce înseamnă că nu este necesar să "
"se apeleze niciun gestionar în punctul corespunzător."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_atfork> can be called several times to install several sets of "
"handlers. At B<fork>(2) time, the I<prepare> handlers are called in LIFO "
"order (last added with B<pthread_atfork>, first called before B<fork>), "
"while the I<parent> and I<child> handlers are called in FIFO order (first "
"added, first called)."
msgstr ""
"B<pthread_atfork> poate fi apelat de mai multe ori pentru a instala mai "
"multe seturi de gestionari. În momentul B<fork>(2), gestionarii I<prepare> "
"sunt apelați în ordine LIFO (ultimul adăugat cu B<pthread_atfork>, primul "
"apelat înainte de B<fork>), în timp ce gestionarii I<parent> și I<child> "
"sunt apelați în ordine FIFO (primul adăugat, primul apelat)."

#. type: Plain text
#: debian-bookworm
msgid ""
"To understand the purpose of B<pthread_atfork>, recall that B<fork>(2)  "
"duplicates the whole memory space, including mutexes in their current "
"locking state, but only the calling thread: other threads are not running in "
"the child process.  The mutexes are not usable after the B<fork> and must be "
"initialized with I<pthread_mutex_init> in the child process.  This is a "
"limitation of the current implementation and might or might not be present "
"in future versions."
msgstr ""
"Pentru a înțelege scopul lui B<pthread_atfork>, reamintim că B<fork>(2) "
"dublează întregul spațiu de memorie, inclusiv mutex-urile în starea lor de "
"blocare curentă, dar numai firul apelant: alte fire nu rulează în procesul "
"copil. Mutex-urile nu sunt utilizabile după B<fork> și trebuie să fie "
"inițializate cu I<pthread_mutex_init> în procesul copil. Aceasta este o "
"limitare a implementării actuale și ar putea sau nu să fie prezentă în "
"versiunile viitoare."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_atfork> returns 0 on success and a non-zero error code on error."
msgstr ""
"B<pthread_atfork> returnează 0 în caz de succes și un cod de eroare diferit "
"de zero în caz de eroare."

#. type: Plain text
#: debian-bookworm
msgid "insufficient memory available to register the handlers."
msgstr "memorie disponibilă insuficientă pentru a înregistra gestionarii."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm
msgid "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"
msgstr "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"

#. type: Plain text
#: debian-bookworm
msgid "B<fork>(2), B<pthread_mutex_lock>(3), B<pthread_mutex_unlock>(3)."
msgstr "B<fork>(2), B<pthread_mutex_lock>(3), B<pthread_mutex_unlock>(3)."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
