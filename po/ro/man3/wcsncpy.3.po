# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-12-06 18:21+0100\n"
"PO-Revision-Date: 2024-07-01 19:35+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "wcsncpy"
msgstr "wcsncpy"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 iunie 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "wcsncpy - copy a fixed-size string of wide characters"
msgstr "wcsncpy - copiază un șir de caractere late de dimensiune fixă"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<wchar_t *wcsncpy(wchar_t >I<dest>B<[restrict .>I<n>B<],>\n"
"B<                 const wchar_t >I<src>B<[restrict .>I<n>B<],>\n"
"B<                 size_t >I<n>B<);>\n"
msgstr ""
"B<wchar_t *wcsncpy(wchar_t >I<destinație>B<[restrict .>I<n>B<],>\n"
"B<                 const wchar_t >I<sursă>B<[restrict .>I<n>B<],>\n"
"B<                 size_t >I<n>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The B<wcsncpy>()  function is the wide-character equivalent of the "
"B<strncpy>(3)  function.  It copies at most I<n> wide characters from the "
"wide-character string pointed to by I<src>, including the terminating null "
"wide character (L\\[aq]\\[rs]0\\[aq]), to the array pointed to by I<dest>.  "
"Exactly I<n> wide characters are written at I<dest>.  If the length "
"I<wcslen(src)> is smaller than I<n>, the remaining wide characters in the "
"array pointed to by I<dest> are filled with null wide characters.  If the "
"length I<wcslen(src)> is greater than or equal to I<n>, the string pointed "
"to by I<dest> will not be terminated by a null wide character."
msgstr ""
"Funcția B<wcsncpy>() este echivalentul în caractere late al funcției "
"B<strncpy>(3). Aceasta copiază cel mult I<n> caractere late din șirul de "
"caractere late indicat de I<sursă>, inclusiv caracterul lat null de "
"terminare (L\\[aq]\\[rs]0\\[aq]), în vectorul indicat de I<destinație>. "
"Exact I<n> caractere late sunt scrise în I<destinație>. În cazul în care "
"lungimea I<wcslen(sursă)> este mai mică decât I<n>, caracterele late rămase "
"în vectorul indicat de I<destinație> sunt completate cu caractere late nule. "
"În cazul în care lungimea I<wcslen(sursă)> este mai mare sau egală cu I<n>, "
"șirul indicat de I<destinație> nu se termină cu un caracter lat nul."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The strings may not overlap."
msgstr "Șirurile nu se pot suprapune."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The programmer must ensure that there is room for at least I<n> wide "
"characters at I<dest>."
msgstr ""
"Programatorul trebuie să se asigure că există spațiu pentru cel puțin I<n> "
"caractere late la I<destinație>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<wcsncpy>()  returns I<dest>."
msgstr "B<wcsncpy>()  returns I<dest>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<wcsncpy>()"
msgstr "B<wcsncpy>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strncpy>(3)"
msgstr "B<strncpy>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The B<wcsncpy>()  function is the wide-character equivalent of the "
"B<strncpy>(3)  function.  It copies at most I<n> wide characters from the "
"wide-character string pointed to by I<src>, including the terminating null "
"wide character (L\\[aq]\\e0\\[aq]), to the array pointed to by I<dest>.  "
"Exactly I<n> wide characters are written at I<dest>.  If the length "
"I<wcslen(src)> is smaller than I<n>, the remaining wide characters in the "
"array pointed to by I<dest> are filled with null wide characters.  If the "
"length I<wcslen(src)> is greater than or equal to I<n>, the string pointed "
"to by I<dest> will not be terminated by a null wide character."
msgstr ""
"Funcția B<wcsncpy>() este echivalentul în caractere late al funcției "
"B<strncpy>(3). Aceasta copiază cel mult I<n> caractere late din șirul de "
"caractere late indicat de I<sursă>, inclusiv caracterul lat null de "
"terminare (L\\[aq]\\e0\\[aq]), în vectorul indicat de I<destinație>. Exact "
"I<n> caractere late sunt scrise în I<destinație>. În cazul în care lungimea "
"I<wcslen(sursă)> este mai mică decât I<n>, caracterele late rămase în "
"vectorul indicat de I<destinație> sunt completate cu caractere late nule. În "
"cazul în care lungimea I<wcslen(sursă)> este mai mare sau egală cu I<n>, "
"șirul indicat de I<destinație> nu se termină cu un caracter lat nul."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
