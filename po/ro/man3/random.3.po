# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:10+0100\n"
"PO-Revision-Date: 2024-02-02 00:26+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "random"
msgstr "random"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "random, srandom, initstate, setstate - random number generator"
msgstr "random, srandom, initstate, setstate - generator de numere aleatoare"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long random(void);>\n"
"B<void srandom(unsigned int >I<seed>B<);>\n"
msgstr ""
"B<long random(void);>\n"
"B<void srandom(unsigned int >I<seed>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *initstate(unsigned int >I<seed>B<, char >I<state>B<[.>I<n>B<], size_t >I<n>B<);>\n"
"B<char *setstate(char *>I<state>B<);>\n"
msgstr ""
"B<char *initstate(unsigned int >I<seed>B<, char >I<state>B<[.>I<n>B<], size_t >I<n>B<);>\n"
"B<char *setstate(char *>I<state>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<random>(), B<srandom>(), B<initstate>(), B<setstate>():"
msgstr "B<random>(), B<srandom>(), B<initstate>(), B<setstate>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<random>()  function uses a nonlinear additive feedback random number "
"generator employing a default table of size 31 long integers to return "
"successive pseudo-random numbers in the range from 0 to 2\\[ha]31\\ -\\ 1.  "
"The period of this random number generator is very large, approximately "
"I<16\\ *\\ ((2\\[ha]31)\\ -\\ 1)>."
msgstr ""
"Funcția B<random>() utilizează un generator de numere aleatoare cu reacție "
"aditivă neliniară care utilizează un tabel implicit de 31 de numere întregi "
"lungi pentru a returna numere pseudoaleatoare succesive în intervalul 0-2\\"
"[ha]31\\ -\\ 1. Perioada acestui generator de numere aleatoare este foarte "
"mare, aproximativ I<16\\ *\\ ((2\\[ha]31)\\ -\\ 1)>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<srandom>()  function sets its argument as the seed for a new sequence "
"of pseudo-random integers to be returned by B<random>().  These sequences "
"are repeatable by calling B<srandom>()  with the same seed value.  If no "
"seed value is provided, the B<random>()  function is automatically seeded "
"with a value of 1."
msgstr ""
"Funcția B<srandom>() își stabilește argumentul ca sămânță pentru o nouă "
"secvență de numere întregi pseudoaleatoare care va fi returnată de "
"B<random>(). Aceste secvențe pot fi repetate prin apelarea B<srandom>() cu "
"aceeași valoare de sămânță. În cazul în care nu se furnizează nicio valoare "
"de sămânță, funcția B<random>() este automat însămânțată cu o valoare de 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<initstate>()  function allows a state array I<state> to be initialized "
"for use by B<random>().  The size of the state array I<n> is used by "
"B<initstate>()  to decide how sophisticated a random number generator it "
"should use\\[em]the larger the state array, the better the random numbers "
"will be.  Current \"optimal\" values for the size of the state array I<n> "
"are 8, 32, 64, 128, and 256 bytes; other amounts will be rounded down to the "
"nearest known amount.  Using less than 8 bytes results in an error.  I<seed> "
"is the seed for the initialization, which specifies a starting point for the "
"random number sequence, and provides for restarting at the same point."
msgstr ""
"Funcția B<initstate>() permite inițializarea unei matrice de stări I<state> "
"pentru a fi utilizată de B<random>(). Dimensiunea matricei de stare I<n> "
"este utilizată de B<initstate>() pentru a decide cât de sofisticat trebuie "
"să fie generatorul de numere aleatoare pe care trebuie să-l utilizeze - cu "
"cât matricea de stare este mai mare, cu atât mai bune vor fi numerele "
"aleatoare. Valorile „optime” actuale pentru dimensiunea matricei de stări "
"I<n> sunt 8, 32, 64, 128 și 256 de octeți; alte valori vor fi rotunjite la "
"cea mai apropiată valoare cunoscută. Dacă se utilizează mai puțin de 8 "
"octeți, se produce o eroare. I<seed> este sămânța pentru inițializare, care "
"specifică un punct de pornire pentru secvența de numere aleatoare și asigură "
"repornirea în același punct."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<setstate>()  function changes the state array used by the B<random>()  "
"function.  The state array I<state> is used for random number generation "
"until the next call to B<initstate>()  or B<setstate>().  I<state> must "
"first have been initialized using B<initstate>()  or be the result of a "
"previous call of B<setstate>()."
msgstr ""
"Funcția B<setstate>() modifică matricea de stare utilizată de funcția "
"B<random>(). Matricea de stare I<state> este utilizată pentru generarea "
"numerelor aleatoare până la următorul apel al funcției B<initstate>() sau "
"B<setstate>(). I<state> trebuie mai întâi să fi fost inițializat cu ajutorul "
"B<initstate>() sau să fie rezultatul unui apel anterior al B<setstate>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<random>()  function returns a value between 0 and I<(2\\[ha]31)\\ -\\ "
"1>.  The B<srandom>()  function returns no value."
msgstr ""
"Funcția B<random>() returnează o valoare între 0 și I<(2\\[ha]31)\\ -\\ 1>. "
"Funcția B<srandom>() nu returnează nicio valoare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<initstate>()  function returns a pointer to the previous state array.  "
"On failure, it returns NULL, and I<errno> is set to indicate the error."
msgstr ""
"Funcția B<initstate>() returnează un indicator la matricea de stare "
"anterioară. În caz de eșec, aceasta returnează NULL, iar I<errno> este "
"configurată pentru a indica eroarea."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<setstate>()  returns a pointer to the previous state array.  "
"On failure, it returns NULL, and I<errno> is set to indicate the error."
msgstr ""
"În caz de succes, B<setstate>() returnează un indicator la matricea de stare "
"anterioară. În caz de eșec, aceasta returnează NULL, iar I<errno> este "
"configurată pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<state> argument given to B<setstate>()  was NULL."
msgstr "Argumentul I<state> dat la B<setstate>() a fost NULL."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A state array of less than 8 bytes was specified to B<initstate>()."
msgstr ""
"O matrice de stare mai mică de 8 octeți a fost specificată la B<initstate>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<random>(),\n"
"B<srandom>(),\n"
"B<initstate>(),\n"
"B<setstate>()"
msgstr ""
"B<random>(),\n"
"B<srandom>(),\n"
"B<initstate>(),\n"
"B<setstate>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, 4.3BSD."
msgstr "POSIX.1-2001, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Random-number generation is a complex topic.  I<Numerical Recipes in C: The "
"Art of Scientific Computing> (William H.\\& Press, Brian P.\\& Flannery, "
"Saul A.\\& Teukolsky, William T.\\& Vetterling; New York: Cambridge "
"University Press, 2007, 3rd ed.)  provides an excellent discussion of "
"practical random-number generation issues in Chapter 7 (Random Numbers)."
msgstr ""
"Generarea numerelor aleatoare este un subiect complex. I<Numerical Recipes "
"in C: The Art of Scientific Computing> (William H.\\& Press, Brian P.\\& "
"Flannery, Saul A.\\& Teukolsky, William T.\\& Vetterling; New York: "
"Cambridge University Press, 2007, ed. a 3-a) oferă o discuție excelentă a "
"problemelor practice de generare a numerelor aleatoare în capitolul 7 "
"(Random Numbers)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For a more theoretical discussion which also covers many practical issues in "
"depth, see Chapter 3 (Random Numbers) in Donald E.\\& Knuth's I<The Art of "
"Computer Programming>, volume 2 (Seminumerical Algorithms), 2nd ed.; "
"Reading, Massachusetts: Addison-Wesley Publishing Company, 1981."
msgstr ""
"Pentru o discuție mai teoretică, care acoperă în profunzime și multe aspecte "
"practice, a se vedea capitolul 3 (Random Numbers) din I<The Art of Computer "
"Programming> Donald E.\\& Knuth's, volumul 2 (Seminumerical Algorithms), 2nd "
"ed.; Reading, Massachusetts: Addison-Wesley Publishing Company, 1981."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr "PRECAUȚII"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<random>()  function should not be used in multithreaded programs where "
"reproducible behavior is required.  Use B<random_r>(3)  for that purpose."
msgstr ""
"Funcția B<random>() nu ar trebui utilizată în programele cu mai multe fire "
"de execuție în care este necesar un comportament reproductibil. Folosiți "
"B<random_r>(3) în acest scop."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#.  http://sourceware.org/bugzilla/show_bug.cgi?id=15380
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"According to POSIX, B<initstate>()  should return NULL on error.  In the "
"glibc implementation, I<errno> is (as specified) set on error, but the "
"function does not return NULL."
msgstr ""
"În conformitate cu POSIX, B<initstate>() ar trebui să returneze NULL în caz "
"de eroare. În implementarea glibc, I<errno> este (așa cum este specificat) "
"stabilită în caz de eroare, dar funcția nu returnează NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getrandom>(2), B<drand48>(3), B<rand>(3), B<random_r>(3), B<srand>(3)"
msgstr ""
"B<getrandom>(2), B<drand48>(3), B<rand>(3), B<random_r>(3), B<srand>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-10"
msgstr "10 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
