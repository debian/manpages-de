# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:08+0100\n"
"PO-Revision-Date: 2024-02-16 09:36+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "popen"
msgstr "popen"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "popen, pclose - pipe stream to or from a process"
msgstr "popen, pclose - flux de conductă către sau de la un proces"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<FILE *popen(const char *>I<command>B<, const char *>I<type>B<);>\n"
"B<int pclose(FILE *>I<stream>B<);>\n"
msgstr ""
"B<FILE *popen(const char *>I<command>B<, const char *>I<type>B<);>\n"
"B<int pclose(FILE *>I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<popen>(), B<pclose>():"
msgstr "B<popen>(), B<pclose>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _POSIX_C_SOURCE E<gt>= 2\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _POSIX_C_SOURCE E<gt>= 2\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<popen>()  function opens a process by creating a pipe, forking, and "
"invoking the shell.  Since a pipe is by definition unidirectional, the "
"I<type> argument may specify only reading or writing, not both; the "
"resulting stream is correspondingly read-only or write-only."
msgstr ""
"Funcția B<popen>() deschide un proces prin crearea unei conducte, prin "
"bifurcare și prin invocarea shell-ului. Deoarece o conductă este, prin "
"definiție, unidirecțională, argumentul I<type> poate specifica numai citire "
"sau scriere, nu ambele; fluxul rezultat este, în consecință, numai-pentru-"
"citire sau numai-pentru-scriere."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<command> argument is a pointer to a null-terminated string containing "
"a shell command line.  This command is passed to I</bin/sh> using the B<-c> "
"flag; interpretation, if any, is performed by the shell."
msgstr ""
"Argumentul I<command> este un indicator la un șir de caractere cu terminație "
"nulă care conține o linie de comandă de tip shell.  Această comandă este "
"transmisă către I</bin/sh> folosind opțiunea B<-c>; interpretarea, dacă este "
"cazul, este efectuată de către shell."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<type> argument is a pointer to a null-terminated string which must "
"contain either the letter \\[aq]r\\[aq] for reading or the letter \\[aq]w\\"
"[aq] for writing.  Since glibc 2.9, this argument can additionally include "
"the letter \\[aq]e\\[aq], which causes the close-on-exec flag "
"(B<FD_CLOEXEC>)  to be set on the underlying file descriptor; see the "
"description of the B<O_CLOEXEC> flag in B<open>(2)  for reasons why this may "
"be useful."
msgstr ""
"Argumentul I<type> este un indicator către un șir de caractere cu terminație "
"nulă care trebuie să conțină fie litera „r” pentru citire, fie litera „w” "
"pentru scriere. Începând cu glibc 2.9, acest argument poate include, de "
"asemenea, litera „e”, care determină activarea fanionului „close-on-exec” "
"(B<FD_CLOEXEC>) pe descriptorul de fișier subiacent; a se vedea descrierea "
"fanionului B<O_CLOEXEC> în B<open>(2) pentru motivele pentru care acest "
"lucru poate fi util."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The return value from B<popen>()  is a normal standard I/O stream in all "
"respects save that it must be closed with B<pclose>()  rather than "
"B<fclose>(3).  Writing to such a stream writes to the standard input of the "
"command; the command's standard output is the same as that of the process "
"that called B<popen>(), unless this is altered by the command itself.  "
"Conversely, reading from the stream reads the command's standard output, and "
"the command's standard input is the same as that of the process that called "
"B<popen>()."
msgstr ""
"Valoarea returnată de B<popen>() este un flux de intrare/ieșire standard "
"normal în toate privințele, cu excepția faptului că trebuie închis cu "
"B<pclose>() în loc de B<fclose>(3). Scrierea pe un astfel de flux se scrie "
"în intrarea standard a comenzii; ieșirea standard a comenzii este aceeași cu "
"cea a procesului care a apelat B<popen>(), cu excepția cazului în care "
"aceasta este modificată de comanda însăși. În schimb, citirea de pe flux "
"citește ieșirea standard a comenzii, iar intrarea standard a comenzii este "
"aceeași cu cea a procesului care a apelat B<popen>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Note that output B<popen>()  streams are block buffered by default."
msgstr ""
"Rețineți că fluxurile de ieșire B<popen>() sunt stocate în bloc în mod "
"implicit."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pclose>()  function waits for the associated process to terminate and "
"returns the exit status of the command as returned by B<wait4>(2)."
msgstr ""
"Funcția B<pclose>() așteaptă ca procesul asociat să se încheie și returnează "
"starea de ieșire a comenzii, așa cum este returnată de B<wait4>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<popen>(): on success, returns a pointer to an open stream that can be used "
"to read or write to the pipe; if the B<fork>(2)  or B<pipe>(2)  calls fail, "
"or if the function cannot allocate memory, NULL is returned."
msgstr ""
"B<popen>(): în caz de succes, returnează un indicator către un flux deschis "
"care poate fi utilizat pentru a citi sau a scrie în conductă; dacă apelurile "
"B<fork>(2) sau B<pipe>(2) eșuează sau dacă funcția nu poate aloca memorie, "
"se returnează NULL."

#.  These conditions actually give undefined results, so I commented
#.  them out.
#.  .I stream
#.  is not associated with a "popen()ed" command, if
#. .I stream
#.  already "pclose()d", or if
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pclose>(): on success, returns the exit status of the command; if "
"B<wait4>(2)  returns an error, or some other error is detected, -1 is "
"returned."
msgstr ""
"B<pclose>(): în caz de succes, returnează starea de ieșire a comenzii; dacă "
"B<wait4>(2) returnează o eroare sau dacă este detectată o altă eroare, se "
"returnează -1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "On failure, both functions set I<errno> to indicate the error."
msgstr ""
"În caz de eșec, ambele funcții configurează I<errno> pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<popen>()  function does not set I<errno> if memory allocation fails.  "
"If the underlying B<fork>(2)  or B<pipe>(2)  fails, I<errno> is set to "
"indicate the error.  If the I<type> argument is invalid, and this condition "
"is detected, I<errno> is set to B<EINVAL>."
msgstr ""
"Funcția B<popen>() nu configurează I<errno> dacă alocarea memoriei eșuează. "
"În cazul în care funcția B<fork>(2) sau B<pipe>(2) subiacentă eșuează, "
"I<errno> este configurată pentru a indica eroarea. În cazul în care "
"argumentul I<type> nu este valid, iar această condiție este detectată, "
"I<errno> este stabilită la B<EINVAL>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If B<pclose>()  cannot obtain the child status, I<errno> is set to B<ECHILD>."
msgstr ""
"Dacă B<pclose>() nu poate obține starea copilului, I<errno> este stabilită "
"la B<ECHILD>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<popen>(),\n"
"B<pclose>()"
msgstr ""
"B<popen>(),\n"
"B<pclose>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The \\[aq]e\\[aq] value for I<type> is a Linux extension."
msgstr "Valoarea „e” pentru I<type> este o extensie Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr "AVERTISMENTE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Carefully read Caveats in B<system>(3)."
msgstr "Citiți cu atenție Avertismentele din B<system>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since the standard input of a command opened for reading shares its seek "
"offset with the process that called B<popen>(), if the original process has "
"done a buffered read, the command's input position may not be as expected.  "
"Similarly, the output from a command opened for writing may become "
"intermingled with that of the original process.  The latter can be avoided "
"by calling B<fflush>(3)  before B<popen>()."
msgstr ""
"Deoarece intrarea standard a unei comenzi deschise pentru citire își împarte "
"poziția de căutare cu procesul care a apelat B<popen>(), în cazul în care "
"procesul original a efectuat o citire în memoria tampon, este posibil ca "
"poziția de intrare a comenzii să nu fie cea așteptată. În mod similar, "
"ieșirea unei comenzi deschise pentru scriere se poate amesteca cu cea a "
"procesului original. Aceasta din urmă poate fi evitată prin apelarea "
"B<fflush>(3) înainte de B<popen>()."

#.  .SH HISTORY
#.  A
#.  .BR popen ()
#.  and a
#.  .BR pclose ()
#.  function appeared in Version 7 AT&T UNIX.
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Failure to execute the shell is indistinguishable from the shell's failure "
"to execute the command, or an immediate exit of the command.  The only hint "
"is an exit status of 127."
msgstr ""
"Eșecul executării shell-ului nu se deosebește de eșecul executării comenzii "
"shell-ului sau de ieșirea imediată a comenzii. Singurul indiciu este o stare "
"de ieșire de 127."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sh>(1), B<fork>(2), B<pipe>(2), B<wait4>(2), B<fclose>(3), B<fflush>(3), "
"B<fopen>(3), B<stdio>(3), B<system>(3)"
msgstr ""
"B<sh>(1), B<fork>(2), B<pipe>(2), B<wait4>(2), B<fclose>(3), B<fflush>(3), "
"B<fopen>(3), B<stdio>(3), B<system>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: debian-bookworm
msgid "B<Note>: carefully read Caveats in B<system>(3)."
msgstr "B<Notă>: citiți cu atenție Avertismentele din B<sistem>(3)."

#.  .SH HISTORY
#.  A
#.  .BR popen ()
#.  and a
#.  .BR pclose ()
#.  function appeared in Version 7 AT&T UNIX.
#. type: Plain text
#: debian-bookworm
msgid ""
"Failure to execute the shell is indistinguishable from the shell's failure "
"to execute command, or an immediate exit of the command.  The only hint is "
"an exit status of 127."
msgstr ""
"Eșecul executării shell-ului nu se deosebește de eșecul executării comenzii "
"shell-ului sau de ieșirea imediată a comenzii. Singurul indiciu este o stare "
"de ieșire de 127."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
