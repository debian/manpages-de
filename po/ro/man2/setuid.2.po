# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-12-06 18:14+0100\n"
"PO-Revision-Date: 2024-01-27 09:32+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "setuid"
msgstr "setuid"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "setuid - set user identity"
msgstr "setuid - stabilește identitatea utilizatorului"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int setuid(uid_t >I<uid>B<);>\n"
msgstr "B<int setuid(uid_t >I<uid>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<setuid>()  sets the effective user ID of the calling process.  If the "
"calling process is privileged (more precisely: if the process has the "
"B<CAP_SETUID> capability in its user namespace), the real UID and saved set-"
"user-ID are also set."
msgstr ""
"B<setuid>() stabilește ID-ul de utilizator efectiv al procesului apelant. În "
"cazul în care procesul apelant este privilegiat (mai precis: dacă procesul "
"are capacitatea B<CAP_SETUID> în spațiul său de nume de utilizator), se "
"stabilesc, de asemenea, UID real și set-user-ID salvat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Under Linux, B<setuid>()  is implemented like the POSIX version with the "
"B<_POSIX_SAVED_IDS> feature.  This allows a set-user-ID (other than root) "
"program to drop all of its user privileges, do some un-privileged work, and "
"then reengage the original effective user ID in a secure manner."
msgstr ""
"În Linux, B<setuid>() este implementat ca versiunea POSIX cu caracteristica "
"B<_POSIX_SAVED_IDS>. Acest lucru permite unui program set-user-ID (altul "
"decât root) să renunțe la toate privilegiile de utilizator, să facă unele "
"lucrări fără privilegii și apoi să reangajeze ID-ul de utilizator efectiv "
"inițial într-un mod sigur."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the user is root or the program is set-user-ID-root, special care must be "
"taken: B<setuid>()  checks the effective user ID of the caller and if it is "
"the superuser, all process-related user ID's are set to I<uid>.  After this "
"has occurred, it is impossible for the program to regain root privileges."
msgstr ""
"Dacă utilizatorul este root sau dacă programul este set-user-ID-root, "
"trebuie să se acorde o atenție specială: B<setuid>() verifică ID-ul de "
"utilizator efectiv al apelantului și, dacă acesta este superutilizatorul, "
"toate ID-urile de utilizator legate de proces sunt stabilite la I<uid>. După "
"ce acest lucru s-a întâmplat, este imposibil ca programul să recâștige "
"privilegiile de root."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Thus, a set-user-ID-root program wishing to temporarily drop root "
"privileges, assume the identity of an unprivileged user, and then regain "
"root privileges afterward cannot use B<setuid>().  You can accomplish this "
"with B<seteuid>(2)."
msgstr ""
"Astfel, un program set-user-ID-root care dorește să renunțe temporar la "
"privilegiile de root, să își asume identitatea unui utilizator neprivilegiat "
"și apoi să redobândească privilegiile de root nu poate utiliza B<setuid>().  "
"Puteți realiza acest lucru cu B<seteuid>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"În caz de succes, se returnează zero. În caz de eroare, se returnează -1, "
"iar I<errno> este configurată pentru a indica eroarea."

# R-GC, scrie:
# am modificat traducerea acestui mesaj de la:
# „... verificarea unui răspuns negativ de la ...”
# la:
# „... verificarea pentru o returnare a unui eșec de la ....”
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<Note>: there are cases where B<setuid>()  can fail even when the caller is "
"UID 0; it is a grave security error to omit checking for a failure return "
"from B<setuid>()."
msgstr ""
"I<Notă>: există cazuri în care B<setuid>() poate eșua chiar și atunci când "
"apelantul este UID 0; este o gravă eroare de securitate să se omită "
"verificarea pentru o returnare a unui eșec de la B<setuid>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The call would change the caller's real UID (i.e., I<uid> does not match the "
"caller's real UID), but there was a temporary failure allocating the "
"necessary kernel data structures."
msgstr ""
"Apelul ar schimba UID-ul real al apelantului (adică I<uid> nu se potrivește "
"cu UID-ul real al apelantului), dar a existat un eșec temporar în alocarea "
"structurilor de date necesare ale nucleului."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<uid> does not match the real user ID of the caller and this call would "
"bring the number of processes belonging to the real user ID I<uid> over the "
"caller's B<RLIMIT_NPROC> resource limit.  Since Linux 3.1, this error case "
"no longer occurs (but robust applications should check for this error); see "
"the description of B<EAGAIN> in B<execve>(2)."
msgstr ""
"I<uid> nu se potrivește cu ID-ul real al apelantului și acest apel ar face "
"ca numărul de procese aparținând ID-ului real al utilizatorului I<uid> să "
"depășească limita de resurse B<RLIMIT_NPROC> a apelantului. Începând cu "
"Linux 3.1, acest caz de eroare nu mai apare (dar aplicațiile robuste ar "
"trebui să verifice dacă există această eroare); a se vedea descrierea lui "
"B<EAGAIN> din B<execve>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The user ID specified in I<uid> is not valid in this user namespace."
msgstr ""
"ID-ul de utilizator specificat în I<uid> nu este valid în acest spațiu de "
"nume de utilizator."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The user is not privileged (Linux: does not have the B<CAP_SETUID> "
"capability in its user namespace) and I<uid> does not match the real UID or "
"saved set-user-ID of the calling process."
msgstr ""
"Utilizatorul nu este privilegiat (Linux: nu are capacitatea B<CAP_SETUID> în "
"spațiul său de nume de utilizator) și I<uid> nu se potrivește cu UID-ul real "
"sau cu set-user-ID-ul salvat al procesului de apelare."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Diferențe între biblioteca C și nucleu"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"At the kernel level, user IDs and group IDs are a per-thread attribute.  "
"However, POSIX requires that all threads in a process share the same "
"credentials.  The NPTL threading implementation handles the POSIX "
"requirements by providing wrapper functions for the various system calls "
"that change process UIDs and GIDs.  These wrapper functions (including the "
"one for B<setuid>())  employ a signal-based technique to ensure that when "
"one thread changes credentials, all of the other threads in the process also "
"change their credentials.  For details, see B<nptl>(7)."
msgstr ""
"La nivelul nucleului, ID-urile de utilizator și ID-urile de grup sunt un "
"atribut pentru fiecare fir de execuție. Cu toate acestea, POSIX impune ca "
"toate firele dintr-un proces să aibă aceleași credențiale. Implementarea "
"„NPTL threading” se ocupă de cerințele POSIX prin furnizarea de funcții de "
"învăluire pentru diferitele apeluri de sistem care modifică UID și GID ale "
"proceselor. Aceste funcții învăluitoare (inclusiv cea pentru B<setuid>()) "
"utilizează o tehnică bazată pe semnale pentru a se asigura că, atunci când "
"un fir de execuție își schimbă acreditările, toate celelalte fire de "
"execuție din proces își schimbă și ele acreditările. Pentru detalii, a se "
"vedea B<nptl>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4."
msgstr "POSIX.1-2001, SVr4."

#.  SVr4 documents an additional EINVAL error condition.
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Not quite compatible with the 4.4BSD call, which sets all of the real, "
"saved, and effective user IDs."
msgstr ""
"Nu este foarte compatibil cu apelul 4.4BSD, care stabilește toate ID-urile "
"de utilizator reale, salvate și efective."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The original Linux B<setuid>()  system call supported only 16-bit user IDs.  "
"Subsequently, Linux 2.4 added B<setuid32>()  supporting 32-bit IDs.  The "
"glibc B<setuid>()  wrapper function transparently deals with the variation "
"across kernel versions."
msgstr ""
"Apelul de sistem original Linux B<setuid>() suporta doar ID-uri de "
"utilizator pe 16 biți. Ulterior, Linux 2.4 a adăugat B<setuid32>() care "
"acceptă ID-uri pe 32 de biți.  Funcția învăluitoare glibc B<setuid>() "
"tratează în mod transparent variația dintre versiunile de kernel."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Linux has the concept of the filesystem user ID, normally equal to the "
"effective user ID.  The B<setuid>()  call also sets the filesystem user ID "
"of the calling process.  See B<setfsuid>(2)."
msgstr ""
"Linux are conceptul de ID de utilizator al sistemului de fișiere, în mod "
"normal egal cu ID-ul efectiv al utilizatorului. Apelul B<setuid>() "
"stabilește, de asemenea, ID-ul de utilizator din sistemul de fișiere al "
"procesului care îl apelează. A se vedea B<setfsuid>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<uid> is different from the old effective UID, the process will be "
"forbidden from leaving core dumps."
msgstr ""
"Dacă I<uid> este diferit de vechiul UID efectiv, procesului îi va fi "
"interzis să lase descărcări de imagine de memorie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getuid>(2), B<seteuid>(2), B<setfsuid>(2), B<setreuid>(2), "
"B<capabilities>(7), B<credentials>(7), B<user_namespaces>(7)"
msgstr ""
"B<getuid>(2), B<seteuid>(2), B<setfsuid>(2), B<setreuid>(2), "
"B<capabilities>(7), B<credentials>(7), B<user_namespaces>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#.  SVr4 documents an additional EINVAL error condition.
#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4.  Not quite compatible with the 4.4BSD "
"call, which sets all of the real, saved, and effective user IDs."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4. Nu este foarte compatibil cu apelul "
"4.4BSD, care stabilește toate ID-urile reale, salvate și efective ale "
"utilizatorilor."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
