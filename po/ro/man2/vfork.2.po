# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2024-12-06 18:20+0100\n"
"PO-Revision-Date: 2024-08-09 13:16+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "vfork"
msgstr "vfork"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "vfork - create a child process and block parent"
msgstr "vfork - creează un proces copil și blochează părintele"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<pid_t vfork(void);>\n"
msgstr "B<pid_t vfork(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<vfork>():"
msgstr "B<vfork>():"

#.      || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Începând cu glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* Începând cu  glibc 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Înainte de glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Standard description"
msgstr "Descrierea standard"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(From POSIX.1)  The B<vfork>()  function has the same effect as B<fork>(2), "
"except that the behavior is undefined if the process created by B<vfork>()  "
"either modifies any data other than a variable of type I<pid_t> used to "
"store the return value from B<vfork>(), or returns from the function in "
"which B<vfork>()  was called, or calls any other function before "
"successfully calling B<_exit>(2)  or one of the B<exec>(3)  family of "
"functions."
msgstr ""
"(Din POSIX.1 ) Funcția B<vfork>() are același efect ca și B<fork>(2), cu "
"excepția faptului că comportamentul este nedefinit dacă procesul creat de "
"B<vfork>() fie modifică alte date decât o variabilă de tip I<pid_t> "
"utilizată pentru a stoca valoarea de returnare de la B<vfork>(), fie "
"returnează din funcția în care a fost apelată B<vfork>(), fie apelează orice "
"altă funcție înainte de a apela cu succes B<_exit>(2) sau una dintre "
"funcțiile din familia B<exec>(3)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux description"
msgstr "Descrierea Linux"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<vfork>(), just like B<fork>(2), creates a child process of the calling "
"process.  For details and return value and errors, see B<fork>(2)."
msgstr ""
"B<vfork>(), la fel ca B<fork>(2), creează un proces-copil al procesului "
"apelant. Pentru detalii și valoarea de returnare și erori, consultați "
"B<fork>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<vfork>()  is a special case of B<clone>(2).  It is used to create new "
"processes without copying the page tables of the parent process.  It may be "
"useful in performance-sensitive applications where a child is created which "
"then immediately issues an B<execve>(2)."
msgstr ""
"B<vfork>() este un caz special de B<clone>(2). Acesta este utilizat pentru a "
"crea procese noi fără a copia tabelele de pagini ale procesului părinte. "
"Aceasta poate fi utilă în aplicațiile sensibile la performanță în care se "
"creează un proces-copil care apoi emite imediat un B<execve>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<vfork>()  differs from B<fork>(2)  in that the calling thread is suspended "
"until the child terminates (either normally, by calling B<_exit>(2), or "
"abnormally, after delivery of a fatal signal), or it makes a call to "
"B<execve>(2).  Until that point, the child shares all memory with its "
"parent, including the stack.  The child must not return from the current "
"function or call B<exit>(3)  (which would have the effect of calling exit "
"handlers established by the parent process and flushing the parent's "
"B<stdio>(3)  buffers), but may call B<_exit>(2)."
msgstr ""
"B<vfork>() diferă de B<fork>(2) prin faptul că firul apelant este suspendat "
"până când firul copil termină (fie în mod normal, prin apelarea B<_exit>(2), "
"fie în mod anormal, după transmiterea unui semnal fatal), sau efectuează un "
"apel la B<execve>(2). Până în acel moment, copilul împarte toată memoria cu "
"părintele său, inclusiv stiva. Copilul nu trebuie să se întoarcă din funcția "
"curentă sau să apeleze B<exit>(3) (care ar avea ca efect apelarea "
"gestionarilor de ieșire stabiliți de procesul părinte și golirea tampoanelor "
"B<stdio>(3) ale părintelui), dar poate apela B<_exit>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"As with B<fork>(2), the child process created by B<vfork>()  inherits copies "
"of various of the caller's process attributes (e.g., file descriptors, "
"signal dispositions, and current working directory); the B<vfork>()  call "
"differs only in the treatment of the virtual address space, as described "
"above."
msgstr ""
"Ca și în cazul B<fork>(2), procesul-copil creat de B<vfork>() moștenește "
"copii ale diferitelor atribute ale procesului apelantului (de exemplu, "
"descriptori de fișiere, dispoziții de semnal și directorul curent de lucru); "
"apelul B<vfork>() diferă doar în ceea ce privește tratarea spațiului de "
"adrese virtuale, așa cum s-a descris mai sus."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Signals sent to the parent arrive after the child releases the parent's "
"memory (i.e., after the child terminates or calls B<execve>(2))."
msgstr ""
"Semnalele trimise părintelui sosesc după ce copilul eliberează memoria "
"părintelui (de exemplu, după ce copilul termină sau apelează B<execve>(2))."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Historic description"
msgstr "Descrierea istorică (veche)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Under Linux, B<fork>(2)  is implemented using copy-on-write pages, so the "
"only penalty incurred by B<fork>(2)  is the time and memory required to "
"duplicate the parent's page tables, and to create a unique task structure "
"for the child.  However, in the bad old days a B<fork>(2)  would require "
"making a complete copy of the caller's data space, often needlessly, since "
"usually immediately afterward an B<exec>(3)  is done.  Thus, for greater "
"efficiency, BSD introduced the B<vfork>()  system call, which did not fully "
"copy the address space of the parent process, but borrowed the parent's "
"memory and thread of control until a call to B<execve>(2)  or an exit "
"occurred.  The parent process was suspended while the child was using its "
"resources.  The use of B<vfork>()  was tricky: for example, not modifying "
"data in the parent process depended on knowing which variables were held in "
"a register."
msgstr ""
"În Linux, B<fork>(2) este implementat folosind pagini copy-on-write, astfel "
"încât singura penalizare suferită de B<fork>(2) este timpul și memoria "
"necesare pentru a duplica tabelele de pagini ale părintelui și pentru a crea "
"o structură de sarcini unică pentru copil.  Cu toate acestea, în vremurile "
"de demult, un B<fork>(2) ar fi necesitat realizarea unei copii complete a "
"spațiului de date al apelantului, adesea inutil, deoarece, de obicei, "
"imediat după aceea se realizează un B<exec>(3). Astfel, pentru o mai mare "
"eficiență, BSD a introdus apelul de sistem B<vfork>(), care nu copia complet "
"spațiul de adrese al procesului părinte, ci împrumuta memoria și firul de "
"control al părintelui până la un apel la B<execve>(2) sau până la o ieșire. "
"Procesul părinte a fost suspendat în timp ce copilul folosea resursele sale. "
"Utilizarea B<vfork>() era complicată: de exemplu, pentru a nu modifica "
"datele din procesul părinte, era necesar să se cunoască variabilele care se "
"aflau într-un registru."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#.  In AIXv3.1 vfork is equivalent to fork.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The requirements put on B<vfork>()  by the standards are weaker than those "
"put on B<fork>(2), so an implementation where the two are synonymous is "
"compliant.  In particular, the programmer cannot rely on the parent "
"remaining blocked until the child either terminates or calls B<execve>(2), "
"and cannot rely on any specific behavior with respect to shared memory."
msgstr ""
"Cerințele impuse lui B<vfork>() de către standarde sunt mai slabe decât cele "
"impuse lui B<fork>(2), astfel încât o implementare în care cele două sunt "
"sinonime este conformă. În special, programatorul nu se poate baza pe faptul "
"că părintele rămâne blocat până când copilul fie se termină, fie apelează "
"B<execve>(2) și nu se poate baza pe niciun comportament specific cu privire "
"la memoria partajată."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some consider the semantics of B<vfork>()  to be an architectural blemish, "
"and the 4.2BSD man page stated: \\[lq]This system call will be eliminated "
"when proper system sharing mechanisms are implemented.  Users should not "
"depend on the memory sharing semantics of I<vfork> as it will, in that case, "
"be made synonymous to I<fork>.\\[rq] However, even though modern memory "
"management hardware has decreased the performance difference between "
"B<fork>(2)  and B<vfork>(), there are various reasons why Linux and other "
"systems have retained B<vfork>():"
msgstr ""
"Unii consideră că semantica lui B<vfork>() este un defect arhitectural, iar "
"pagina de manual 4.2BSD afirmă: „Acest apel de sistem va fi eliminat atunci "
"când sunt implementate mecanisme adecvate de partajare a sistemului. "
"Utilizatorii nu ar trebui să depindă de semantica de partajare a memoriei a "
"I<vfork> deoarece, în acest caz, va fi sinonim cu I<fork>.” Cu toate "
"acestea, chiar dacă hardware-ul modern de gestionare a memoriei a redus "
"diferența de performanță dintre B<fork>(2) și B<vfork>(), există diverse "
"motive pentru care Linux și alte sisteme au păstrat B<vfork>():"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Some performance-critical applications require the small performance "
"advantage conferred by B<vfork>()."
msgstr ""
"Unele aplicații cu performanțe critice necesită micul avantaj de performanță "
"conferit de B<vfork>()."

#.  http://stackoverflow.com/questions/4259629/what-is-the-difference-between-fork-and-vfork
#.  http://developers.sun.com/solaris/articles/subprocess/subprocess.html
#.  http://mailman.uclinux.org/pipermail/uclinux-dev/2009-April/000684.html
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<vfork>()  can be implemented on systems that lack a memory-management unit "
"(MMU), but B<fork>(2)  can't be implemented on such systems.  (POSIX.1-2008 "
"removed B<vfork>()  from the standard; the POSIX rationale for the "
"B<posix_spawn>(3)  function notes that that function, which provides "
"functionality equivalent to B<fork>(2)+ B<exec>(3), is designed to be "
"implementable on systems that lack an MMU.)"
msgstr ""
"Funcția B<vfork>() poate fi implementată pe sisteme care nu dispun de o "
"unitate de gestionare a memoriei (MMU), dar funcția B<fork>(2) nu poate fi "
"implementată pe astfel de sisteme. (POSIX.1-2008 a eliminat B<vfork>() din "
"standard; justificarea POSIX pentru funcția B<posix_spawn>(3) menționează că "
"această funcție, care oferă o funcționalitate echivalentă cu B<fork>(2)+ "
"B<exec>(3), este concepută pentru a putea fi implementată pe sisteme care nu "
"dispun de o MMU)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On systems where memory is constrained, B<vfork>()  avoids the need to "
"temporarily commit memory (see the description of I</proc/sys/vm/"
"overcommit_memory> in B<proc>(5))  in order to execute a new program.  (This "
"can be especially beneficial where a large parent process wishes to execute "
"a small helper program in a child process.)  By contrast, using B<fork>(2)  "
"in this scenario requires either committing an amount of memory equal to the "
"size of the parent process (if strict overcommitting is in force)  or "
"overcommitting memory with the risk that a process is terminated by the out-"
"of-memory (OOM) killer."
msgstr ""
"Pe sistemele în care memoria este limitată, B<vfork>() evită necesitatea de "
"a angaja temporar memoria (a se vedea descrierea I</proc/sys/vm/"
"overcommit_memory> în B<proc>(5)) pentru a executa un program nou. (Acest "
"lucru poate fi deosebit de benefic în cazul în care un proces părinte mare "
"dorește să execute un mic program ajutător într-un proces copil). În schimb, "
"utilizarea B<fork>(2) în acest scenariu necesită fie angajarea unei "
"cantități de memorie egală cu dimensiunea procesului părinte (în cazul în "
"care este în vigoare supraangajarea strictă), fie supraangajarea memoriei cu "
"riscul ca un proces să fie întrerupt de oom-killer."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux notes"
msgstr "Note Linux"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Fork handlers established using B<pthread_atfork>(3)  are not called when a "
"multithreaded program employing the NPTL threading library calls "
"B<vfork>().  Fork handlers are called in this case in a program using the "
"LinuxThreads threading library.  (See B<pthreads>(7)  for a description of "
"Linux threading libraries.)"
msgstr ""
"Gestionarii de bifurcare stabiliți utilizând B<pthread_atfork>(3) nu sunt "
"apelați atunci când un program multi-fir care utilizează biblioteca multi-"
"fire NPTL apelează B<vfork>(). În acest caz, gestionarii de bifurcări sunt "
"apelați într-un program care utilizează biblioteca multi-fire LinuxThreads. "
"Consultați B<pthreads>(7) pentru o descriere a bibliotecilor de multi-fire "
"Linux."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A call to B<vfork>()  is equivalent to calling B<clone>(2)  with I<flags> "
"specified as:"
msgstr ""
"Un apel la B<vfork>() este echivalent cu un apel la B<clone>(2) cu I<flags> "
"specificat ca:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " CLONE_VM | CLONE_VFORK | SIGCHLD\n"
msgstr " CLONE_VM | CLONE_VFORK | SIGCHLD\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Niciunul."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"4.3BSD; POSIX.1-2001 (but marked OBSOLETE).  POSIX.1-2008 removes the "
"specification of B<vfork>()."
msgstr ""
"4.3BSD; POSIX.1-2001 (dar marcat ca OBSOLET).  POSIX.1-2008 elimină "
"specificația B<vfork>()."

#.  In the release notes for 4.2BSD Sam Leffler wrote: `vfork: Is still
#.  present, but definitely on its way out'.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<vfork>()  system call appeared in 3.0BSD.  In 4.4BSD it was made "
"synonymous to B<fork>(2)  but NetBSD introduced it again; see E<.UR http://"
"www.netbsd.org\\:/Documentation\\:/kernel\\:/vfork.html> E<.UE .> In Linux, "
"it has been equivalent to B<fork>(2)  until Linux 2.2.0-pre6 or so.  Since "
"Linux 2.2.0-pre9 (on i386, somewhat later on other architectures) it is an "
"independent system call.  Support was added in glibc 2.0.112."
msgstr ""
"Apelul de sistem B<vfork>() a apărut în 3.0BSD. În 4.4BSD a devenit sinonim "
"cu B<fork>(2), dar NetBSD l-a introdus din nou; a se vedea E<.UR http://"
"www.netbsd.org\\:/Documentation\\:/kernel\\:/vfork.html> E<.UE >. În Linux, "
"a fost echivalent cu B<fork>(2) până la Linux 2.2.0-pre6 aproximativ. De la "
"Linux 2.2.0-pre9 (pe i386, ceva mai târziu pe alte arhitecturi) este un apel "
"de sistem independent. Suportul pentru acesta a fost adăugat în glibc "
"2.0.112."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr "AVERTISMENTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The child process should take care not to modify the memory in unintended "
"ways, since such changes will be seen by the parent process once the child "
"terminates or executes another program.  In this regard, signal handlers can "
"be especially problematic: if a signal handler that is invoked in the child "
"of B<vfork>()  changes memory, those changes may result in an inconsistent "
"process state from the perspective of the parent process (e.g., memory "
"changes would be visible in the parent, but changes to the state of open "
"file descriptors would not be visible)."
msgstr ""
"Procesul-copil trebuie să aibă grijă să nu modifice memoria în moduri "
"neintenționate, deoarece astfel de modificări vor fi văzute de procesul "
"părinte odată ce copilul termină sau execută un alt program. În această "
"privință, gestionarii de semnal pot fi deosebit de problematici: dacă un "
"gestionar de semnal care este invocat în copilul lui B<vfork>() modifică "
"memoria, aceste modificări pot duce la o stare inconsecventă a procesului "
"din perspectiva procesului părinte (de exemplu, modificările de memorie ar "
"fi vizibile în procesul părinte, dar modificările stării descriptorilor de "
"fișiere deschise nu ar fi vizibile)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When B<vfork>()  is called in a multithreaded process, only the calling "
"thread is suspended until the child terminates or executes a new program.  "
"This means that the child is sharing an address space with other running "
"code.  This can be dangerous if another thread in the parent process changes "
"credentials (using B<setuid>(2)  or similar), since there are now two "
"processes with different privilege levels running in the same address "
"space.  As an example of the dangers, suppose that a multithreaded program "
"running as root creates a child using B<vfork>().  After the B<vfork>(), a "
"thread in the parent process drops the process to an unprivileged user in "
"order to run some untrusted code (e.g., perhaps via plug-in opened with "
"B<dlopen>(3)).  In this case, attacks are possible where the parent process "
"uses B<mmap>(2)  to map in code that will be executed by the privileged "
"child process."
msgstr ""
"Atunci când B<vfork>() este apelat într-un proces cu mai multe fire de "
"execuție, numai firul de execuție apelant este suspendat până când copilul "
"termină sau execută un nou program. Aceasta înseamnă că copilul împarte un "
"spațiu de adrese cu alt cod care rulează. Acest lucru poate fi periculos "
"dacă un alt fir de execuție din procesul părinte își schimbă acreditările "
"(utilizând B<setuid>(2) sau similar), deoarece există acum două procese cu "
"niveluri de privilegii diferite care rulează în același spațiu de adrese. Ca "
"exemplu al pericolelor, să presupunem că un program cu mai multe fire de "
"execuție care rulează ca root creează un copil folosind B<vfork>(). După "
"B<vfork>(), un fir de execuție din procesul părinte scade procesul la un "
"utilizator fără privilegii pentru a rula un cod de neîncredere (de exemplu, "
"poate prin intermediul unui modul deschis cu B<dlopen>(3)). În acest caz, "
"sunt posibile atacuri în care procesul părinte utilizează B<mmap>(2) pentru "
"a pune în memorie codul care va fi executat de procesul copil privilegiat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#
#.  As far as I can tell, the following is not true in Linux 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Details of the signal handling are obscure and differ between systems.  The "
"BSD man page states: \"To avoid a possible deadlock situation, processes "
"that are children in the middle of a B<vfork>()  are never sent B<SIGTTOU> "
"or B<SIGTTIN> signals; rather, output or I<ioctl>s are allowed and input "
"attempts result in an end-of-file indication.\""
msgstr ""
"Detaliile de gestionare a semnalelor sunt obscure și diferă de la un sistem "
"la altul. Pagina de manual BSD afirmă: „Pentru a evita o posibilă situație "
"de blocaj, procesele care sunt copii în mijlocul unui B<vfork>() nu primesc "
"niciodată semnale B<SIGTTOU> sau B<SIGTTIN>; mai degrabă, sunt permise "
"ieșirile sau I<ioctl>s, iar încercările de intrare au ca rezultat o "
"indicație de sfârșit de fișier.”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<clone>(2), B<execve>(2), B<_exit>(2), B<fork>(2), B<unshare>(2), B<wait>(2)"
msgstr ""
"B<clone>(2), B<execve>(2), B<_exit>(2), B<fork>(2), B<unshare>(2), B<wait>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: debian-bookworm
msgid ""
"Some consider the semantics of B<vfork>()  to be an architectural blemish, "
"and the 4.2BSD man page stated: \"This system call will be eliminated when "
"proper system sharing mechanisms are implemented.  Users should not depend "
"on the memory sharing semantics of B<vfork>()  as it will, in that case, be "
"made synonymous to B<fork>(2).  \" However, even though modern memory "
"management hardware has decreased the performance difference between "
"B<fork>(2)  and B<vfork>(), there are various reasons why Linux and other "
"systems have retained B<vfork>():"
msgstr ""
"Unii consideră că semantica lui B<vfork>() este o imperfecțiune "
"arhitecturală, iar pagina de manual 4.2BSD a declarat: „Acest apel de sistem "
"va fi eliminat atunci când sunt implementate mecanisme adecvate de partajare "
"a sistemului. Utilizatorii nu ar trebui să depindă de semantica de partajare "
"a memoriei a B<vfork>() deoarece, în acest caz, va fi sinonim cu "
"B<fork>(2).” Totuși, chiar dacă hardware-ul modern de gestionare a memoriei "
"a redus diferența de performanță dintre B<fork>(2) și B<vfork>(), există "
"diverse motive pentru care Linux și alte sisteme au păstrat B<vfork>():"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Caveats"
msgstr "Avertismente"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "History"
msgstr "Istoric"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
