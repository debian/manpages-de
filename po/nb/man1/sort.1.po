# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2024-12-06 18:15+0100\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SORT"
msgstr "SORT"

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2024"
msgstr "August 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sort - sort lines of text files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sort> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<sort> [I<\\,VALG\\/>]... [I<\\,FIL\\/>]..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sort> [I<\\,OPTION\\/>]... I<\\,--files0-from=F\\/>"
msgstr "B<sort> [I<\\,VALG\\/>]... I<\\,--files0-from=F\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Write sorted concatenation of all FILE(s) to standard output."
msgstr "Skriv ut sorterte sammenslåinger av alle FILer til standardutdata."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"Hvis ingen FIL er valgt, eller hvis FIL er «-», leser programmet standard "
"inndata."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too.  "
"Ordering options:"
msgstr ""
"Argumenter som er obligatoriske for lange valg, er også obligatoriske for "
"korte valg. Sorteringsvalg:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--ignore-leading-blanks>"
msgstr "B<-b>, B<--ignore-leading-blanks>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ignore leading blanks"
msgstr "Ignorer ledende tomrom."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--dictionary-order>"
msgstr "B<-d>, B<--dictionary-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "consider only blanks and alphanumeric characters"
msgstr "Bare sjekk tomrom-, alfabet- og talltegn."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--ignore-case>"
msgstr "B<-f>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "fold lower case to upper case characters"
msgstr "Behandle store og små bokstaver likt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-g>, B<--general-numeric-sort>"
msgstr "B<-g>, B<--general-numeric-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "compare according to general numerical value"
msgstr "Sammenlikn i henhold til generell tallverdi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-nonprinting>"
msgstr "B<-i>, B<--ignore-nonprinting>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "consider only printable characters"
msgstr "Bare vurder utskrivbare tegn."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-M>, B<--month-sort>"
msgstr "B<-M>, B<--month-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "compare (unknown) E<lt> 'JAN' E<lt> ... E<lt> 'DEC'"
msgstr "Sammenlikn (ukjent) E<lt> «JAN» E<lt> ... E<lt> «DEC»"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-numeric-sort>"
msgstr "B<-h>, B<--human-numeric-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "compare human readable numbers (e.g., 2K 1G)"
msgstr "Sammenlikne menneskelig lesbare tall (f.eks. 2K 1G)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--numeric-sort>"
msgstr "B<-n>, B<--numeric-sort>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "compare according to string numerical value"
msgid ""
"compare according to string numerical value; see manual for which strings "
"are supported"
msgstr "Sammenlikne i henhold til strengens tallverdi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--random-sort>"
msgstr "B<-R>, B<--random-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "shuffle, but group identical keys.  See shuf(1)"
msgid "shuffle, but group identical keys.  See B<shuf>(1)"
msgstr "Stokk om, men gruppér identiske nøkler. Se B<shuf>(1)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--random-source>=I<\\,FILE\\/>"
msgstr "B<--random-source>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "get random bytes from FILE"
msgstr "Hent vilkårlighetsdata fra FIL."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--reverse>"
msgstr "B<-r>, B<--reverse>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "reverse the result of comparisons"
msgstr "Vis resutater i omvendt rekkefølge."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--sort>=I<\\,WORD\\/>"
msgstr "B<--sort>=I<\\,ORD\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"sort according to WORD: general-numeric B<-g>, human-numeric B<-h>, month B<-"
"M>, numeric B<-n>, random B<-R>, version B<-V>"
msgstr ""
"Sorter i henhold til ORD: general-numeric B<-g>, human-numeric B<-h>, month "
"B<-M>, numeric B<-n>, random B<-R>, version B<-V>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version-sort>"
msgstr "B<-V>, B<--version-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "natural sort of (version) numbers within text"
msgstr "Sorter etter evt. versjonsnummer i tekst."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Other options:"
msgstr "Andre valg:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--batch-size>=I<\\,NMERGE\\/>"
msgstr "B<--batch-size>=I<\\,NMERGE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "merge at most NMERGE inputs at once; for more use temp files"
msgstr ""
"Ikke slå sammen flere enn NMERGE antall inndata på én gang (bruk "
"midlertidige filer for å slå sammen flere)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--check>, B<--check>=I<\\,diagnose-first\\/>"
msgstr "B<-c>, B<--check>, B<--check>=I<\\,diagnose-first\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "check for sorted input; do not sort"
msgstr "Sjekk om inndata allerede er sortert (ikke sorter)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--check>=I<\\,quiet\\/>, B<--check>=I<\\,silent\\/>"
msgstr "B<-C>, B<--check>=I<\\,quiet\\/>, B<--check>=I<\\,silent\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "like B<-c>, but do not report first bad line"
msgstr "Likner B<-c>, men rapporterer ikke første feilaktige linje."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--compress-program>=I<\\,PROG\\/>"
msgstr "B<--compress-program>=I<\\,PROG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "compress temporaries with PROG; decompress them with PROG B<-d>"
msgstr "Pakker ned midlertidige filer med PROG (pakk dem ut med PROG B<-d>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--debug>"
msgstr "B<--debug>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"annotate the part of the line used to sort, and warn about questionable "
"usage to stderr"
msgstr ""
"Marker den delen av linja som brukes i sortering, og varsle om tvilsom bruk "
"til standardfeil."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--files0-from>=I<\\,F\\/>"
msgstr "B<--files0-from>=I<\\,F\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"read input from the files specified by NUL-terminated names in file F; If F "
"is - then read names from standard input"
msgstr ""
"Les inndata fra NUL(tomrom-)-adskilte filnavn i fil F (hvis F er «-», leser "
"programmet standard inndata)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--key>=I<\\,KEYDEF\\/>"
msgstr "B<-k>, B<--key>=I<\\,KNAPPDEF\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sort via a key; KEYDEF gives location and type"
msgstr "Sorter via en knapp (KNAPPDEF angir plassering og type)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--merge>"
msgstr "B<-m>, B<--merge>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "merge already sorted files; do not sort"
msgstr "Slå sammen filer som allerede er sortert (ikke sorter)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "write result to FILE instead of standard output"
msgstr "Skriv resultat til valgt FIL, i stedet for standardutdata."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--stable>"
msgstr "B<-s>, B<--stable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "stabilize sort by disabling last-resort comparison"
msgstr "Stabiliser programmet ved å deaktivere siste løsning-metoden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--buffer-size>=I<\\,SIZE\\/>"
msgstr "B<-S>, B<--buffer-size>=I<\\,STØRR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use SIZE for main memory buffer"
msgstr "Bruk valgt STØRRelse på hovedmellomlager."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--field-separator>=I<\\,SEP\\/>"
msgstr "B<-t>, B<--field-separator>=I<\\,SKIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use SEP instead of non-blank to blank transition"
msgstr ""
"Bruk valgt SKILletegn, i stedet for overgang fra ikke-tomrom til tomrom."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--temporary-directory>=I<\\,DIR\\/>"
msgstr "B<-T>, B<--temporary-directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use DIR for temporaries, not $TMPDIR or I<\\,/tmp\\/>; multiple options "
"specify multiple directories"
msgstr ""
"Bruk angitt MAPPE for midlertidige filer, i stedet for $TMPDIR eller I<\\,/"
"tmp\\/> (flere valg angir flere mapper her)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--parallel>=I<\\,N\\/>"
msgstr "B<--parallel>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "change the number of sorts run concurrently to N"
msgstr "Endre hvor mange (N) sorteringer som kan utføres samtidig."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--unique>"
msgstr "B<-u>, B<--unique>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"with B<-c>, check for strict ordering; without B<-c>, output only the first "
"of an equal run"
msgstr ""
"Ved bruk sammen med B<-c>: se etter streng sortering ved bruk uten B<-c>: "
"bare skriv ut det første tilfellet av en tilsvarende kjøring."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "Skill mellom linjer med NUL i stedet for linjeskift."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "Vis denne hjelpeteksten og avslutt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Vis versjonsinformasjon og avslutt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"KEYDEF is F[.C][OPTS][,F[.C][OPTS]] for start and stop position, where F is "
"a field number and C a character position in the field; both are origin 1, "
"and the stop position defaults to the line's end.  If neither B<-t> nor B<-"
"b> is in effect, characters in a field are counted from the beginning of the "
"preceding whitespace.  OPTS is one or more single-letter ordering options "
"[bdfgiMhnRrV], which override global ordering options for that key.  If no "
"key is given, use the entire line as the key.  Use B<--debug> to diagnose "
"incorrect key usage."
msgstr ""
"KNAPPDEF er F[.C][VALG][,F[.C][VALG]] for start- og stoppunkt, der F er et "
"feltnummer og C en tegnposisjon innenfor feltet. Begge telles fra 1, og "
"stoppunktet er slutten av linja som standard.  Hvis hverken B<-t> eller B<-"
"b> er i bruk, telles tegnene i et felt fra tomrommet som ligger foran. "
"VALGene kan gjøres i form av énbokstavs-sorteringsvalg [bdfgiMhnRrV], og "
"overstyrer globale sorteringsvalg for aktuell nøkkel. Hele linja brukes som "
"nøkkel hvis ingen nøkkel oppgis. Bruk B<--debug> for å se etter feil i "
"nøkler."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SIZE may be followed by the following multiplicative suffixes: % 1% of "
#| "memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y."
msgid ""
"SIZE may be followed by the following multiplicative suffixes: % 1% of "
"memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y, R, Q."
msgstr ""
"STØRRelsen kan etterfølges av følgende multiplikasjonssuffikser: % 1% av "
"minne, b 1, K 1024 (standard) og så videre (M, G, T, P, E, Z, Y)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"*** WARNING *** The locale specified by the environment affects sort order.  "
"Set LC_ALL=C to get the traditional sort order that uses native byte values."
msgstr ""
"*** ADVARSEL ***: Miljøets regioninnstilling påvirker sorteringsrekkefølge. "
"Velg LC_ALL=C for tradisjonell sortering med innebygde byte-verdier."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "OPPHAVSMANN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Mike Haertel and Paul Eggert."
msgstr "Skrevet av Mike Haertel og Paul Eggert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Få hjelp til bruk av GNU coreutils på nett: E<lt>https://www.gnu.org/"
"software/coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapporter oversettelsesfeil til E<lt>https://translationproject.org/team/nb."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPPHAVSRETT"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er fri programvare. Du kan endre og dele den videre. Det stilles INGEN "
"GARANTI, i den grad dette tillates av gjeldende lovverk."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "shuf(1), uniq(1)"
msgid "B<shuf>(1), B<uniq>(1)"
msgstr "B<shuf>(1), B<uniq>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/sortE<gt>"
msgstr ""
"Fullstendig dokumentasjon: E<lt>https://www.gnu.org/software/coreutils/"
"sortE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) sort invocation\\(aq"
msgstr "eller lokalt: info \\(aq(coreutils) sort invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0
msgid "compare according to string numerical value"
msgstr "Sammenlikne i henhold til strengens tallverdi."

#. type: Plain text
#: debian-bookworm
msgid ""
"SIZE may be followed by the following multiplicative suffixes: % 1% of "
"memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y."
msgstr ""
"STØRRelsen kan etterfølges av følgende multiplikasjonssuffikser: % 1% av "
"minne, b 1, K 1024 (standard) og så videre (M, G, T, P, E, Z, Y)."

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "Oktober 2024"

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "September 2024"
msgstr "September 2024"

#. type: TH
#: fedora-rawhide
#, fuzzy, no-wrap
#| msgid "September 2024"
msgid "November 2024"
msgstr "September 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "August 2023"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Januar 2024"
