# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-02-15 18:53+0100\n"
"PO-Revision-Date: 2022-07-22 14:47+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GRUB-RPM-SORT"
msgstr "GRUB-RPM-SORT"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2022"
msgstr "Oktober 2022"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemadministrasjonsverktøy"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: mageia-cauldron
msgid "grub-rpm-sort - sort input according to RPM version compare"
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid "B<grub-mklayout> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgid "B<grub-rpm-sort> [I<\\,OPTION\\/>...] [I<\\,INPUT_FILES\\/>]"
msgstr "B<grub-mklayout> [I<\\,VALG\\/>...] [I<\\,VALG\\/>]"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: mageia-cauldron
msgid "Sort a list of strings in RPM version sort order."
msgstr ""

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: mageia-cauldron
msgid "give this help list"
msgstr "vis denne hjelpelista"

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: mageia-cauldron
msgid "give a short usage message"
msgstr "vis en kortfattet bruksanvisning"

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: mageia-cauldron
msgid "print program version"
msgstr "skriv ut programversjon"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: mageia-cauldron
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Rapporter feil til E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "The full documentation for B<grub-reboot> is maintained as a Texinfo "
#| "manual.  If the B<info> and B<grub-reboot> programs are properly "
#| "installed at your site, the command"
msgid ""
"The full documentation for B<grub-rpm-sort> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-rpm-sort> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-reboot> opprettholdes som en "
"Texinfo manual. Dersom B<info> og B<grub-reboot> programmene er riktig "
"installert på ditt sted burde kommandoen"

#. type: Plain text
#: mageia-cauldron
msgid "B<info grub-rpm-sort>"
msgstr "B<info grub-rpm-sort>"

#. type: Plain text
#: mageia-cauldron
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."
