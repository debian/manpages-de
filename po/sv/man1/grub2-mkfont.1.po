# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:00+0100\n"
"PO-Revision-Date: 2024-05-04 22:51+0200\n"
"Last-Translator: Göran Uddeborg <goeran@uddeborg.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKFONT"
msgstr "GRUB-MKFONT"

#. type: TH
#: fedora-41 opensuse-leap-16-0
#, no-wrap
msgid "October 2024"
msgstr "oktober 2024"

#. type: TH
#: fedora-41 fedora-rawhide
#, no-wrap
msgid "GRUB 2.12"
msgstr "GRUB 2.12"

#. type: TH
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "grub-mkfont - make GRUB font files"
msgstr "grub-mkfont — skapa GRUB-typsnittsfiler"

#. type: SH
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<grub-mkfont> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>] I<\\,FONT_FILES\\/>"
msgstr ""
"B<grub-mkfont> [I<\\,FLAGGA\\/>...] [I<\\,FLAGGOR\\/>] I<\\,TYPSNITTSFILER\\/"
">"

#. type: SH
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "Convert common font file formats into PF2"
msgstr "Konvertera vanliga typsnittsfilformat till PF2"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--force-autohint>"
msgstr "B<-a>, B<--force-autohint>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "force autohint"
msgstr "tvinga autotips"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--bold>"
msgstr "B<-b>, B<--bold>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "convert to bold font"
msgstr "konvertera till fett typsnitt"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--asce>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--asce>=I<\\,NUM\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "set font ascent"
msgstr "set typsnittsascent"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--desc>=I<\\,NUM\\/>"
msgstr "B<-d>, B<--desc>=I<\\,NUM\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "set font descent"
msgstr "Ange typsnittsunderhäng"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--index>=I<\\,NUM\\/>"
msgstr "B<-i>, B<--index>=I<\\,NUM\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "select face index"
msgstr "välj typsnittsindex"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-bitmap>"
msgstr "B<--no-bitmap>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "ignore bitmap strikes when loading"
msgstr "ignorera bitmaplinjer vid inläsning"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-hinting>"
msgstr "B<--no-hinting>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "disable hinting"
msgstr "inaktivera autotips"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--name>=I<\\,NAME\\/>"
msgstr "B<-n>, B<--name>=I<\\,NAMN\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "set font family name"
msgstr "ange namn för typsnittsfamilj"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "save output in FILE [required]"
msgstr "spara utmatning i FIL [krävs]"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--range>=I<\\,FROM-TO[\\/>,FROM-TO]"
msgstr "B<-r>, B<--range>=I<\\,FRÅN-TILL[\\/>,FRÅN-TILL]"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "set font range"
msgstr "sätt omfång för typsnitt"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--size>=I<\\,SIZE\\/>"
msgstr "B<-s>, B<--size>=I<\\,STORLEK\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "set font size"
msgstr "ange typsnittstorlek"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "print verbose messages."
msgstr "skriv ut informativa meddelanden."

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "give this help list"
msgstr "visa denna hjälplista"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "give a short usage message"
msgstr "ge ett kort användningsmeddelande"

#. type: TP
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "print program version"
msgstr "skriv ut programversion"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriska eller valfria argument till långa flaggor är också "
"obligatoriska eller valfria för motsvarande korta flaggor."

#. type: SH
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapportera fel till E<lt>bug-grub@gnu.orgE<gt>. Skicka synpunkter på "
"översättningen till E<gt>tp-sv@listor.tp-sv.seE<lt>."

#. type: SH
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkfont> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkfont> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<grub-mkfont> underhålls som en "
"Texinfo-manual. Om programmen B<info> och B<grub-mkfont> är ordentligt "
"installerade på ditt system, bör kommandot"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info grub-mkfont>"
msgstr "B<info grub-mkfont>"

#. type: Plain text
#: fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "November 2024"
msgstr "november 2024"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"
