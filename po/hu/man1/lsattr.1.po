# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Németh Péter <qgenpete@gold.uni-miskolc.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Németh Péter <qgenpete@gold.uni-miskolc.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LSATTR"
msgstr "LSATTR"

#. type: TH
#: archlinux debian-bookworm fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "May 2024"
msgstr "2024 május"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "E2fsprogs version 1.47.1"
msgstr "E2fsprogs verzió 1.47.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "lsattr - list ext2/ext3/ext4 file attributes"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<lsattr> [ B<-RVadlpv> ] [ I<files...> ]"
msgstr "B<lsattr> [ B<-RVadlpv> ] [ I<fájlok...> ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<lsattr> lists the file attributes on a second extended file system."
msgid ""
"B<lsattr> lists the file attributes on an ext2/ext3/ext4 file system.  See "
"B<chattr>(1)  for a description of the attributes and what they mean."
msgstr "Az B<lsattr> megmutatja a fájlok attribútumát az ext2 fájlrendszeren."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "KAPCSOLÓK"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>"
msgstr "B<-R>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Recursively list attributes of directories and their contents."
msgstr "Minden könyvtár tartalmát rekurzívan listázza."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "List the files version."
msgid "Display the program version."
msgstr "Kiírja a fájl verziószámát."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "List all files in directories, including files that start with `.'."
msgstr "Minden könyvtárbeli fájlt listáz, beleértve a `.'-tal kezdődőeket is."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "List directories like other files, rather than listing their contents."
msgstr ""
"A könyvtárakat a többi fájlhoz hasonlóan listázza ahelyett, hogy a "
"tartalmukat listázná."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Print the options using long names instead of single character abbreviations."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "List the files version."
msgid "List the file's project number."
msgstr "Kiírja a fájl verziószámát."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "List the files version."
msgid "List the file's version/generation number."
msgstr "Kiírja a fájl verziószámát."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "SZERZŐ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<lsattr> was written by Remy Card E<lt>Remy.Card@linux.orgE<gt>.  It is "
"currently being maintained by Theodore Ts'o E<lt>tytso@alum.mit.eduE<gt>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "HIBÁK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "There are none :-)."
msgstr "Nincs jelenleg semmilyen ismert hiba :-)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "ELÉRHETŐSÉG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<lsattr> is part of the e2fsprogs package and is available from http://"
"e2fsprogs.sourceforge.net."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chattr>(1)"
msgstr "B<chattr>(1)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "E2fsprogs version 1.47.1-rc2"
msgstr "E2fsprogs verzió 1.47.1-rc2"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
msgid "lsattr - list file attributes on a Linux second extended file system"
msgstr "lsattr - megmutatja a fájlok attribútumát az ext2 fájlrendszeren"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<lsattr> lists the file attributes on a second extended file system."
msgid ""
"B<lsattr> lists the file attributes on a second extended file system.  See "
"B<chattr>(1)  for a description of the attributes and what they mean."
msgstr "Az B<lsattr> megmutatja a fájlok attribútumát az ext2 fájlrendszeren."

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "September 2024"
msgid "November 2024"
msgstr "2024 szeptember"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "E2fsprogs version 1.47.2-rc1"
msgstr "E2fsprogs verzió 1.47.2-rc1"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "2023 február"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr "E2fsprogs verzió 1.47.0"
