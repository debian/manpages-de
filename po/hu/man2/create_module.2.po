# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Böszörményi Zoltán <zboszor@mail.externet.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:56+0100\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Böszörményi Zoltán <zboszor@mail.externet.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "create_module"
msgstr "create_module"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2024. május 2"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "create_module - create a loadable module entry"
msgstr "create_module - betölthető modul területet hoz létre"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/module.hE<gt>>\n"
msgstr "B<#include E<lt>linux/module.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<caddr_t create_module(const char *>I<name>B<, size_t >I<size>B<);>\n"
msgid "B<[[deprecated]] caddr_t create_module(const char *>I<name>B<, size_t >I<size>B<);>\n"
msgstr "B<caddr_t create_module(const char *>I<name>B<, size_t >I<size>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<Note>: This system call is present only before Linux 2.6."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<create_module> attempts to create a loadable module entry and reserve "
#| "the kernel memory that will be needed to hold the module.  This system "
#| "call is only open to the superuser."
msgid ""
"B<create_module>()  attempts to create a loadable module entry and reserve "
"the kernel memory that will be needed to hold the module.  This system call "
"requires privilege."
msgstr ""
"A B<create_module> megpróbál egy betölthető modul területet létrehozni, és "
"kernelmemóriát lefoglalni, amely a modul tárolására szolgál. Ezt a "
"rendszerhívást csak a superuser használhatja."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VISSZATÉRÉSI ÉRTÉK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, returns the kernel address at which the module will reside.  "
#| "On error -1 is returned and I<errno> is set appropriately."
msgid ""
"On success, returns the kernel address at which the module will reside.  On "
"error, -1 is returned and I<errno> is set to indicate the error."
msgstr ""
"Siker esetén azt a kernel memória címet adja vissza, ahol a modul fog helyet "
"foglalni.  Hiba esetén az érték -1, és az I<errno> megfelelően lesz "
"beállítva."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "HIBÁK"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "A module by that name already exists."
msgstr "Egy ilyen nevű modul már létezik."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<name> is outside the program's accessible address space."
msgstr "A I<name> kívül esik a program által elérhető címtartományon."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The requested size is too small even for the module header information."
msgstr "A kívánt méret túl kicsi még a modul fejléc információjának is."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The kernel could not allocate a contiguous block of memory large enough for "
"the module."
msgstr ""
"A kernel nem tudott a modul számára elegendően nagy folyamatos "
"memóriablokkot lefoglalni."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<create_module>()  is not supported in this version of the kernel (e.g., "
"Linux 2.6 or later)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The caller was not privileged (did not have the B<CAP_SYS_MODULE> "
"capability)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "SZABVÁNYOK"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "TÖRTÉNET"

#.  Removed in Linux 2.5.48
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Removed in Linux 2.6."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This obsolete system call is not supported by glibc.  No declaration is "
"provided in glibc headers, but, through a quirk of history, glibc versions "
"before glibc 2.23 did export an ABI for this system call.  Therefore, in "
"order to employ this system call, it was sufficient to manually declare the "
"interface in your code; alternatively, you could invoke the system call "
"using B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<delete_module>(2), B<init_module>(2), B<query_module>(2)"
msgstr "B<delete_module>(2), B<init_module>(2), B<query_module>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "2022. december 4"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERZIÓK"

#.  Removed in Linux 2.5.48
#. type: Plain text
#: debian-bookworm
msgid ""
"This system call is present only up until Linux 2.4; it was removed in Linux "
"2.6."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<create_module>()  is Linux-specific."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "MEGJEGYZÉS"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.7"
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages 6.7"
