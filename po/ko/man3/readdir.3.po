# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 임종균 <hermes44@secsm.org>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:10+0100\n"
"PO-Revision-Date: 2001-06-28 08:57+0900\n"
"Last-Translator: 임종균 <hermes44@secsm.org>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "readdir"
msgstr "readdir"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-16"
msgstr "2024년 6월 16일"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "readdir - read a directory"
msgstr "readdir - 디렉토리를 읽다"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>dirent.hE<gt>>\n"
msgstr "B<#include E<lt>dirent.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<struct dirent *readdir(DIR *>I<dirp>B<);>\n"
msgstr "B<struct dirent *readdir(DIR *>I<dirp>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<readdir()> function returns a pointer to a dirent structure "
#| "representing the next directory entry in the directory stream pointed to "
#| "by I<dir>.  It returns NULL on reaching the end-of-file or if an error "
#| "occurred."
msgid ""
"The B<readdir>()  function returns a pointer to a I<dirent> structure "
"representing the next directory entry in the directory stream pointed to by "
"I<dirp>.  It returns NULL on reaching the end of the directory stream or if "
"an error occurred."
msgstr ""
"B<readdir()> 함수는 I<dir>이 가리키는 디렉토리 기술자의 다음 디렉토리 항목을 "
"나타내는 dirent 구조체를 반환한다. 파일 끝에 도달하거나 에러가 발생하면 NULL"
"을 반환한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the glibc implementation, the I<dirent> structure is defined as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct dirent {\n"
"    ino_t          d_ino;       /* Inode number */\n"
"    off_t          d_off;       /* Not an offset; see below */\n"
"    unsigned short d_reclen;    /* Length of this record */\n"
"    unsigned char  d_type;      /* Type of file; not supported\n"
"                                   by all filesystem types */\n"
"    char           d_name[256]; /* Null-terminated filename */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The only fields in the I<dirent> structure that are mandated by POSIX.1 are "
"I<d_name> and I<d_ino>.  The other fields are unstandardized, and not "
"present on all systems; see VERSIONS."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "The options are as follows:"
msgid "The fields of the I<dirent> structure are as follows:"
msgstr "여기서 사용되는 옵션은 다음과 같다:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<d_ino>"
msgstr "I<d_ino>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This is the inode number of the file."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<d_off>"
msgstr "I<d_off>"

#.  https://lwn.net/Articles/544298/
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value returned in I<d_off> is the same as would be returned by calling "
"B<telldir>(3)  at the current position in the directory stream.  Be aware "
"that despite its type and name, the I<d_off> field is seldom any kind of "
"directory offset on modern filesystems.  Applications should treat this "
"field as an opaque value, making no assumptions about its contents; see also "
"B<telldir>(3)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<d_reclen>"
msgstr "I<d_reclen>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This is the size (in bytes) of the returned record.  This may not match the "
"size of the structure definition shown above; see VERSIONS."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<d_type>"
msgstr "I<d_type>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This field contains a value indicating the file type, making it possible to "
"avoid the expense of calling B<lstat>(2)  if further actions depend on the "
"type of the file."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When a suitable feature test macro is defined (B<_DEFAULT_SOURCE> since "
"glibc 2.19, or B<_BSD_SOURCE> on glibc 2.19 and earlier), glibc defines the "
"following macro constants for the value returned in I<d_type>:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DT_BLK>"
msgstr "B<DT_BLK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This is a block device."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DT_CHR>"
msgstr "B<DT_CHR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This is a character device."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DT_DIR>"
msgstr "B<DT_DIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "No such directory."
msgid "This is a directory."
msgstr "해당 디렉토리가 없다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DT_FIFO>"
msgstr "B<DT_FIFO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This is a named pipe (FIFO)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DT_LNK>"
msgstr "B<DT_LNK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This is a symbolic link."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DT_REG>"
msgstr "B<DT_REG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This is a regular file."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DT_SOCK>"
msgstr "B<DT_SOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This is a UNIX domain socket."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<DT_UNKNOWN>"
msgstr "B<DT_UNKNOWN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The file type could not be determined."
msgstr ""

#.  kernel 2.6.27
#.  The same sentence is in getdents.2
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Currently, only some filesystems (among them: Btrfs, ext2, ext3, and ext4)  "
"have full support for returning the file type in I<d_type>.  All "
"applications must properly handle a return of B<DT_UNKNOWN>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<d_name>"
msgstr "I<d_name>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "This field contains the null terminated filename; see VERSIONS."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The data returned by B<readdir()> is overwritten by subsequent calls to "
#| "B<readdir()> for the same directory stream."
msgid ""
"The data returned by B<readdir>()  may be overwritten by subsequent calls to "
"B<readdir>()  for the same directory stream."
msgstr ""
"B<readdir()>에 의해 반환되는 데이터는 같은 디렉토리 스트림에 대한 다음의 "
"B<readdir()> 호출에 의해 덮어 쓰여진다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<readdir>()  returns a pointer to a I<dirent> structure.  (This "
"structure may be statically allocated; do not attempt to B<free>(3)  it.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the end of the directory stream is reached, NULL is returned and I<errno> "
"is not changed.  If an error occurs, NULL is returned and I<errno> is set to "
"indicate the error.  To distinguish end of stream from an error, set "
"I<errno> to zero before calling B<readdir>()  and then check the value of "
"I<errno> if NULL is returned."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Invalid directory stream descriptor I<dirp>."
msgstr "무효한 디렉토리 스트림 기술자 I<dirp>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "속성"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"이 섹션에서 사용되는 용어에 대한 설명은 B<attributes>(7)을 참조하십시오."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "상호 작용"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "속성"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "번호"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<readdir>()"
msgstr "B<readdir>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:dirstream"
msgstr ""

#.  FIXME .
#.  http://www.austingroupbugs.net/view.php?id=696
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the current POSIX.1 specification (POSIX.1-2008), B<readdir>()  is not "
"required to be thread-safe.  However, in modern implementations (including "
"the glibc implementation), concurrent calls to B<readdir>()  that specify "
"different directory streams are thread-safe.  In cases where multiple "
"threads must read from the same directory stream, using B<readdir>()  with "
"external synchronization is still preferable to the use of the deprecated "
"B<readdir_r>(3)  function.  It is expected that a future version of POSIX.1 "
"will require that B<readdir>()  be thread-safe when concurrently employed on "
"different directory streams."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#.  POSIX.1-2001, POSIX.1-2008
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Only the fields I<d_name> and (as an XSI extension)  I<d_ino> are specified "
"in POSIX.1.  Other than Linux, the I<d_type> field is available mainly only "
"on BSD systems.  The remaining fields are available on many, but not all "
"systems.  Under glibc, programs can check for the availability of the fields "
"not defined in POSIX.1 by testing whether the macros "
"B<_DIRENT_HAVE_D_NAMLEN>, B<_DIRENT_HAVE_D_RECLEN>, B<_DIRENT_HAVE_D_OFF>, "
"or B<_DIRENT_HAVE_D_TYPE> are defined."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "The d_name field"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<dirent> structure definition shown above is taken from the glibc "
"headers, and shows the I<d_name> field with a fixed size."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<Warning>: applications should avoid any dependence on the size of the "
"I<d_name> field.  POSIX defines it as I<char\\ d_name[]>, a character array "
"of unspecified size, with at most B<NAME_MAX> characters preceding the "
"terminating null byte (\\[aq]\\[rs]0\\[aq])."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 explicitly notes that this field should not be used as an lvalue.  "
"The standard also notes that the use of I<sizeof(d_name)> is incorrect; use "
"I<strlen(d_name)> instead.  (On some systems, this field is defined as "
"I<char\\ d_name[1]>!)  By implication, the use I<sizeof(struct dirent)> to "
"capture the size of the record including the size of I<d_name> is also "
"incorrect."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Note that while the call"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "fpathconf(fd, _PC_NAME_MAX)\n"
msgstr "fpathconf(fd, _PC_NAME_MAX)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"returns the value 255 for most filesystems, on some filesystems (e.g., CIFS, "
"Windows SMB servers), the null-terminated filename that is (correctly) "
"returned in I<d_name> can actually exceed this size.  In such cases, the "
"I<d_reclen> field will contain a value that exceeds the size of the glibc "
"I<dirent> structure shown above."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "Invalid directory stream descriptor I<dir>."
msgid "A directory stream is opened using B<opendir>(3)."
msgstr "무효한 디렉토리 스트림 기술자 I<dir>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The order in which filenames are read by successive calls to B<readdir>()  "
"depends on the filesystem implementation; it is unlikely that the names will "
"be sorted in any fashion."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getdents>(2), B<read>(2), B<closedir>(3), B<dirfd>(3), B<ftw>(3), "
"B<offsetof>(3), B<opendir>(3), B<readdir_r>(3), B<rewinddir>(3), "
"B<scandir>(3), B<seekdir>(3), B<telldir>(3)"
msgstr ""
"B<getdents>(2), B<read>(2), B<closedir>(3), B<dirfd>(3), B<ftw>(3), "
"B<offsetof>(3), B<opendir>(3), B<readdir_r>(3), B<rewinddir>(3), "
"B<scandir>(3), B<seekdir>(3), B<telldir>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The only fields in the I<dirent> structure that are mandated by POSIX.1 are "
"I<d_name> and I<d_ino>.  The other fields are unstandardized, and not "
"present on all systems; see NOTES below for some further details."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"This is the size (in bytes) of the returned record.  This may not match the "
"size of the structure definition shown above; see NOTES."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "This field contains the null terminated filename.  I<See NOTES>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"I<Warning>: applications should avoid any dependence on the size of the "
"I<d_name> field.  POSIX defines it as I<char\\ d_name[]>, a character array "
"of unspecified size, with at most B<NAME_MAX> characters preceding the "
"terminating null byte (\\[aq]\\e0\\[aq])."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages 6.03"
