# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:59+0100\n"
"PO-Revision-Date: 2000-07-26 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "getgroups"
msgstr "getgroups"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2024년 5월 2일"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "getgroups, setgroups - get/set list of supplementary group IDs"
msgstr "getgroups, setgroups - 부가 그룹 ID의 리스트 소유/설정"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int getgroups(int >I<size>B<, gid_t >I<list>B<[]);>\n"
msgstr "B<int getgroups(int >I<size>B<, gid_t >I<list>B<[]);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>grp.hE<gt>>\n"
msgstr "B<#include E<lt>grp.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int setgroups(size_t >I<size>B<, const gid_t *_Nullable >I<list>B<);>\n"
msgstr "B<int setgroups(size_t >I<size>B<, const gid_t *_Nullable >I<list>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<setgroups>():"
msgstr "B<setgroups>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getgroups>()  returns the supplementary group IDs of the calling process "
"in I<list>.  The argument I<size> should be set to the maximum number of "
"items that can be stored in the buffer pointed to by I<list>.  If the "
"calling process is a member of more than I<size> supplementary groups, then "
"an error results."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It is unspecified whether the effective group ID of the calling process is "
"included in the returned list.  (Thus, an application should also call "
"B<getegid>(2)  and add or remove the resulting value.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<size> is zero, I<list> is not modified, but the total number of "
"supplementary group IDs for the process is returned.  This allows the caller "
"to determine the size of a dynamically allocated I<list> to be used in a "
"further call to B<getgroups>()."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<setgroups>()  sets the supplementary group IDs for the calling process.  "
"Appropriate privileges are required (see the description of the B<EPERM> "
"error, below).  The I<size> argument specifies the number of supplementary "
"group IDs in the buffer pointed to by I<list>.  A process can drop all of "
"its supplementary groups with the call:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "setgroups(0, NULL);\n"
msgstr "setgroups(0, NULL);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, the number of supplementary group IDs is returned.  On error, "
#| "-1 is returned, and I<errno> is set appropriately."
msgid ""
"On success, B<getgroups>()  returns the number of supplementary group IDs.  "
"On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"성공시, 부가 그룹 아이디의 수가 반환된다.  에러시에는 -1이 반환된다.  "
"I<errno> 는 적당히 설정된다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, B<setgroups>()  returns 0.  On error, -1 is returned, and "
"I<errno> is set to indicate the error."
msgstr ""
"성공시, 0을 반환한다. 에러시, -1 이 반환돠니다. 그리고 I<errno> 는 적당히 설"
"정된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<list> has an invalid address."
msgstr "I<list> 가 실제 없는 주소를 가지고 있다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getgroups>()  can additionally fail with the following error:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "For B<setgroups>, I<size> is greater than B<NGROUPS> (32 for Linux "
#| "2.0.32).  For B<getgroups>, I<size> is less than the number of "
#| "supplementary group IDs, but is not zero."
msgid ""
"I<size> is less than the number of supplementary group IDs, but is not zero."
msgstr ""
"B<setgroups>, 에게 I<크기> 가 B<NGROUPS> (32 for Linux 2.0.32)보다 크다..  "
"B<getgroups>, 에게 I<크기> 가 부가 그룹 아디 수보다 작지만 0은 아니다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<setgroups>()  can additionally fail with the following errors:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<size> is greater than B<NGROUPS_MAX> (32 before Linux 2.6.4; 65536 since "
"Linux 2.6.4)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Out of memory."
msgstr "메모리가 부족하다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The calling process has insufficient privilege (the caller does not have the "
"B<CAP_SETGID> capability in the user namespace in which it resides)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> (since Linux 3.19)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The use of B<setgroups>()  is denied in this user namespace.  See the "
"description of I</proc/>pidI</setgroups> in B<user_namespaces>(7)."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"At the kernel level, user IDs and group IDs are a per-thread attribute.  "
"However, POSIX requires that all threads in a process share the same "
"credentials.  The NPTL threading implementation handles the POSIX "
"requirements by providing wrapper functions for the various system calls "
"that change process UIDs and GIDs.  These wrapper functions (including the "
"one for B<setgroups>())  employ a signal-based technique to ensure that when "
"one thread changes credentials, all of the other threads in the process also "
"change their credentials.  For details, see B<nptl>(7)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getgroups>()"
msgstr "B<getgroups>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<setgroups>()"
msgstr "B<setgroups>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SVr4, SVID (issue 4 only; these calls were not present in SVr3), X/OPEN, "
#| "4.3BSD.  The B<getgroups> function is in POSIX.1.  Since B<setgroups> "
#| "requires privilege, it is not covered by POSIX.1."
msgid ""
"SVr4, 4.3BSD.  Since B<setgroups>()  requires privilege, it is not covered "
"by POSIX.1."
msgstr ""
"SVr4, SVID (issue 4 only; 이 호출은 SVr3에 없다), X/OPEN, 4.3BSD.  The "
"B<getgroups> 함수는 POSIX.1안에 있다.  B<setgroups> 는 특권이 필요하기 때문"
"에, POSIX.1로 지원되지 않는다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The original Linux B<getgroups>()  system call supported only 16-bit group "
"IDs.  Subsequently, Linux 2.4 added B<getgroups32>(), supporting 32-bit "
"IDs.  The glibc B<getgroups>()  wrapper function transparently deals with "
"the variation across kernel versions."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A process can have up to B<NGROUPS_MAX> supplementary group IDs in addition "
"to the effective group ID.  The constant B<NGROUPS_MAX> is defined in "
"I<E<lt>limits.hE<gt>>.  The set of supplementary group IDs is inherited from "
"the parent process, and preserved across an B<execve>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The maximum number of supplementary group IDs can be found at run time using "
"B<sysconf>(3):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"long ngroups_max;\n"
"ngroups_max = sysconf(_SC_NGROUPS_MAX);\n"
msgstr ""
"long ngroups_max;\n"
"ngroups_max = sysconf(_SC_NGROUPS_MAX);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The maximum return value of B<getgroups>()  cannot be larger than one more "
"than this value.  Since Linux 2.6.4, the maximum number of supplementary "
"group IDs is also exposed via the Linux-specific read-only file, I</proc/sys/"
"kernel/ngroups_max>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getgid>(2), B<setgid>(2), B<getgrouplist>(3), B<group_member>(3), "
"B<initgroups>(3), B<capabilities>(7), B<credentials>(7)"
msgstr ""
"B<getgid>(2), B<setgid>(2), B<getgrouplist>(3), B<group_member>(3), "
"B<initgroups>(3), B<capabilities>(7), B<credentials>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "B<getgroups>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "B<getgroups>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "SVr4, SVID (issue 4 only; these calls were not present in SVr3), X/OPEN, "
#| "4.3BSD.  The B<getgroups> function is in POSIX.1.  Since B<setgroups> "
#| "requires privilege, it is not covered by POSIX.1."
msgid ""
"B<setgroups>(): SVr4, 4.3BSD.  Since B<setgroups>()  requires privilege, it "
"is not covered by POSIX.1."
msgstr ""
"SVr4, SVID (issue 4 only; 이 호출은 SVr3에 없다), X/OPEN, 4.3BSD.  The "
"B<getgroups> 함수는 POSIX.1안에 있다.  B<setgroups> 는 특권이 필요하기 때문"
"에, POSIX.1로 지원되지 않는다."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages 6.03"
