# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Román Ramírez <rramirez@encomix.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-08-02 17:12+0200\n"
"PO-Revision-Date: 1998-07-26 19:53+0200\n"
"Last-Translator: Román Ramírez <rramirez@encomix.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: Dd
#: opensuse-tumbleweed
#, no-wrap
msgid "April 29, 1995"
msgstr "29 de abril de 1995"

#. type: Dt
#: opensuse-tumbleweed
#, no-wrap
msgid "BANNER 6"
msgstr "BANNER 6"

#. type: Sh
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: opensuse-tumbleweed
msgid "E<.Nm banner>"
msgstr "E<.Nm banner>"

#. type: Nd
#: opensuse-tumbleweed
#, no-wrap
msgid "print large banner on printer"
msgstr "imprime un título grande en la impresora"

#. type: Sh
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: opensuse-tumbleweed
msgid "E<.Nm> E<.Op Fl w Ar width> E<.Ar message ...>"
msgstr "E<.Nm> E<.Op Fl w Ar ancho> E<.Ar mensaje ...>"

#. type: Sh
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"E<.Nm> prints a large, high quality banner on the standard output.  If the "
"message is omitted, it prompts for and reads one line of its standard "
"input.  If E<.Fl w> is given, the output is scrunched down from a width of "
"132 to E<.Ar width>, suitable for a narrow terminal."
msgstr ""
"E<.Nm> imprime un título grande y de gran calidad en la salida estandar.  Si "
"se omite el mensaje, pregunta por él, y lee una línea de su entrada "
"estandar. Si E<.Fl w> se incluye, la salida se achata desde una anchura de "
"132 a E<.Ar width>, conveniente para una terminal más estrecha."

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"The output should be printed on paper of the appropriate width, with no "
"breaks between the pages."
msgstr ""

#. type: Sh
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"Several ASCII characters are not defined, notably \\*[Lt], \\*[Gt], [, ], "
"\\e, ^, _, {, }, |, and ~.  Also, the characters \", ', and \\*[Am] are "
"funny looking (but in a useful way.)"
msgstr ""
"Agunos caracteres ASCII no están definidos, destacando \\*[Lt], \\*[Gt], "
"[, ], \\e, ^, _, {, }, |, y ~.  También, los caracteres \", ', y \\*[Am] "
"tienen un aspecto divertido (pero util.)"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"The E<.Fl w> option is implemented by skipping some rows and columns.  The "
"smaller it gets, the grainier the output.  Sometimes it runs letters "
"together."
msgstr ""
"La opción E<.Fl w> se implementa saltando algunas filas y columnas.  Cuanto "
"más pequeño se haga, más grano tendrá la salida.  Algunas veces mezcla las "
"letras."

#. type: Sh
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: opensuse-tumbleweed
msgid "E<.An Mark Horton>"
msgstr "E<.An Mark Horton>"
