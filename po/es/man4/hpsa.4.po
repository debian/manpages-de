# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2024-12-06 18:01+0100\n"
"PO-Revision-Date: 2021-09-03 21:20+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#
#
#.  Copyright (C) 2011, Hewlett-Packard Development Company, L.P.
#.  Written by Stephen M. Cameron <scameron@beardog.cce.hp.com>
#.  SPDX-License-Identifier: GPL-2.0-only
#.  shorthand for double quote that works everywhere.
#. type: ds q
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\N'34'"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "hpsa"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "hpsa - HP Smart Array SCSI driver"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "modprobe hpsa [ hpsa_allow_any=1 ]\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<hpsa> is a SCSI driver for HP Smart Array RAID controllers."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Options"
msgstr "Opciones"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<hpsa_allow_any=1>: This option allows the driver to attempt to operate on "
"any HP Smart Array hardware RAID controller, even if it is not explicitly "
"known to the driver.  This allows newer hardware to work with older "
"drivers.  Typically this is used to allow installation of operating systems "
"from media that predates the RAID controller, though it may also be used to "
"enable B<hpsa> to drive older controllers that would normally be handled by "
"the B<cciss>(4)  driver.  These older boards have not been tested and are "
"not supported with B<hpsa>, and B<cciss>(4)  should still be used for these."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Supported hardware"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The B<hpsa> driver supports the following Smart Array boards:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Smart Array P700M\n"
"    Smart Array P212\n"
"    Smart Array P410\n"
"    Smart Array P410i\n"
"    Smart Array P411\n"
"    Smart Array P812\n"
"    Smart Array P712m\n"
"    Smart Array P711m\n"
"    StorageWorks P1210m\n"
msgstr ""

#.  commit 135ae6edeb51979d0998daf1357f149a7d6ebb08
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Since Linux 4.14, the following Smart Array boards are also supported:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Smart Array 5300\n"
"    Smart Array 5312\n"
"    Smart Array 532\n"
"    Smart Array 5i\n"
"    Smart Array 6400\n"
"    Smart Array 6400 EM\n"
"    Smart Array 641\n"
"    Smart Array 642\n"
"    Smart Array 6i\n"
"    Smart Array E200\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E200i\n"
"    Smart Array E500\n"
"    Smart Array P400\n"
"    Smart Array P400i\n"
"    Smart Array P600\n"
"    Smart Array P700m\n"
"    Smart Array P800\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Configuration details"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To configure HP Smart Array controllers, use the HP Array Configuration "
"Utility (either B<hpacuxe>(8)  or B<hpacucli>(8))  or the Offline ROM-based "
"Configuration Utility (ORCA)  run from the Smart Array's option ROM at boot "
"time."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Device nodes"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Logical drives are accessed via the SCSI disk driver (B<sd>(4)), tape drives "
"via the SCSI tape driver (B<st>(4)), and the RAID controller via the SCSI "
"generic driver (B<sg>(4)), with device nodes named I</dev/sd*>, I</dev/st*>, "
"and I</dev/sg*>, respectively."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HPSA-specific host attribute files in /sys"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_host/host*/rescan>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is a write-only attribute.  Writing to this attribute will cause the "
"driver to scan for new, changed, or removed devices (e.g., hot-plugged tape "
"drives, or newly configured or deleted logical drives, etc.)  and notify the "
"SCSI midlayer of any changes detected.  Normally a rescan is triggered "
"automatically by HP's Array Configuration Utility (either the GUI or the "
"command-line variety); thus, for logical drive changes, the user should not "
"normally have to use this attribute.  This attribute may be useful when hot "
"plugging devices like tape drives, or entire storage boxes containing "
"preconfigured logical drives."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_host/host*/firmware_revision>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This attribute contains the firmware version of the Smart Array."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "For example:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<cd /sys/class/scsi_host/host4>\n"
"# B<cat firmware_revision>\n"
"7.14\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HPSA-specific disk attribute files in /sys"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_disk/c:b:t:l/device/unique_id>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This attribute contains a 32 hex-digit unique ID for each logical drive."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat unique_id>\n"
"600508B1001044395355323037570F77\n"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_disk/c:b:t:l/device/raid_level>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This attribute contains the RAID level of each logical drive."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat raid_level>\n"
"RAID 0\n"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</sys/class/scsi_disk/c:b:t:l/device/lunid>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This attribute contains the 16 hex-digit (8 byte) LUN ID by which a logical "
"drive or physical device can be addressed.  I<c>:I<b>:I<t>:I<l> are the "
"controller, bus, target, and lun of the device."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<cd /sys/class/scsi_disk/4:0:0:0/device>\n"
"# B<cat lunid>\n"
"0x0000004000000000\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Supported ioctl() operations"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For compatibility with applications written for the B<cciss>(4)  driver, "
"many, but not all of the ioctls supported by the B<cciss>(4)  driver are "
"also supported by the B<hpsa> driver.  The data structures used by these "
"ioctls are described in the Linux kernel source file I<include/linux/"
"cciss_ioctl.h>."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_DEREGDISK>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_REGNEWDISK>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_REGNEWD>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These three ioctls all do exactly the same thing, which is to cause the "
"driver to rescan for new devices.  This does exactly the same thing as "
"writing to the hpsa-specific host \"rescan\" attribute."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_GETPCIINFO>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Returns PCI domain, bus, device, and function and \"board ID\" (PCI "
"subsystem ID)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_GETDRIVVER>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Returns driver version in three bytes encoded as:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"(major_version E<lt>E<lt> 16) | (minor_version E<lt>E<lt> 8) |\n"
"    (subminor_version)\n"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_PASSTHRU>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CCISS_BIG_PASSTHRU>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Allows \"BMIC\" and \"CISS\" commands to be passed through to the Smart "
"Array.  These are used extensively by the HP Array Configuration Utility, "
"SNMP storage agents, and so on.  See I<cciss_vol_status> at E<.UR http://"
"cciss.sf.net> E<.UE> for some examples."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<cciss>(4), B<sd>(4), B<st>(4), B<cciss_vol_status>(8), B<hpacucli>(8), "
"B<hpacuxe>(8)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"E<.UR http://cciss.sf.net> E<.UE ,> and I<Documentation/scsi/hpsa.txt> and "
"I<Documentation/ABI/testing/sysfs-bus-pci-devices-cciss> in the Linux kernel "
"source tree"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 Octubre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<CCISS_DEREGDISK>, B<CCISS_REGNEWDISK>, B<CCISS_REGNEWD>"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<CCISS_PASSTHRU>, B<CCISS_BIG_PASSTHRU>"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
