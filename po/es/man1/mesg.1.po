# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Fidel García <fidelgq@dinamic.net>, 1999.
# Marcos Fouces <marcos@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-29 09:44+0100\n"
"PO-Revision-Date: 2023-05-09 20:37+0200\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "MESG"
msgstr "MESG"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 Mayo 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Órdenes de usuario"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: debian-bookworm
msgid "mesg - display (or do not display) messages from other users"
msgstr "mesg - muestra (o no) mensajes de otros usuarios"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm
msgid "B<mesg> [I<option>] [B<n>|B<y>]"
msgstr "B<mesg> [I<opción>] [B<n>|B<y>]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<mesg> utility is invoked by a user to control write access others have "
"to the terminal device associated with standard error output. If write "
"access is allowed, then programs such as B<talk>(1) and B<write>(1) may "
"display messages on the terminal."
msgstr ""
"Un usuario invoca la herramienta B<mesg> para controlar el acceso a la "
"salida de error estándar de su terminal por terceros. Generalmente, se usa "
"para permitir o negar que aplicaciones como B<talk>(1) y B<write>(1) puedan "
"mostrar mensajes por el terminal."

#. type: Plain text
#: debian-bookworm
msgid ""
"Traditionally, write access is allowed by default. However, as users become "
"more conscious of various security risks, there is a trend to remove write "
"access by default, at least for the primary login shell. To make sure your "
"ttys are set the way you want them to be set, B<mesg> should be executed in "
"your login scripts."
msgstr ""
"El permiso de escritura está otorgado por defecto. A medida que los usuarios "
"son cada vez más conscientes de los riesgos para la segurudad de esta "
"práctica, se tienda cada vez más a eliminar este permiso predeterminado, al "
"menos para el login primario de la shell. Asegúrese de tener configurados "
"los tty del modo en que desea, B<mesg> debería ejecutarse en el script de "
"inicio."

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<mesg> utility silently exits with error status 2 if not executed on "
"terminal. In this case execute B<mesg> is pointless. The command line option "
"B<--verbose> forces mesg to print a warning in this situation. This "
"behaviour has been introduced in version 2.33."
msgstr ""
"La herramienta B<mesg> termina sin más su ejecución con el error 2 si no se "
"ejecuta desde un terminal. No sería útil ejecutar así B<mesg>. Desde la "
"versión 2.33, si se define la opción B<--verbose>, mesg mostrará un mensaje "
"de advertencia en este caso."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ARGUMENTS"
msgstr "ARGUMENTOS"

#. type: Plain text
#: debian-bookworm
msgid "B<n>"
msgstr "B<n>"

#. type: Plain text
#: debian-bookworm
msgid "Disallow messages."
msgstr "Desactiva los mensajes."

#. type: Plain text
#: debian-bookworm
msgid "B<y>"
msgstr "B<y>"

#. type: Plain text
#: debian-bookworm
msgid "Allow messages to be displayed."
msgstr "Permite que se muestren mensajes."

#. type: Plain text
#: debian-bookworm
msgid ""
"If no arguments are given, B<mesg> shows the current message status on "
"standard error output."
msgstr ""
"Si no se indica ninguna opción, B<mesg> imprimirá el estado actual a través "
"de la salida de error estándar."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm
msgid "Explain what is being done."
msgstr "Describe las acciones que lleva a cabo."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Muestra un texto de ayuda y finaliza."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Muestra la versión y finaliza."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr "ESTADO DE SALIDA"

#. type: Plain text
#: debian-bookworm
msgid "The B<mesg> utility exits with one of the following values:"
msgstr "La herramienta B<mesg> finaliza con uno de los siguientes estados:"

#. type: Plain text
#: debian-bookworm
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: debian-bookworm
msgid "Messages are allowed."
msgstr "Se permiten los mensajes."

#. type: Plain text
#: debian-bookworm
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: debian-bookworm
msgid "Messages are not allowed."
msgstr "No se permiten los mensajes."

#. type: Plain text
#: debian-bookworm
msgid "B<E<gt>1>"
msgstr "B<E<gt>1>"

#. type: Plain text
#: debian-bookworm
msgid "An error has occurred."
msgstr "Ocurrió un error."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: debian-bookworm
msgid "I</dev/[pt]ty[pq]?>"
msgstr "I</dev/[pt]ty[pq]?>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: debian-bookworm
msgid "A B<mesg> command appeared in Version 6 AT&T UNIX."
msgstr "Una versión de B<mesg> apareció en la Versión 6 del UNIX de AT&T."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: debian-bookworm
msgid "B<login>(1), B<talk>(1), B<write>(1), B<wall>(1), B<xterm>(1)"
msgstr "B<login>(1), B<talk>(1), B<write>(1), B<wall>(1), B<xterm>(1)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "INFORMAR DE ERRORES"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Para informar de cualquier error, utilice el sistema de seguimiento de fallos"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILIDAD"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<mesg> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La orden B<mesg> forma parte del paquete util-linux que puede descargarse "
"desde"
