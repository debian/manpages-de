# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Enrique Ferrero Puchades <enferpuc@olemail.com>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 17:58+0100\n"
"PO-Revision-Date: 1999-05-28 19:53+0200\n"
"Last-Translator: Enrique Ferrero Puchades <enferpuc@olemail.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PSPDFUTILS"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-11-18"
msgstr "18 Noviembre 2024"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "diffutils 3.6"
msgid "pspdfutils 3.3.6"
msgstr "diffutils 3.6"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Órdenes de usuario"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "pacutils"
msgid "pspdfutils"
msgstr "pacutils"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux
msgid "B<pspdfutils> [OPTION...] [INFILE [OUTFILE]]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Extractres> extracts resources (fonts, procsets, patterns, files, etc) "
#| "appearing in a PostScript document, and puts appropriate I<%"
#| "%IncludeResource> comments in the document prologue. The extracted "
#| "resources are written to files with the same name as the resource, and an "
#| "appropriate extension. The pipeline"
msgid ""
"B<extractres> extracts resources (fonts, procsets, patterns, files, etc) "
"appearing in a PostScript document, and puts appropriate B<%"
"%IncludeResource> comments in the document prologue.  The extracted "
"resources are written to files with the same name as the resource, and an "
"appropriate extension.  The pipeline"
msgstr ""
"I<Extractres> extrae recursos (fuentes, procsets, patrones, ficheros, etc) "
"que aparecen en un documento PostScript, e inserta los comentarios I<%"
"%IncludeResource> apropiados en el prólogo del documento. Los recursos "
"extraídos se escriben en ficheros con el mismo nombre que el recurso, y una "
"estensión apropiada. La tubería"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "extractres file.ps | includeres E<gt>out.ps"
msgstr "extractres file.ps | includeres E<gt>out.ps"

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid ""
#| "will move all resources appearing in a document to the document prologue, "
#| "removing redundant copies.  The output file can then be put through page "
#| "re-arrangement filters such as B<psnup> or B<pstops> safely."
msgid ""
"will move all resources appearing in a document to the document prologue, "
"removing redundant copies.  The output file can then be put through page re-"
"arrangement filters such as B<psnup> or B<pstops> safely.  Extract resources "
"from a PostScript document."
msgstr ""
"mueve al prólogo todos los recursos que aparecen en el documento, quitando "
"copias redundantes. De esta manera el archivo de salida se puede meter a "
"través de filtros de reordenación de páginas tales como B<psnup> o B<pstops> "
"de una manera segura."

#. type: TP
#: archlinux
#, fuzzy, no-wrap
#| msgid "B<ENFILE>"
msgid "B<INFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "With no FILE, or when FILE is -, read standard input."
msgid "`-' or no INFILE argument means standard input"
msgstr ""
"Si no se define ningún ARCHIVO o bien se indica '-', se lee la entrada "
"estándar."

#. type: TP
#: archlinux
#, fuzzy, no-wrap
#| msgid "B<EMFILE>"
msgid "B<OUTFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "write result to FILE instead of standard output"
msgid "`-' or no OUTFILE argument means standard output"
msgstr ""
"envía el resultado a ARCHIVO en lugar de hacerlo por la salida estándar"

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--merge>"
msgstr "B<-m>, B<--merge>"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"merge resources of the same name into one file (needed e.g. for fonts output "
"in multiple blocks)"
msgstr ""

#. type: TP
#: archlinux fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "Prints version number and exits."
msgid "show program's version number and exit"
msgstr "Muestra información de versión y finalizar."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: archlinux
msgid "don't show progress"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<extractres> does not alter the B<%%DocumentSuppliedResources> comments."
msgstr ""
"B<extractres> no altera los comentarios B<%%DocumentSuppliedResources>."

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXTRACTRES"
msgstr "EXTRACTRES"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "extractres - filter to extract resources from a PostScript document"
msgstr "extractres - filtro para extraer recursos de un documento PostScript"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<extractres> [ B<-m> ] E<lt> I<document.ps> E<gt> I<output.ps>"
msgstr "B<extractres> [ B<-m> ] E<lt> I<document.ps> E<gt> I<output.ps>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<Extractres> extracts resources (fonts, procsets, patterns, files, etc) "
"appearing in a PostScript document, and puts appropriate I<%"
"%IncludeResource> comments in the document prologue. The extracted resources "
"are written to files with the same name as the resource, and an appropriate "
"extension. The pipeline"
msgstr ""
"I<Extractres> extrae recursos (fuentes, procsets, patrones, ficheros, etc) "
"que aparecen en un documento PostScript, e inserta los comentarios I<%"
"%IncludeResource> apropiados en el prólogo del documento. Los recursos "
"extraídos se escriben en ficheros con el mismo nombre que el recurso, y una "
"estensión apropiada. La tubería"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"will move all resources appearing in a document to the document prologue, "
"removing redundant copies. The output file can then be put through page re-"
"arrangement filters such as I<psnup> or I<pstops> safely."
msgstr ""
"mueve al prólogo todos los recursos que aparecen en el documento, quitando "
"copias redundantes. De esta manera el archivo de salida se puede meter a "
"través de filtros de reordenación de páginas tales como I<psnup> o I<pstops> "
"de una manera segura."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<-m> option combines the resources of the same name into one file; this "
"must be used for some programs which download fonts a bit at a time."
msgstr ""
"La opción B<-m> combina los recursos del mismo nombre en un fichero. Esto "
"debe ser usado por algunos programas que descargan fuentes bit a bit."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Copyright (C) Angus J. C. Duggan 1991-1995"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"B<psbook>(1), B<psselect>(1), B<pstops>(1), B<epsffit>(1), B<psnup>(1), "
"B<psresize>(1), B<psmerge>(1), B<fixscribeps>(1), B<getafm>(1), "
"B<fixdlsrps>(1), B<fixfmps>(1), B<fixpsditps>(1), B<fixpspps>(1), "
"B<fixtpps>(1), B<fixwfwps>(1), B<fixwpps>(1), B<fixwwps>(1), "
"B<extractres>(1), B<includeres>(1), B<showchar>(1)"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "TRADEMARKS"
msgstr "MARCAS REGISTRADAS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> es una marca registrada de Adobe Systems Incorporated."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<extractres> does not alter the I<%%DocumentSuppliedResources> comments."
msgstr ""
"I<extractres> no altera los comentarios I<%%DocumentSuppliedResources>."

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "February 2023"
msgstr "Febrero de 2023"

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "extractres 2.09"
msgid "extractres 2.10"
msgstr "extractres 2.09"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid "extractres - filter to extract resources from a PostScript document"
msgid "extractres - extract resources from a PostScript document"
msgstr "extractres - filtro para extraer recursos de un documento PostScript"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<extractres> [I<\\,OPTION\\/>...] [I<\\,INFILE \\/>[I<\\,OUTFILE\\/>]]"
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid "extractres - filter to extract resources from a PostScript document"
msgid "Extract resources from a PostScript document."
msgstr "extractres - filtro para extraer recursos de un documento PostScript"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "display this help and exit"
msgstr "muestra la ayuda y finaliza"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "display version information and exit"
msgstr "mostrar información de versión y finalizar"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"will move all resources appearing in a document to the document prologue, "
"removing redundant copies.  The output file can then be put through page re-"
"arrangement filters such as B<psnup> or B<pstops> safely."
msgstr ""
"mueve al prólogo todos los recursos que aparecen en el documento, quitando "
"copias redundantes. De esta manera el archivo de salida se puede meter a "
"través de filtros de reordenación de páginas tales como B<psnup> o B<pstops> "
"de una manera segura."

#. type: SS
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "Estado de salida:"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "if OK,"
msgstr "si todo fue bien,"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"if arguments or options are incorrect, or there is some other problem "
"starting up,"
msgstr ""

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"if there is some problem during processing, typically an error reading or "
"writing an input or output file."
msgstr ""

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Written by Angus J. C. Duggan and Reuben Thomas."
msgstr "Escrito por Angus J. C. Duggan y Reuben Thomas."

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid "Copyright \\(co Reuben Thomas 2012-2022."
msgstr "Copyright \\(co Reuben Thomas 2012-2022."

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Copyright \\(co Angus J. C. Duggan 1991-1997."
msgstr "Copyright \\(co Angus J. C. Duggan 1991-1997."

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<psutils>(1), B<paper>(1)"
msgstr "B<psutils>(1), B<paper>(1)"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "December 2021"
msgstr "Diciembre de 2021"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "extractres 2.08"
msgstr "extractres 2.08"

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Copyright \\(co Reuben Thomas 2012-2019."
msgstr "Copyright \\(co Reuben Thomas 2012-2019."
