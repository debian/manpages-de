# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-29 09:45+0100\n"
"PO-Revision-Date: 1998-09-08 23:07+0200\n"
"Last-Translator: Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "MKSWAP"
msgstr "MKSWAP"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 Mayo 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr "Administración del sistema"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: debian-bookworm
msgid "mkswap - set up a Linux swap area"
msgstr "mkswap - construye un área de trasiego para Linux"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm
msgid "B<mkswap> [options] I<device> [I<size>]"
msgstr "B<mkswap> [opciones] I<dispositivo> [I<tamaño>]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: debian-bookworm
msgid "B<mkswap> sets up a Linux swap area on a device or in a file."
msgstr ""
"B<mkswap> establece un área de trasiego para Linux sobre un dispositivo o en "
"un fichero."

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<device> argument will usually be a disk partition (something like I</"
"dev/sdb7>) but can also be a file. The Linux kernel does not look at "
"partition IDs, but many installation scripts will assume that partitions of "
"hex type 82 (LINUX_SWAP) are meant to be swap partitions. (B<Warning: "
"Solaris also uses this type. Be careful not to kill your Solaris partitions."
">)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<size> parameter is superfluous but retained for backwards "
"compatibility. (It specifies the desired size of the swap area in 1024-byte "
"blocks. B<mkswap> will use the entire partition or file if it is omitted. "
"Specifying it is unwise - a typo may destroy your disk.)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"After creating the swap area, you need the B<swapon>(8) command to start "
"using it. Usually swap areas are listed in I</etc/fstab> so that they can be "
"taken into use at boot time by a B<swapon -a> command in some boot script."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "WARNING"
msgstr "AVISO"

#. type: Plain text
#: debian-bookworm
msgid ""
"The swap header does not touch the first block. A boot loader or disk label "
"can be there, but it is not a recommended setup. The recommended setup is to "
"use a separate partition for a Linux swap area."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<mkswap>, like many others mkfs-like utils, B<erases the first partition "
"block to make any previous filesystem invisible.>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"However, B<mkswap> refuses to erase the first block on a device with a disk "
"label (SUN, BSD, ...)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--check>"
msgstr "B<-c>, B<--check>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Check the device (if it is a block device) for bad blocks before creating "
"the swap area. If any bad blocks are found, the count is printed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Go ahead even if the command is stupid. This allows the creation of a swap "
"area larger than the file or partition it resides on."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Also, without this option, B<mkswap> will refuse to erase the first block on "
"a device with a partition table."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "suppress most error messages"
msgid "Suppress output and warning messages."
msgstr "suprime la mayoría de los mensajes de error"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<-h>,B< --help>"
msgid "B<-L>, B<--label> I<label>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: debian-bookworm
msgid "Specify a I<label> for the device, to allow B<swapon>(8) by label."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--lock>[=I<mode>]"
msgstr "B<--lock>[=I<modo>]"

#. type: Plain text
#: debian-bookworm
msgid ""
"Use exclusive BSD lock for device or file it operates. The optional argument "
"I<mode> can be B<yes>, B<no> (or 1 and 0) or B<nonblock>. If the I<mode> "
"argument is omitted, it defaults to B<yes>. This option overwrites "
"environment variable B<$LOCK_BLOCK_DEVICE>. The default is not to use any "
"lock at all, but it\\(cqs recommended to avoid collisions with B<systemd-"
"udevd>(8) or other tools."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<-p>,B< --pagesize >I<size>"
msgid "B<-p>, B<--pagesize> I<size>"
msgstr "B<-p>,B< --pagesize >I<tamaño>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the page I<size> (in bytes) to use. This option is usually "
"unnecessary; B<mkswap> reads the size from the kernel."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<-D>, B<--dired>"
msgid "B<-U>, B<--uuid> I<UUID>"
msgstr "B<-D>, B<--dired>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the I<UUID> to use. The default is to generate a UUID. The format of "
"the UUID is a series of hex digits separated by hyphens, like this: "
"\"c1b9d5a2-f162-11cf-9ece-0020afc76f16\". The UUID parameter may also be one "
"of the following:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<clear>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "clear the filesystem UUID"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<random>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "generate a new randomly-generated UUID"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<time>"
msgstr "B<time>"

#. type: Plain text
#: debian-bookworm
msgid "generate a new time-based UUID"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<-v>,B< --swapversion 1>"
msgid "B<-v>, B<--swapversion 1>"
msgstr "B<-v>,B< --swapversion 1>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the swap-space version. (This option is currently pointless, as the "
"old B<-v 0> option has become obsolete and now only B<-v 1> is supported. "
"The kernel has not supported v0 swap-space format since 2.5.22 (June 2002). "
"The new version v1 is supported since 2.1.117 (August 1998).)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Verbose execution. With this option B<mkswap> will output more details about "
"detected problems during swap area set up."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Muestra un texto de ayuda y finaliza."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Muestra la versión y finaliza."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENTORNO"

#. type: Plain text
#: debian-bookworm
msgid "LIBBLKID_DEBUG=all"
msgstr "LIBBLKID_DEBUG=all"

#. type: Plain text
#: debian-bookworm
msgid "enables libblkid debug output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "LOCK_BLOCK_DEVICE=E<lt>modeE<gt>"
msgstr "LOCK_BLOCK_DEVICE=E<lt>modoE<gt>"

#. type: Plain text
#: debian-bookworm
msgid ""
"use exclusive BSD lock. The mode is \"1\" or \"0\". See B<--lock> for more "
"details."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: debian-bookworm
msgid ""
"The maximum useful size of a swap area depends on the architecture and the "
"kernel version."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The maximum number of the pages that is possible to address by swap area "
"header is 4294967295 (32-bit unsigned int). The remaining space on the swap "
"device is ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Presently, Linux allows 32 swap areas. The areas in use can be seen in the "
"file I</proc/swaps>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<mkswap> refuses areas smaller than 10 pages."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If you don\\(cqt know the page size that your machine uses, you can look it "
"up with B<getconf PAGESIZE>."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "To setup a swap file, it is necessary to create that file before running "
#| "B<mkswap .> A sequence of commands similar to the following is reasonable "
#| "for this purpose:"
msgid ""
"To set up a swap file, it is necessary to create that file before "
"initializing it with B<mkswap>, e.g. using a command like"
msgstr ""
"Para establecer un fichero para el trasiego, es necesario crear dicho "
"fichero antes de ejecutar B<mkswap>.  Para este propósito, sería razonable "
"una secuencia de órdenes similar a la siguiente:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "# dd if=/dev/zero of=swapfile bs=1MiB count=$((8*1024))\n"
msgstr "# dd if=/dev/zero of=swapfile bs=1MiB count=$((8*1024))\n"

#. type: Plain text
#: debian-bookworm
msgid "to create 8GiB swapfile."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Please read notes from B<swapon>(8) about B<the swap file use restrictions> "
"(holes, preallocation and copy-on-write issues)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: debian-bookworm
msgid "B<fdisk>(8), B<swapon>(8)"
msgstr "B<fdisk>(8), B<swapon>(8)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "INFORMAR DE ERRORES"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Para informar de cualquier error, utilice el sistema de seguimiento de fallos"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILIDAD"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<mkswap> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
