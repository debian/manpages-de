# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Vicente Pastor Gómez <vpastorg@santandersupernet.com>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:10+0100\n"
"PO-Revision-Date: 2021-01-31 10:27+0100\n"
"Last-Translator: Vicente Pastor Gómez <vpastorg@santandersupernet.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "readdir"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 Junio 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "readdir - read directory entry"
msgstr "readdir - lee una entrada de un directorio"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definición de las constantes B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int readdir(unsigned int >I<fd>B<, struct old_linux_dirent *>I<dirp>B<,>\n"
#| "B<            unsigned int >I<count>B<);>\n"
msgid ""
"B<int syscall(SYS_readdir, unsigned int >I<fd>B<,>\n"
"B<            struct old_linux_dirent *>I<dirp>B<, unsigned int >I<count>B<);>\n"
msgstr ""
"B<int readdir(unsigned int >I<fd>B<, struct old_linux_dirent *>I<dirp>B<,>\n"
"B<            unsigned int >I<count>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgid ""
"I<Note>: There is no definition of B<struct old_linux_dirent>; see NOTES."
msgstr ""
"I<Nota>: No hay envoltorio glibc para esta llamada al sistema; véase NOTAS."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This is not the function you are interested in.  Look at B<readdir>(3)  "
#| "for the POSIX conforming C library interface.  This page documents the "
#| "bare kernel system call interface, which can change, and which is "
#| "superseded by B<getdents>(2)."
msgid ""
"This is not the function you are interested in.  Look at B<readdir>(3)  for "
"the POSIX conforming C library interface.  This page documents the bare "
"kernel system call interface, which is superseded by B<getdents>(2)."
msgstr ""
"Esta no es la función que buscaba.  Mire B<readdir>(3)  para ver la interfaz "
"de la biblioteca C conforme con POSIX.  Esta página documenta la interfaz "
"desnuda con la llamada al sistema del núcleo, que puede cambiar, y que es "
"reemplazada por B<getdents>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<readdir> reads one I<dirent> structure from the directory pointed at by "
#| "I<fd> into the memory area pointed to by I<dirp>.  The parameter I<count> "
#| "is ignored; at most one dirent structure is read."
msgid ""
"B<readdir>()  reads one I<old_linux_dirent> structure from the directory "
"referred to by the file descriptor I<fd> into the buffer pointed to by "
"I<dirp>.  The argument I<count> is ignored; at most one I<old_linux_dirent> "
"structure is read."
msgstr ""
"B<readdir> lee una estructura I<dirent> del directorio al que apunta I<fd> y "
"la almacena en el área de memoria apuntada por I<dirp>.  El parámetro "
"I<count> es ignorado; como mucho se lee una estructura dirent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "The I<dirent> structure is declared as follows:"
msgid ""
"The I<old_linux_dirent> structure is declared (privately in Linux kernel "
"file B<fs/readdir.c>)  as follows:"
msgstr "La estructura I<dirent> se declara como sigue:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct old_linux_dirent {\n"
"    unsigned long d_ino;     /* inode number */\n"
"    unsigned long d_offset;  /* offset to this I<old_linux_dirent> */\n"
"    unsigned short d_namlen; /* length of this I<d_name> */\n"
"    char  d_name[1];         /* filename (null-terminated) */\n"
"}\n"
msgstr ""
"struct old_linux_dirent {\n"
"    unsigned long d_ino;     /* número de nodo-í */\n"
"    unsigned long d_offset;  /* desplazamiento hasta el I<old_linux_dirent> */\n"
"    unsigned short d_namlen; /* longitud del I<d_name> */\n"
"    char  d_name[1];         /* nombre de fichero\n"
"                                (acabado en nulo)*/\n"
"}\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<d_ino> is an inode number.  I<d_off> is the distance from the start of "
#| "the directory to this I<dirent>.  I<d_reclen> is the size of I<d_name,> "
#| "not counting the null terminator.  I<d_name> is a null-terminated file "
#| "name."
msgid ""
"I<d_ino> is an inode number.  I<d_offset> is the distance from the start of "
"the directory to this I<old_linux_dirent>.  I<d_reclen> is the size of "
"I<d_name>, not counting the terminating null byte (\\[aq]\\[rs]0\\[aq]).  "
"I<d_name> is a null-terminated filename."
msgstr ""
"I<d_ino> es un número de nodo-í.  I<d_off> es la distancia desde el "
"principio del directorio hasta este I<dirent>.  I<d_reclen> es el tamaño de "
"I<d_name,> sin contar el carácter nulo del final.  I<d_name> es un nombre de "
"fichero, una cadena de caracteres terminada en nulo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, 1 is returned.  On end of directory, 0 is returned.  On "
#| "error, -1 is returned, and I<errno> is set appropriately."
msgid ""
"On success, 1 is returned.  On end of directory, 0 is returned.  On error, "
"-1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"En caso de éxito se devuelve 1.  Si se alcanzó el final del directorio se "
"devuelve 0.  Si hubo un error se devuelve -1 y la variable I<errno> se "
"modifica apropiadamente."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Invalid file descriptor I<fd>."
msgstr "Descriptor de fichero I<fd> inválido."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Argument points outside the calling process's address space."
msgstr ""
"El argumento señala fuera del espacio de direcciones del proceso que realiza "
"la llamada."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Result buffer is too small."
msgstr "El buffer para el resultado es demasiado pequeño."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No such directory."
msgstr "No existe el directorio."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "File descriptor does not refer to a directory."
msgstr "El descriptor de fichero no se refiere a un directorio."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You will need to define the I<old_linux_dirent> structure yourself.  "
"However, probably you should use B<readdir>(3)  instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "This system call is Linux specific."
msgid "This system call does not exist on x86-64."
msgstr "Esta llamada al sistema es específica de Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getdents>(2), B<readdir>(3)"
msgstr "B<getdents>(2), B<readdir>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "I<d_ino> is an inode number.  I<d_off> is the distance from the start of "
#| "the directory to this I<dirent>.  I<d_reclen> is the size of I<d_name,> "
#| "not counting the null terminator.  I<d_name> is a null-terminated file "
#| "name."
msgid ""
"I<d_ino> is an inode number.  I<d_offset> is the distance from the start of "
"the directory to this I<old_linux_dirent>.  I<d_reclen> is the size of "
"I<d_name>, not counting the terminating null byte (\\[aq]\\e0\\[aq]).  "
"I<d_name> is a null-terminated filename."
msgstr ""
"I<d_ino> es un número de nodo-í.  I<d_off> es la distancia desde el "
"principio del directorio hasta este I<dirent>.  I<d_reclen> es el tamaño de "
"I<d_name,> sin contar el carácter nulo del final.  I<d_name> es un nombre de "
"fichero, una cadena de caracteres terminada en nulo."

#. type: Plain text
#: debian-bookworm
msgid "This system call is Linux-specific."
msgstr "Esta llamada del sistema es específica de Linux."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
