# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
# Marcos Fouces <marcos@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-10-04 17:49+0200\n"
"PO-Revision-Date: 2024-05-28 00:00+0200\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "ftpusers"
msgstr "ftpusers"

#. type: TH
#: archlinux fedora-41 fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. #-#-#-#-#  archlinux: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-41: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-16-0: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "ftpusers - list of users that may not log in via the FTP daemon"
msgstr ""
"ftpusers - lista de usuarios que no pueden ingresar a través del demonio FTP"

#. #-#-#-#-#  archlinux: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-41: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-16-0: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The text file B<ftpusers> contains a list of users that may not log in using "
"the File Transfer Protocol (FTP) server daemon.  This file is used not "
"merely for system administration purposes but also for improving security "
"within a TCP/IP networked environment."
msgstr ""
"El archivo de texto B<ftpusers> contiene una lista de usuarios que no pueden "
"iniciar sesión utilizando el servidor del Protocolo de Transferencia de "
"Archivos (FTP). Este archivo se utiliza no sólo para la administración del "
"sistema, sino también para mejorar la seguridad de la red."

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The B<ftpusers> file will typically contain a list of the users that either "
"have no business using ftp or have too many privileges to be allowed to log "
"in through the FTP server daemon.  Such users usually include root, daemon, "
"bin, uucp, and news."
msgstr ""
"El archivo B<ftpusers> suele contener una lista de los usuarios que o bien "
"no tienen ningún motivo para usar ftp o bien tienen demasiados privilegios "
"para que se les permita entrar al sistema a través del servidor FTP. Estos "
"usuarios suelen ser: root, daemon, bin, uucp y news."

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If your FTP server daemon doesn't use B<ftpusers>, then it is suggested that "
"you read its documentation to find out how to block access for certain "
"users.  Washington University FTP server Daemon (wuftpd) and Professional "
"FTP Daemon (proftpd) are known to make use of B<ftpusers>."
msgstr ""
"Si su servidor FTP no utiliza B<ftpusers>, le sugerimos que lea su "
"documentación para averiguar cómo bloquear el acceso a determinados "
"usuarios.  Se sabe que el servidor FTP de la Universidad de Washington "
"(wuftpd) y el demonio FTP profesional (proftpd) utilizan B<ftpusers>."

#. type: SS
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Format"
msgstr "Formato"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The format of B<ftpusers> is very simple.  There is one account name (or "
"username) per line.  Lines starting with a # are ignored."
msgstr ""
"El formato de B<ftpusers> es muy simple. Hay un nombre de cuenta (o nombre "
"de usuario) por línea. Las líneas que comienzan por # son ignoradas."

#. #-#-#-#-#  archlinux: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-41: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-16-0: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I</etc/ftpusers>"
msgstr "I</etc/ftpusers>"

#. #-#-#-#-#  archlinux: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  debian-unstable: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Sh
#. #-#-#-#-#  fedora-41: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-16-0: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: ftpusers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<passwd>(5), B<proftpd>(8), B<wuftpd>(8)"
msgstr "B<passwd>(5), B<proftpd>(8), B<wuftpd>(8)"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "April 22, 1994"
msgstr "22 de Abril de 1994"

#. type: Dt
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FTPUSERS 5"
msgstr "FTPUSERS 5"

#. type: Os
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Linux NetKit (0.17)"
msgstr "Linux NetKit (0.17)"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm ftpusers>"
msgstr "E<.Nm ftpusers>"

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "file which lists users who are not allowed to use ftp"
msgstr "archivo donde se listan los usuarios que no pueden emplear ftp"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Pa /etc/ftpusers> is used by E<.Xr ftpd 8>; the file contains a list of "
"users who are not allowed to use the ftp command. For security reasons at "
"least users like ``root'', ``bin'', ``uucp'' and ``news'' should be listed "
"in this file.  Blank lines and lines beginning with `#' are ignored."
msgstr ""
"E<.Pa /etc/ftpusers> es usado por E<.Xr ftpd 8>; el archivo contiene una "
"lista de usuarios a los que no se les permite usar ftp. Por razones de "
"seguridad al menos usuarios como ``root'', ``bin'', ``uucp'' y ``news'' "
"deberían figurar en ese archivo. Las líneas en blanco y las que empiezan por "
"`#' se ignoran."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Note: a lines with `#' in the E<.Em middle> is E<.Em not> a comment. Don't "
"put `#' after a name to comment it; use another line, or things will "
"silently fail on you."
msgstr ""
"Nota: una línea con `#' en el E<.Em medio> E<.Em no> es un comentario. No "
"ponga `#' después de un nombre para comentarlo, simplemente use otra línea. "
"No se muestra ningún error, por lo que puede ser dificil darse cuenta. "

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Pa /etc/ftpusers> might contain the following entries:"
msgstr "E<.Pa /etc/ftpusers> puede contener las siguientes entradas:"

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"#\n"
"# /etc/ftpusers\n"
"#\n"
"root\n"
"uucp\n"
"news\n"
msgstr ""
"#\n"
"# /etc/ftpusers\n"
"#\n"
"root\n"
"uucp\n"
"news\n"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Pa /etc/ftpusers>"
msgstr "E<.Pa /etc/ftpusers>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Xr ftp 1>, E<.Xr ftpd 8>"
msgstr "E<.Xr ftp 1>, E<.Xr ftpd 8>"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
