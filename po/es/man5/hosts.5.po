# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
# Marcos Fouces <marcos@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:01+0100\n"
"PO-Revision-Date: 2023-09-08 18:29+0200\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "hosts"
msgstr "hosts"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "hosts - static table lookup for hostnames"
msgstr "hosts - tabla estática de búsqueda para nombres de host"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B</etc/hosts>\n"
msgstr "B</etc/hosts>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page describes the format of the I</etc/hosts> file.  This file "
"is a simple text file that associates IP addresses with hostnames, one line "
"per IP address.  For each host a single line should be present with the "
"following information:"
msgstr ""
"Esta página de manual describe el formato del fichero I</etc/hosts>. Es un "
"simple archivo de texto que asocia direcciones IP con nombres de host. Para "
"cada host debería aparecer una línea con la siguiente información:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "IP_address canonical_hostname [aliases...]"
msgstr "Dirección_IP nombre_host_canónico [alias...]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The IP address can conform to either IPv4 or IPv6.  Fields of the entry are "
"separated by any number of blanks and/or tab characters.  Text from a \"#\" "
"character until the end of the line is a comment, and is ignored.  Host "
"names may contain only alphanumeric characters, minus signs (\"-\"), and "
"periods (\".\").  They must begin with an alphabetic character and end with "
"an alphanumeric character.  Optional aliases provide for name changes, "
"alternate spellings, shorter hostnames, or generic hostnames (for example, "
"I<localhost>).  If required, a host may have two separate entries in this "
"file; one for each version of the Internet Protocol (IPv4 and IPv6)."
msgstr ""
"La dirección IP puede ser IPv4 o IPv6. Los campos están separados por una "
"cantidad de espacios y/o tabulaciones. Cualquier texto que se incluya a "
"continuación del carácter \"#\" se considera un comentario y no se "
"interpretará. Los nombres de host debería estar compuestos únicamente por "
"caracteres alfanuméricos, el signo menos (\"-\") y puntos (\".\"). Tienen "
"que empezar por una letra y terminar por cualquier carácter alfanumérico. "
"Pueden tener nombres alternativos (\"alias\") que permitan otro nombres, más "
"cortos o incluso genéricos como por ejemplo I<localhost>. Un host puede "
"tener dos entradas distintas, una para cada versión de IP (IPv4 e IPv6)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Berkeley Internet Name Domain (BIND) Server implements the Internet name "
"server for UNIX systems.  It augments or replaces the I</etc/hosts> file or "
"hostname lookup, and frees a host from relying on I</etc/hosts> being up to "
"date and complete."
msgstr ""
"El Servidor Berkeley Internet Name Domain (BIND) implementa el servidor de "
"nombres de Internet para los sistemas UNIX. Este servidor complementa o "
"reemplaza el fichero I</etc/hosts> o la búsqueda de nombres de hosts, y "
"evita que un host tenga que confiar en que su fichero I</etc/hosts> esté "
"actualizado y completo."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In modern systems, even though the host table has been superseded by DNS, it "
"is still widely used for:"
msgstr ""
"Aunque actualmente DNS ha sustituido la tabla de host, todavía se utiliza "
"ampliamente para:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<bootstrapping>"
msgstr "B<el proceso de arranque>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Most systems have a small host table containing the name and address "
"information for important hosts on the local network.  This is useful when "
"DNS is not running, for example during system bootup."
msgstr ""
"La mayoría de los sistemas tienen una pequeña tabla de hosts que contiene el "
"nombre y la dirección de hosts importantes en la red local. Esto es útil "
"cuando el servicio DNS no se está ejecutando, por ejemplo, durante el "
"arranque inicial del sistema."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<NIS>"
msgstr "B<NIS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sites that use NIS use the host table as input to the NIS host database.  "
"Even though NIS can be used with DNS, most NIS sites still use the host "
"table with an entry for all local hosts as a backup."
msgstr ""
"Los sitios que usan NIS utilizan la tabla de hosts como dato de entrada para "
"la base de datos de hosts de NIS. Aunque se puede usar NIS junto con DNS, la "
"mayoría de sitios con NIS todavía usan la tabla de hosts como respaldo, la "
"cual contiene una entrada para cada nodo local."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<isolated nodes>"
msgstr "B<nodos aislados>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Very small sites that are isolated from the network use the host table "
"instead of DNS.  If the local information rarely changes, and the network is "
"not connected to the Internet, DNS offers little advantage."
msgstr ""
"Los sitios pequeños que están aislados de la red usan la tabla de hosts en "
"lugar del servicio DNS. Si la información local casi nunca cambia y la red "
"no está conectada a Internet, el servicio DNS ofrecerá pocos beneficios."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</etc/hosts>"
msgstr "I</etc/hosts>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Modifications to this file normally take effect immediately, except in cases "
"where the file is cached by applications."
msgstr ""
"Los cambios en este archivo surten efecto inmediato salvo en aplicaciones "
"que guarden una copia en su caché."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Historical notes"
msgstr "Notas históricas"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"RFC\\ 952 gave the original format for the host table, though it has since "
"changed."
msgstr ""
"En el RFC\\952 se define el formato original de la tabla de host, aunque ha "
"sufrido algunos cambios desde entonces."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Before the advent of DNS, the host table was the only way of resolving "
"hostnames on the fledgling Internet.  Indeed, this file could be created "
"from the official host data base maintained at the Network Information "
"Control Center (NIC), though local changes were often required to bring it "
"up to date regarding unofficial aliases and/or unknown hosts.  The NIC no "
"longer maintains the hosts.txt files, though looking around at the time of "
"writing (circa 2000), there are historical hosts.txt files on the WWW.  I "
"just found three, from 92, 94, and 95."
msgstr ""
"Antes de la llegada de DNS, esta tabla era la única forma de resolver "
"nombres de hosts en la joven Internet. De hecho, este fichero se podía crear "
"a partir de la base de datos oficial de hosts que mantenía en el Network "
"Information Control Center (NIC), aunque eran necesarias frecuentes "
"actualizaciones para tener al día el fichero respecto a alias no oficiales y/"
"o hosts desconocidos. El NIC ya no mantiene los ficheros hosts.txt aunque, "
"buscando por ahí en el momento de escribir esto (alrededor del año 2000), "
"aparecen ficheros hosts.txt históricos en la WWW. Yo he encontrado tres: del "
"año 92, del 94 y del 95."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"# The following lines are desirable for IPv4 capable hosts\n"
"127.0.0.1       localhost\n"
"\\&\n"
"# 127.0.1.1 is often used for the FQDN of the machine\n"
"127.0.1.1       thishost.example.org   thishost\n"
"192.168.1.10    foo.example.org        foo\n"
"192.168.1.13    bar.example.org        bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"
"\\&\n"
"# The following lines are desirable for IPv6 capable hosts\n"
"::1             localhost ip6-localhost ip6-loopback\n"
"ff02::1         ip6-allnodes\n"
"ff02::2         ip6-allrouters\n"
msgstr ""
"# Suele aconsejarse añadir estas líneas en los equipos IPv4\n"
"127.0.0.1       localhost\n"
"\\&\n"
"# 127.0.1.1 se suele usar como el FQDN del equipo\n"
"127.0.1.1       estehost.midominio.org  estehost\n"
"192.168.1.10    foo.midominio.org       foo\n"
"192.168.1.13    bar.midominio.org       bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"
"\\&\n"
"# Suele aconsejarse añadir estas líneas en equipos IPv6\n"
"::1             localhost ip6-localhost ip6-loopback\n"
"ff02::1         ip6-allnodes\n"
"ff02::2         ip6-allrouters\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<hostname>(1), B<resolver>(3), B<host.conf>(5), B<resolv.conf>(5), "
"B<resolver>(5), B<hostname>(7), B<named>(8)"
msgstr ""
"B<hostname>(1), B<resolver>(3), B<host.conf>(5), B<resolv.conf>(5), "
"B<resolver>(5), B<hostname>(7), B<named>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Internet RFC\\ 952"
msgstr "Internet RFC\\ 952"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 Octubre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"# The following lines are desirable for IPv4 capable hosts\n"
"127.0.0.1       localhost\n"
msgstr ""
"# Sería bueno incluir estas líneas en un host con IPv4\n"
"127.0.0.1\t   localhost\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"# 127.0.1.1 is often used for the FQDN of the machine\n"
"127.0.1.1       thishost.example.org   thishost\n"
"192.168.1.10    foo.example.org        foo\n"
"192.168.1.13    bar.example.org        bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"
msgstr ""
"# 127.0.1.1 se suele usar como el FQDN del equipo\n"
"127.0.1.1       estehost.midominio.org  estehost\n"
"192.168.1.10    foo.midominio.org       foo\n"
"192.168.1.13    bar.midominio.org       bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"# The following lines are desirable for IPv6 capable hosts\n"
"::1             localhost ip6-localhost ip6-loopback\n"
"ff02::1         ip6-allnodes\n"
"ff02::2         ip6-allrouters\n"
msgstr ""
"# Sería bueno que un host con IPv6 incluya estas líneas\n"
"::1             localhost ip6-localhost ip6-loopback\n"
"ff02::1         ip6-allnodes\n"
"ff02::2         ip6-allrouters\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
