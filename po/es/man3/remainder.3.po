# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Sebastian Desimone <chipy@argenet.com.ar>, 1998.
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:11+0100\n"
"PO-Revision-Date: 1998-02-14 19:53+0200\n"
"Last-Translator: Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<remainder>():"
msgid "remainder"
msgstr "B<remainder>():"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"drem, dremf, dreml, remainder, remainderf, remainderl - floating-point "
"remainder function"
msgstr ""
"drem, dremf, dreml, remainder, remainderf, remainderl - función resto de "
"coma flotante"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Biblioteca Matemática (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double remainder(double >I<x>B<, double >I<y>B<);>\n"
"B<float remainderf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double remainderl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"/* Obsolete synonyms */\n"
"B<[[deprecated]] double drem(double >I<x>B<, double >I<y>B<);>\n"
"B<[[deprecated]] float dremf(float >I<x>B<, float >I<y>B<);>\n"
"B<[[deprecated]] long double dreml(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<remainder>():"
msgstr "B<remainder>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Desde glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<remainderf>(), B<remainderl>():"
msgstr "B<remainderf>(), B<remainderl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Desde glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<drem>(), B<dremf>(), B<dreml>():"
msgstr "B<drem>(), B<dremf>(), B<dreml>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    /* Desde glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<drem()> function computes the remainder of dividing I<x> by I<y>.  "
#| "The return value is I<x> - I<n> * I<y>, where I<n> is the quotient of "
#| "I<x> / I<y>, rounded to the nearest integer.  If the quotient is 1/2, it "
#| "is rounded to the even number."
msgid ""
"These functions compute the remainder of dividing I<x> by I<y>.  The return "
"value is I<x>-I<n>*I<y>, where I<n> is the value I<x\\ /\\ y>, rounded to "
"the nearest integer.  If the absolute value of I<x>-I<n>*I<y> is 0.5, I<n> "
"is chosen to be even."
msgstr ""
"La función B<drem()> calcula el resto de la división de I<x> entre I<y>. El "
"valor devuelto es I<x> - I<n> * I<y>, donde I<n> es el cociente de I<x> / "
"I<y>, redondeado al entero más cercano. Si el cociente es ½, se redondea al "
"número par."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are unaffected by the current rounding mode (see B<fenv>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The B<drem>()  function does precisely the same thing."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these functions return the floating-point remainder, I<x>-"
"I<n>*I<y>.  If the return value is 0, it has the sign of I<x>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If I<x> or I<y> is a NaN, a NaN is returned."
msgstr "Si I<x> o I<y> es un NaN, NaN es devuelto."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<x> is an infinity, and I<y> is not a NaN, a domain error occurs, and a "
"NaN is returned."
msgstr ""

#.  FIXME . Instead, glibc gives a domain error even if x is a NaN
#.  Interestingly, remquo(3) does not have the same problem.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<y> is zero, and I<x> is not a NaN, a domain error occurs, and a NaN is "
"returned."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Consulte B<math_error>(7) para saber cómo es posible conocer si se ha "
"producido algún error al invocar estas funciones."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Puede ocurrir los siguientes errores"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<x> is an infinity and I<y> is not a NaN"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<EDOM> (but see BUGS).  An invalid floating-point "
"exception (B<FE_INVALID>)  is raised."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "These functions do not set I<errno> for this case."
msgstr ""

#.  [XXX see bug above] and \fIx\fP is not a NaN
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<y> is zero"
msgstr "Error de dominio: I<y> es cero."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<EDOM>.  An invalid floating-point exception "
"(B<FE_INVALID>)  is raised."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<drem>(),\n"
"B<dremf>(),\n"
"B<dreml>(),\n"
"B<remainder>(),\n"
"B<remainderf>(),\n"
"B<remainderl>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<remainder>():"
msgid "B<remainder>()"
msgstr "B<remainder>():"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<remainder>():"
msgid "B<remainderf>()"
msgstr "B<remainder>():"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<remainderl>()"
msgstr "B<remainderl>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<drem>()"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<dremf>()"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<remainderl>()"
msgid "B<dreml>()"
msgstr "B<remainderl>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "4.3BSD."
msgstr "4.3BSD."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Tru64, glibc2."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. #-#-#-#-#  archlinux: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  debian-unstable: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  fedora-41: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6779
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "Before glibc 2.10:"
msgid "Before glibc 2.15, the call"
msgstr "Antes de glibc 2.10:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "    remainder(nan(\"\"), 0);\n"
msgid "remainder(nan(\"\"), 0);\n"
msgstr "    remainder(nan(\"\"), 0);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"returned a NaN, as expected, but wrongly caused a domain error.  Since glibc "
"2.15, a silent NaN (i.e., no domain error) is returned."
msgstr ""

#. #-#-#-#-#  archlinux: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  debian-unstable: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  fedora-41: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: remainder.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6783
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Before glibc 2.15, I<errno> was not set to B<EDOM> for the domain error that "
"occurs when I<x> is an infinity and I<y> is not a NaN."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The call \"remainder(29.0, 3.0)\" returns -1."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<div>(3), B<fmod>(3), B<remquo>(3)"
msgstr "B<div>(3), B<fmod>(3), B<remquo>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* The C99 versions */\n"
"B<double remainder(double >I<x>B<, double >I<y>B<);>\n"
"B<float remainderf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double remainderl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"/* Obsolete synonyms */\n"
"B<double drem(double >I<x>B<, double >I<y>B<);>\n"
"B<float dremf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double dreml(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#.  IEC 60559.
#. type: Plain text
#: debian-bookworm
msgid ""
"The functions B<remainder>(), B<remainderf>(), and B<remainderl>()  are "
"specified in C99, POSIX.1-2001, and POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The function B<drem>()  is from 4.3BSD.  The I<float> and I<long double> "
"variants B<dremf>()  and B<dreml>()  exist on some systems, such as Tru64 "
"and glibc2.  Avoid the use of these functions in favor of B<remainder>()  "
"etc."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
