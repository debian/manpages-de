# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: 2004-08-06 19:53+0200\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<lockf>()"
msgid "lockf"
msgstr "B<lockf>()"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 Mayo 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "lockf - apply, test or remove a POSIX lock on an open file"
msgstr ""
"lockf - aplica, comprueba o elimina un bloqueo POSIX sobre un fichero abierto"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int lockf(int >I<fd>B<, int >I<cmd>B<, off_t >I<len>B<);>"
msgid "B<int lockf(int >I<fd>B<, int >I<op>B<, off_t >I<len>B<);>\n"
msgstr "B<int lockf(int >I<fd>B<, int >I<cmd>B<, off_t >I<len>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<lockf>():"
msgstr "B<lockf>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Apply, test or remove a POSIX lock on a section of an open file.  The "
#| "file is specified by I<fd>, a file descriptor open for writing, the "
#| "action by I<cmd>, and the section consists of byte positions I<pos>.."
#| "I<pos>+I<len>-1 if I<len> is positive, and I<pos>-I<len>..I<pos>-1 if "
#| "I<len> is negative, where I<pos> is the current file position, and if "
#| "I<len> is zero, the section extends from the current file position to "
#| "infinity, encompassing the present and future end-of-file positions.  In "
#| "all cases, the section may extend past current end-of-file."
msgid ""
"Apply, test, or remove a POSIX lock on a section of an open file.  The file "
"is specified by I<fd>, a file descriptor open for writing, the action by "
"I<op>, and the section consists of byte positions I<pos>..I<pos>+I<len>-1 if "
"I<len> is positive, and I<pos>-I<len>..I<pos>-1 if I<len> is negative, where "
"I<pos> is the current file position, and if I<len> is zero, the section "
"extends from the current file position to infinity, encompassing the present "
"and future end-of-file positions.  In all cases, the section may extend past "
"current end-of-file."
msgstr ""
"Aplica, comprueba o elimina un bloqueo POSIX sobre una sección de un fichero "
"abierto.  El fichero está especificado por I<fd>, un descriptor de fichero "
"abierto para escritura, la acción por I<cmd>, y la sección consiste en las "
"posiciones de byte I<pos>..I<pos>+I<len>-1, si I<len> es positivo, y I<pos>-"
"I<len>..I<pos>-1, si I<len> es negativo, donde I<pos> es la posición actual "
"del fichero. Si I<len> es cero, la sección se extiende desde la posición "
"actual del fichero al infinito, abarcando las posiciones actual y futura de "
"«fin de fichero». En todos los casos, la sección se puede extender más alla "
"del «fin de fichero» actual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Linux, B<lockf>()  is just an interface on top of B<fcntl>(2)  locking.  "
"Many other systems implement B<lockf>()  in this way, but note that POSIX.1 "
"leaves the relationship between B<lockf>()  and B<fcntl>(2)  locks "
"unspecified.  A portable application should probably avoid mixing calls to "
"these interfaces."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Valid operations are given below:"
msgstr "Las operaciones válidas son:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<F_LOCK>"
msgstr "B<F_LOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Set an exclusive lock on the specified section of the file.  If (part of) "
"this section is already locked, the call blocks until the previous lock is "
"released.  If this section overlaps an earlier locked section, both are "
"merged.  File locks are released as soon as the process holding the locks "
"closes some file descriptor for the file.  A child process does not inherit "
"these locks."
msgstr ""
"Establece un bloqueo exclusivo en la sección especificada del fichero.  Si "
"(parte de) esta sección ya está bloqueada, la llamada se bloquea hasta que "
"el bloqueo anterior sea liberado.  Si esta sección se solapa con una sección "
"previamente bloqueada, ambas se fusionan.  Los bloqueos sobre el fichero son "
"liberados tan pronto como el proceso que mantiene los bloqueos cierre "
"algunos descriptores de fichero para el fichero.  Un proceso hijo no hereda "
"estos bloqueos."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<F_TLOCK>"
msgstr "B<F_TLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Same as B<F_LOCK> but the call never blocks and returns an error instead if "
"the file is already locked."
msgstr ""
"Igual que B<F_LOCK> pero la llamada nunca se bloquea y devuelve un error en "
"su lugar si el fichero ya está bloqueado."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<F_ULOCK>"
msgstr "B<F_ULOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unlock the indicated section of the file.  This may cause a locked section "
"to be split into two locked sections."
msgstr ""
"Desbloquea la sección indicada del fichero.  Ésto puede provocar que una "
"sección bloqueada se divida en dos secciones bloqueadas."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<F_TEST>"
msgstr "B<F_TEST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Test the lock: return 0 if the specified section is unlocked or locked by "
#| "this process; return -1, set I<errno> to B<EACCES>, if another process "
#| "holds a lock."
msgid ""
"Test the lock: return 0 if the specified section is unlocked or locked by "
"this process; return -1, set I<errno> to B<EAGAIN> (B<EACCES> on some other "
"systems), if another process holds a lock."
msgstr ""
"Comprueba el bloqueo: devuelve 0 si la sección especificada está "
"desbloqueada o bloqueada por este proceso; devuelve -1 y asigna a I<errno> "
"el valor B<EACCES> si otro proceso mantiene un bloqueo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"En caso de éxito se devuelve cero. En caso de error se devuelve -1, y "
"I<errno> se configura para indicar el error."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES> or B<EAGAIN>"
msgstr "B<EACCES> o B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The file is locked and B<F_TLOCK> or B<F_TEST> was specified, or the "
"operation is prohibited because the file has been memory-mapped by another "
"process."
msgstr ""
"El fichero está bloqueado y se especificó B<F_TLOCK> o B<F_TEST>, o se "
"prohibe la operación porque el fichero ha sido ubicado en memoria por otro "
"proceso."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<fd> is not an open file descriptor; or I<op> is B<F_LOCK> or B<F_TLOCK> "
"and I<fd> is not a writable file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EDEADLK>"
msgstr "B<EDEADLK>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The command was B<F_LOCK> and this lock operation would cause a deadlock."
msgid "I<op> was B<F_LOCK> and this lock operation would cause a deadlock."
msgstr ""
"La orden fue B<F_LOCK> y esta operación de bloqueo causaría una situación de "
"interbloqueo."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"While waiting to acquire a lock, the call was interrupted by delivery of a "
"signal caught by a handler; see B<signal>(7)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid "An invalid operation was specified in I<cmd>."
msgid "An invalid operation was specified in I<op>."
msgstr "Se especificó una operación inválida en I<cmd>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOLCK>"
msgstr "B<ENOLCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Too many segment locks open, lock table is full."
msgstr ""
"Demasiados bloqueos de segmento abiertos, la tabla de bloqueos está llena."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<lockf>()"
msgstr "B<lockf>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4."
msgstr "POSIX.1-2001, SVr4."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<fcntl>(2), B<flock>(2)"
msgstr "B<fcntl>(2), B<flock>(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<locks.txt> and I<mandatory-locking.txt> in the Linux kernel source "
"directory I<Documentation/filesystems> (on older kernels, these files are "
"directly under the I<Documentation> directory, and I<mandatory-locking.txt> "
"is called I<mandatory.txt>)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<int lockf(int >I<fd>B<, int >I<cmd>B<, off_t >I<len>B<);>"
msgid "B<int lockf(int >I<fd>B<, int >I<cmd>B<, off_t >I<len>B<);>\n"
msgstr "B<int lockf(int >I<fd>B<, int >I<cmd>B<, off_t >I<len>B<);>"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "Apply, test or remove a POSIX lock on a section of an open file.  The "
#| "file is specified by I<fd>, a file descriptor open for writing, the "
#| "action by I<cmd>, and the section consists of byte positions I<pos>.."
#| "I<pos>+I<len>-1 if I<len> is positive, and I<pos>-I<len>..I<pos>-1 if "
#| "I<len> is negative, where I<pos> is the current file position, and if "
#| "I<len> is zero, the section extends from the current file position to "
#| "infinity, encompassing the present and future end-of-file positions.  In "
#| "all cases, the section may extend past current end-of-file."
msgid ""
"Apply, test, or remove a POSIX lock on a section of an open file.  The file "
"is specified by I<fd>, a file descriptor open for writing, the action by "
"I<cmd>, and the section consists of byte positions I<pos>..I<pos>+I<len>-1 "
"if I<len> is positive, and I<pos>-I<len>..I<pos>-1 if I<len> is negative, "
"where I<pos> is the current file position, and if I<len> is zero, the "
"section extends from the current file position to infinity, encompassing the "
"present and future end-of-file positions.  In all cases, the section may "
"extend past current end-of-file."
msgstr ""
"Aplica, comprueba o elimina un bloqueo POSIX sobre una sección de un fichero "
"abierto.  El fichero está especificado por I<fd>, un descriptor de fichero "
"abierto para escritura, la acción por I<cmd>, y la sección consiste en las "
"posiciones de byte I<pos>..I<pos>+I<len>-1, si I<len> es positivo, y I<pos>-"
"I<len>..I<pos>-1, si I<len> es negativo, donde I<pos> es la posición actual "
"del fichero. Si I<len> es cero, la sección se extiende desde la posición "
"actual del fichero al infinito, abarcando las posiciones actual y futura de "
"«fin de fichero». En todos los casos, la sección se puede extender más alla "
"del «fin de fichero» actual."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"I<fd> is not an open file descriptor; or I<cmd> is B<F_LOCK> or B<F_TLOCK> "
"and I<fd> is not a writable file descriptor."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The command was B<F_LOCK> and this lock operation would cause a deadlock."
msgstr ""
"La orden fue B<F_LOCK> y esta operación de bloqueo causaría una situación de "
"interbloqueo."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "An invalid operation was specified in I<cmd>."
msgstr "Se especificó una operación inválida en I<cmd>."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, SVr4."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
