# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2018-2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2024-03-29 09:39+0100\n"
"PO-Revision-Date: 2024-05-03 18:06+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "FINDMNT"
msgstr "FINDMNT"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr "System-Administration"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm
msgid "findmnt - find a filesystem"
msgstr "findmnt - Ein Dateisystem finden"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt> [options]"
msgstr "B<findmnt> [Optionen]"

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt> [options] I<device>|I<mountpoint>"
msgstr "B<findmnt> [Optionen] I<Gerät>|I<Einhängepunkt>"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<findmnt> [options] [B<--source>] I<device> [B<--target> I<path>|B<--"
"mountpoint> I<mountpoint>]"
msgstr ""
"B<findmnt> [Optionen] [B<--source>] I<Gerät> [B<--target> I<Pfad>|B<--"
"mountpoint>] I<Einhängepunkt>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<findmnt> will list all mounted filesystems or search for a filesystem. The "
"B<findmnt> command is able to search in I</etc/fstab>, I</etc/mtab> or I</"
"proc/self/mountinfo>. If I<device> or I<mountpoint> is not given, all "
"filesystems are shown."
msgstr ""
"B<findmnt> listet alle eingehängten Dateisysteme auf oder sucht nach einem "
"Dateisystem. Der Befehl B<findmnt> kann in I</etc/fstab>, I</etc/mtab> oder "
"I</proc/self/mountinfo> suchen. Falls kein I<Gerät> oder I<Einhängepunkt> "
"angegeben wird, werden alle Dateisysteme angezeigt."

#. type: Plain text
#: debian-bookworm
msgid ""
"The device may be specified by device name, major:minor numbers, filesystem "
"label or UUID, or partition label or UUID. Note that B<findmnt> follows "
"B<mount>(8) behavior where a device name may be interpreted as a mountpoint "
"(and vice versa) if the B<--target>, B<--mountpoint> or B<--source> options "
"are not specified."
msgstr ""
"Das Gerät kann anhand des Gerätenamens, der Major:Minor-Nummern, der "
"Dateisystembezeichnung oder -UUID oder der Partitionsbezeichnung oder -UUID "
"angegeben werden. Beachten Sie, dass B<findmnt> dem Verhalten von "
"B<mount>(8) folgt, bei dem ein Gerätename als Einhängepunkt interpretiert "
"werden kann (und umgekehrt ein Einhängepunkt als Gerätename), wenn die "
"Optionen B<--target>, B<--mountpoint> oder B<--source> nicht angegeben sind."

#. type: Plain text
#: debian-bookworm
msgid ""
"The command-line option B<--target> accepts any file or directory and then "
"B<findmnt> displays the filesystem for the given path."
msgstr ""
"Die Befehlszeilenoption B<--target> akzeptiert jede Datei oder jedes "
"Verzeichnis und B<findmnt> zeigt das Dateisystem für den angegebenen Pfad an."

#. type: Plain text
#: debian-bookworm
msgid ""
"The command prints all mounted filesystems in the tree-like format by "
"default."
msgstr ""
"Standardmäßig gibt der Befehl alle eingehängten Dateisysteme in einer "
"Baumansicht aus."

#. type: Plain text
#: debian-bookworm
msgid ""
"The relationship between block devices and filesystems is not always one-to-"
"one. The filesystem may use more block devices. This is why B<findmnt> "
"provides SOURCE and SOURCES (pl.) columns. The column SOURCES displays all "
"devices where it is possible to find the same filesystem UUID (or another "
"tag specified in I<fstab> when executed with B<--fstab> and B<--evaluate>)."
msgstr ""
"Die Beziehung zwischen blockorientierten Geräten und Dateisystemen ist nicht "
"immer 1:1. Das Dateisystem kann mehrere blockorientierte Geräte verwenden. "
"Deshalb stellt B<findmnt> die Spalten SOURCE und SOURCES (Plural) bereit. "
"Die Spalte SOURCES zeigt alle Geräte mit der gleichen Dateisystem-UUID (oder "
"bei der Ausführung mit B<--fstab> und B<--evaluate> mit einer anderweitigen "
"in der I<fstab>-Datei definierten Markierung) an."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm
msgid "B<-A>, B<--all>"
msgstr "B<-A>, B<--all>"

#. type: Plain text
#: debian-bookworm
msgid "Disable all built-in filters and print all filesystems."
msgstr "deaktiviert alle eingebauten Filter und gibt alle Dateisysteme aus."

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--ascii>"
msgstr "B<-a>, B<--ascii>"

#. type: Plain text
#: debian-bookworm
msgid "Use ascii characters for tree formatting."
msgstr "verwendet ASCII-Zeichen für die Formatierung der Baumansicht."

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr "gibt die Größen in Byte anstelle eines menschenlesbaren Formats aus."

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""
"Standardmäßig werden die Größen in Byte ausgedrückt und die Präfixe sind "
"Potenzen der Form 2^10 (1024). Die Abkürzungen der Symbole werden zur "
"besseren Lesbarkeit abgeschnitten, indem jeweils nur der erste Buchstabe "
"dargestellt wird. Beispiele: »1 KiB« und »1 MiB« werden als »1 K« bzw. »1 M« "
"dargestellt. Die Erwähnung des »iB«-Anteils, der Teil der Abkürzung ist, "
"entfällt absichtlich."

#. type: Plain text
#: debian-bookworm
msgid "B<-C>, B<--nocanonicalize>"
msgstr "B<-C>, B<--nocanonicalize>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Do not canonicalize paths at all. This option affects the comparing of paths "
"and the evaluation of tags (LABEL, UUID, etc.)."
msgstr ""
"kanonisiert die Pfade nicht. Diese Option beeinflusst den Vergleich von "
"Pfaden und die Auswertung von Markierungen (LABEL, UUID, usw.)."

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--canonicalize>"
msgstr "B<-c>, B<--canonicalize>"

#. type: Plain text
#: debian-bookworm
msgid "Canonicalize all printed paths."
msgstr "kanonisiert alle ausgegebenen Pfade."

#. type: Plain text
#: debian-bookworm
msgid "B<--deleted>"
msgstr "B<--deleted>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Print filesystems where target (mountpoint) is marked as deleted by kernel."
msgstr ""
"gibt Dateisysteme aus, deren Ziel (Einhängepunkt) vom Kernel als gelöscht "
"markiert wurde."

#. type: Plain text
#: debian-bookworm
msgid "B<-D>, B<--df>"
msgstr "B<-D>, B<--df>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Imitate the output of B<df>(1). This option is equivalent to B<-o "
"SOURCE,FSTYPE,SIZE,USED,AVAIL,USE%,TARGET> but excludes all pseudo "
"filesystems. Use B<--all> to print all filesystems."
msgstr ""
"imitiert die Ausgabe von B<df>(1). Diese Option ist gleichbedeutend mit B<-o "
"SOURCE,FSTYPE,SIZE,USED,AVAIL,USE%,TARGET>, schließt aber alle Pseudo-"
"Dateisysteme aus. Verwenden Sie B<--all>, um alle Dateisysteme anzuzeigen."

#. type: Plain text
#: debian-bookworm
msgid "B<-d>, B<--direction> I<word>"
msgstr "B<-d>, B<--direction> I<Wort>"

#. type: Plain text
#: debian-bookworm
msgid "The search direction, either B<forward> or B<backward>."
msgstr ""
"gibt die Suchrichtung an, entweder B<forward> (vorwärts) oder B<backward> "
"(rückwärts)."

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--evaluate>"
msgstr "B<-e>, B<--evaluate>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Convert all tags (LABEL, UUID, PARTUUID, or PARTLABEL) to the corresponding "
"device names for the SOURCE column.  It\\(cqs an unusual situation, but the "
"same tag may be duplicated (used for more devices). For this purpose, there "
"is SOURCES (pl.) column. This column displays by multi-line cell all devices "
"where the tag is detected by libblkid. This option makes sense for I<fstab> "
"only."
msgstr ""
"wandelt alle Markierungen (LABEL, UUID, PARTUUID oder PARTLABEL) in die "
"korrespondierenden Gerätenamen für die SOURCE-Spalte um. Das ist zwar ein "
"unüblicher Fall, aber damit kann eine Markierung dupliziert werden (um sie "
"für weitere Geräte zu verwenden). Diesem Zweck dient die Spalte SOURCEB<S>. "
"Diese Spalte zeigt in mehrzeiligen Zellen alle Geräte an, deren Markierung "
"von Libblkid erkannt wurde. Diese Option ist nur für I<fstab> sinnvoll."

#. type: Plain text
#: debian-bookworm
msgid "B<-F>, B<--tab-file> I<path>"
msgstr "B<-F>, B<--tab-file> I<Pfad>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Search in an alternative file. If used with B<--fstab>, B<--mtab> or B<--"
"kernel>, then it overrides the default paths. If specified more than once, "
"then tree-like output is disabled (see the B<--list> option)."
msgstr ""
"sucht in einer alternativen Datei. Wenn diese Option mit B<--fstab>, B<--"
"mtab> oder B<--kernel> verwendet wird, werden die Standardpfade außer Kraft "
"gesetzt. Wenn dies mehrmals angegeben wird, ist die Baumansicht deaktiviert "
"(siehe die Option B<--list>)."

#. type: Plain text
#: debian-bookworm
msgid "B<-f>, B<--first-only>"
msgstr "B<-f>, B<--first-only>"

#. type: Plain text
#: debian-bookworm
msgid "Print the first matching filesystem only."
msgstr "gibt nur das erste passende Dateisystem aus."

#. type: Plain text
#: debian-bookworm
msgid "B<-i>, B<--invert>"
msgstr "B<-i>, B<--invert>"

#. type: Plain text
#: debian-bookworm
msgid "Invert the sense of matching."
msgstr "kehrt die Suchlogik um."

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr "B<-J>, B<--json>"

#. type: Plain text
#: debian-bookworm
msgid "Use JSON output format."
msgstr "verwendet das JSON-Ausgabeformat."

#. type: Plain text
#: debian-bookworm
msgid "B<-k>, B<--kernel>"
msgstr "B<-k>, B<--kernel>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Search in I</proc/self/mountinfo>. The output is in the tree-like format. "
"This is the default. The output contains only mount options maintained by "
"kernel (see also B<--mtab>)."
msgstr ""
"sucht in I</proc/self/mountinfo>. Die Ausgabe erfolgt in einer Baumansicht. "
"Dies ist die Vorgabe. Die Ausgabe enthält nur Einhängeoptionen, die vom "
"Kernel verwaltet werden (siehe auch B<--mtab>)."

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Use the list output format. This output format is automatically enabled if "
"the output is restricted by the B<-t>, B<-O>, B<-S> or B<-T> option and the "
"option B<--submounts> is not used or if more that one source file (the "
"option B<-F>) is specified."
msgstr ""
"formatiert die Ausgabe als Liste. Dieses Ausgabeformat ist automatisch "
"aktiviert, wenn die Ausgabe durch die Optionen B<-t>, B<-O>, B<-S> oder B<-"
"T> eingeschränkt ist und die Option B<--submounts> nicht verwendet wird oder "
"mehr als eine Quelldatei (mit der Option B<-F>) angegeben wird."

#. type: Plain text
#: debian-bookworm
msgid "B<-M>, B<--mountpoint> I<path>"
msgstr "B<-M>, B<--mountpoint> I<Pfad>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Explicitly define the mountpoint file or directory. See also B<--target>."
msgstr ""
"definiert explizit die Einhängepunkt-Datei oder das Einhängepunkt-"
"Verzeichnis. Siehe auch B<--target>."

#. type: Plain text
#: debian-bookworm
msgid "B<-m>, B<--mtab>"
msgstr "B<-m>, B<--mtab>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Search in I</etc/mtab>. The output is in the list format by default (see B<--"
"tree>). The output may include user space mount options."
msgstr ""
"sucht in I</etc/mtab>. Die Ausgabe wird standardmäßig als Liste formatiert "
"(siehe B<--tree>). Die Ausgabe kann weitere Einhängeoptionen auf "
"Anwendungsebene enthalten."

#. type: Plain text
#: debian-bookworm
msgid "B<-N>, B<--task> I<tid>"
msgstr "B<-N>, B<--task> I<Thread-Kennung>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Use alternative namespace I</proc/E<lt>tidE<gt>/mountinfo> rather than the "
"default I</proc/self/mountinfo>. If the option is specified more than once, "
"then tree-like output is disabled (see the B<--list> option). See also the "
"B<unshare>(1) command."
msgstr ""
"verwendet anstelle des vorgegebenen I</proc/self/mountinfo> den alternativen "
"Namensraum I</proc/E<lt>Thread-KennungE<gt>/mountinfo>. Wenn diese Option "
"mehr als einmal angegeben wird, dann wird die Baumansicht deaktiviert (siehe "
"die Option B<--list>). Siehe auch den Befehl B<unshare>(1)."

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--noheadings>"
msgstr "B<-n>, B<--noheadings>"

#. type: Plain text
#: debian-bookworm
msgid "Do not print a header line."
msgstr "unterdrückt die Ausgabe einer Kopfzeile."

#. type: Plain text
#: debian-bookworm
msgid "B<-O>, B<--options> I<list>"
msgstr "B<-O>, B<--options> I<Liste>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the set of printed filesystems. More than one option may be specified "
"in a comma-separated list. The B<-t> and B<-O> options are cumulative in "
"effect. It is different from B<-t> in that each option is matched exactly; a "
"leading I<no> at the beginning does not have global meaning. The \"no\" can "
"used for individual items in the list. The \"no\" prefix interpretation can "
"be disabled by \"+\" prefix."
msgstr ""
"begrenzt die Gruppe der ausgegebenen Dateisysteme. Mehrere Optionen können "
"in einer durch Kommata getrennten Liste angegeben werden. Die Optionen B<-t> "
"und B<-O> wirken kumulativ. Im Gegensatz zu B<-t> wird jede Option für sich "
"ausgewertet; ein vorangestelltes I<no> wirkt sich nicht global aus. Das "
"I<no> kann für individuelle Listeneinträge verwendet werden; dessen "
"Interpretation kann mit dem Präfix »+« deaktiviert werden."

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<Liste>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Define output columns. See the B<--help> output to get a list of the "
"currently supported columns. The B<TARGET> column contains tree formatting "
"if the B<--list> or B<--raw> options are not specified."
msgstr ""
"legt die Ausgabespalten fest. In der Ausgabe von B<--help> wird eine Liste "
"der verfügbaren Spalten angezeigt. Die Spalte B<TARGET> wird als Baum "
"formatiert, falls die Optionen B<--list> oder B<--raw> nicht angegeben "
"werden."

#. type: Plain text
#: debian-bookworm
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g., B<findmnt -o +PROPAGATION>)."
msgstr ""
"Die Standardliste der Spalten kann erweitert werden, wenn die I<Liste> im "
"Format I<+Liste> (z.B. B<findmnt -o +PROPAGATION>) vorliegt."

#. type: Plain text
#: debian-bookworm
msgid "B<--output-all>"
msgstr "B<--output-all>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Output almost all available columns. The columns that require B<--poll> are "
"not included."
msgstr ""
"gibt fast alle verfügbaren Spalten aus. Die Spalten, welche B<--poll> "
"erfordern, werden nicht angezeigt."

#. type: Plain text
#: debian-bookworm
msgid "B<-P>, B<--pairs>"
msgstr "B<-P>, B<--pairs>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"value characters are hex-escaped (\\(rsxE<lt>codeE<gt>). See also option B<--"
"shell>."
msgstr ""
"gibt die Daten in Form von Schlüssel=\"Wert\"-Paaren aus. Alle potenziell "
"unsicheren Zeichen werden hexadezimal maskiert (\\(rsxE<lt>codeE<gt>). Siehe "
"auch die Option B<--shell>."

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--poll>[I<=list>]"
msgstr "B<-p>, B<--poll>[I<=Liste>]"

#. type: Plain text
#: debian-bookworm
msgid ""
"Monitor changes in the I</proc/self/mountinfo> file. Supported actions are: "
"mount, umount, remount and move. More than one action may be specified in a "
"comma-separated list. All actions are monitored by default."
msgstr ""
"überwacht Änderungen in der Datei I</proc/self/mountinfo>. Dabei werden die "
"Aktionen »mount«, »umount«, »remount« und »move« unterstützt. Mehrere "
"Aktionen können in Form einer durch Kommata getrennten Liste angegeben "
"werden. Standardmäßig werden alle Aktionen überwacht."

#. type: Plain text
#: debian-bookworm
msgid ""
"The time for which B<--poll> will block can be restricted with the B<--"
"timeout> or B<--first-only> options."
msgstr ""
"Die mit B<--poll> angegebene Sperrzeit kann mit den Optionen B<--timeout> "
"oder B<--first-only> eingeschränkt werden."

#. type: Plain text
#: debian-bookworm
msgid ""
"The standard columns always use the new version of the information from the "
"mountinfo file, except the umount action which is based on the original "
"information cached by B<findmnt>. The poll mode allows using extra columns:"
msgstr ""
"Die Standardspalten verwenden stets die neue Version der Information aus der "
"»mountinfo«-Datei, außer die »umount«-Aktion, die auf der von B<findmnt> "
"zwischengespeicherten Originalinformation basiert. Der Poll-Modus ermöglicht "
"die Verwendung zusätzlicher Spalten:"

#. type: Plain text
#: debian-bookworm
msgid "B<ACTION>"
msgstr "B<ACTION>"

#. type: Plain text
#: debian-bookworm
msgid ""
"mount, umount, move or remount action name; this column is enabled by default"
msgstr ""
"gibt einen der Aktionsnamen »mount«, »umount«, »move« oder »remount« an. "
"Diese Spalte ist standardmäßig aktiviert."

#. type: Plain text
#: debian-bookworm
msgid "B<OLD-TARGET>"
msgstr "B<OLD-TARGET>"

#. type: Plain text
#: debian-bookworm
msgid "available for umount and move actions"
msgstr "ist für die Aktionen »umount« und »move« verfügbar."

#. type: Plain text
#: debian-bookworm
msgid "B<OLD-OPTIONS>"
msgstr "B<OLD-OPTIONS>"

#. type: Plain text
#: debian-bookworm
msgid "available for umount and remount actions"
msgstr "ist für die Aktionen »umount« und »remount« verfügbar."

#. type: Plain text
#: debian-bookworm
msgid "B<--pseudo>"
msgstr "B<--pseudo>"

#. type: Plain text
#: debian-bookworm
msgid "Print only pseudo filesystems."
msgstr "gibt nur Pseudo-Dateisysteme aus."

#. type: Plain text
#: debian-bookworm
msgid "B<--shadow>"
msgstr "B<--shadow>"

#. type: Plain text
#: debian-bookworm
msgid "Print only filesystems over-mounted by another filesystem."
msgstr ""
"gibt nur Dateisysteme aus, die Übereinhängungen anderer Dateisysteme sind."

#. type: Plain text
#: debian-bookworm
msgid "B<-R>, B<--submounts>"
msgstr "B<-R>, B<--submounts>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Print recursively all submounts for the selected filesystems. The "
"restrictions defined by options B<-t>, B<-O>, B<-S>, B<-T> and B<--"
"direction> are not applied to submounts. All submounts are always printed in "
"tree-like order. The option enables the tree-like output format by default. "
"This option has no effect for B<--mtab> or B<--fstab>."
msgstr ""
"gibt rekursiv alle Untereinhängungen für die ausgewählten Dateisysteme aus. "
"Die durch die Optionen B<-t>, B<-O>, B<-S>, B<-T> und B<--direction> "
"definierten Einschränkungen werden nicht auf die Untereinhängungen "
"angewendet. Alle Untereinhängungen werden immer in einer Baumstruktur "
"ausgegeben. Die Option aktiviert standardmäßig die Ausgabe in einer "
"Baumstruktur. Diese Option ist mit B<--mtab> oder B<--fstab> wirkungslos."

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--raw>"
msgstr "B<-r>, B<--raw>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Use raw output format. All potentially unsafe characters are hex-escaped (\\"
"(rsxE<lt>codeE<gt>)."
msgstr ""
"verwendet das Rohformat für die Ausgabe. Alle potenziell unsicheren Zeichen "
"werden hexadezimal maskiert (\\(rsxE<lt>CodeE<gt>)."

#. type: Plain text
#: debian-bookworm
msgid "B<--real>"
msgstr "B<--real>"

#. type: Plain text
#: debian-bookworm
msgid "Print only real filesystems."
msgstr "gibt nur reale Dateisysteme aus."

#. type: Plain text
#: debian-bookworm
msgid "B<-S>, B<--source> I<spec>"
msgstr "B<-S>, B<--source> I<Quelle>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Explicitly define the mount source. Supported specifications are I<device>, "
"I<maj>B<:>I<min>, B<LABEL=>I<label>, B<UUID=>I<uuid>, B<PARTLABEL=>I<label> "
"and B<PARTUUID=>I<uuid>."
msgstr ""
"definiert explizit die Quelle der Einhängung. Dabei werden die Angaben "
"I<Gerät>, I<maj>B<:>I<min>, B<LABEL=>I<Bezeichnung>, B<UUID=>I<UUID>, "
"B<PARTLABEL=>I<Bezeichnung> und B<PARTUUID=>I<UUID> unterstützt."

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--fstab>"
msgstr "B<-s>, B<--fstab>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Search in I</etc/fstab>. The output is in the list format (see B<--list>)."
msgstr ""
"sucht in I</etc/fstab>. Die Ausgabe wird als Liste formatiert (siehe B<--"
"list>)."

#. type: Plain text
#: debian-bookworm
msgid "B<-T>, B<--target> I<path>"
msgstr "B<-T>, B<--target> I<Pfad>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Define the mount target. If I<path> is not a mountpoint file or directory, "
"then B<findmnt> checks the I<path> elements in reverse order to get the "
"mountpoint (this feature is supported only when searching in kernel files "
"and unsupported for B<--fstab>). It\\(cqs recommended to use the option B<--"
"mountpoint> when checks of I<path> elements are unwanted and I<path> is a "
"strictly specified mountpoint."
msgstr ""
"definiert das Ziel der Einhängung. Falls der I<Pfad> keine "
"Einhängepunktdatei oder -verzeichnis ist, dann wertet B<findmnt> die I<Pfad>-"
"Elemente in umgekehrter Reihenfolge aus, um den Einhängepunkt zu ermitteln "
"(diese Funktion wird nur beim Suchen in Kernel-Dateien unterstützt, nicht "
"jedoch für B<--fstab>). Es wird empfohlen, die Option B<--mountpoint> zu "
"verwenden, wenn die I<Pfad>-Elemente nicht ausgewertet werden sollen und "
"I<Pfad> ein strikt angegebener Einhängepunkt ist."

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--types> I<list>"
msgstr "B<-t>, B<--types> I<Liste>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the set of printed filesystems. More than one type may be specified in "
"a comma-separated list. The list of filesystem types can be prefixed with "
"B<no> to specify the filesystem types on which no action should be taken. "
"For more details see B<mount>(8)."
msgstr ""
"begrenzt die Gruppe der ausgegebenen Dateisysteme. Sie können mehrere Typen "
"in einer durch Kommata getrennten Liste angeben. Der Liste der "
"Dateisystemtypen kann ein B<no> vorangestellt werden, um die Dateisysteme zu "
"bezeichnen, für die keine Aktion ausgeführt werden soll. Für weitere Details "
"siehe B<mount>(8)."

#. type: Plain text
#: debian-bookworm
msgid "B<--tree>"
msgstr "B<--tree>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Enable tree-like output if possible. The options is silently ignored for "
"tables where is missing child-parent relation (e.g., I<fstab>)."
msgstr ""
"aktiviert die Baumansicht, falls möglich. Diese Option wird bei Tabellen "
"stillschweigend ignoriert, in denen die Eltern-Kind-Relation fehlt "
"(beispielsweise I<fstab>)."

#. type: Plain text
#: debian-bookworm
msgid "B<--shadowed>"
msgstr "B<--shadowed>"

#. type: Plain text
#: debian-bookworm
msgid "B<-U>, B<--uniq>"
msgstr "B<-U>, B<--uniq>"

# CHECK
# Was ist over-mounted?
#. type: Plain text
#: debian-bookworm
msgid ""
"Ignore filesystems with duplicate mount targets, thus effectively skipping "
"over-mounted mount points."
msgstr ""
"ignoriert Dateisysteme mit doppelten Einhängezielen, daher werden "
"übereingehängte (overmounted) Einhängepunkte wirkungsvoll übergangen."

#. type: Plain text
#: debian-bookworm
msgid "B<-u>, B<--notruncate>"
msgstr "B<-u>, B<--notruncate>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Do not truncate text in columns. The default is to not truncate the "
"B<TARGET>, B<SOURCE>, B<UUID>, B<LABEL>, B<PARTUUID>, B<PARTLABEL> columns. "
"This option disables text truncation also in all other columns."
msgstr ""
"kürzt den Text in den Spalten nicht. Standardmäßig werden die Spalten "
"B<TARGET>, B<SOURCE>, B<UUID>, B<LABEL>, B<PARTUUID> und B<PARTLABEL> nicht "
"gekürzt. Diese Option deaktiviert die Textkürzung auch in allen anderen "
"Spalten."

#. type: Plain text
#: debian-bookworm
msgid "B<-v>, B<--nofsroot>"
msgstr "B<-v>, B<--nofsroot>"

# https://wiki.natenom.de/linux/verschiedenes/btrfs/subvolumes
# (nur rein informativ, lausiges Deutsch, orthografisch ein Graus)
#. type: Plain text
#: debian-bookworm
msgid ""
"Do not print a [/dir] in the SOURCE column for bind mounts or btrfs "
"subvolumes."
msgstr ""
"gibt kein [/Verzeichnis] in der Spalte SOURCE für Bind-Einhängungen oder "
"Btrfs-Teildatenträger aus."

#. type: Plain text
#: debian-bookworm
msgid "B<-w>, B<--timeout> I<milliseconds>"
msgstr "B<-w>, B<--timeout> I<Millisekunden>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify an upper limit on the time for which B<--poll> will block, in "
"milliseconds."
msgstr ""
"gibt eine obere Grenze der Zeitspanne in Millisekunden an, für die B<--poll> "
"blockiert."

#. type: Plain text
#: debian-bookworm
msgid "B<-x>, B<--verify>"
msgstr "B<-x>, B<--verify>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Check mount table content. The default is to verify I</etc/fstab> "
"parsability and usability. It\\(cqs possible to use this option also with "
"B<--tab-file>. It\\(cqs possible to specify source (device) or target "
"(mountpoint) to filter mount table. The option B<--verbose> forces "
"B<findmnt> to print more details."
msgstr ""
"überprüft den Inhalt der Einhängetabelle. Standardmäßig wird die "
"Auswertbarkeit und Verwendbarkeit der Datei I</etc/fstab> überprüft. Sie "
"können diese Option auch mit B<--tab-file> verwenden. Außerdem können Sie "
"eine Quelle (Gerät) oder ein Ziel (Einhängepunkt) angeben, um die "
"Einhängetabelle zu filtern. Die Option B<--verbose> bewirkt, dass die "
"Ausgaben von B<findmnt> ausführlicher werden."

#. type: Plain text
#: debian-bookworm
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. type: Plain text
#: debian-bookworm
msgid "Force B<findmnt> to print more information (B<--verify> only for now)."
msgstr ""
"bewirkt, dass B<findmnt> mehr Informationen ausgibt (derzeit nur B<--"
"verify>)."

#. type: Plain text
#: debian-bookworm
msgid "B<--vfs-all>"
msgstr "B<--vfs-all>"

#. type: Plain text
#: debian-bookworm
msgid ""
"When used with B<VFS-OPTIONS> column, print all VFS (fs-independent) flags. "
"This option is designed for auditing purposes to list also default VFS "
"kernel mount options which are normally not listed."
msgstr ""
"gibt alle VFS- (dateisystemunabhängigen) Schalter aus, wenn dies mit der "
"Spalte B<VFS-OPTIONS> verwendet wird. Diese Option dient Prüfzwecken, damit "
"auch standardmäßige VFS-Kernel-Einhängeoptionen aufgelistet werden, die "
"normalerweise nicht aufgelistet werden würden."

#. type: Plain text
#: debian-bookworm
msgid "B<-y>, B<--shell>"
msgstr "B<-y>, B<--shell>"

#. type: Plain text
#: debian-bookworm
msgid ""
"The column name will be modified to contain only characters allowed for "
"shell variable identifiers. This is usable, for example, with B<--pairs>. "
"Note that this feature has been automatically enabled for B<--pairs> in "
"version 2.37, but due to compatibility issues, now it\\(cqs necessary to "
"request this behavior by B<--shell>."
msgstr ""
"Der Spaltenname wird so geändert, dass er nur Zeichen enthält, die als Shell-"
"Variablenbezeichner genutzt werden können. Dies ist beispielsweise nützlich "
"mit B<--pairs>. Beachten Sie, dass diese Funktion für B<--pairs> in Version "
"2.37 automatisch aktiviert wurde, dass es nun aber aus "
"Kompatibilitätsgründen erforderlich ist, dieses Verhalten durch B<--shell> "
"anzufordern."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: Plain text
#: debian-bookworm
msgid ""
"The exit value is 0 if there is something to display, or 1 on any error (for "
"example if no filesystem is found based on the user\\(cqs filter "
"specification, or the device path or mountpoint does not exist)."
msgstr ""
"Der Rückgabewert ist 0, wenn irgendetwas angezeigt werden kann, oder 1 bei "
"einem Fehler (zum Beispiel falls anhand der vom Benutzer angegebenen Filter "
"kein Dateisystem gefunden werden konnte oder der Gerätepfad oder der "
"Einhängepunkt nicht existiert."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_FSTAB>=E<lt>pathE<gt>"
msgstr "B<LIBMOUNT_FSTAB>=E<lt>PfadE<gt>"

#. type: Plain text
#: debian-bookworm
msgid "overrides the default location of the I<fstab> file"
msgstr "setzt den standardmäßigen Ort der Datei I<fstab> außer Kraft."

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_MTAB>=E<lt>pathE<gt>"
msgstr "B<LIBMOUNT_MTAB>=E<lt>PfadE<gt>"

#. type: Plain text
#: debian-bookworm
msgid "overrides the default location of the I<mtab> file"
msgstr "setzt den standardmäßigen Ort der Datei I<mtab> außer Kraft."

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_DEBUG>=all"
msgstr "B<LIBMOUNT_DEBUG>=all"

#. type: Plain text
#: debian-bookworm
msgid "enables libmount debug output"
msgstr "aktiviert die Fehlersuch-Ausgabe von Libmount."

#. type: Plain text
#: debian-bookworm
msgid "B<LIBSMARTCOLS_DEBUG>=all"
msgstr "B<LIBSMARTCOLS_DEBUG>=all"

#. type: Plain text
#: debian-bookworm
msgid "enables libsmartcols debug output"
msgstr "aktiviert die Fehlerdiagnose-Ausgabe für Libsmartcols."

#. type: Plain text
#: debian-bookworm
msgid "B<LIBSMARTCOLS_DEBUG_PADDING>=on"
msgstr "B<LIBSMARTCOLS_DEBUG_PADDING>=on"

#. type: Plain text
#: debian-bookworm
msgid "use visible padding characters."
msgstr "verwendet sichtbare Auffüllzeichen."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt --fstab -t nfs>"
msgstr "B<findmnt --fstab -t nfs>"

#. type: Plain text
#: debian-bookworm
msgid "Prints all NFS filesystems defined in I</etc/fstab>."
msgstr "gibt alle in I</etc/fstab> definierten NFS-Dateisysteme aus."

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt --fstab /mnt/foo>"
msgstr "B<findmnt --fstab /mnt/foo>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Prints all I</etc/fstab> filesystems where the mountpoint directory is I</"
"mnt/foo>. It also prints bind mounts where I</mnt/foo> is a source."
msgstr ""
"gibt alle in der Datei I</etc/fstab> aufgeführten Dateisysteme aus, deren "
"Einhängepunktverzeichnis I</mnt/foo> ist. Außerdem werden Bind-Einhängungen "
"ausgegeben, deren Quelle I</mnt/foo> ist."

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt --fstab --target /mnt/foo>"
msgstr "B<findmnt --fstab --target /mnt/foo>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Prints all I</etc/fstab> filesystems where the mountpoint directory is I</"
"mnt/foo>."
msgstr ""
"gibt alle in der Datei I</etc/fstab> aufgeführten Dateisysteme aus, deren "
"Einhängepunktverzeichnis I</mnt/foo> ist."

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt --fstab --evaluate>"
msgstr "B<findmnt --fstab --evaluate>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Prints all I</etc/fstab> filesystems and converts LABEL= and UUID= tags to "
"the real device names."
msgstr ""
"gibt alle in der Datei I</etc/fstab> aufgeführten Dateisysteme aus und "
"wandelt die Markierungen LABEL= und UUID= in die echten Gerätenamen um."

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt -n --raw --evaluate --output=target LABEL=/boot>"
msgstr "B<findmnt -n --raw --evaluate --output=target LABEL=/boot>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Prints only the mountpoint where the filesystem with label \"/boot\" is "
"mounted."
msgstr ""
"gibt nur den Einhängepunkt aus, in dem das Dateisystem mit der Bezeichnung »/"
"boot« eingehängt ist."

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt --poll --mountpoint /mnt/foo>"
msgstr "B<findmnt --poll --mountpoint /mnt/foo>"

#. type: Plain text
#: debian-bookworm
msgid "Monitors mount, unmount, remount and move on I</mnt/foo>."
msgstr ""
"überwacht Einhängungen, Aushängungen, erneute Einhängungen und "
"Verschiebungen von I</mnt/foo>."

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt --poll=umount --first-only --mountpoint /mnt/foo>"
msgstr "B<findmnt --poll=umount --first-only --mountpoint /mnt/foo>"

#. type: Plain text
#: debian-bookworm
msgid "Waits for I</mnt/foo> unmount."
msgstr "wartet auf das Aushängen von I</mnt/foo>."

#. type: Plain text
#: debian-bookworm
msgid "B<findmnt --poll=remount -t ext3 -O ro>"
msgstr "B<findmnt --poll=remount -t ext3 -O ro>"

#. type: Plain text
#: debian-bookworm
msgid "Monitors remounts to read-only mode on all ext3 filesystems."
msgstr ""
"überwacht erneutes Einhängen im schreibgeschützten Modus aller Ext3-"
"Dateisysteme."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm
msgid "B<fstab>(5), B<mount>(8)"
msgstr "B<fstab>(5), B<mount>(8)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<findmnt> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<findmnt> ist Teil des Pakets util-linux, welches "
"heruntergeladen werden kann von:"
