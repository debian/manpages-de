# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2018, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-12-22 07:43+0100\n"
"PO-Revision-Date: 2024-11-29 19:47+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-IMPORTD\\&.SERVICE"
msgstr "SYSTEMD-IMPORTD\\&.SERVICE"

#. type: TH
#: archlinux
#, no-wrap
msgid "systemd 257"
msgstr "systemd 257"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "systemd-importd.service"
msgstr "systemd-importd.service"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-importd.service, systemd-importd - VM and container image import and "
"export service"
msgstr ""
"systemd-importd.service, systemd-importd - VM- und Container-Abbild-Import "
"und -Exportdienst"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-importd\\&.service"
msgstr "systemd-importd\\&.service"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/usr/lib/systemd/systemd-importd"
msgstr "/usr/lib/systemd/systemd-importd"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<systemd-importd> is a system service that allows importing, exporting and "
"downloading of disk images\\&. It provides the implementation for "
"B<importctl>(1)\\*(Aqs B<pull-raw>, B<pull-tar>, B<import-raw>, B<import-"
"tar>, B<import-fs>, B<export-raw>, and B<export-tar> commands\\&."
msgstr ""
"B<systemd-importd> ist ein Systemdienst, der den Import, Export und das "
"Herunterladen von Plattenabbildern\\&. Es stellt die Implementierung für die "
"Befehle B<pull-raw>, B<pull-tar>, B<import-raw>, B<import-tar>, B<import-"
"fs>, B<export-raw> und B<export-tar> von B<importctl>(1) bereit\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"See B<org.freedesktop.import1>(5)  and B<org.freedesktop.LogControl1>(5)  "
"for a description of the D-Bus API\\&."
msgstr ""
"Siehe B<org.freedesktop.import1>(5) und B<org.freedesktop.LogControl1>(5) "
"für eine Beschreibung des D-Bus APIs\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<importctl>(1), B<systemd-machined.service>(8), B<systemd-"
"nspawn>(1), B<org.freedesktop.import1>(5)"
msgstr ""
"B<systemd>(1), B<importctl>(1), B<systemd-machined.service>(8), B<systemd-"
"nspawn>(1), B<org.freedesktop.import1>(5)"

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-importd"
msgstr "/lib/systemd/systemd-importd"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0
msgid ""
"B<systemd-importd> is a system service that allows importing, exporting and "
"downloading of system images suitable for running as VM or containers\\&. It "
"is a companion service for B<systemd-machined.service>(8), and provides the "
"implementation for B<machinectl>(1)\\*(Aqs B<pull-raw>, B<pull-tar>, "
"B<import-raw>, B<import-tar>, B<import-fs>, B<export-raw>, and B<export-tar> "
"commands\\&."
msgstr ""
"B<systemd-importd> ist ein Systemdienst, der den Import, Export und das "
"Herunterladen von Systemabbildern ermöglicht, die zum Betrieb als VM oder "
"Container geeignet sind\\&. Es ist ein Begleitdienst für B<systemd-"
"machined.service>(8) und stellt die Implementierung für die Befehle B<pull-"
"raw>, B<pull-tar>, B<import-raw>, B<import-tar>, B<import-fs>, B<export-raw> "
"und B<export-tar> von B<machinectl>(1) bereit\\&."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0
msgid ""
"B<systemd>(1), B<machinectl>(1), B<systemd-machined.service>(8), B<systemd-"
"nspawn>(1)"
msgstr ""
"B<systemd>(1), B<machinectl>(1), B<systemd-machined.service>(8), B<systemd-"
"nspawn>(1)"

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 257.1"
msgstr "systemd 257.1"

#. type: TH
#: fedora-41
#, no-wrap
msgid "systemd 256.7"
msgstr "systemd 256.7"

#. type: Plain text
#: fedora-41
msgid ""
"B<systemd>(1), B<importctl>(1), B<systemd-machined.service>(8), B<systemd-"
"nspawn>(1)"
msgstr ""
"B<systemd>(1), B<importctl>(1), B<systemd-machined.service>(8), B<systemd-"
"nspawn>(1)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 256.8"
msgstr "systemd 256.8"
