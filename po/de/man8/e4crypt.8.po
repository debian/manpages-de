# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-12-06 17:57+0100\n"
"PO-Revision-Date: 2023-05-14 05:27+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "E4CRYPT"
msgstr "E4CRYPT"

#. type: TH
#: archlinux debian-bookworm fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "May 2024"
msgstr "Mai 2024"

#. type: TH
#: archlinux fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "E2fsprogs version 1.47.1"
msgstr "E2fsprogs Version 1.47.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "e4crypt - ext4 file system encryption utility"
msgstr "e4crypt - Ext4-Dateisystem-Verschlüsselungshilfswerkzeug"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<e4crypt add_key -S >[B< -k >I<keyring> ] [B<-v>] [B<-q>] [B< -p >I<pad> ] "
"[ I<path> ... ]"
msgstr ""
"B<e4crypt add_key -S >[B< -k >I<Schlüsselbund> ] [B<-v>] [B<-q>] [B< -p "
">I<Block> ] [ I<Pfad> … ]"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<e4crypt new_session>"
msgstr "B<e4crypt new_session>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<e4crypt get_policy >I<path> ..."
msgstr "B<e4crypt get_policy >I<Pfad> …"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<e4crypt set_policy >[B< -p >I<pad> ] I<policy path> ..."
msgstr "B<e4crypt set_policy >[B< -p >I<Block> ] I<Richtlinie Pfad> …"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<e4crypt> performs encryption management for ext4 file systems."
msgstr ""
"B<e4crypt> führt die Verschlüsselungsverwaltung für Ext4-Dateisysteme durch."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMMANDS"
msgstr "BEFEHLE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<e4crypt add_key >[B<-vq>] [B<-S>I< salt> ] [B<-k >I<keyring> ] [B< -p >I<pad> ] [ I<path> ... ]"
msgstr "B<e4crypt add_key >[B<-vq>] [B<-S>I< Salz> ] [B<-k >I<Schlüsselbund> ] [B< -p >I<Block> ] [ I<Pfad> … ]"

# FIXME e4crypt → B<e4crypt>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Prompts the user for a passphrase and inserts it into the specified "
"keyring.  If no keyring is specified, e4crypt will use the session keyring "
"if it exists or the user session keyring if it does not."
msgstr ""
"Bittet den Benutzer um eine Passphrase und fügt diese in den angegebenen "
"Schlüsselbund ein. Falls kein Schlüsselbund angegeben ist, verwendet "
"B<e4crypt> den Sitzungsschlüsselbund, falls er existiert, oder andernfalls "
"den Benutzersitzungsschlüsselbund."

# FIXME its prefix value → its prefix value is (or how its prefixed)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<salt> argument is interpreted in a number of different ways, depending "
"on how its prefix value.  If the first two characters are \"s:\", then the "
"rest of the argument will be used as an text string and used as the salt "
"value.  If the first two characters are \"0x\", then the rest of the "
"argument will be parsed as a hex string as used as the salt.  If the first "
"characters are \"f:\" then the rest of the argument will be interpreted as a "
"filename from which the salt value will be read.  If the string begins with "
"a '/' character, it will similarly be treated as filename.  Finally, if the "
"I<salt> argument can be parsed as a valid UUID, then the UUID value will be "
"used as a salt value."
msgstr ""
"Das Argument I<Salz> wird auf verschiedene Arten interpretiert, abhängig, "
"was vor dem Wert steht. Falls die ersten zwei Zeichen »s:« sind, dann wird "
"der Rest des Arguments als Textzeichenkette und als Salzwert verwandt. Falls "
"die ersten zwei Zeichen »0x« sind, dann wird der Rest des Arguments als "
"hexadezimale Zeichenkette ausgewertet und als Salzwert verwandt. Falls die "
"ersten zwei Zeichen »f:« sind, dann wird der Rest des Arguments als "
"Dateiname interpretiert, aus dem der Salzwert gelesen wird. Falls die "
"Zeichenkette mit einem »/« beginnt, wird sie ähnlich als Dateiname "
"behandelt. Falls schließlich das Argument I<Salz> als gültige UUID "
"ausgewertet werden kann, dann wird der UUID-Wert als Salzwert verwandt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<keyring> argument specifies the keyring to which the key should be "
"added."
msgstr ""
"Das Argument I<Schlüsselbund> legt den Schlüsselbund fest, zu dem der "
"Schlüssel hinzugefügt werden soll."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<pad> value specifies the number of bytes of padding will be added to "
"directory names for obfuscation purposes.  Valid I<pad> values are 4, 8, 16, "
"and 32."
msgstr ""
"Das Argument I<Block> legt die Anzahl an Byte fest, mit dem Verzeichnisnamen "
"zum Zwecke der Verschleierung aufgefüllt werden sollen. Gültige Werte für "
"I<Block> sind 4, 8, 16 und 32."

# FIXME e4crypt → B<e4crypt>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If one or more directory paths are specified, e4crypt will try to set the "
"policy of those directories to use the key just added by the B<add_key> "
"command.  If a salt was explicitly specified, then it will be used to derive "
"the encryption key of those directories.  Otherwise a directory-specific "
"default salt will be used."
msgstr ""
"Falls ein oder mehrere Verzeichnisnamen angegeben sind, dann wird B<e4crypt> "
"versuchen, die Richtlinie für diese Verzeichnisse zu setzen, den gerade "
"durch den Befehl B<add_key> hinzugefügten Schlüssel zu benutzen. Falls ein "
"Salz explizit angegeben wurde, dann wird dieser dazu verwandt, den "
"Verschlüsselungsschlüssel dieser Verzeichnisse abzuleiten. Andernfalls wird "
"ein verzeichnisspezifisches Standardsalz verwandt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print the policy for the directories specified on the command line."
msgstr ""
"Gibt die Richtlinie für die auf der Befehlszeile angegebenen Verzeichnisse "
"aus."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Give the invoking process (typically a shell) a new session keyring, "
"discarding its old session keyring."
msgstr ""
"Gibt dem aufrufenden Prozess (normalerweise einer Shell) einen neuen "
"Sitzungsschlüsselbund. Der alte Sitzungsschlüsselbund wird verworfen."

# FIXME e4crypt → B<e4crypt>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the policy for the directories specified on the command line.  All "
"directories must be empty to set the policy; if the directory already has a "
"policy established, e4crypt will validate that the policy matches what was "
"specified.  A policy is an encryption key identifier consisting of 16 "
"hexadecimal characters."
msgstr ""
"Setzt die Richtlinie für die auf der Befehlszeile angegebenen Verzeichnisse. "
"Alle Verzeichnisse müssen leer sein, damit die Richtlinie gesetzt werden "
"kann; falls für das Verzeichnis bereits eine Richtlinie etabliert wurde, "
"wird B<e4crypt> überprüfen, ob diese Richtlinie mit der angegebenen "
"übereinstimmt. Eine Richtline ist ein Verschlüsselungsschlüsselbezeichner, "
"der aus 16 hexadezimalen Zeichen besteht."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Written by Michael Halcrow E<lt>mhalcrow@google.comE<gt>, Ildar Muslukhov "
"E<lt>muslukhovi@gmail.comE<gt>, and Theodore Ts'o E<lt>tytso@mit.eduE<gt>"
msgstr ""
"Geschrieben von Michael Halcrow E<lt>mhalcrow@google.comE<gt>, Ildar "
"Muslukhov E<lt>muslukhovi@gmail.comE<gt> und Theodore Ts'o "
"E<lt>tytso@mit.eduE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<keyctl>(1), B<mke2fs>(8), B<mount>(8)."
msgstr "B<keyctl>(1), B<mke2fs>(8), B<mount>(8)."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "E2fsprogs version 1.47.1-rc2"
msgstr "E2fsprogs Version 1.47.1-rc2"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "November 2024"
msgstr "November 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "E2fsprogs version 1.47.2-rc1"
msgstr "E2fsprogs Version 1.47.2-rc1"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "Februar 2023"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr "E2fsprogs Version 1.47.0"
