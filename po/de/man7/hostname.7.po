# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:01+0100\n"
"PO-Revision-Date: 2024-02-16 18:41+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "hostname"
msgstr "Rechnername"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "hostname - hostname resolution description"
msgstr "hostname - Beschreibung der Auflösung von Rechnernamen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Hostnames are domains, where a domain is a hierarchical, dot-separated list "
"of subdomains; for example, the machine \"monet\", in the \"example\" "
"subdomain of the \"com\" domain would be represented as "
"\"monet.example.com\"."
msgstr ""
"Rechnernamen sind Domains, wobei eine Domain eine hierarchische, durch "
"Punkte getrennte Liste von Subdomains ist; beispielsweise würde die Maschine "
"»monet«, die in der Subdomain »example« der Domain »com« ist, durch "
"»monet.example.com« dargestellt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each element of the hostname must be from 1 to 63 characters long and the "
"entire hostname, including the dots, can be at most 253 characters long.  "
"Valid characters for hostnames are B<ASCII>(7)  letters from I<a> to I<z>, "
"the digits from I<0> to I<9>, and the hyphen (-).  A hostname may not start "
"with a hyphen."
msgstr ""
"Jedes Element des Rechnernamens muss zwischen 1 und 63 Zeichen lang sein und "
"der gesamte Rechnername, einschließlich Punkten, darf höchstens 253 Zeichen "
"lang sein. Gültige Zeichen sind aus B<ASCII>(7) die Zeichen I<a> bis I<z> "
"und die Ziffern I<0> bis I<9> und der Bindestrich (»-«). Ein Rechnername "
"darf nicht mit einem Bindestrich beginnen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Hostnames are often used with network client and server programs, which must "
"generally translate the name to an address for use.  (This task is generally "
"performed by either B<getaddrinfo>(3)  or the obsolete B<gethostbyname>(3).)"
msgstr ""
"Rechnernamen werden oft mit Client-Server-Programmen im Netzwerk verwandt, "
"die im Allgemeinen für die Verwendung den Namen in eine Adresse übersetzen "
"müssen. (Diese Aufgabe wird in der Regel durch entweder B<getaddrinfo>(3) "
"oder das veraltete B<gethostbyname>(3) durchgeführt.)"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Hostnames are resolved by the NSS framework in glibc according to the "
"B<hosts> configuration in B<nsswitch.conf>(5).  The DNS-based name resolver "
"(in the B<dns> NSS service module) resolves them in the following fashion."
msgstr ""
"Rechnernamen werden durch das NSS-Rahmenwerk in der Glibc entsprechend der "
"Konfiguration B<hosts> in B<nsswitch.conf>(5) aufgelöst. Der DNS-basierte "
"Namensauflöser (in dem NSS-Dienstemodul B<dns>) löst sie auf die folgende "
"Art auf:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the name consists of a single component, that is, contains no dot, and if "
"the environment variable B<HOSTALIASES> is set to the name of a file, that "
"file is searched for any string matching the input hostname.  The file "
"should consist of lines made up of two white-space separated strings, the "
"first of which is the hostname alias, and the second of which is the "
"complete hostname to be substituted for that alias.  If a case-insensitive "
"match is found between the hostname to be resolved and the first field of a "
"line in the file, the substituted name is looked up with no further "
"processing."
msgstr ""
"Falls der Name aus einer einzelnen Komponente besteht, d.h. er keinen Punkt "
"enthält, und falls die Umgebungsvariable B<HOSTALIASES> auf den Namen einer "
"Datei gesetzt ist, dann wird diese Datei auf alle Zeichenketten durchsucht, "
"die auf den Eingaberechnernamen passen. Die Datei sollte aus Zeilen "
"bestehen, die jeweils aus zwei durch Leerraum getrennten Zeichenketten "
"bestehen; die erste davon ist der Rechnernamenalias und die zweite davon ist "
"der vollständige Rechnername, der für diesen Alias ersetzt werden soll. "
"Falls eine Übereinstimmung (ohne Berücksichtigung der Groß-/Kleinschreibung) "
"zwischen dem aufzulösenden Rechnernamen und dem ersten Feld einer Zeile in "
"dieser Datei gefunden wird, dann wird der ersetzte Name ohne weitere "
"Verarbeitung nachgeschlagen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the input name ends with a trailing dot, the trailing dot is removed, and "
"the remaining name is looked up with no further processing."
msgstr ""
"Falls der Eingabename mit einem abschließenden Punkt endet, wird der "
"abschließende Punkte entfernt und der verbleibende Name wird ohne weitere "
"Verarbeitung nachgeschlagen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the input name does not end with a trailing dot, it is looked up by "
"searching through a list of domains until a match is found.  The default "
"search list includes first the local domain, then its parent domains with at "
"least 2 name components (longest first).  For example, in the domain "
"cs.example.com, the name lithium.cchem will be checked first as "
"lithium.cchem.cs.example and then as lithium.cchem.example.com.  "
"lithium.cchem.com will not be tried, as there is only one component "
"remaining from the local domain.  The search path can be changed from the "
"default by a system-wide configuration file (see B<resolver>(5))."
msgstr ""
"Falls der Eingabename nicht auf einen abschließenden Punkt endet, wird er "
"nachgeschlagen, indem durch eine Liste von Domains gesucht wird, bis ein "
"Treffer gefunden wird. Die Standardsuchliste enthält zuerst die lokale "
"Domain, dann ihre übergeordnete Domain mit mindestens zwei Namenskomponenten "
"(längste zuerst). Beispielsweise wird in der Domain cs.example.com der Name "
"lithium.cchem zuerst nach lithium.cchem.cs.example und dann nach "
"lithium.cchem.example.com geprüft. lithium.cchem.com wird nicht versucht, da "
"nur eine verbleibende Komponente von der lokalen Domain übrig bleibt. Der "
"Suchpfad kann von der Vorgabe durch eine systemweite Konfigurationsdatei "
"geändert werden (siehe B<resolver>(5))."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getaddrinfo>(3), B<gethostbyname>(3), B<nsswitch.conf>(5), B<resolver>(5), "
"B<mailaddr>(7), B<named>(8)"
msgstr ""
"B<getaddrinfo>(3), B<gethostbyname>(3), B<nsswitch.conf>(5), B<resolver>(5), "
"B<mailaddr>(7), B<named>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "E<.UR http://www.ietf.org\\:/rfc\\:/rfc1123.txt> IETF RFC\\ 1123 E<.UE>"
msgstr ""
"E<.UR http://www.ietf.org\\:/rfc\\:/rfc1123.txt> IETF RFC\\ 1123 E<.UE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "E<.UR http://www.ietf.org\\:/rfc\\:/rfc1178.txt> IETF RFC\\ 1178 E<.UE>"
msgstr ""
"E<.UR http://www.ietf.org\\:/rfc\\:/rfc1178.txt> IETF RFC\\ 1178 E<.UE>"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30. Oktober 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

# FIXME B<nsswitch.conf> → I<nsswitch.conf>
#. type: Plain text
#: debian-bookworm
msgid ""
"Hostnames are resolved by the NSS framework in glibc according to the "
"B<hosts> configuration in B<nsswitch.conf>.  The DNS-based name resolver (in "
"the B<dns> NSS service module) resolves them in the following fashion."
msgstr ""
"Rechnernamen werden durch das NSS-Rahmenwerk in der Glibc entsprechend der "
"Konfiguration B<hosts> in I<nsswitch.conf> aufgelöst. Der DNS-basierte "
"Namensauflöser (in dem NSS-Dienstemodul B<dns>) löst sie auf die folgende "
"Art auf:"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-11-11"
msgstr "11. November 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
