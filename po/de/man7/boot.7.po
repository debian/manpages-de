# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022,2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-12-06 17:54+0100\n"
"PO-Revision-Date: 2023-09-01 16:24+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "boot"
msgstr "boot"

#. type: TH
#: archlinux debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "boot - System bootup process based on UNIX System V Release 4"
msgstr "boot - Systemhochfahrprozess, basierend auf UNIX System V Release 4"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The B<bootup process> (or \"B<boot sequence>\") varies in details among "
"systems, but can be roughly divided into phases controlled by the following "
"components:"
msgstr ""
"Der B<Systemstartprozess> (oder die »B<Hochfahrsequenz>«) unterscheidet sich "
"im Detail zwischen den Systemen, kann aber grob in Phasen eingeteilt werden, "
"die von folgenden Komponenten gesteuert werden:"

#. type: IP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "(1)"
msgstr "(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "hardware"
msgstr "Hardware"

#. type: IP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "(2)"
msgstr "(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "operating system (OS) loader"
msgstr "Betriebssystem- (OS-)Ladeprogramm"

#. type: IP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "(3)"
msgstr "(3)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "kernel"
msgstr "Kernel"

#. type: IP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "(4)"
msgstr "(4)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "root user-space process (I<init> and I<inittab>)"
msgstr "Wurzel-Anwendungsraum-Prozess (I<init> und I<inittab>)"

#. type: IP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "(5)"
msgstr "(5)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "boot scripts"
msgstr "Systemstartskripte"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Each of these is described below in more detail."
msgstr "Jeder dieser wird nachfolgend in größerem Detail beschrieben."

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Hardware"
msgstr "Hardware"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"After power-on or hard reset, control is given to a program stored in read-"
"only memory (normally PROM); for historical reasons involving the personal "
"computer, this program is often called \"the B<BIOS>\"."
msgstr ""
"Nach dem Einschalten oder einem harten Zurücksetzen wird die Steuerung an "
"ein Programm übergeben, das in schreibgeschütztem Speicher (normalerweise "
"PROM) liegt; aus historischen Gründen, die den PC betreffen, heißt dieses "
"Programm oft »das B<BIOS>«."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This program normally performs a basic self-test of the machine and accesses "
"nonvolatile memory to read further parameters.  This memory in the PC is "
"battery-backed CMOS memory, so most people refer to it as \"the B<CMOS>\"; "
"outside of the PC world, it is usually called \"the B<NVRAM>\" (nonvolatile "
"RAM)."
msgstr ""
"Dieses Programm führt normalerweise eine Reihe von Selbsttests der Maschine "
"durch und greift auf nichtflüchtigen Speicher zu, um weitere Parameter zu "
"lesen. Dieser Speicher im PC wird durch Batterien gesichert, daher verweisen "
"die meisten Leute in der PC-Welt auf »das B<CMOS>«, ansonsten wird es "
"normalerweise »das B<NVRAM>« (nichtflüchtiger RAM) genannt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The parameters stored in the NVRAM vary among systems, but as a minimum, "
"they should specify which device can supply an OS loader, or at least which "
"devices may be probed for one; such a device is known as \"the B<boot "
"device>\".  The hardware boot stage loads the OS loader from a fixed "
"position on the boot device, and then transfers control to it."
msgstr ""
"Die im NVRAM gespeicherten Parameter unterscheiden sich zwischen Systemen, "
"aber minimal sollten sie festlegen, welches Gerät das "
"Betriebssystemladeprogramm bereitstellen kann oder mindestens auf welchen "
"Geräten danach gesucht werden kann; ein solches Gerät ist als »das "
"B<Systemstartgerät>« bekannt. Die Hardware-Systemstartstufe lädt das "
"Betriebssystemladeprogramm von einer festen Position auf dem Systemladegerät "
"und übergibt ihm dann die Steuerung."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Note:"
msgstr "Hinweis:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The device from which the OS loader is read may be attached via a network, "
"in which case the details of booting are further specified by protocols such "
"as DHCP, TFTP, PXE, Etherboot, etc."
msgstr ""
"Das Gerät, von dem das Betriebssystemladeprogramm gelesen wird, kann über "
"ein Netzwerk angehängt sein. In diesem Fall werden die Systemladedetails "
"weiter in Protokollen wie DHCP, TFTP, PXE, Etherboot usw. festgelegt."

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "OS loader"
msgstr "Betriebssystemladeprogramm"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The main job of the OS loader is to locate the kernel on some device, load "
"it, and run it.  Most OS loaders allow interactive use, in order to enable "
"specification of an alternative kernel (maybe a backup in case the one last "
"compiled isn't functioning) and to pass optional parameters to the kernel."
msgstr ""
"Die Hauptaufgabe des Betriebssystemladeprogramms ist es, den Kernel auf "
"einem Gerät zu finden, ihn zu laden und auszuführen. Die meisten "
"Betriebssystemladeprogramme erlauben eine interaktive Verwendung, um die "
"Festlegung eines alternativen Kernels zu ermöglichen (vielleicht ein "
"Rückfallkernel, falls der letzte kompilierte nicht funktioniert) und "
"optionale Parameter an den Kernel zu übergeben."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"In a traditional PC, the OS loader is located in the initial 512-byte block "
"of the boot device; this block is known as \"the B<MBR>\" (Master Boot "
"Record)."
msgstr ""
"Auf einem traditionellen PC befindet sich das Betriebssystemladeprogramm in "
"dem ersten 512-byte-Block des Systemstartgeräts; dieser Block ist als »der "
"B<MBR>« (Master Boot Record) bekannt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"In most systems, the OS loader is very limited due to various constraints.  "
"Even on non-PC systems, there are some limitations on the size and "
"complexity of this loader, but the size limitation of the PC MBR (512 bytes, "
"including the partition table) makes it almost impossible to squeeze much "
"functionality into it."
msgstr ""
"Auf den meisten Systemen ist das Betriebssystemladeprogramm aufgrund "
"verschiedener Beschränkungen sehr eingeschränkt. Selbst auf Nicht-PC-"
"Systemen gibt es einige Beschränkungen der Größe und der Komplexität dieses "
"Ladeprogramms, aber die Größenbeschränkung des PC MBRs (512 bytes, "
"einschließlich der Partitionstabelle) macht es fast unmöglich, viel "
"Funktionalität dort hinein zu quetschen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Therefore, most systems split the role of loading the OS between a primary "
"OS loader and a secondary OS loader; this secondary OS loader may be located "
"within a larger portion of persistent storage, such as a disk partition."
msgstr ""
"Daher teilen die meisten Systeme die Rolle des Betriebssystemladens in ein "
"primäres Betriebssystemladeprogramm und ein sekundäres "
"Betriebssystemladeprogramm; dieses sekundäre Betriebssystemladeprogramm kann "
"sich auf einer größeren Portion des dauerhaften Speichers befinden, "
"beispielsweise einer Plattenpartition."

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"In Linux, the OS loader is often B<grub>(8)  (an alternative is B<lilo>(8))."
msgstr ""
"Unter Linux ist das Betriebssystemladeprogramm oft B<grub>(8) (B<lilo>(8) "
"ist eine Alternative)."

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Kernel"
msgstr "Kernel"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When the kernel is loaded, it initializes various components of the computer "
"and operating system; each portion of software responsible for such a task "
"is usually consider \"a B<driver>\" for the applicable component.  The "
"kernel starts the virtual memory swapper (it is a kernel process, called "
"\"kswapd\" in a modern Linux kernel), and mounts some filesystem at the root "
"path, I</>."
msgstr ""
"Wenn der Kernel geladen wird, initialisiert er verschiedene Komponenten des "
"Computers und Betriebssystems; jeder für eine solche Aufgabe zuständige "
"Softwareteil wird normalerweise als »ein B<Treiber>« für die zugehörige "
"Komponente betrachtet. Der Kernel startet den Auslagerungsprozess für "
"virtuellen Speicher (das ist ein Kernelprozess, der bei einem modernen Linux-"
"Kernel »kswapd« genannt wird) und hängt einige Dateisysteme am Wurzelpfad I</"
"> ein."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Some of the parameters that may be passed to the kernel relate to these "
"activities (for example, the default root filesystem can be overridden); for "
"further information on Linux kernel parameters, read B<bootparam>(7)."
msgstr ""
"Einige an den Kernel übergebbare Parameter beziehen sich auf diese "
"Aktivitäten (beispielsweise kann das Wurzeldateisystem außer Kraft gesetzt "
"werden); für weitere Informationen über Linux-Kernelparameter lesen Sie "
"B<bootparam>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Only then does the kernel create the initial userland process, which is "
"given the number 1 as its B<PID> (process ID).  Traditionally, this process "
"executes the program I</sbin/init>, to which are passed the parameters that "
"haven't already been handled by the kernel."
msgstr ""
"Erst dann erstellt der Kernel den anfänglichen Prozess im Anwendungsraum, "
"dem die Nummer 1 als seine B<PID> (Prozesskennung) gegeben wird. "
"Traditionell führt dieser Prozess das Programm I</sbin/init> aus, an den die "
"Parameter übergeben werden, die noch nicht vom Kernel berücksichtigt wurden."

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Root user-space process"
msgstr "Wurzelprozess des Anwendungsraums"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The following description applies to an OS based on UNIX System V Release "
"4.  However, a number of widely used systems have adopted a related but "
"fundamentally different approach known as B<systemd>(1), for which the "
"bootup process is detailed in its associated B<bootup>(7)."
msgstr ""
"Die folgende Beschreibung gilt für ein auf UNIX System V Release 4 "
"basierendes Betriebssystem. Eine Reihe von breit genutzten Systemen hat "
"einen verwandten, aber fundamental verschiedenen Ansatz eingeführt, der als "
"B<systemd>(1) bekannt ist und dessen Systemstartprozess im Detail in seiner "
"zugehörigen B<bootup>(7) beschrieben ist."

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When I</sbin/init> starts, it reads I</etc/inittab> for further "
"instructions.  This file defines what should be run when the I</sbin/init> "
"program is instructed to enter a particular run level, giving the "
"administrator an easy way to establish an environment for some usage; each "
"run level is associated with a set of services (for example, run level B<S> "
"is single-user mode, and run level B<2> entails running most network "
"services)."
msgstr ""
"Wenn I</sbin/init> startet, liest es I</etc/inittab> für weitere "
"Anweisungen. Diese Datei definiert, was ausgeführt werden soll, wenn das "
"Programm I</sbin/init> angewiesen wird, in einen bestimmten Runlevel "
"einzutreten. Damit hat der Administrator eine einfache Möglichkeit, eine "
"Umgebung für einen bestimmten Anwendungsfall einzurichten; jedem Runlevel "
"wird ein Satz an bestimmten Diensten zugeordnet. (Beispielsweise ist "
"Runlevel B<S> der Einzelbenutzer-Modus und Runlevel B<2> bedeutet die "
"Ausführung der meisten Netzwerkdienste.)"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The administrator may change the current run level via B<init>(1), and query "
"the current run level via B<runlevel>(8)."
msgstr ""
"Der Administrator kann den aktuellen Runlevel mit B<init>(1) ändern und den "
"aktuellen Runlevel mittels B<runlevel>(8) abfragen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"However, since it is not convenient to manage individual services by editing "
"this file, I</etc/inittab> only bootstraps a set of scripts that actually "
"start/stop the individual services."
msgstr ""
"Da es allerdings nicht praktisch ist, individuelle Dienste durch Bearbeitung "
"dieser Datei zu verwalten, startet I</etc/inittab> eine Reihe von Skripten, "
"die dann tatsächlich die einzelnen Dienste starten bzw. stoppen."

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Boot scripts"
msgstr "Systemstartskripte"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The following description applies to an OS based on UNIX System V Release "
"4.  However, a number of widely used systems (Slackware Linux, FreeBSD, "
"OpenBSD)  have a somewhat different scheme for boot scripts."
msgstr ""
"Die folgende Beschreibung gilt für ein auf UNIX System V Release 4 "
"basierendes Betriebssystem. Eine Reihe viel genutzter Systeme (Slackware "
"Linux, FreeBSD, OpenBSD) haben allerdings ein leicht anderes Schema für "
"Systemstartskripte."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"For each managed service (mail, nfs server, cron, etc.), there is a single "
"startup script located in a specific directory (I</etc/init.d> in most "
"versions of Linux).  Each of these scripts accepts as a single argument the "
"word \"start\" (causing it to start the service) or the word \\&\"stop\" "
"(causing it to stop the service).  The script may optionally accept other "
"\"convenience\" parameters (e.g., \"restart\" to stop and then start, "
"\"status\" to display the service status, etc.).  Running the script without "
"parameters displays the possible arguments."
msgstr ""
"Für jeden verwalteten Dienst (E-Mail, NFS-Server, Cron usw.) gibt es ein "
"einzelnes Startskript, das sich in einem bestimmten Verzeichnis (I</etc/"
"init.d> in den meisten Linux-Versionen) befindet. Jedes dieser Skripte "
"akzeptiert als einziges Argument »start« (wodurch der Dienst gestartet wird) "
"oder »stop« (wodurch der Dienst gestoppt wird). Das Skript kann optional "
"weitere »Bequemlichkeitsparameter« akzeptieren (z.B. »restart«, um den "
"Dienst zu stoppen und dann zu starten, »status«, um den Dienstestatus "
"anzuzeigen usw.). Wird das Skript ohne Parameter ausgeführt, dann werden die "
"möglichen Argumente angezeigt."

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Sequencing directories"
msgstr "Ablaufverzeichnisse"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"To make specific scripts start/stop at specific run levels and in a specific "
"order, there are I<sequencing directories>, normally of the form I</etc/"
"rc[0-6S].d>.  In each of these directories, there are links (usually "
"symbolic) to the scripts in the I</etc/init.d> directory."
msgstr ""
"Damit bestimmte Skripte in bestimmten Runleveln in einer bestimmten "
"Reihenfolge starten/stoppen, gibt es I<Ablaufverzeichnisse>, normalerweise "
"der Form I</etc/rc[0-6S].d>. In jedem dieser Verzeichnisse gibt es Links "
"(normalerweise symbolische) auf die Skripte im Verzeichnis I</etc/init.d>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"A primary script (usually I</etc/rc>) is called from B<inittab>(5); this "
"primary script calls each service's script via a link in the relevant "
"sequencing directory.  Each link whose name begins with \\[aq]S\\[aq] is "
"called with the argument \"start\" (thereby starting the service).  Each "
"link whose name begins with \\[aq]K\\[aq] is called with the argument "
"\"stop\" (thereby stopping the service)."
msgstr ""
"Ein primäres Skript (normalerweise I</etc/rc>) wird von B<inittab>(5) "
"aufgerufen; dieses primäre Skript ruft jedes Dienste-Skript über einen "
"Symlink im relevanten Ablaufverzeichnis auf. Jeder Link, dessen Name mit »S« "
"beginnt, wird mit dem Argument »start« aufgerufen (und startet daher den "
"Dienst). Jeder Link, dessen Name mit »K« beginnt, wird mit dem Argument "
"»stop« aufgerufen (und stoppt damit den Dienst)."

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"To define the starting or stopping order within the same run level, the name "
"of a link contains an B<order-number>.  Also, for clarity, the name of a "
"link usually ends with the name of the service to which it refers.  For "
"example, the link I</etc/rc2.d/S80sendmail> starts the B<sendmail>(8)  "
"service on run level 2.  This happens after I</etc/rc2.d/S12syslog> is run "
"but before I</etc/rc2.d/S90xfs> is run."
msgstr ""
"Um innerhalb des gleichen Runlevels die Start- oder Stoppreihenfolge zu "
"definieren, enthält der Link eine B<Ordnungsnummer>. Zur Klarheit endet der "
"Linkname normalerweise mit dem Dienstenamen, auf den er sich bezieht. "
"Beispielsweise startet der Link I</etc/rc2.d/S80sendmail> den Dienst "
"B<sendmail>(8) im Runlevel 2. Dies passiert nach der Ausführung von I</etc/"
"rc2.d/S12syslog> aber vor der Ausführung von I</etc/rc2.d/S90xfs>."

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"To manage these links is to manage the boot order and run levels; under many "
"systems, there are tools to help with this task (e.g., B<chkconfig>(8))."
msgstr ""
"Durch Verwaltung dieser Links werden die Systemstartreihenfolge und Runlevel "
"gesteuert; unter vielen Systemen gibt es Werkzeuge, um bei dieser Aufgabe zu "
"helfen (z.B. B<chkconfig>(8))."

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Boot configuration"
msgstr "Systemstartkonfiguration"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"A program that provides a service is often called a \"B<daemon>\".  Usually, "
"a daemon may receive various command-line options and parameters.  To allow "
"a system administrator to change these inputs without editing an entire boot "
"script, some separate configuration file is used, and is located in a "
"specific directory where an associated boot script may find it (I</etc/"
"sysconfig> on older Red Hat systems)."
msgstr ""
"Ein Programm, das einen Dienst bereitstellt, wird oft »B<Daemon>« genannt. "
"Normalerweise kann ein Daemon viele Befehlszeilenoptionen und -parameter "
"empfangen. Damit ein Systemadministrator die Möglichkeit hat, diese Eingaben "
"zu ändern, ohne gleich ein komplettes Startskript anpassen zu müssen, wird "
"eine getrennte Konfigurationsdatei verwandt. Diese befindet sich in einem "
"bestimmten Verzeichnis, wo die zugehörigen Systemstartskripte sie finden "
"können (I</etc/sysconfig> auf älteren Red-Hat-Systemen)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"In older UNIX systems, such a file contained the actual command line options "
"for a daemon, but in modern Linux systems (and also in HP-UX), it just "
"contains shell variables.  A boot script in I</etc/init.d> reads and "
"includes its configuration file (that is, it \"B<sources>\" its "
"configuration file) and then uses the variable values."
msgstr ""
"Auf älteren UNIX-Systemen enthielt eine solche Datei die tatsächlichen "
"Befehlszeilenoptionen für einen Daemon. Auf modernen Linux-Systemen (und "
"auch bei HP-UX) enthält sie aber nur Shell-Variablen. Ein Systemstartskript "
"in I</etc/init.d> liest diese Konfigurationsdatei und bindet sie ein "
"(englisch »B<sources>«) und verwendet dann die Variablenwerte."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I</etc/init.d/>, I</etc/rc[S0-6].d/>, I</etc/sysconfig/>"
msgstr "I</etc/init.d/>, I</etc/rc[S0-6].d/>, I</etc/sysconfig/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<init>(1), B<systemd>(1), B<inittab>(5), B<bootparam>(7), B<bootup>(7), "
"B<runlevel>(8), B<shutdown>(8)"
msgstr ""
"B<init>(1), B<systemd>(1), B<inittab>(5), B<bootparam>(7), B<bootup>(7), "
"B<runlevel>(8), B<shutdown>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

# FIXME This is true only on x86 and Lilo is probably not much used anymore
#. type: Plain text
#: debian-bookworm
msgid "In Linux, the OS loader is often either B<lilo>(8)  or B<grub>(8)."
msgstr ""
"Unter Linux ist das Betriebssystemladeprogramm oft entweder B<lilo>(8) oder "
"B<grub>(8)."

#. type: Plain text
#: debian-bookworm
msgid ""
"When I</sbin/init> starts, it reads I</etc/inittab> for further "
"instructions.  This file defines what should be run when the I</sbin/init> "
"program is instructed to enter a particular I<run-level>, giving the "
"administrator an easy way to establish an environment for some usage; each "
"run-level is associated with a set of services (for example, run-level B<S> "
"is I<single-user> mode, and run-level B<2> entails running most network "
"services)."
msgstr ""
"Wenn I</sbin/init> startet, liest es I</etc/inittab> für weitere "
"Anweisungen. Diese Datei definiert, was ausgeführt werden soll, wenn das "
"Programm I</sbin/init> angewiesen wird, in einen bestimmten I<Runlevel> "
"einzutreten. Damit hat der Administrator eine einfache Möglichkeit, eine "
"Umgebung für einen bestimmten Anwendungsfall einzurichten; jedem Runlevel "
"wird ein Satz an bestimmten Diensten zugeordnet. (Beispielsweise ist "
"Runlevel B<S> der I<Einzelbenutzer>-Modus und Runlevel B<2> bedeutet die "
"Ausführung der meisten Netzwerkdienste.)"

#. type: Plain text
#: debian-bookworm
msgid ""
"The administrator may change the current run-level via B<init>(1), and query "
"the current run-level via B<runlevel>(8)."
msgstr ""
"Der Administrator kann den aktuellen Runlevel mit B<init>(1) ändern und den "
"aktuellen Runlevel mittels B<runlevel>(8) abfragen."

#. type: Plain text
#: debian-bookworm
msgid ""
"To make specific scripts start/stop at specific run-levels and in a specific "
"order, there are I<sequencing directories>, normally of the form I</etc/"
"rc[0-6S].d>.  In each of these directories, there are links (usually "
"symbolic) to the scripts in the I</etc/init.d> directory."
msgstr ""
"Damit bestimmte Skripte in bestimmten Runleveln in einer bestimmten "
"Reihenfolge starten/stoppen, gibt es I<Ablaufverzeichnisse>, normalerweise "
"der Form I</etc/rc[0-6S].d>. In jedem dieser Verzeichnisse gibt es Links "
"(normalerweise symbolische) auf die Skripte im Verzeichnis I</etc/init.d>."

# FIXME runlevel 2 → run-level 2
# FIXME sendmail → B<sendmail>(8)
#. type: Plain text
#: debian-bookworm
msgid ""
"To define the starting or stopping order within the same run-level, the name "
"of a link contains an B<order-number>.  Also, for clarity, the name of a "
"link usually ends with the name of the service to which it refers.  For "
"example, the link I</etc/rc2.d/S80sendmail> starts the sendmail service on "
"runlevel 2.  This happens after I</etc/rc2.d/S12syslog> is run but before I</"
"etc/rc2.d/S90xfs> is run."
msgstr ""
"Um innerhalb des gleichen Runlevels die Start- oder Stoppreihenfolge zu "
"definieren, enthält der Link eine B<Ordnungsnummer>. Zur Klarheit endet der "
"Linkname normalerweise mit dem Dienstenamen, auf den er sich bezieht. "
"Beispielsweise startet der Link I</etc/rc2.d/S80sendmail> den Dienst "
"B<sendmail>(8) im Runlevel 2. Dies passiert nach der Ausführung von I</etc/"
"rc2.d/S12syslog> aber vor der Ausführung von I</etc/rc2.d/S90xfs>."

#. type: Plain text
#: debian-bookworm
msgid ""
"To manage these links is to manage the boot order and run-levels; under many "
"systems, there are tools to help with this task (e.g., B<chkconfig>(8))."
msgstr ""
"Durch Verwaltung dieser Links werden die Systemstartreihenfolge und Runlevel "
"gesteuert; unter vielen Systemen gibt es Werkzeuge, um bei dieser Aufgabe zu "
"helfen (z.B. B<chkconfig>(8))."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
