# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tobias Burnus <burnus@gmx.de>
# Thomas Hoffmann
# Helge Kreutzmann <debian@helgefjell.de>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: 2024-10-04 20:27+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "PS2PS"
msgstr "PS2PS"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "18 Sept 2024"
msgstr "18. September 2024"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "10.04.0"
msgstr "10.04.0"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Ghostscript Tools"
msgstr "Ghostscript-Werkzeuge"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ps2ps, eps2eps - Ghostscript PostScript \"distiller\""
msgstr "ps2ps, eps2eps - Ghostscript PostScript-»Distiller«"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<ps2ps> [ I<options> ] I<input output.ps>"
msgstr "B<ps2ps> [ I<Optionen> ] I<Eingabe Ausgabe.ps>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<eps2eps> [ I<options> ] I<input output.eps>"
msgstr "B<eps2eps> [ I<Optionen> ] I<Eingabe Ausgabe.eps>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<ps2ps> uses I<gs>(1) to convert B<PostScript>(tm) or B<PDF>(tm) file "
"\"input\" to simpler, normalized and (usually) faster PostScript in "
"\"output.ps\".  The output is level 2 DSC 3.0 conforming PostScript."
msgstr ""
"B<ps2ps> verwendet B<gs>(1), um die B<PostScript>(™) oder B<PDF>(™)-Datei "
"»Eingabe« in einfacheres, normalisiertes und (meist) schnelleres "
"B<PostScript> in »Ausgabe.ps« zu konvertieren. Die Ausgabe ist B<PostScript> "
"gemäß Level 2 DSC 3.0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<eps2eps> performs the equivalent optimization, creating Encapsulated "
"PostScript (EPS) files. NB, despite the name, the input need not be an EPS "
"file, PostScript or indeed PDF files are equally acceptable."
msgstr ""
"B<eps2eps> führt die entsprechende Optimierung durch und erstellt "
"Encapsulated PostScript (EPS)-Dateien. Hinweis: Trotz des Namens muss die "
"Eingabe keine EPS-Datei sein, genauso werden PostScript oder sogar PDF-"
"Dateien akzeptiert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Both accept any general Ghostscript command line options, and options "
"specific to the ps2write and eps2write devices."
msgstr ""
"Beide akzeptieren allgemeine Ghostscript-Befehlszeilenoptionen und Optionen, "
"die für die Geräte ps2write und eps2write spezifisch sind."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Run \"B<gs -h>\" to find the location of Ghostscript documentation on your "
"system, from which you can get more details."
msgstr ""
"Führen Sie »B<gs -h>« aus, um zu sehen, wo die Ghostscript-Dokumentation auf "
"Ihrem System installiert ist, um dort weitere Details zu erfahren."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<ps2pdf>(1), B<ps2ascii>(1), B<ps2epsi>(1)"
msgstr "B<ps2pdf>(1), B<ps2ascii>(1), B<ps2epsi>(1)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSION"
msgstr "VERSION"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "This document was last revised for Ghostscript version 10.04.0."
msgstr ""
"Dieses Dokument wurde letztmalig für Ghostscript Version 10.04.0 "
"überarbeitet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Artifex Software, Inc. are the primary maintainers of Ghostscript."
msgstr "Artifex Software, Inc. sind die Hauptautoren von Ghostscript."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "21 September 2022"
msgstr "21. September 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10.00.0"
msgstr "10.00.0"

# FIXME ps2pdf(1), ps2ascii(1), ps2epsi(1) → B<ps2pdf>(1), B<ps2ascii>(1), B<ps2epsi>(1)
#. type: Plain text
#: debian-bookworm fedora-41
msgid "ps2pdf(1), ps2ascii(1), ps2epsi(1)"
msgstr "B<ps2pdf>(1), B<ps2ascii>(1), B<ps2epsi>(1)"

#. type: Plain text
#: debian-bookworm
msgid "This document was last revised for Ghostscript version 10.00.0."
msgstr ""
"Dieses Dokument wurde letztmalig für Ghostscript Version 10.00.0 "
"überarbeitet."

#. type: TH
#: fedora-41
#, no-wrap
msgid "06 May 2024"
msgstr "6. Mai 2024"

#. type: TH
#: fedora-41
#, no-wrap
msgid "10.03.1"
msgstr "10.03.1"

#. type: Plain text
#: fedora-41
msgid "This document was last revised for Ghostscript version 10.03.1."
msgstr ""
"Dieses Dokument wurde letztmalig für Ghostscript Version 10.03.1 "
"überarbeitet."
