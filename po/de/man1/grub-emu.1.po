# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2023.
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-08-02 17:18+0200\n"
"PO-Revision-Date: 2023-11-13 21:42+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-EMU"
msgstr "GRUB-EMU"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2024"
msgstr "Februar 2024"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.12-1~bpo12+1"
msgstr "GRUB 2.12-1~bpo12+1"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-emu - GRUB emulator"
msgstr "grub-emu - GRUB-Emulator"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-emu> [I<\\,OPTION\\/>...]"
msgstr "B<grub-emu> [I<\\,OPTION\\/> …]"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "GRUB emulator."
msgstr "GRUB-Emulator."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use GRUB files in the directory DIR [default=/boot/grub]"
msgstr ""
"verwendet GRUB-Dateien im angegebenen VERZEICHNIS (Vorgabe ist /boot/grub)"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-H>, B<--hold>[=I<\\,SECS\\/>]"
msgstr "B<-H>, B<--hold>[=I<\\,SEKUNDEN\\/>]"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "wait until a debugger will attach"
msgstr "wartet auf das Anhängen eines Debuggers."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--memdisk>=I<\\,FILE\\/>"
msgstr "B<--memdisk>=I<\\,DATEI\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as memdisk"
msgstr "verwendet die angegebene DATEI als Arbeitsspeicherplatte."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,DATEI\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr ""
"verwendet die angegebene DATEI als Gerätezuordnung (Vorgabe ist /boot/grub/"
"device.map)."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr "B<-r>, B<--root>=I<\\,GERÄTENAME\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Set root device."
msgstr "legt das Wurzelgerät fest."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "gibt ausführliche Meldungen aus."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-X>, B<--kexec>"
msgstr "B<-X>, B<--kexec>"

# FIXME systemctl → B<systemctl>(1)
# FIXME kexec → B<kexec>(8)
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"use kexec to boot Linux kernels via systemctl (pass twice to enable "
"dangerous fallback to non-systemctl)."
msgstr ""
"Verwendet B<kexec>(8), um Linux-Kernel mittels B<systemctl>(1) zu starten "
"(übergeben Sie es zweimal, um einen gefährlichen Rückfall auf nicht-"
"Systemctl zu aktivieren)."

# FIXME -? → B<-?>
#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Erforderliche oder optionale Argumente für lange Optionen sind ebenso "
"erforderlich bzw. optional für die entsprechenden Kurzoptionen."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<.MT bug-grub@gnu.org> E<.ME .>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If you are trying to install GRUB, then you should use B<grub-install>(8)  "
"rather than this program."
msgstr ""
"Wenn Sie versuchen, GRUB zu installieren, sollten Sie statt dieses Programms "
"besser B<grub-install>(8) verwenden."

# FIXME B<info> -> B<info>(1)
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-emu> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-emu> programs are properly installed at your site, "
"the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-emu> wird als ein Texinfo-Handbuch "
"gepflegt. Wenn die Programme B<info>(1) und B<grub-emu> auf Ihrem Rechner "
"ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<info grub-emu>"
msgstr "B<info grub-emu>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2024"
msgstr "Juli 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-5"
msgstr "GRUB 2.12-5"
