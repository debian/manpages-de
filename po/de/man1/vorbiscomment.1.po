# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2013-2014, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2024-09-06 18:35+0200\n"
"PO-Revision-Date: 2021-04-17 20:23+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "VORBISCOMMENT"
msgstr "VORBISCOMMENT"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "December 30, 2008"
msgstr "30. Dezember 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Xiph.Org Foundation"
msgstr "Xiph.Org Foundation"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Ogg Vorbis Tools"
msgstr "Ogg Vorbis Tools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "vorbiscomment - List or edit comments in Ogg Vorbis files"
msgstr ""
"vorbiscomment - Kommentare in Ogg-Vorbis-Dateien auflisten oder bearbeiten"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<vorbiscomment> B<[-l]> [B<-R>] [B<-e>] I<file.ogg>"
msgstr "B<vorbiscomment> B<[-l]> [B<-R>] [B<-e>] I<Datei.ogg>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<vorbiscomment> B<-a> B<[ -c commentfile | -t ``name=value'' | -d "
"``name=value'' ]> [B<-q>] [B<-R>] [B<-e>] I<in.ogg> I<[out.ogg]>"
msgstr ""
"B<vorbiscomment> B<-a> B<[ -c Kommentardatei | -t ``name=Wert'' | -d "
"``name=Wert'']> [B<-q>] [B<-R>] [B<-e>] I<ein.ogg> I<[aus.ogg]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<vorbiscomment> B<-w> B<[ -c commentfile | -t ``name=value'' ]> [B<-q>] [B<-"
"R>] [B<-e>] I<in.ogg> I<[out.ogg]>"
msgstr ""
"B<vorbiscomment> B<-w> B<[ -c Kommentardatei | -t ``name=Wert'' ]> [B<-q>] "
"[B<-R>] [B<-e>] I<ein.ogg> I<[aus.ogg]>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<vorbiscomment> Reads, modifies, and appends Ogg Vorbis audio file metadata "
"tags."
msgstr ""
"B<vorbiscomment> liest und bearbeitet Metadaten-Tags von Ogg-Vorbis-"
"Audiodateien oder hängt diese an bestehende Kommentare an."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-a, --append"
msgstr "-a, --append"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Updates comments."
msgstr "Aktualisiert Kommentare."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-c file, --commentfile file"
msgstr "-c Datei, --commentfile Datei"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Take comments from a file. The file is the same format as is output by the "
"the -l option or given to the -t option: one element per line in 'tag=value' "
"format. If the file is /dev/null and -w was passed, the existing comments "
"will be removed."
msgstr ""
"verwendet Kommentare in einer Datei. Die Datei hat dabei das gleiche Format "
"wie die Ausgabe mit der Option -l bzw. wie die Eingabe mit der Option -t: "
"Ein Element pro Zeile im Format »tag=Wert«. Falls die Datei /dev/null ist "
"und -w übergeben wird, dann werden vorhandene Kommentare entfernt."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-h, --help"
msgstr "-h, --help"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Show command help."
msgstr "zeigt Hilfe zur Befehlszeile an."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-l, --list"
msgstr "-l, --list"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "List the comments in the Ogg Vorbis file."
msgstr "listet die Kommentare in der Ogg-Vorbis-Datei auf."

#. type: IP
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "-q, --quiet"
msgstr "-q, --quiet"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "Quiet mode.  No messages are displayed."
msgstr "Stiller Modus. Es werden keine Meldungen angezeigt."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-t 'name=value', --tag 'name=value'"
msgstr "-t 'name=Wert', --tag 'name=Wert'"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Specify a new tag on the command line. Each tag is given as a single string. "
"The part before the '=' is treated as the tag name and the part after as the "
"value."
msgstr ""
"übergibt einen neuen Tag in der Befehlszeile. Jeder Tag wird als einzelne "
"Zeichenkette angegeben. Der Teil vor dem »=« wird als der Name des Tags "
"interpretiert und der Teil danach als dessen Wert."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-d 'name[=value]', --rm 'name[=value]'"
msgstr "-d 'name[=Wert]', --rm 'name[=Wert]'"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Specify a tag on the command line for removal. Each tag is given as a single "
"string. The part before the '=' is treated as the tag name and the part "
"after as the value. If no value is given all tags are deleted with the given "
"name. Otherwise only those with matching values are deleted."
msgstr ""
"übergibt einen neuen zu entfernenden Tag in der Befehlszeile. Jeder Tag wird "
"als einzelne Zeichenkette angegeben. Der Teil vor dem »=« wird als der Name "
"des Tags interpretiert und der Teil danach als dessen Wert. Falls kein Wert "
"angegeben ist, werden alle Tags mit dem angegebenen Namen gelöscht. "
"Anderenfalls werden nur die Tags mit den passenden Werten gelöscht."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-w, --write"
msgstr "-w, --write"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Replace comments with the new set given either on the command line with -t "
"or from a file with -c. If neither -c nor -t is given, the new set will be "
"read from the standard input."
msgstr ""
"ersetzt Kommentare mit dem neuen, entweder in der Befehlszeile mit -t oder "
"aus einer Datei mit -c übergebenen Kommentarsatz. Falls weder -c noch -t "
"angegeben werden, wird der neue Satz aus der Standardeingabe gelesen."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-R, --raw"
msgstr "-R, --raw"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Read and write comments in UTF-8, rather than converting to the user's "
"character set."
msgstr ""
"schreibt und liest Kommentare in UTF-8, statt diese in die für den Benutzer "
"festgelegte Zeichenkodierung umzuwandeln."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-e, --escapes"
msgstr "-e, --escapes"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Quote/unquote newlines and backslashes in the comments. This ensures every "
"comment is exactly one line in the output (or input), allowing to filter and "
"round-trip them. Without it, you can only write multi-line comments by using "
"-t and you can't reliably distinguish them from multiple one-line comments."
msgstr ""
"maskiert oder demaskiert Zeilenumbrüche und Backslashes in den Kommentaren. "
"Dies stellt sicher, dass jeder Kommentar exakt einer Zeile in der Ausgabe "
"(oder Eingabe) entspricht, was Filtern und Durchsuchen ermöglicht. Ohne dies "
"können Sie mehrzeilige Kommentare nur mit der Option -t schreiben, wobei "
"diese dann nicht ohne Weiteres von mehreren einzeiligen Kommentaren zu "
"unterscheiden wären."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Supported escapes are c-style \"\\en\", \"\\er\", \"\\e\\e\" and \"\\e0\". A "
"backslash followed by anything else is an error."
msgstr ""
"Die im C-Stil unterstützten Escape-Sequenzen sind »\\en«, »\\er«, »\\e\\e« "
"und »\\e0«. Ein Backslash gefolgt von irgendetwas anderem wird als Fehler "
"aufgefasst."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Note: currently, anything after the first \"\\e0\" is thrown away while "
"writing.  This is a bug -- the Vorbis format can safely store null "
"characters, but most other tools wouldn't handle them anyway."
msgstr ""
"Hinweis: Gegenwärtig wird alles nach dem ersten »\\e0« beim Schreiben "
"verworfen. Das ist ein Fehler, das Vorbis-Format kann Null-Zeichen sicher "
"speichern, aber die meisten anderen Werkzeuge können dies nicht verarbeiten."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "-V, --version"
msgstr "-V, --version"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Display the version of vorbiscomment."
msgstr "zeigt die Version von vorbiscomment an."

#.  Examples go here
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "To just see what comment tags are in a file:"
msgstr "Sehen, welche Kommentartags in einer Datei enthalten sind:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    vorbiscomment -l file.ogg\n"
msgstr "    vorbiscomment -l Datei.ogg\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "To edit those comments:"
msgstr "Bearbeiten dieser Kommentare:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    vorbiscomment -l file.ogg E<gt> file.txt\n"
"    [edit the comments in file.txt to your satisfaction]\n"
"    vorbiscomment -w -c file.txt file.ogg newfile.ogg\n"
msgstr ""
"    vorbiscomment -l Datei.ogg E<gt> Datei.txt\n"
"    [Kommentare in Datei.txt wie gewünscht bearbeiten]\n"
"    vorbiscomment -w -c Datei.txt Datei.ogg Neue_Datei.ogg\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "To simply add a comment:"
msgstr "Hinzufügen eines Kommentars:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    vorbiscomment -a -t 'ARTIST=No One You Know' file.ogg newfile.ogg\n"
msgstr "    vorbiscomment -a -t 'ARTIST=Unbekannt' Datei.ogg Neue_Datei.ogg\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "To add a set of comments from the standard input:"
msgstr "Setzen von Kommentaren aus der Standardeingabe:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    vorbiscomment -a file.ogg\n"
"    ARTIST=No One You Know\n"
"    ALBUM=The Famous Album\n"
"    E<lt>ctrl-dE<gt>\n"
msgstr ""
"    vorbiscomment -a Datei.ogg\n"
"    ARTIST=Unbekannt\n"
"    ALBUM=Hervorragendes Album\n"
"    E<lt>Strg-dE<gt>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "TAG FORMAT"
msgstr "TAG-FORMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"See https://xiph.org/vorbis/doc/v-comment.html for documentation on the Ogg "
"Vorbis tag format, including a suggested list of canonical tag names."
msgstr ""
"Auf https://www.xiph.org/vorbis/doc/v-comment.html finden Sie Informationen "
"über das Tag-Format von Ogg Vorbis und eine Liste mit kanonischen Tag-"
"Bezeichnungen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Program Authors:"
msgstr "Programmautoren:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Michael Smith E<lt>msmith@xiph.orgE<gt>"
msgstr "Michael Smith E<lt>msmith@xiph.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Ralph Giles E<lt>giles@xiph.orgE<gt>"
msgstr "Ralph Giles E<lt>giles@xiph.orgE<gt>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Manpage Author:"
msgstr "Autor der Handbuchseite:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Christopher L Cheney E<lt>ccheney@debian.orgE<gt>"
msgstr "Christopher L Cheney E<lt>ccheney@debian.orgE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<oggenc>(1), B<oggdec>(1), B<ogg123>(1), B<ogginfo>(1)"
msgstr "B<oggenc>(1), B<oggdec>(1), B<ogg123>(1), B<ogginfo>(1)"

#. type: Plain text
#: fedora-41 fedora-rawhide
msgid ""
"Take comments from a file. The file is the same format as is output by the "
"-l option or given to the -t option: one element per line in 'tag=value' "
"format. If the file is /dev/null and -w was passed, the existing comments "
"will be removed."
msgstr ""
"verwendet Kommentare in einer Datei. Die Datei hat dabei das gleiche Format "
"wie die Ausgabe mit der Option -l bzw. wie die Eingabe mit der Option -t: "
"Ein Element pro Zeile im Format »tag=Wert«. Falls die Datei /dev/null ist "
"und -w übergeben wird, dann werden vorhandene Kommentare entfernt."
