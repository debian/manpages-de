# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christoph Brinkhaus <c.brinkhaus@t-online.de>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-11-15 16:34+0100\n"
"PO-Revision-Date: 2024-11-15 18:45+0100\n"
"Last-Translator: Christoph Brinkhaus <c.brinkhaus@t-online.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Qoitopam User Manual"
msgstr "Qoitopam Benutzerhandbuch"

#. type: TH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "21 May 2022"
msgstr "21. Mai 2022"

#. type: TH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr "Netpbm-Dokumentation"

#. type: SH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

# FIXME pamtoqoi → qoitopam
# FIXME Netpbm. → Netpbm
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "pamtoqoi - Convert QOI format (Quite OK Image format) to Netpbm."
msgstr ""
"qoitopam - Konvertiert das QOI-Format (Quite-OK-Image-Format) zu Netpbm"

#. type: SH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

# FIXME should it be qoifile instead of pnmfile?
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<qoitopam> [I<pnmfile>]"
msgstr "B<qoitopam> [I<PNM-Datei>]"

#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"All options can be abbreviated to their shortest unique prefix.  You may use "
"two hyphens instead of one.  You may separate an option name and its value "
"with white space instead of an equals sign."
msgstr ""
"Alle Optionen können auf ihren kürzesten eindeutigen Präfix abgekürzt "
"werden. Sie können auch zwei anstatt nur eines Bindestrichs verwenden. Sie "
"können den Namen der Option und ihren Wert durch Leerzeichen anstatt eines "
"Gleichheitszeichens trennen."

#. type: SH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME B<Netpbm> → B<netpbm>
# WONTFIX avoid the control character \\& - Generated by makeman, fix needs to be done there
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr "Dieses Programm ist Teil von B<netpbm>(1)."

#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<qoitopam> reads a QOI image from standard input and converts it to a "
"Netpbm image written to standard output."
msgstr ""
"B<qoitopam> liest ein QOI-Bild von der Standardeingabe, konvertiert es in "
"ein Netpbm-Bild und schreibt es in die Standardausgabe."

# WONTFIX avoid the control character \\& - Generated by makeman, fix needs to be done there
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"For the QOI specification and details, see E<.UR http://qoiformat.org> the "
"QOI web site E<.UE> \\&."
msgstr ""
"Die QOI-Spezifikation und weitere Details finden Sie auf E<.UR http://"
"qoiformat.org> der QOI-Website E<.UE>."

# WONTFIX avoid the control character \\& - Generated by makeman, fix needs to be done there
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Use B<pamtoqoi>(1)  \\& to convert a Netpbm image to QOI format."
msgstr ""
"Verwenden Sie B<pamtoqoi>(1), um ein Netpbm-Bild in das QOI-Format zu "
"konvertieren."

# FIXME should it be qoifile instead of pnmfile?
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Input is from Standard Input if you don't specify the input file I<pnmfile>."
msgstr ""
"Wenn Sie keine Eingabedatei I<PNM-Datei> angeben, wird von der "
"Standardeingabe gelesen."

#. type: SH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

# WONTFIX avoid the control character \\& - Generated by makeman, fix needs to be done there
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"The only options are those common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&).\n"
msgstr ""
"Die einzigen Optionen sind die, die alle auf libnetpbm-basierenden\n"
"Programme gemeinsam haben (insbesondere B<-quiet>, siehe \n"
"E<.UR index.html#commonoptions> gemeinsame Optionen E<.UE>).\n"

#. type: SH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# WONTFIX avoid the control character \\& - Generated by makeman, fix needs to be done there
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid "B<pamtoqoi>(1)  \\&, B<pam>(1)  \\&"
msgstr "B<pamtoqoi>(1), B<pam>(1)"

#. type: SH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

# FIXME pamtoqoi → qoitopam
#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<pamtoqoi> was new in Netpbm 10.99 (June 2022)."
msgstr "B<qoitopam> ist in Netpbm 10.99 (Juni 2022) hinzugekommen."

#. type: SH
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr "URSPRUNG DES DOKUMENTS"

#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""
"Diese Handbuchseite wurde vom Netpbm-Werkzeug »makeman« aus der HTML-Quelle "
"erstellt. Das Hauptdokument befindet sich unter"

#. type: Plain text
#: debian-unstable fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/qoitopam.html>"
msgstr "B<http://netpbm.sourceforge.net/doc/qoitopam.html>"

# WONTFIX avoid the control character \\& - Generated by makeman, fix needs to be done there
#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pamtoqoi>(1)  \\&, B<pam>(5)  \\&"
msgstr "B<pamtoqoi>(1), B<pam>(5)"
