# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christian Schmidt <c.schmidt@ius.gun.de>, 1996.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
# Helge Kreutzmann <debian@helgefjell.de>, 2013.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020-2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2024-12-06 18:20+0100\n"
"PO-Revision-Date: 2024-06-29 19:19+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "vcs"
msgstr "vcs"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15. Juni 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "vcs, vcsa - virtual console memory"
msgstr "vcs, vcsa - Speicher der virtuellen Konsolen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I</dev/vcs0> is a character device with major number 7 and minor number 0, "
"usually with mode 0644 and ownership root:tty.  It refers to the memory of "
"the currently displayed virtual console terminal."
msgstr ""
"I</dev/vcs0> ist ein zeichenorientiertes Gerät mit der Major-Nummer 7 und "
"der Minor-Nummer 0. Das Gerät gehört üblicherweise root:tty; der "
"Zugriffsmodus ist auf 0644 gesetzt. Es verweist auf den Speicher der aktuell "
"angezeigten virtuellen Konsole."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I</dev/vcs[1-63]> are character devices for virtual console terminals, they "
"have major number 7 and minor number 1 to 63, usually mode 0644 and "
"ownership root:tty.  I</dev/vcsa[0-63]> are the same, but using I<unsigned "
"short>s (in host byte order) that include attributes, and prefixed with four "
"bytes giving the screen dimensions and cursor position: I<lines>, "
"I<columns>, I<x>, I<y>.  (I<x> = I<y> = 0 at the top left corner of the "
"screen.)"
msgstr ""
"I</dev/vcs[1-63]> sind zeichenorientierte Geräte für virtuelle Konsolen mit "
"der Major-Nummer 7 und den Minor-Nummern 1 bis 63, normalerweise Modus 0644 "
"und die Eigentümerschaft root:tty. I</dev/vcsa[0-63]> sind identisch, "
"verwenden aber I<unsigned short>s (in der Reihenfolge des Rechners), die "
"Attribute enthalten und denen vier Bytes vorangestellt sind, die die "
"Bildschirmgröße und die Cursor-Position angeben: I<lines>, I<columns>, I<x>, "
"I<y>.  (I<x> = I<y> = 0 linke obere Bildschirmecke)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When a 512-character font is loaded, the 9th bit position can be fetched by "
"applying the B<ioctl>(2)  B<VT_GETHIFONTMASK> operation (available since "
"Linux 2.6.18)  on I</dev/tty[1-63]>; the value is returned in the I<unsigned "
"short> pointed to by the third B<ioctl>(2)  argument."
msgstr ""
"Wenn eine Schrift mit 512 Zeichen geladen wird, kann die neunte Bit-Position "
"durch die Anwendung der B<ioctl>(2)-Operation B<VT_GETHIFONTMASK> (verfügbar "
"seit Linux 2.6.18) auf I</dev/tty[1-63]> bestimmt werden; der Wert wird in "
"dem I<unsigned short> zurückgegeben, auf den das dritte Argument von "
"B<ioctl>(2) zeigt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These devices replace the screendump B<ioctl>(2)  operations of "
"B<ioctl_console>(2), so the system administrator can control access using "
"filesystem permissions."
msgstr ""
"Diese Geräte ersetzen die »Screendump«-B<ioctl>(2)-Operationen von "
"B<ioctl_console>(2), sodass der Systemadministrator die Rechte für die "
"einzelnen Konsolen durch das Setzen von normalen Dateisystemattributen "
"vergeben kann."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The devices for the first eight virtual consoles may be created by:"
msgstr ""
"Die Geräte für die ersten acht virtuellen Konsolen können wie folgt erstellt "
"werden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"for x in 0 1 2 3 4 5 6 7 8; do\n"
"    mknod -m 644 /dev/vcs$x c 7 $x;\n"
"    mknod -m 644 /dev/vcsa$x c 7 $[$x+128];\n"
"done\n"
"chown root:tty /dev/vcs*\n"
msgstr ""
"for x in 0 1 2 3 4 5 6 7 8; do\n"
"    mknod -m 644 /dev/vcs$x c 7 $x;\n"
"    mknod -m 644 /dev/vcsa$x c 7 $[$x+128];\n"
"done\n"
"chown root:tty /dev/vcs*\n"

# No support for ioctl, but in example program?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No B<ioctl>(2)  requests are supported."
msgstr "Aufrufe von B<ioctl>(2) werden nicht unterstützt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/vcs[0-63]>"
msgstr "I</dev/vcs[0-63]>"

#.  .SH AUTHOR
#.  Andries Brouwer <aeb@cwi.nl>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/vcsa[0-63]>"
msgstr "I</dev/vcsa[0-63]>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Introduced with Linux 1.1.92."
msgstr "Eingeführt in der Linux-Version 1.1.92."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "You may do a screendump on vt3 by switching to vt1 and typing"
msgstr ""
"Sie können ein »Screendump« (Bildschirmphoto) von VT2 erstellen, indem Sie "
"auf VT1 wechseln und folgendes eingeben:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "cat /dev/vcs3 E<gt>foo\n"
msgstr "cat /dev/vcs3 E<gt>foo\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that the output does not contain newline characters, so some processing "
"may be required, like in"
msgstr ""
"Beachten Sie, dass die Ausgabe kein Zeilenumbruchzeichen enthält und "
"Nacharbeit notwendig sein kann, wie "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "fold -w 81 /dev/vcs3 | lpr\n"
msgstr "fold -w 81 /dev/vcs3 | lpr\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or (horrors)"
msgstr "oder (Horror)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "setterm -dump 3 -file /proc/self/fd/1\n"
msgstr "setterm -dump 3 -file /proc/self/fd/1\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I</dev/vcsa0> device is used for Braille support."
msgstr "Das Gerät I</dev/vcsa0> unterstützt Braille."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This program displays the character and screen attributes under the cursor "
"of the second virtual console, then changes the background color there:"
msgstr ""
"Das folgende Programm zeigt die Zeichen- und Bildschirmattribute unter dem "
"Cursor der zweiten virtuellen Konsole an und ändert danach dort die "
"Hintergrundfarbe."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"
"\\&\n"
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\[ti]mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\[rs]n\", ch, attrib);\n"
"    s \\[ha]= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"
"\\&\n"
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\[ti]mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\[rs]n\", ch, attrib);\n"
"    s \\[ha]= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<ioctl_console>(2), B<tty>(4), B<ttyS>(4), B<gpm>(8)"
msgstr "B<ioctl_console>(2), B<tty>(4), B<ttyS>(4), B<gpm>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"
msgstr ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\[ti]mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
"    s \\[ha]= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\[ti]mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
"    s \\[ha]= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"
"\\&\n"
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\[ti]mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
"    s \\[ha]= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"
"\\&\n"
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\[ti]mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
"    s \\[ha]= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
