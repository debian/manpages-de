# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-09-06 18:25+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "RECDEL"
msgstr "RECDEL"

#. type: TH
#: debian-bookworm fedora-41 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "Tháng 4 năm 2022"

#. type: TH
#: debian-bookworm fedora-41 fedora-rawhide
#, fuzzy, no-wrap
#| msgid "recdel 1.8"
msgid "recdel 1.9"
msgstr "recdel 1.8"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, fuzzy
#| msgid "Remove (or comment out) records from a rec file.\n"
msgid "recdel - remove records from a recfile"
msgstr "Gỡ bỏ (biến thành ghi chú) các bản ghi từ tập tin rec.\n"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"B<recdel> [I<\\,OPTIONS\\/>]... [I<\\,-t TYPE\\/>] [I<\\,-n NUM | -e "
"RECORD_EXPR | -q STR | -m NUM\\/>] [I<\\,FILE\\/>]"
msgstr ""
"B<recdel> [I<\\,TÙY-CHỌN\\/>]… [I<\\,-t KIỂU\\/>] [I<\\,-n SỐ | -e "
"BTHỨC_BẢNGHI | -q CHUỖI | -m SỐ\\/>] [I<\\,TẬP-TIN\\/>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Remove (or comment out) records from a rec file."
msgstr "Gỡ bỏ (biến thành ghi chú) các bản ghi từ tập tin rec."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-c>, B<--comment>"
msgstr "B<-c>, B<--comment>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "comment out the matching records instead of deleting them."
msgstr ""
"đánh dấu là ghi chú cho các bản ghi được tìm thấy thay vì xóa chúng đi."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--force>"
msgstr "B<--force>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"delete even in potentially dangerous situations, and if the deletion is "
"violating record restrictions."
msgstr "xóa ngay cả khi nó tiềm ẩn rủi ro, và khi nó vi phạm các hạn chế."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--no-external>"
msgstr "B<--no-external>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "don't use external descriptors."
msgstr "không dùng mô tả từ bên ngoài."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "give a detailed report if the integrity check fails."
msgstr "đưa ra báo cáo chi tiết nếu việc kiểm tra tính toàn vẹn thất bại."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "print a help message and exit."
msgstr "hiển thị trợ giúp rồi thoát."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "show version and exit."
msgstr "hiển thị phiên bản rồi thoát."

#. type: SS
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Record selection options:"
msgstr "Các tùy chọn để lấy bản ghi:"

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-i>, B<--case-insensitive>"
msgstr "B<-i>, B<--case-insensitive>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "make strings case-insensitive in selection expressions."
msgstr "làm cho biểu thức chọn phân biệt HOA/thường"

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,KIỂU\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "operate on records of the specified type only."
msgstr "chỉ thao tác với các bản ghi có kiểu đã cho."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-e>, B<--expression>=I<\\,RECORD_EXPR\\/>"
msgstr "B<-e>, B<--expression>=I<\\,BTHỨC_BGHI\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "selection expression."
msgstr "biểu thức chọn."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-q>, B<--quick>=I<\\,STR\\/>"
msgstr "B<-q>, B<--quick>=I<\\,CHUỖI\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "select records with fields containing a string."
msgstr "lấy các bản ghi với các trường chứa chuỗi."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-n>, B<--number>=I<\\,NUM\\/>,..."
msgstr "B<-n>, B<--number>=I<\\,SỐ\\/>,…"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "select specific records by position, with ranges."
msgstr "lấy các bản ghi theo vị trí với một vùng."

#. type: TP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "B<-m>, B<--random>=I<\\,NUM\\/>"
msgstr "B<-m>, B<--random>=I<\\,SỐ\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "select a given number of random records."
msgstr "lấy ra số bản ghi ngẫu nhiên."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"If no FILE is specified then the command acts like a filter, getting the "
"data from standard input and writing the result to standard output."
msgstr ""
"Nếu không đưa ra TẬP-TIN thì lệnh thực hiện như là một bộ lọc, lấy dữ liệu "
"từ đầu vào tiêu chuẩn và kết quả đưa ra đầu ra tiêu chuẩn."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Written by Jose E. Marchesi."
msgstr "Được viết bởi Jose E. Marchesi."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "Report bugs to: bug-recutils@gnu.org"
msgstr ""
"Thông báo lỗi cho: E<lt>bug-recutils@gnu.orgE<gt>. Thông báo lỗi dịch cho: "
"E<lt>http://translationproject.org/team/vi.htmlE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"GNU recutils home page: E<lt>https://www.gnu.org/software/recutils/E<gt>"
msgstr ""
"Trang chủ GNU recutils: E<lt>https://www.gnu.org/software/recutils/E<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Trợ giúp chung về sử dụng phần mềm GNU: E<lt>https://www.gnu.org/gethelp/"
"E<gt>"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"Copyright \\(co 2010-2020 Jose E. Marchesi.  License GPLv3+: GNU GPL version "
"3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2010-2020 Jose E. Marchesi.  Giấy phép GPL pb3+ : Giấy phép "
"Công cộng GNU phiên bản 3 hay sau E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Đây là phần mềm tự do: bạn có quyền sửa đổi và phát hành lại nó. KHÔNG CÓ "
"BẢO HÀNH GÌ CẢ, với điều khiển được pháp luật cho phép."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid ""
"The full documentation for B<recdel> is maintained as a Texinfo manual.  If "
"the B<info> and B<recdel> programs are properly installed at your site, the "
"command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<recdel> được bảo trì dưới dạng một sổ tay "
"Texinfo.  Nếu chương trình B<info> và B<recdel> được cài đặt đúng ở địa chỉ "
"của bạn thì câu lệnh"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "B<info recutils>"
msgstr "B<info recutils>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "March 2024"
msgstr "Tháng 3 năm 2024"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "GNU coreutils 9.1"
msgid "GNU recutils 1.9"
msgstr "GNU coreutils 9.1"
