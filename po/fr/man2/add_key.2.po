# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2024-12-22 07:17+0100\n"
"PO-Revision-Date: 2024-11-05 12:07+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "add_key"
msgstr "add_key"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 juin 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "add_key - add a key to the kernel's key management facility"
msgstr "add_key - Ajouter une clé au gestionnaire de clés du noyau"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>keyutils.hE<gt>>\n"
msgstr "B<#include E<lt>keyutils.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<key_serial_t add_key(const char *>I<type>B<, const char *>I<description>B<,>\n"
"B<                     const void >I<payload>B<[.>I<plen>B<], size_t >I<plen>B<,>\n"
"B<                     key_serial_t >I<keyring>B<);>\n"
msgstr ""
"B<key_serial_t add_key(const char *>I<type>B<, const char *>I<description>B<,>\n"
"B<                     const void >I<payload>B<[.>I<plen>B<], size_t >I<plen>B<,>\n"
"B<                     key_serial_t >I<keyring>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgstr ""
"I<Note> : il n'existe pas d'enveloppe pour cet appel système dans la glibc ; "
"voir NOTES."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<add_key>()  creates or updates a key of the given I<type> and "
"I<description>, instantiates it with the I<payload> of length I<plen>, "
"attaches it to the nominated I<keyring>, and returns the key's serial number."
msgstr ""
"B<add_key>() crée ou met à jour une clé ayant un I<type> et une "
"I<description> donnés, l'instancie avec une charge utile (I<payload>) de "
"longueur I<plen>, l'attache au trousseau (I<keyring>) spécifié, et renvoie "
"son numéro de série."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The key may be rejected if the provided data is in the wrong format or it is "
"invalid in some other way."
msgstr ""
"La clé peut être rejetée si les données fournies sont dans un mauvais format "
"ou si elles sont non valables de toute autre façon."

#.  FIXME The aforementioned phrases begs the question:
#.  which key types support this?
#.  FIXME Perhaps elaborate the implications here? Namely, the new
#.  key will have a new ID, and if the old key was a keyring that
#.  is consequently unlinked, then keys that it was anchoring
#.  will have their reference count decreased by one (and may
#.  consequently be garbage collected). Is this all correct?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the destination I<keyring> already contains a key that matches the "
"specified I<type> and I<description>, then, if the key type supports it, "
"that key will be updated rather than a new key being created; if not, a new "
"key (with a different ID) will be created and it will displace the link to "
"the extant key from the keyring."
msgstr ""
"Si le trousseau (I<keyring>) de destination contient déjà une clé avec ce "
"I<type> et cette I<description>, alors, si le type de la clé le permet, "
"cette clé sera mise à jour au lieu de créer une nouvelle clé. Dans le cas "
"contraire, une nouvelle clé sera créée, et le trousseau sera mis à jour pour "
"remplacer le lien vers l'ancienne clé par un lien vers la nouvelle."

#.  FIXME . Perhaps have a separate page describing special keyring IDs?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The destination I<keyring> serial number may be that of a valid keyring for "
"which the caller has I<write> permission.  Alternatively, it may be one of "
"the following special keyring IDs:"
msgstr ""
"Le numéro de série du trousseau de destination peut être celui d'un "
"trousseau valable sur lequel l'appelant a le droit d'écriture. Il peut aussi "
"être un des identifiants spéciaux suivants :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<KEY_SPEC_THREAD_KEYRING>"
msgstr "B<KEY_SPEC_THREAD_KEYRING>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This specifies the caller's thread-specific keyring (B<thread-keyring>(7))."
msgstr ""
"Cela spécifie le trousseau spécifique aux processus légers (B<thread-"
"keyring>(7)) de l'appelant."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<KEY_SPEC_PROCESS_KEYRING>"
msgstr "B<KEY_SPEC_PROCESS_KEYRING>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This specifies the caller's process-specific keyring (B<process-keyring>(7))."
msgstr ""
"Cela spécifie le trousseau spécifique aux processus de l'appelant (B<process-"
"keyring>(7))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<KEY_SPEC_SESSION_KEYRING>"
msgstr "B<KEY_SPEC_SESSION_KEYRING>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This specifies the caller's session-specific keyring (B<session-keyring>(7))."
msgstr ""
"Cela spécifie le trousseau spécifique à la session de l'appelant (B<session-"
"keyring>(7))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<KEY_SPEC_USER_KEYRING>"
msgstr "B<KEY_SPEC_USER_KEYRING>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This specifies the caller's UID-specific keyring (B<user-keyring>(7))."
msgstr ""
"Cela spécifie le trousseau spécifique à l'UID de l'appelant (B<user-"
"keyring>(7))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<KEY_SPEC_USER_SESSION_KEYRING>"
msgstr "B<KEY_SPEC_USER_SESSION_KEYRING>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This specifies the caller's UID-session keyring (B<user-session-keyring>(7))."
msgstr ""
"Cela spécifie le trousseau spécifique à la session de l'UID de l'appelant "
"(B<user-session-keyring>(7))."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Key types"
msgstr "Types de clé"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The key I<type> is a string that specifies the key's type.  Internally, the "
"kernel defines a number of key types that are available in the core key "
"management code.  Among the types that are available for user-space use and "
"can be specified as the I<type> argument to B<add_key>()  are the following:"
msgstr ""
"Le I<type> de clé est une chaîne qui indique le type de la clé. En interne, "
"le noyau définit un certain nombre de types de clé disponibles au cœur du "
"système de gestion des clés. Parmi les types disponibles pour l'utilisateur "
"que vous pouvez spécifier comme paramètre I<type> de B<add_key>(), se "
"trouvent :"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I<\\[dq]keyring\\[dq]>"
msgstr "I<\\[dq]keyring\\[dq]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Keyrings are special key types that may contain links to sequences of other "
"keys of any type.  If this interface is used to create a keyring, then "
"I<payload> should be NULL and I<plen> should be zero."
msgstr ""
"Les trousseaux (I<keyring>) sont des types de clé spéciaux qui peuvent "
"contenir des liens vers des séquences d'autres clés de tout type. Si cette "
"interface est utilisée pour créer un trousseau, alors l'argument I<payload> "
"doit valoir NULL, et I<plen> doit être zéro."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I<\\[dq]user\\[dq]>"
msgstr "I<\\[dq]user\\[dq]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is a general purpose key type whose payload may be read and updated by "
"user-space applications.  The key is kept entirely within kernel memory.  "
"The payload for keys of this type is a blob of arbitrary data of up to "
"32,767 bytes."
msgstr ""
"Il s'agit d'un type de clé généraliste dont la charge utile peut être lue et "
"mise à jour par des applications de l'espace utilisateur. La clé est "
"entièrement conservée dans la mémoire du noyau. La charge utile pour les "
"clés de ce type est un bloc de données de votre choix jusqu'à 32 767 octets."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I<\\[dq]logon\\[dq]> (since Linux 3.3)"
msgstr "I<\\[dq]logon\\[dq]> (depuis Linux 3.3)"

#.  commit 9f6ed2ca257fa8650b876377833e6f14e272848b
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This key type is essentially the same as I<\\[dq]user\\[dq]>, but it does "
"not permit the key to read.  This is suitable for storing payloads that you "
"do not want to be readable from user space."
msgstr ""
"Ce type de clé est pour l'essentiel le même que I<\\[dq]user\\[dq]>, mais il "
"ne permet pas de lire la clé. Cela convient pour stocker les charges utiles "
"dont vous ne voulez pas que l'utilisateur puisse lire."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This key type vets the I<description> to ensure that it is qualified by a "
"\"service\" prefix, by checking to ensure that the I<description> contains a "
"':' that is preceded by other characters."
msgstr ""
"Ce type de clé analyse une I<description> en profondeur pour garantir "
"qu'elle est qualifiée par le préfixe d'un « service », en vérifiant que la "
"I<description> contient un « : » précédé d’autres caractères."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I<\\[dq]big_key\\[dq]> (since Linux 3.13)"
msgstr "I<\\[dq]big_key\\[dq]> (depuis Linux 3.13)"

#.  commit ab3c3587f8cda9083209a61dbe3a4407d3cada10
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This key type is similar to I<\\[dq]user\\[dq]>, but may hold a payload of "
"up to 1\\ MiB.  If the key payload is large enough, then it may be stored "
"encrypted in tmpfs (which can be swapped out) rather than kernel memory."
msgstr ""
"Ce type de clé est similaire à I<\\[dq]user\\[dq]>, mais il peut contenir "
"une charge utile jusqu'à 1 MiO. Si la charge utile de la clé est assez "
"grande, elle peut être stockée, chiffrée, dans tmpfs (qui peut être mis sur "
"l'espace d'échange) et non dans la mémoire du noyau."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "For further details on these key types, see B<keyrings>(7)."
msgstr "Pour plus de détails sur ces types de clé, voir B<keyrings>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<add_key>()  returns the serial number of the key it created or "
"updated.  On error, -1 is returned and I<errno> is set to indicate the error."
msgstr ""
"En cas de succès, B<add_key>() renvoie le numéro de série de la clé créée ou "
"mise à jour. En cas d'erreur, B<-1> est renvoyé et I<errno> est positionné "
"pour indiquer l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The keyring wasn't available for modification by the user."
msgstr ""
"Le trousseau n'était pas disponible pour pouvoir être modifié par "
"l'utilisateur."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The key quota for this user would be exceeded by creating this key or "
"linking it to the keyring."
msgstr ""
"Le quota de clés de cet utilisateur serait dépassé si la clé était créée ou "
"ajoutée au trousseau."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"One or more of I<type>, I<description>, and I<payload> points outside "
"process's accessible address space."
msgstr ""
"Un ou plusieurs I<type>, I<description> et I<payload> (charge utile) "
"pointent à l'extérieur de l'espace d'adresses accessible au processus."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The size of the string (including the terminating null byte) specified in "
"I<type> or I<description> exceeded the limit (32 bytes and 4096 bytes "
"respectively)."
msgstr ""
"La longueur de la chaîne (y compris l'octet NULL final) spécifié dans "
"I<type> ou I<description> a dépassé la limite (respectivement 32 et "
"4096 octets)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The payload data was invalid."
msgstr "La charge utile (I<payload>) n’est pas valable."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<type> was I<\\[dq]logon\\[dq]> and the I<description> was not qualified "
"with a prefix string of the form I<\\[dq]service:\\[dq]>."
msgstr ""
"I<type> était I<\\[dq]logon\\[dq]> et la I<description> n'était pas "
"qualifiée avec une chaîne de préfixes sous la forme I<\\[dq]service:\\[dq]>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EKEYEXPIRED>"
msgstr "B<EKEYEXPIRED>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The keyring has expired."
msgstr "Le trousseau a expiré."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EKEYREVOKED>"
msgstr "B<EKEYREVOKED>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The keyring has been revoked."
msgstr "Le trousseau a été révoqué."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOKEY>"
msgstr "B<ENOKEY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The keyring doesn't exist."
msgstr "Le trousseau n'existe pas."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient memory to create a key."
msgstr "Il n'y a pas assez de mémoire pour créer une clé."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<type> started with a period (\\[aq].\\[aq]).  Key types that begin "
"with a period are reserved to the implementation."
msgstr ""
"I<type> commençait par un point (« . »). Les types de clé commençant par un "
"point sont réservés à l'implémentation."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<type> was I<\\[dq]keyring\\[dq]> and the I<description> started with a "
"period (\\[aq].\\[aq]).  Keyrings with descriptions (names)  that begin with "
"a period are reserved to the implementation."
msgstr ""
"I<type> valait I<\\[dq]keyring\\[dq]> et la I<description> commençait par un "
"point (« . »). Les trousseaux dont les descriptions (noms) commençant par un "
"point sont réservés à l'implémentation."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 2.6.10."
msgstr "Linux 2.6.10."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"glibc does not provide a wrapper for this system call.  A wrapper is "
"provided in the I<libkeyutils> library.  (The accompanying package provides "
"the I<E<lt>keyutils.hE<gt>> header file.)  When employing the wrapper in "
"that library, link with I<-lkeyutils>."
msgstr ""
"Aucune enveloppe n'est fournie pour cet appel système dans la glibc. Une "
"enveloppe est fournie dans le paquet I<libkeyutils> (le paquet qui "
"l'accompagne fournit le fichier d'en-tête I<E<lt>keyutils.hE<gt>>). Quand "
"vous utilisez l'enveloppe de cette bibliothèque, liez-la avec I<-lkeyutils>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program below creates a key with the type, description, and payload "
"specified in its command-line arguments, and links that key into the session "
"keyring.  The following shell session demonstrates the use of the program:"
msgstr ""
"Le programme ci-dessous crée une clé dont le type, la description et la "
"charge utile sont indiqués dans les paramètres de la ligne de commande, puis "
"il lie la clé au trousseau de la session. La session d'interpréteur suivante "
"montre l'utilisation du programme :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out user mykey \"Some payload\">\n"
"Key ID is 64a4dca\n"
"$ B<grep \\[aq]64a4dca\\[aq] /proc/keys>\n"
"064a4dca I--Q---    1 perm 3f010000  1000  1000 user    mykey: 12\n"
msgstr ""
"$ B<./a.out user mykey \"Une charge utile\">\n"
"L'identifiant de la clé est is 64a4dca\n"
"$ B<grep \\[aq]64a4dca\\[aq] /proc/keys>\n"
"064a4dca I--Q---    1 perm 3f010000  1000  1000 user    mykey: 12\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>keyutils.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    key_serial_t key;\n"
"\\&\n"
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Usage: %s type description payload\\[rs]n\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    key = add_key(argv[1], argv[2], argv[3], strlen(argv[3]),\n"
"                  KEY_SPEC_SESSION_KEYRING);\n"
"    if (key == -1) {\n"
"        perror(\"add_key\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"Key ID is %jx\\[rs]n\", (uintmax_t) key);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>keyutils.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    key_serial_t key;\n"
"\\&\n"
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Utilisation : %s type description charge_utile\\[rs]n\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    key = add_key(argv[1], argv[2], argv[3], strlen(argv[3]),\n"
"                  KEY_SPEC_SESSION_KEYRING);\n"
"    if (key == -1) {\n"
"        perror(\"add_key\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"L'identifiant de la clé est %jx\\[rs]n\", (uintmax_t) key);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<keyctl>(1), B<keyctl>(2), B<request_key>(2), B<keyctl>(3), B<keyrings>(7), "
"B<keyutils>(7), B<persistent-keyring>(7), B<process-keyring>(7), B<session-"
"keyring>(7), B<thread-keyring>(7), B<user-keyring>(7), B<user-session-"
"keyring>(7)"
msgstr ""
"B<keyctl>(1), B<keyctl>(2), B<request_key>(2), B<keyctl>(3), B<keyrings>(7), "
"B<keyutils>(7), B<persistent-keyring>(7), B<process-keyring>(7), B<session-"
"keyring>(7), B<thread-keyring>(7), B<user-keyring>(7), B<user-session-"
"keyring>(7)"

#.  commit b68101a1e8f0263dbc7b8375d2a7c57c6216fb76
#.  commit 3db38ed76890565772fcca3279cc8d454ea6176b
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The kernel source files I<Documentation/security/keys/core.rst> and "
"I<Documentation/keys/request-key.rst> (or, before Linux 4.13, in the files "
"I<Documentation/security/keys.txt> and I<Documentation/security/keys-request-"
"key.txt>)."
msgstr ""
"Les fichiers I<Documentation/security/keys/core.rst> et I<Documentation/keys/"
"request-key.rst> des sources du noyau (ou, avant Linux 4.13, I<Documentation/"
"security/keys.txt> et I<Documentation/security/keys-request-key.txt>)."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "I<\"keyring\">"
msgstr "I<\"keyring\">"

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "I<\"user\">"
msgstr "I<\"user\">"

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "I<\"logon\"> (since Linux 3.3)"
msgstr "I<\"logon\"> (depuis Linux 3.3)"

#.  commit 9f6ed2ca257fa8650b876377833e6f14e272848b
#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"This key type is essentially the same as I<\"user\">, but it does not permit "
"the key to read.  This is suitable for storing payloads that you do not want "
"to be readable from user space."
msgstr ""
"Ce type de clé est pour l'essentiel le même que I<user>, mais il ne permet "
"pas de lire la clé. Cela convient pour stocker les charges utiles dont vous "
"ne voulez pas que l'utilisateur puisse lire."

#. type: TP
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "I<\"big_key\"> (since Linux 3.13)"
msgstr "I<\"big_key\"> (depuis Linux 3.13)"

#.  commit ab3c3587f8cda9083209a61dbe3a4407d3cada10
#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"This key type is similar to I<\"user\">, but may hold a payload of up to 1\\ "
"MiB.  If the key payload is large enough, then it may be stored encrypted in "
"tmpfs (which can be swapped out) rather than kernel memory."
msgstr ""
"Ce type de clé est similaire à I<user>, mais il peut contenir une charge "
"utile jusqu'à 1 MiO. Si la charge utile de la clé est assez grande, elle "
"peut être stockée, chiffrée, dans tmpfs (qui peut être mis sur l'espace "
"d'échange) et non dans la mémoire du noyau."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"I<type> was I<\"logon\"> and the I<description> was not qualified with a "
"prefix string of the form I<\"service:\">."
msgstr ""
"I<type> était I<logon> et la I<description> n'était pas qualifiée avec une "
"chaîne de préfixes sous la forme I<service:>."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"I<type> was I<\"keyring\"> and the I<description> started with a period "
"(\\[aq].\\[aq]).  Keyrings with descriptions (names)  that begin with a "
"period are reserved to the implementation."
msgstr ""
"I<type> valait I<keyring> et la I<description> commençait par un point "
"(« . »). Les trousseaux dont les descriptions (noms) commençant par un point "
"sont réservés à l'implémentation."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm
msgid "This system call first appeared in Linux 2.6.10."
msgstr "Cet appel système est apparu pour la première fois dans Linux 2.6.10."

#. type: Plain text
#: debian-bookworm
msgid "This system call is a nonstandard Linux extension."
msgstr "Cet appel système est une extension Linux non standard."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>keyutils.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>keyutils.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    key_serial_t key;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    key_serial_t key;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Usage: %s type description payload\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Utilisation: %s type description charge_utile\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    key = add_key(argv[1], argv[2], argv[3], strlen(argv[3]),\n"
"                  KEY_SPEC_SESSION_KEYRING);\n"
"    if (key == -1) {\n"
"        perror(\"add_key\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    key = add_key(argv[1], argv[2], argv[3], strlen(argv[3]),\n"
"                  KEY_SPEC_SESSION_KEYRING);\n"
"    if (key == -1) {\n"
"        perror(\\\"add_key\\\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    printf(\"Key ID is %jx\\en\", (uintmax_t) key);\n"
msgstr "    printf(\"L'identifiant de la clé est %jx\\en\", (uintmax_t) key);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>keyutils.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    key_serial_t key;\n"
"\\&\n"
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Usage: %s type description payload\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    key = add_key(argv[1], argv[2], argv[3], strlen(argv[3]),\n"
"                  KEY_SPEC_SESSION_KEYRING);\n"
"    if (key == -1) {\n"
"        perror(\"add_key\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"Key ID is %jx\\en\", (uintmax_t) key);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>keyutils.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    key_serial_t key;\n"
"\\&\n"
"    if (argc != 4) {\n"
"        fprintf(stderr, \"Utilisation : %s type description charge_utile\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    key = add_key(argv[1], argv[2], argv[3], strlen(argv[3]),\n"
"                  KEY_SPEC_SESSION_KEYRING);\n"
"    if (key == -1) {\n"
"        perror(\"add_key\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"L'identifiant de la clé est %jx\\en\", (uintmax_t) key);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
