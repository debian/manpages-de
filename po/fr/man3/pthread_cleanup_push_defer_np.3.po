# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-06 18:10+0100\n"
"PO-Revision-Date: 2024-04-22 11:34+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_cleanup_push_defer_np"
msgstr "pthread_cleanup_push_defer_np"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pthread_cleanup_push_defer_np, pthread_cleanup_pop_restore_np - push and pop "
"thread cancelation clean-up handlers while saving cancelability type"
msgstr ""
"pthread_cleanup_push_defer_np, pthread_cleanup_pop_restore_np — Empiler et "
"dépiler les gestionnaires de nettoyage tout en préservant le mode "
"d'annulation"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "Bibliothèque de threads POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void pthread_cleanup_push_defer_np(void (*>I<routine>B<)(void *), void *>I<arg>B<);>\n"
"B<void pthread_cleanup_pop_restore_np(int >I<execute>B<);>\n"
msgstr ""
"B<void pthread_cleanup_push_defer_np(void (*>I<routine>B<)(void *), void *>I<arg>B<);>\n"
"B<void pthread_cleanup_pop_restore_np(int >I<execute>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_cleanup_push_defer_np>(), B<pthread_cleanup_pop_defer_np>():"
msgstr ""
"B<pthread_cleanup_push_defer_np>(), B<pthread_cleanup_pop_defer_np>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are the same as B<pthread_cleanup_push>(3)  and "
"B<pthread_cleanup_pop>(3), except for the differences noted on this page."
msgstr ""
"Ces fonctions opèrent de la même façon que B<pthread_cleanup_push>(3) et "
"B<pthread_cleanup_pop>(3), à l'exception des différences décrites dans cette "
"page."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Like B<pthread_cleanup_push>(3), B<pthread_cleanup_push_defer_np>()  pushes "
"I<routine> onto the thread's stack of cancelation clean-up handlers.  In "
"addition, it also saves the thread's current cancelability type, and sets "
"the cancelability type to \"deferred\" (see B<pthread_setcanceltype>(3)); "
"this ensures that cancelation clean-up will occur even if the thread's "
"cancelability type was \"asynchronous\" before the call."
msgstr ""
"Comme B<pthread_cleanup_push>(3), B<pthread_cleanup_push_defer_np>() empile "
"I<routine> sur la pile des gestionnaires de nettoyage du thread. De plus, "
"elle sauvegarde le mode actuel d'annulation, et le change en «\\ deferred\\ "
"» (retardé), consultez B<pthread_setcanceltype>(3). Cela garantit que le "
"nettoyage à l'annulation sera appelé même si le mode d'annulation était à "
"«\\ asynchronous\\ » (asynchrone) avant l'appel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Like B<pthread_cleanup_pop>(3), B<pthread_cleanup_pop_restore_np>()  pops "
"the top-most clean-up handler from the thread's stack of cancelation clean-"
"up handlers.  In addition, it restores the thread's cancelability type to "
"its value at the time of the matching B<pthread_cleanup_push_defer_np>()."
msgstr ""
"Comme B<pthread_cleanup_pop>(3), B<pthread_cleanup_pop_restore_np>() dépile "
"le gestionnaire de nettoyage depuis la pile des gestionnaires de nettoyage "
"du thread. De plus, elle remet le mode d'annulation à la valeur qu'il avait "
"lors de l'appel à B<pthread_cleanup_push_defer_np>() correspondant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The caller must ensure that calls to these functions are paired within the "
"same function, and at the same lexical nesting level.  Other restrictions "
"apply, as described in B<pthread_cleanup_push>(3)."
msgstr ""
"L'appelant doit vérifier que les appels à ces fonctions sont appariés à "
"l'intérieur de la même fonction, et au même niveau d'imbriquement lexical. "
"D'autres restrictions s'appliquent, comme expliqué dans "
"B<pthread_cleanup_push>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This sequence of calls:"
msgstr "Cette séquence d'appels\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"pthread_cleanup_push_defer_np(routine, arg);\n"
"pthread_cleanup_pop_restore_np(execute);\n"
msgstr ""
"pthread_cleanup_push_defer_np(routine, arg);\n"
"pthread_cleanup_pop_restore_np(execute);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "is equivalent to (but shorter and more efficient than):"
msgstr "est équivalente à (mais en plus court et plus efficace)\\ :"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"int oldtype;\n"
"\\&\n"
"pthread_cleanup_push(routine, arg);\n"
"pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype);\n"
"\\&...\n"
"pthread_setcanceltype(oldtype, NULL);\n"
"pthread_cleanup_pop(execute);\n"
msgstr ""
"int oldtype;\n"
"\\&\n"
"pthread_cleanup_push(routine, arg);\n"
"pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype);\n"
"\\&...\n"
"pthread_setcanceltype(oldtype, NULL);\n"
"pthread_cleanup_pop(execute);\n"

#. #-#-#-#-#  archlinux: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SH VERSIONS
#.  Available since glibc 2.0
#. type: SH
#. #-#-#-#-#  debian-unstable: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-41: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-16-0: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU; hence the suffix \"_np\" (nonportable) in the names."
msgstr "GNU ; d'où le suffixe « _np » (non portable) dans leur nom."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.0"
msgstr "glibc 2.0"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_cancel>(3), B<pthread_cleanup_push>(3), "
"B<pthread_setcancelstate>(3), B<pthread_testcancel>(3), B<pthreads>(7)"
msgstr ""
"B<pthread_cancel>(3), B<pthread_cleanup_push>(3), "
"B<pthread_setcancelstate>(3), B<pthread_testcancel>(3), B<pthreads>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "int oldtype;\n"
msgstr "int oldtype;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"pthread_cleanup_push(routine, arg);\n"
"pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype);\n"
"\\&...\n"
"pthread_setcanceltype(oldtype, NULL);\n"
"pthread_cleanup_pop(execute);\n"
msgstr ""
"pthread_cleanup_push(routine, arg);\n"
"pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype);\n"
"\\&...\n"
"pthread_setcanceltype(oldtype, NULL);\n"
"pthread_cleanup_pop(execute);\n"

#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are nonstandard GNU extensions; hence the suffix "
"\"_np\" (nonportable) in the names."
msgstr ""
"Ces fonctions sont des extensions GNU non standard ; d'où le suffixe "
"« _np » (non portable) dans leur nom."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
