# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Lucien Gentis <lucien.gentis@waika9.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 17:59+0100\n"
"PO-Revision-Date: 2024-03-14 16:22+0100\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: VIM 8.2 with GTK3 GUI\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "getpwent_r"
msgstr "getpwent_r"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 juin 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "getpwent_r, fgetpwent_r - get passwd file entry reentrantly"
msgstr ""
"getpwent_r, fgetpwent_r – Obtenir un enregistrement du fichier passwd de "
"manière réentrante"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pwd.hE<gt>>\n"
msgstr "B<#include E<lt>pwd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getpwent_r(struct passwd *restrict >I<pwbuf>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct passwd **restrict >I<pwbufp>B<);>\n"
"B<int fgetpwent_r(FILE *restrict >I<stream>B<, struct passwd *restrict >I<pwbuf>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct passwd **restrict >I<pwbufp>B<);>\n"
msgstr ""
"B<int getpwent_r(struct passwd *restrict >I<tampon_pw>B<,>\n"
"B<               char >I<tampon>B<[restrict .>I<taille_tampon>B<], size_t >I<taille_tampon>B<,>\n"
"B<               struct passwd **restrict >I<pointeur_tampon_pw>B<);>\n"
"B<int fgetpwent_r(FILE *restrict >I<flux>B<, struct passwd *restrict >I<tampon_pw>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<taille_tampon>B<], size_t >I<taille_tampon>B<,>\n"
"B<               struct passwd **restrict >I<pointeur_tampon_pw>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getpwent_r>(),"
msgstr "B<getpwent_r>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.19 :\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 et antérieures :\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<fgetpwent_r>():"
msgstr "B<fgetpwent_r>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _SVID_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.19 :\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    glibc 2.19 et antérieures :\n"
"        _ATFILE_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The functions B<getpwent_r>()  and B<fgetpwent_r>()  are the reentrant "
"versions of B<getpwent>(3)  and B<fgetpwent>(3).  The former reads the next "
"passwd entry from the stream initialized by B<setpwent>(3).  The latter "
"reads the next passwd entry from I<stream>."
msgstr ""
"Les fonctions B<getpwent_r>() et B<fgetpwent_r>() sont les versions "
"réentrantes des fonctions B<getpwent>(3) et B<fgetpwent>(3). La première lit "
"l'enregistrement passwd suivant à partir du flux initialisé par "
"B<setpwent>(3). La seconde lit l'enregistrement passwd suivant à partir du "
"flux I<flux>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<passwd> structure is defined in I<E<lt>pwd.hE<gt>> as follows:"
msgstr ""
"La structure I<passwd> est définie dans I<E<lt>pwd.hE<gt>> comme ceci\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct passwd {\n"
"    char    *pw_name;      /* username */\n"
"    char    *pw_passwd;    /* user password */\n"
"    uid_t    pw_uid;       /* user ID */\n"
"    gid_t    pw_gid;       /* group ID */\n"
"    char    *pw_gecos;     /* user information */\n"
"    char    *pw_dir;       /* home directory */\n"
"    char    *pw_shell;     /* shell program */\n"
"};\n"
msgstr ""
"struct passwd {\n"
"    char    *pw_name;      /* Nom d'utilisateur */\n"
"    char    *pw_passwd;    /* Mot de passe de l'utilisateur */\n"
"    uid_t    pw_uid;       /* ID de l'utilisateur */\n"
"    gid_t    pw_gid;       /* ID du groupe */\n"
"    char    *pw_gecos;     /* Information utilisateur */\n"
"    char    *pw_dir;       /* Répertoire personnel */\n"
"    char    *pw_shell;     /* Interpréteur de commande */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For more information about the fields of this structure, see B<passwd>(5)."
msgstr ""
"Pour plus d'informations à propos des champs de cette structure, consultez "
"B<passwd>(5)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The nonreentrant functions return a pointer to static storage, where this "
"static storage contains further pointers to user name, password, gecos "
"field, home directory and shell.  The reentrant functions described here "
"return all of that in caller-provided buffers.  First of all there is the "
"buffer I<pwbuf> that can hold a I<struct passwd>.  And next the buffer "
"I<buf> of size I<buflen> that can hold additional strings.  The result of "
"these functions, the I<struct passwd> read from the stream, is stored in the "
"provided buffer I<*pwbuf>, and a pointer to this I<struct passwd> is "
"returned in I<*pwbufp>."
msgstr ""
"Les fonctions non réentrantes renvoient un pointeur sur une zone statique, "
"zone qui contient d'autres pointeurs vers le nom, le mot de passe, le champ "
"gecos, le répertoire personnel et l'interpréteur de commandes de "
"l'utilisateur. Les fonctions réentrantes décrites ici renvoient tout cela "
"dans des tampons fournis par l'appelant. Il y a tout d'abord le tampon "
"I<tampon_pw> qui contient une structure I<passwd>, puis le tampon I<tampon> "
"de taille I<taille_tampon> qui peut contenir des chaînes supplémentaires. Le "
"résultat de ces fonctions, la structure I<passwd> lue dans le flux, est "
"enregistré dans le tampon I<*tampon_pw> fourni, et un pointeur vers cette "
"structure I<passwd> est renvoyé dans I<*pointeur_tampon_pw>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these functions return 0 and I<*pwbufp> is a pointer to the "
"I<struct passwd>.  On error, these functions return an error value and "
"I<*pwbufp> is NULL."
msgstr ""
"Si elles réussissent, ces fonctions renvoient B<0> et I<*pointeur_tampon_pw> "
"est un pointeur vers la structure I<passwd>. Si elles échouent, ces "
"fonctions renvoient une valeur d'erreur et I<*pointeur_tampon_pw> est NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No more entries."
msgstr "Il n'y a plus d'entrées."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient buffer space supplied.  Try again with larger buffer."
msgstr ""
"La taille du tampon fourni est insuffisante. Veuillez essayer à nouveau avec "
"un tampon plus grand."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getpwent_r>()"
msgstr "B<getpwent_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:pwent locale"
msgstr "MT-Unsafe race:pwent locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<fgetpwent_r>()"
msgstr "B<fgetpwent_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In the above table, I<pwent> in I<race:pwent> signifies that if any of the "
"functions B<setpwent>(), B<getpwent>(), B<endpwent>(), or B<getpwent_r>()  "
"are used in parallel in different threads of a program, then data races "
"could occur."
msgstr ""
"Dans la table ci-dessus, I<pwent> dans I<race:pwent> signifie que si une des "
"fonctions B<setpwent>(), B<getpwent>(), B<endpwent>() ou B<getpwent_r>() est "
"utilisée en parallèle dans différents threads d'un programme, des situations "
"de compétition entre données peuvent apparaître."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Other systems use the prototype"
msgstr "D'autres systèmes utilisent le prototype"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct passwd *\n"
"getpwent_r(struct passwd *pwd, char *buf, int buflen);\n"
msgstr ""
"struct passwd *\n"
"getpwent_r(struct passwd *pwd, char *tampon, int taille_tampon);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or, better,"
msgstr "ou mieux"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"getpwent_r(struct passwd *pwd, char *buf, int buflen,\n"
"           FILE **pw_fp);\n"
msgstr ""
"int\n"
"getpwent_r(struct passwd *pwd, char *tampon, int taille_tampon,\n"
"           FILE **pw_fp);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Aucun."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are done in a style resembling the POSIX version of "
"functions like B<getpwnam_r>(3)."
msgstr ""
"Ces fonctions sont effectuées dans un style ressemblant à la version POSIX "
"de fonctions comme B<getpwnam_r>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<getpwent_r>()  is not really reentrant since it shares the "
"reading position in the stream with all other threads."
msgstr ""
"La fonction B<getpwent_r>() n'est pas vraiment réentrante puisqu'elle "
"partage la position de lecture dans le flux avec tous les autres threads."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#define _GNU_SOURCE\n"
#| "#include E<lt>pwd.hE<gt>\n"
#| "#include E<lt>stdint.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "\\&\n"
#| "#define BUFLEN 4096\n"
#| "\\&\n"
#| "int\n"
#| "main(void)\n"
#| "{\n"
#| "    struct passwd pw;\n"
#| "    struct passwd *pwp;\n"
#| "    char buf[BUFLEN];\n"
#| "    int i;\n"
#| "\\&\n"
#| "    setpwent();\n"
#| "    while (1) {\n"
#| "        i = getpwent_r(&pw, buf, sizeof(buf), &pwp);\n"
#| "        if (i)\n"
#| "            break;\n"
#| "        printf(\"%s (%jd)\\etHOME %s\\etSHELL %s\\en\", pwp-E<gt>pw_name,\n"
#| "               (intmax_t) pwp-E<gt>pw_uid, pwp-E<gt>pw_dir, pwp-E<gt>pw_shell);\n"
#| "    }\n"
#| "    endpwent();\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct passwd pw;\n"
"    struct passwd *pwp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setpwent();\n"
"    while (1) {\n"
"        i = getpwent_r(&pw, buf, sizeof(buf), &pwp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd)\\[rs]tHOME %s\\[rs]tSHELL %s\\[rs]n\", pwp-E<gt>pw_name,\n"
"               (intmax_t) pwp-E<gt>pw_uid, pwp-E<gt>pw_dir, pwp-E<gt>pw_shell);\n"
"    }\n"
"    endpwent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct passwd pw;\n"
"    struct passwd *pwp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setpwent();\n"
"    while (1) {\n"
"        i = getpwent_r(&pw, buf, sizeof(buf), &pwp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd)\\etHOME %s\\etSHELL %s\\en\", pwp-E<gt>pw_name,\n"
"               (intmax_t) pwp-E<gt>pw_uid, pwp-E<gt>pw_dir, pwp-E<gt>pw_shell);\n"
"    }\n"
"    endpwent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  perhaps add error checking - should use strerror_r
#.  #include <errno.h>
#.  #include <stdlib.h>
#.          if (i) {
#.                if (i == ENOENT)
#.                      break;
#.                printf("getpwent_r: %s", strerror(i));
#.                exit(EXIT_SUCCESS);
#.          }
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<fgetpwent>(3), B<getpw>(3), B<getpwent>(3), B<getpwnam>(3), "
"B<getpwuid>(3), B<putpwent>(3), B<passwd>(5)"
msgstr ""
"B<fgetpwent>(3), B<getpw>(3), B<getpwent>(3), B<getpwnam>(3), "
"B<getpwuid>(3), B<putpwent>(3), B<passwd>(5)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are GNU extensions, done in a style resembling the POSIX "
"version of functions like B<getpwnam_r>(3).  Other systems use the prototype"
msgstr ""
"Ces fonctions sont des extensions de GNU effectuées dans un style "
"ressemblant à la version POSIX de fonctions comme B<getpwnam_r>(3). D'autres "
"systèmes utilisent le prototype"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "#define BUFLEN 4096\n"
msgstr "#define BUFLEN 4096\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct passwd pw;\n"
"    struct passwd *pwp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    struct passwd pw;\n"
"    struct passwd *pwp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    setpwent();\n"
"    while (1) {\n"
"        i = getpwent_r(&pw, buf, sizeof(buf), &pwp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd)\\etHOME %s\\etSHELL %s\\en\", pwp-E<gt>pw_name,\n"
"               (intmax_t) pwp-E<gt>pw_uid, pwp-E<gt>pw_dir, pwp-E<gt>pw_shell);\n"
"    }\n"
"    endpwent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    setpwent();\n"
"    while (1) {\n"
"        i = getpwent_r(&pw, buf, sizeof(buf), &pwp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd)\\etHOME %s\\etSHELL %s\\en\", pwp-E<gt>pw_name,\n"
"               (intmax_t) pwp-E<gt>pw_uid, pwp-E<gt>pw_dir, pwp-E<gt>pw_shell);\n"
"    }\n"
"    endpwent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct passwd pw;\n"
"    struct passwd *pwp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setpwent();\n"
"    while (1) {\n"
"        i = getpwent_r(&pw, buf, sizeof(buf), &pwp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd)\\etHOME %s\\etSHELL %s\\en\", pwp-E<gt>pw_name,\n"
"               (intmax_t) pwp-E<gt>pw_uid, pwp-E<gt>pw_dir, pwp-E<gt>pw_shell);\n"
"    }\n"
"    endpwent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#define BUFLEN 4096\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct passwd pw;\n"
"    struct passwd *pwp;\n"
"    char buf[BUFLEN];\n"
"    int i;\n"
"\\&\n"
"    setpwent();\n"
"    while (1) {\n"
"        i = getpwent_r(&pw, buf, sizeof(buf), &pwp);\n"
"        if (i)\n"
"            break;\n"
"        printf(\"%s (%jd)\\etHOME %s\\etSHELL %s\\en\", pwp-E<gt>pw_name,\n"
"               (intmax_t) pwp-E<gt>pw_uid, pwp-E<gt>pw_dir, pwp-E<gt>pw_shell);\n"
"    }\n"
"    endpwent();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
