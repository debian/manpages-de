# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2012.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-12-06 17:58+0100\n"
"PO-Revision-Date: 2023-05-08 14:42+0200\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "fenv"
msgstr "fenv"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"feclearexcept, fegetexceptflag, feraiseexcept, fesetexceptflag, "
"fetestexcept, fegetenv, fegetround, feholdexcept, fesetround, fesetenv, "
"feupdateenv, feenableexcept, fedisableexcept, fegetexcept - floating-point "
"rounding and exception handling"
msgstr ""
"feclearexcept, fegetexceptflag, feraiseexcept, fesetexceptflag, "
"fetestexcept, fegetenv, fegetround, feholdexcept, fesetround, fesetenv, "
"feupdateenv, feenableexcept, fedisableexcept, fegetexcept - Gestion des "
"exceptions et des arrondis des nombres flottants"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Bibliothèque de math (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>fenv.hE<gt>>\n"
msgstr "B<#include E<lt>fenv.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int feclearexcept(int >I<excepts>B<);>\n"
"B<int fegetexceptflag(fexcept_t *>I<flagp>B<, int >I<excepts>B<);>\n"
"B<int feraiseexcept(int >I<excepts>B<);>\n"
"B<int fesetexceptflag(const fexcept_t *>I<flagp>B<, int >I<excepts>B<);>\n"
"B<int fetestexcept(int >I<excepts>B<);>\n"
msgstr ""
"B<int feclearexcept(int >I<excepts>B<);>\n"
"B<int fegetexceptflag(fexcept_t *>I<flagp>B<, int >I<excepts>B<);>\n"
"B<int feraiseexcept(int >I<excepts>B<);>\n"
"B<int fesetexceptflag(const fexcept_t *>I<flagp>B<, int >I<excepts>B<);>\n"
"B<int fetestexcept(int >I<excepts>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int fegetround(void);>\n"
"B<int fesetround(int >I<rounding_mode>B<);>\n"
msgstr ""
"B<int fegetround(void);>\n"
"B<int fesetround(int >I<rounding_mode>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int fegetenv(fenv_t *>I<envp>B<);>\n"
"B<int feholdexcept(fenv_t *>I<envp>B<);>\n"
"B<int fesetenv(const fenv_t *>I<envp>B<);>\n"
"B<int feupdateenv(const fenv_t *>I<envp>B<);>\n"
msgstr ""
"B<int fegetenv(fenv_t *>I<envp>B<);>\n"
"B<int feholdexcept(fenv_t *>I<envp>B<);>\n"
"B<int fesetenv(const fenv_t *>I<envp>B<);>\n"
"B<int feupdateenv(const fenv_t *>I<envp>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These eleven functions were defined in C99, and describe the handling of "
"floating-point rounding and exceptions (overflow, zero-divide, etc.)."
msgstr ""
"Ces onze fonctions ont été définies dans la norme C99, et décrivent la "
"gestion des arrondis des nombres flottants et des exceptions (dépassement, "
"division par zéro, etc.) sur les nombres flottants."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Exceptions"
msgstr "Exceptions"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<divide-by-zero> exception occurs when an operation on finite numbers "
"produces infinity as exact answer."
msgstr ""
"L'exception I<divide-by-zero> (division par zéro) se produit quand une "
"opération sur des nombres finis donne un résultat infini."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<overflow> exception occurs when a result has to be represented as a "
"floating-point number, but has (much) larger absolute value than the largest "
"(finite) floating-point number that is representable."
msgstr ""
"L'exception I<overflow> (dépassement) se produit quand un résultat doit être "
"représenté par un nombre flottant, mais que sa valeur absolue est trop "
"grande pour être représentée par un nombre flottant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<underflow> exception occurs when a result has to be represented as a "
"floating-point number, but has smaller absolute value than the smallest "
"positive normalized floating-point number (and would lose much accuracy when "
"represented as a denormalized number)."
msgstr ""
"L'exception I<underflow> (soupassement) se produit quand un résultat doit "
"être représenté par un nombre flottant, mais que sa valeur absolue est trop "
"petite pour être représentée en nombre flottant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<inexact> exception occurs when the rounded result of an operation is "
"not equal to the infinite precision result.  It may occur whenever "
"I<overflow> or I<underflow> occurs."
msgstr ""
"L'exception I<inexact> se produit quand le résultat arrondi d'une opération "
"n'est pas égal au résultat en précision infinie. Elle peut se déclencher "
"quand les exceptions I<overflow> ou I<underflow> se produisent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<invalid> exception occurs when there is no well-defined result for an "
"operation, as for 0/0 or infinity - infinity or sqrt(-1)."
msgstr ""
"L'exception I<invalid> se produit quand il n'y a pas de résultat bien défini "
"pour une opération, comme «\\ 0/0\\ » ou «\\ infini-infini\\ » ou «\\ "
"sqrt(-1)\\ »."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Exception handling"
msgstr "Gestion des exceptions"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Exceptions are represented in two ways: as a single bit (exception present/"
"absent), and these bits correspond in some implementation-defined way with "
"bit positions in an integer, and also as an opaque structure that may "
"contain more information about the exception (perhaps the code address where "
"it occurred)."
msgstr ""
"Les exceptions sont représentées de deux manières\\ : en tant qu'un unique "
"bit (exception présente ou absente), et ces bits correspondent, de manière "
"dépendant de l'implémentation, avec une position au sein d'un entier, et "
"aussi en tant que structure opaque pouvant contenir plus d'informations "
"concernant l'exception (éventuellement l'adresse du code déclenchant "
"l'erreur)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each of the macros B<FE_DIVBYZERO>, B<FE_INEXACT>, B<FE_INVALID>, "
"B<FE_OVERFLOW>, B<FE_UNDERFLOW> is defined when the implementation supports "
"handling of the corresponding exception, and if so then defines the "
"corresponding bit(s), so that one can call exception handling functions, for "
"example, using the integer argument B<FE_OVERFLOW>|B<FE_UNDERFLOW>.  Other "
"exceptions may be supported.  The macro B<FE_ALL_EXCEPT> is the bitwise OR "
"of all bits corresponding to supported exceptions."
msgstr ""
"Chacune des macros B<FE_DIVBYZERO>, B<FE_INEXACT>, B<FE_INVALID>, "
"B<FE_OVERFLOW>, B<FE_UNDERFLOW> est définie lorsque l'implémentation gère "
"l'exception correspondante. Les bits sont alors définis, ainsi on peut "
"appeler, par exemple, les fonctions de gestion des exceptions avec un "
"argument entier B<FE_OVERFLOW>|B<FE_UNDERFLOW>. D'autres exceptions peuvent "
"être supportées. La macro B<FE_ALL_EXCEPT> est un masque au format OU "
"binaire correspondant à toutes les exceptions supportées."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feclearexcept>()  function clears the supported exceptions represented "
"by the bits in its argument."
msgstr ""
"La fonction B<feclearexcept>() efface les exceptions supportées représentées "
"par les bits de son argument."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fegetexceptflag>()  function stores a representation of the state of "
"the exception flags represented by the argument I<excepts> in the opaque "
"object I<*flagp>."
msgstr ""
"La fonction B<fegetexceptflag>() stocke une représentation de l'état des "
"exceptions contenues dans son argument I<excepts> dans l'objet opaque "
"I<*flagp>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feraiseexcept>()  function raises the supported exceptions represented "
"by the bits in I<excepts>."
msgstr ""
"La fonction B<feraiseexcept>() déclenche les exceptions supportées, "
"représentées par les bits de son argument I<excepts>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fesetexceptflag>()  function sets the complete status for the "
"exceptions represented by I<excepts> to the value I<*flagp>.  This value "
"must have been obtained by an earlier call of B<fegetexceptflag>()  with a "
"last argument that contained all bits in I<excepts>."
msgstr ""
"La fonction B<fesetexceptflag>() définit l'état des exceptions représentées "
"par l'argument I<excepts> à la valeur I<*flagp>. Cette valeur doit être le "
"résultat d'un appel préalable à B<fegetexceptflag>() avec un dernier "
"argument contenant tous les bits dans I<excepts>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fetestexcept>()  function returns a word in which the bits are set "
"that were set in the argument I<excepts> and for which the corresponding "
"exception is currently set."
msgstr ""
"La fonction B<fetestexcept>() renvoie un mot dont les bits définis sont "
"également les bits définis dans l'argument I<excepts> et pour lesquels "
"l'exception correspondante est définie."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Rounding mode"
msgstr "Mode d'arrondis"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The rounding mode determines how the result of floating-point operations is "
"treated when the result cannot be exactly represented in the significand.  "
"Various rounding modes may be provided: round to nearest (the default), "
"round up (toward positive infinity), round down (toward negative infinity), "
"and round toward zero."
msgstr ""
"Le mode d'arrondi détermine comment le résultat des opérations en virgule "
"flottante doit être traité quand le résultat ne peut pas être représenté "
"exactement dans la mantisse. Plusieurs modes d'arrondis peuvent être "
"fournis\\ : arrondi au plus proche (le mode par défaut), arrondi vers le "
"haut (vers l'infini positif), arrondi vers le bas (vers l'infini négatif) et "
"l'arrondi vers zéro."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each of the macros B<FE_TONEAREST>, B<FE_UPWARD>, B<FE_DOWNWARD>, and "
"B<FE_TOWARDZERO> is defined when the implementation supports getting and "
"setting the corresponding rounding direction."
msgstr ""
"Chacune des macros B<FE_TONEAREST>, B<FE_UPWARD>, B<FE_DOWNWARD> et "
"B<FE_TOWARDZERO> est définie lorsque l'implémentation gère la définition et "
"la lecture de la direction d'arrondi correspondante."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fegetround>()  function returns the macro corresponding to the current "
"rounding mode."
msgstr ""
"La fonction B<fegetround>() renvoie la macro correspondant au mode d'arrondi "
"en cours."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fesetround>()  function sets the rounding mode as specified by its "
"argument and returns zero when it was successful."
msgstr ""
"La fonction B<fesetround>() définit le mode d'arrondi tel qu'il est spécifié "
"par son argument et renvoie zéro en cas de succès."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"C99 and POSIX.1-2008 specify an identifier, B<FLT_ROUNDS>, defined in "
"I<E<lt>float.hE<gt>>, which indicates the implementation-defined rounding "
"behavior for floating-point addition.  This identifier has one of the "
"following values:"
msgstr ""
"C99 et POSIX.1-2008 spécifient un identifiant, B<FLT_ROUNDS>, défini dans "
"I<E<lt>float.hE<gt>>, qui indique le mode d'arrondi de l'implémentation pour "
"les additions en virgule flottante. Cet identifiant peut prendre une des "
"valeurs suivantes :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-1>"
msgstr "B<-1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The rounding mode is not determinable."
msgstr "Le mode d'arrondi est indéterminé."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Rounding is toward 0."
msgstr "L'arrondi se fait vers 0."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Rounding is toward nearest number."
msgstr "L'arrondi se fait vers le nombre le plus proche."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Rounding is toward positive infinity."
msgstr "L'arrondi se fait vers l'infini positif."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<3>"
msgstr "B<3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Rounding is toward negative infinity."
msgstr "L'arrondi se fait vers l'infini négatif."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Other values represent machine-dependent, nonstandard rounding modes."
msgstr ""
"Les autres valeurs sont dépendantes des machines, et ne sont pas des modes "
"d'arrondi standard."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value of B<FLT_ROUNDS> should reflect the current rounding mode as set "
"by B<fesetround>()  (but see BUGS)."
msgstr ""
"La valeur de B<FLT_ROUNDS> devrait refléter le mode d'arrondi en cours tel "
"qu'il est configuré par B<fesetround>() (mais consultez la section des "
"BOGUES)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Floating-point environment"
msgstr "Environnement de virgule flottante"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The entire floating-point environment, including control modes and status "
"flags, can be handled as one opaque object, of type I<fenv_t>.  The default "
"environment is denoted by B<FE_DFL_ENV> (of type I<const fenv_t\\ *>).  This "
"is the environment setup at program start and it is defined by ISO C to have "
"round to nearest, all exceptions cleared and a nonstop (continue on "
"exceptions) mode."
msgstr ""
"L'environnement de virgule flottante, y compris les modes de contrôle et les "
"drapeaux d'état, peuvent être manipulés sous forme d'un objet opaque de type "
"I<fenv_t>. L'environnement par défaut est représenté par B<FE_DFL_ENV> (de "
"type I<const fenv_t\\ *>). Il s'agit de la configuration de l'environnement "
"au démarrage d'un programme, et elle est définie par ISO C comme ayant un "
"arrondi au plus proche, toutes les exceptions effacées et un mode sans arrêt "
"(continuer en présence des exceptions)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fegetenv>()  function saves the current floating-point environment in "
"the object I<*envp>."
msgstr ""
"La fonction B<fegetenv>() sauve l'environnement de travail en cours en "
"virgule flottante dans l'objet I<*envp>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feholdexcept>()  function does the same, then clears all exception "
"flags, and sets a nonstop (continue on exceptions) mode, if available.  It "
"returns zero when successful."
msgstr ""
"La fonction B<feholdexcept>() effectue la même chose, puis efface tous les "
"drapeaux d'exceptions, et bascule si possible sur un mode sans arrêt "
"(continuer en présence des exceptions). Elle renvoie zéro en cas de succès."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fesetenv>()  function restores the floating-point environment from the "
"object I<*envp>.  This object must be known to be valid, for example, the "
"result of a call to B<fegetenv>()  or B<feholdexcept>()  or equal to "
"B<FE_DFL_ENV>.  This call does not raise exceptions."
msgstr ""
"La fonction B<fesetenv>() recharge l'environnement de travail en virgule "
"flottante à partir de l'objet I<*envp>. Cet objet doit être valide, c'est-à-"
"dire être le résultat d'un appel à B<fegetenv>() ou B<feholdexcept>(), ou "
"égal à B<FE_DFL_ENV>. Cet appel ne déclenche pas d'exception."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feupdateenv>()  function installs the floating-point environment "
"represented by the object I<*envp>, except that currently raised exceptions "
"are not cleared.  After calling this function, the raised exceptions will be "
"a bitwise OR of those previously set with those in I<*envp>.  As before, the "
"object I<*envp> must be known to be valid."
msgstr ""
"La fonction B<feupdateenv>() installe l'environnement de virgule flottante "
"représenté par l'objet I<*envp>, sauf que les exceptions déjà déclenchées ne "
"sont pas effacées. Après l'appel de cette fonction, les exceptions "
"déclenchées seront un OU binaire entre l'ensemble précédent, et celui "
"contenu dans I<*envp>. Comme précédemment, l'objet  I<*envp> doit être "
"valide."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#.  Earlier seven of these functions were listed as returning void.
#.  This was corrected in Corrigendum 1 (ISO/IEC 9899:1999/Cor.1:2001(E))
#.  of the C99 Standard.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions return zero on success and nonzero if an error occurred."
msgstr ""
"Ces fonctions renvoient 0 en cas de succès et une valeur non nulle en cas "
"d'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<feclearexcept>(),\n"
"B<fegetexceptflag>(),\n"
"B<feraiseexcept>(),\n"
"B<fesetexceptflag>(),\n"
"B<fetestexcept>(),\n"
"B<fegetround>(),\n"
"B<fesetround>(),\n"
"B<fegetenv>(),\n"
"B<feholdexcept>(),\n"
"B<fesetenv>(),\n"
"B<feupdateenv>(),\n"
"B<feenableexcept>(),\n"
"B<fedisableexcept>(),\n"
"B<fegetexcept>()"
msgstr ""
"B<feclearexcept>(),\n"
"B<fegetexceptflag>(),\n"
"B<feraiseexcept>(),\n"
"B<fesetexceptflag>(),\n"
"B<fetestexcept>(),\n"
"B<fegetround>(),\n"
"B<fesetround>(),\n"
"B<fegetenv>(),\n"
"B<feholdexcept>(),\n"
"B<fesetenv>(),\n"
"B<feupdateenv>(),\n"
"B<feenableexcept>(),\n"
"B<fedisableexcept>(),\n"
"B<fegetexcept>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008, IEC 60559 (IEC 559:1989), ANSI/IEEE 854."
msgstr "C11, POSIX.1-2008, IEC 60559 (IEC 559:1989), ANSI/IEEE 854."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C99, POSIX.1-2001.  glibc 2.1."
msgstr "C99, POSIX.1-2001. glibc 2.1."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "glibc notes"
msgstr "Notes de la glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If possible, the GNU C Library defines a macro B<FE_NOMASK_ENV> which "
"represents an environment where every exception raised causes a trap to "
"occur.  You can test for this macro using B<#ifdef>.  It is defined only if "
"B<_GNU_SOURCE> is defined.  The C99 standard does not define a way to set "
"individual bits in the floating-point mask, for example, to trap on specific "
"flags.  Since glibc 2.2, glibc supports the functions B<feenableexcept>()  "
"and B<fedisableexcept>()  to set individual floating-point traps, and "
"B<fegetexcept>()  to query the state."
msgstr ""
"Si possible, la bibliothèque GNU C définit une macro B<FE_NOMASK_ENV> qui "
"représente un environnement où toutes les exceptions déclenchées entraînent "
"une interception. La présence de cette macro peut être testée en utilisant "
"B<#ifdef>. Elle n'est définie que si B<_GNU_SOURCE> est définie. Le standard "
"C99 ne définit pas de méthode pour positionner les bits individuels dans le "
"masque de virgule flottante, par exemple pour intercepter des drapeaux "
"particuliers. Depuis la glibc 2.2, la glibc gère B<feenableexcept>() et "
"B<fedisableexcept>() pour définir individuellement des interceptions de "
"virgules flottantes, et B<fegetexcept>() pour demander l'état."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>fenv.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>         /* Voir feature_test_macros(7) */\n"
"B<#include E<lt>fenv.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int feenableexcept(int >I<excepts>B<);>\n"
"B<int fedisableexcept(int >I<excepts>B<);>\n"
"B<int fegetexcept(void);>\n"
msgstr ""
"B<int feenableexcept(int >I<excepts>B<);>\n"
"B<int fedisableexcept(int >I<excepts>B<);>\n"
"B<int fegetexcept(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feenableexcept>()  and B<fedisableexcept>()  functions enable "
"(disable) traps for each of the exceptions represented by I<excepts> and "
"return the previous set of enabled exceptions when successful, and -1 "
"otherwise.  The B<fegetexcept>()  function returns the set of all currently "
"enabled exceptions."
msgstr ""
"Les fonctions B<feenableexcept>() et B<fedisableexcept>() activent "
"(désactivent) les interceptions pour chaque exception représentée par "
"I<excepts> et renvoient l'ensemble précédent des exceptions activées "
"lorsqu'elles réussissent, et -1 sinon. La fonction B<fegetexcept>() renvoie "
"l'ensemble des exceptions actuellement activées."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#.  Aug 08, glibc 2.8
#.  See http://gcc.gnu.org/ml/gcc/2002-02/msg01535.html
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"C99 specifies that the value of B<FLT_ROUNDS> should reflect changes to the "
"current rounding mode, as set by B<fesetround>().  Currently, this does not "
"occur: B<FLT_ROUNDS> always has the value 1."
msgstr ""
"C99 spécifie que la valeur de B<FLT_ROUNDS> devrait refléter les changements "
"du mode d'arrondi en cours, tels qu'il est configuré par B<fesetround>(). "
"Actuellement, ce n'est pas le cas : B<FLT_ROUNDS> prend toujours la valeur 1."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<math_error>(7)"
msgstr "B<math_error>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm
msgid "These functions were added in glibc 2.1."
msgstr "Ces fonctions ont été ajoutées dans la glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "IEC 60559 (IEC 559:1989), ANSI/IEEE 854, C99, POSIX.1-2001."
msgstr "IEC 60559 (IEC 559:1989), ANSI/IEEE 854, C99, POSIX.1-2001."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
