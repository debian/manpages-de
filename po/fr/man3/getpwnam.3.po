# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-12-22 07:25+0100\n"
"PO-Revision-Date: 2023-09-11 22:54+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "getpwnam"
msgstr "getpwnam"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 juin 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "getpwnam, getpwnam_r, getpwuid, getpwuid_r - get password file entry"
msgstr ""
"getpwnam, getpwnam_r, getpwuid, getpwuid_r — Lire un enregistrement du "
"fichier des mots de passe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct passwd *getpwnam(const char *>I<name>B<);>\n"
"B<struct passwd *getpwuid(uid_t >I<uid>B<);>\n"
msgstr ""
"B<struct passwd *getpwnam(const char *>I<nom>B<);>\n"
"B<struct passwd *getpwuid(uid_t >I<uid>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getpwnam_r(const char *restrict >I<name>B<, struct passwd *restrict >I<pwd>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct passwd **restrict >I<result>B<);>\n"
"B<int getpwuid_r(uid_t >I<uid>B<, struct passwd *restrict >I<pwd>B<,>\n"
"B<               char >I<buf>B<[restrict .>I<buflen>B<], size_t >I<buflen>B<,>\n"
"B<               struct passwd **restrict >I<result>B<);>\n"
msgstr ""
"B<int getpwnam_r(const char *restrict >I<nom>B<, struct passwd *restrict >I<pwd>B<,>\n"
"B<               char >I<tampon>B<[restrict .>I<taille_tampon>B<], size_t >I<taille_tampon>B<,>\n"
"B<               struct passwd **restrict >I<result>B<);>\n"
"B<int getpwuid_r(uid_t >I<uid>B<, struct passwd *restrict >I<pwd>B<,>\n"
"B<               char >I<tampon>B<[restrict .>I<taille_tampon>B<], size_t >I<taille_tampon>B<,>\n"
"B<               struct passwd **restrict >I<result>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getpwnam_r>(), B<getpwuid_r>():"
msgstr "B<getpwnam_r>(), B<getpwuid_r>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _POSIX_C_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _POSIX_C_SOURCE\n"
"        || /* glibc E<lt>= 2.19 : */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<getpwnam>()  function returns a pointer to a structure containing the "
"broken-out fields of the record in the password database (e.g., the local "
"password file I</etc/passwd>, NIS, and LDAP)  that matches the username "
"I<name>."
msgstr ""
"La fonction B<getpwnam>() renvoie un pointeur sur une structure contenant "
"les divers champs de l'enregistrement de la base de données des mots de "
"passe (par exemple, la base de données locale I</etc/passwd>, NIS ou LDAP) "
"correspondant au nom d'utilisateur I<nom>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<getpwuid>()  function returns a pointer to a structure containing the "
"broken-out fields of the record in the password database that matches the "
"user ID I<uid>."
msgstr ""
"La fonction B<getpwuid>() renvoie un pointeur sur une structure contenant "
"les divers champs de l'enregistrement de la base de données des mots de "
"passe correspondant à l'ID utilisateur I<uid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The I<passwd> structure is defined in I<E<lt>pwd.hE<gt>> as follows:"
msgstr ""
"La structure I<passwd> est définie dans I<E<lt>pwd.hE<gt>> comme ceci\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct passwd {\n"
"    char   *pw_name;       /* username */\n"
"    char   *pw_passwd;     /* user password */\n"
"    uid_t   pw_uid;        /* user ID */\n"
"    gid_t   pw_gid;        /* group ID */\n"
"    char   *pw_gecos;      /* user information */\n"
"    char   *pw_dir;        /* home directory */\n"
"    char   *pw_shell;      /* shell program */\n"
"};\n"
msgstr ""
"struct passwd {\n"
"    char   *pw_name;       /* Nom d'utilisateur */\n"
"    char   *pw_passwd;     /* Mot de passe de l'utilisateur */\n"
"    uid_t   pw_uid;        /* ID de l'utilisateur */\n"
"    gid_t   pw_gid;        /* ID du groupe */\n"
"    char   *pw_gecos;      /* Information utilisateur */\n"
"    char   *pw_dir;        /* Répertoire personnel */\n"
"    char   *pw_shell;      /* Interpréteur de commande */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "See B<passwd>(5)  for more information about these fields."
msgstr "Consultez B<passwd>(5) pour plus d'informations sur ces champs."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<getpwnam_r>()  and B<getpwuid_r>()  functions obtain the same "
"information as B<getpwnam>()  and B<getpwuid>(), but store the retrieved "
"I<passwd> structure in the space pointed to by I<pwd>.  The string fields "
"pointed to by the members of the I<passwd> structure are stored in the "
"buffer I<buf> of size I<buflen>.  A pointer to the result (in case of "
"success) or NULL (in case no entry was found or an error occurred) is stored "
"in I<*result>."
msgstr ""
"Les fonctions B<getpwnam_r>() et B<getpwuid_r>() fournissent les mêmes "
"informations que B<getpwnam>() et B<getpwuid>() mais enregistrent la "
"structure I<passwd> trouvée dans l'espace pointé par I<pwd>. Cette structure "
"I<passwd> contient des pointeurs vers des chaînes qui sont enregistrées dans "
"le I<tampon> de taille I<taille_tampon>. Un pointeur vers le résultat (en "
"cas de succès) ou NULL (au cas où aucune entrée n'ait été trouvée ou qu'une "
"erreur soit survenue) est enregistré dans I<*result>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The call"
msgstr "L'appel"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sysconf(_SC_GETPW_R_SIZE_MAX)\n"
msgstr "sysconf(_SC_GETPW_R_SIZE_MAX)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"returns either -1, without changing I<errno>, or an initial suggested size "
"for I<buf>.  (If this size is too small, the call fails with B<ERANGE>, in "
"which case the caller can retry with a larger buffer.)"
msgstr ""
"renvoie soit B<-1> sans modifier I<errno>, soit une suggestion de taille "
"initiale pour I<buf> (si cette taille est trop petite, l'appel échoue avec "
"B<ERANGE>, auquel cas l'appelant peut réessayer avec un tampon plus grand)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<getpwnam>()  and B<getpwuid>()  functions return a pointer to a "
"I<passwd> structure, or NULL if the matching entry is not found or an error "
"occurs.  If an error occurs, I<errno> is set to indicate the error.  If one "
"wants to check I<errno> after the call, it should be set to zero before the "
"call."
msgstr ""
"Les fonctions B<getpwnam>() et B<getpwuid>() renvoient un pointeur sur une "
"structure I<passwd>, ou NULL si une erreur se produit ou si l'enregistrement "
"correspondant n'est pas trouvé. En cas d'erreur, I<errno> est définie pour "
"indiquer l'erreur. Si on souhaite vérifier I<errno> après l'appel, celle-ci "
"doit être définie à zéro avant l'appel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The return value may point to a static area, and may be overwritten by "
"subsequent calls to B<getpwent>(3), B<getpwnam>(), or B<getpwuid>().  (Do "
"not pass the returned pointer to B<free>(3).)"
msgstr ""
"La valeur de retour peut pointer vers une zone statique et donc être écrasée "
"par des appels successifs à B<getpwent>(3), B<getpwnam>() ou B<getpwuid>() "
"(ne pas passer le pointeur renvoyé à B<free>(3))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<getpwnam_r>()  and B<getpwuid_r>()  return zero, and set "
"I<*result> to I<pwd>.  If no matching password record was found, these "
"functions return 0 and store NULL in I<*result>.  In case of error, an error "
"number is returned, and NULL is stored in I<*result>."
msgstr ""
"En cas de succès, B<getpwnam_r>() et B<getpwuid_r>() renvoient B<0> et "
"définissent I<*result> à I<pwd>. Si aucun mot de passe ne correspond, ces "
"fonctions renvoient B<0> et définissent I<*result> à NULL. En cas d'erreur, "
"un code d'erreur est renvoyé et I<*result> est défini à NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<0> or B<ENOENT> or B<ESRCH> or B<EBADF> or B<EPERM> or ..."
msgstr "B<0> ou B<ENOENT> ou B<ESRCH> ou B<EBADF> ou B<EPERM> ou ..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The given I<name> or I<uid> was not found."
msgstr "Le nom I<nom> ou l'identifiant I<uid> n'ont pas été trouvés."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "A signal was caught; see B<signal>(7)."
msgstr "Un signal a été intercepté ; consultez B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I/O error."
msgstr "Erreur d'entrée-sortie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"La limite du nombre de descripteurs de fichiers par processus a été atteinte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"La limite du nombre total de fichiers ouverts pour le système entier a été "
"atteinte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#.  not in POSIX
#.  This structure is static, allocated 0 or 1 times. No memory leak. (libc45)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient memory to allocate I<passwd> structure."
msgstr "Pas assez de mémoire pour allouer la structure I<passwd>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient buffer space supplied."
msgstr "L'espace tampon fourni est insuffisant."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/passwd>"
msgstr "I</etc/passwd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "local password database file"
msgstr "Base de données locale des mots de passe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getpwnam>()"
msgstr "B<getpwnam>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:pwnam locale"
msgstr "MT-Unsafe race:pwnam locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getpwuid>()"
msgstr "B<getpwuid>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:pwuid locale"
msgstr "MT-Unsafe race:pwuid locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<getpwnam_r>(),\n"
"B<getpwuid_r>()"
msgstr ""
"B<getpwnam_r>(),\n"
"B<getpwuid_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<pw_gecos> field is not specified in POSIX, but is present on most "
"implementations."
msgstr ""
"Le champ I<pw_gecos> n'est pas spécifié dans POSIX, mais il est présent sur "
"la plupart des implémentations."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#.  more precisely:
#.  AIX 5.1 - gives ESRCH
#.  OSF1 4.0g - gives EWOULDBLOCK
#.  libc, glibc up to glibc 2.6, Irix 6.5 - give ENOENT
#.  since glibc 2.7 - give 0
#.  FreeBSD 4.8, OpenBSD 3.2, NetBSD 1.6 - give EPERM
#.  SunOS 5.8 - gives EBADF
#.  Tru64 5.1b, HP-UX-11i, SunOS 5.7 - give 0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The formulation given above under \"RETURN VALUE\" is from POSIX.1-2001.  It "
"does not call \"not found\" an error, and hence does not specify what value "
"I<errno> might have in this situation.  But that makes it impossible to "
"recognize errors.  One might argue that according to POSIX I<errno> should "
"be left unchanged if an entry is not found.  Experiments on various UNIX-"
"like systems show that lots of different values occur in this situation: 0, "
"ENOENT, EBADF, ESRCH, EWOULDBLOCK, EPERM, and probably others."
msgstr ""
"La description \"VALEUR RENVOYÉE\" ci-dessus vient de POSIX.1-2001. Elle ne "
"considère pas le cas «\\ non trouvé\\ » comme une erreur, et ne spécifie pas "
"I<errno> dans ce cas. Cela rend la détection d'erreur impossible. On peut "
"dire que, d'après POSIX, I<errno> est inchangée dans le cas où aucune entrée "
"n'est trouvée. Des essais sur de nombreux systèmes UNIX ont fait apparaître "
"différentes valeurs dans ce cas\\ : 0, ENOENT, EBADF, ESRCH, EWOULDBLOCK, "
"EPERM et probablement d'autres."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<pw_dir> field contains the name of the initial working directory of "
"the user.  Login programs use the value of this field to initialize the "
"B<HOME> environment variable for the login shell.  An application that wants "
"to determine its user's home directory should inspect the value of B<HOME> "
"(rather than the value I<getpwuid(getuid())-E<gt>pw_dir>)  since this allows "
"the user to modify their notion of \"the home directory\" during a login "
"session.  To determine the (initial) home directory of another user, it is "
"necessary to use I<getpwnam(\"username\")-E<gt>pw_dir> or similar."
msgstr ""
"Le champ I<pw_dir> contient le nom du répertoire de travail initial de "
"l'utilisateur. Les programmes de connexion («\\ login\\ ») utilisent ce "
"champ pour initialiser la variable d'environnement B<HOME> pour les "
"interpréteurs de commandes initiaux. Une application qui souhaite déterminer "
"le répertoire personnel des utilisateurs doit lire la valeur de B<HOME> (au "
"lieu de la valeur de I<getpwuid(getuid())-E<gt>pw_dir>) puisque cela permet "
"à l'utilisateur de modifier «\\ son répertoire personnel\\ » lorsqu'il est "
"connecté. Pour déterminer le répertoire personnel «\\ initial\\ » d'un autre "
"utilisateur, il est nécessaire d'utiliser I<getpwnam(\"utilisateur\")-"
"E<gt>pw_dir> ou un équivalent."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of B<getpwnam_r>()  to find the full "
"username and user ID for the username supplied as a command-line argument."
msgstr ""
"Le programme suivant est un exemple d'utilisation de B<getpwnam_r>() pour "
"trouver le nom complet et l'identifiant du nom d'utilisateur fourni en "
"paramètre."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>errno.hE<gt>\n"
#| "#include E<lt>pwd.hE<gt>\n"
#| "#include E<lt>stdint.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>unistd.hE<gt>\n"
#| "\\&\n"
#| "int\n"
#| "main(int argc, char *argv[])\n"
#| "{\n"
#| "    struct passwd pwd;\n"
#| "    struct passwd *result;\n"
#| "    char *buf;\n"
#| "    long bufsize;\n"
#| "    int s;\n"
#| "\\&\n"
#| "    if (argc != 2) {\n"
#| "        fprintf(stderr, \"Usage: %s username\\en\", argv[0]);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "\\&\n"
#| "    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
#| "    if (bufsize == -1)          /* Value was indeterminate */\n"
#| "        bufsize = 16384;        /* Should be more than enough */\n"
#| "\\&\n"
#| "    buf = malloc(bufsize);\n"
#| "    if (buf == NULL) {\n"
#| "        perror(\"malloc\");\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "\\&\n"
#| "    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
#| "    if (result == NULL) {\n"
#| "        if (s == 0)\n"
#| "            printf(\"Not found\\en\");\n"
#| "        else {\n"
#| "            errno = s;\n"
#| "            perror(\"getpwnam_r\");\n"
#| "        }\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "\\&\n"
#| "    printf(\"Name: %s; UID: %jd\\en\", pwd.pw_gecos,\n"
#| "           (intmax_t) pwd.pw_uid);\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    long bufsize;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s username\\[rs]n\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
"    if (bufsize == -1)          /* Value was indeterminate */\n"
"        bufsize = 16384;        /* Should be more than enough */\n"
"\\&\n"
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\"malloc\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
"    if (result == NULL) {\n"
"        if (s == 0)\n"
"            printf(\"Not found\\[rs]n\");\n"
"        else {\n"
"            errno = s;\n"
"            perror(\"getpwnam_r\");\n"
"        }\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"Name: %s; UID: %jd\\[rs]n\", pwd.pw_gecos,\n"
"           (intmax_t) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    long bufsize;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Utilisation : %s nom utilisateur\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
"    if (bufsize == -1)          /* Valeur indéterminée */\n"
"        bufsize = 16384;        /* Devrait largement suffire */\n"
"\\&\n"
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\\\"malloc\\\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
"    if (result == NULL) {\n"
"        if (s == 0)\n"
"            printf(\"Non trouvé\\en\");\n"
"        else {\n"
"            errno = s;\n"
"            perror(\"getpwnam_r\");\n"
"        }\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"Nom : %s; UID : %jd\\en\", pwd.pw_gecos,\n"
"           (intmax_t) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), "
"B<getpwent>(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<passwd>(5)"
msgstr ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), "
"B<getpwent>(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<passwd>(5)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NOTE"
msgstr "NOTE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The user password database mostly refers to I</etc/passwd>.  However, with "
"recent systems it also refers to network wide databases using NIS, LDAP and "
"other local files as configured in I</etc/nsswitch.conf>."
msgstr ""
"La base de données des mots de passe des utilisateurs renvoie la plupart du "
"temps à I</etc/passwd>. Cependant, sur les systèmes récents, elle renvoie "
"aussi aux bases de données sur le réseau utilisant NIS, LDAP, ou à d'autres "
"fichiers locaux comme configurés dans I</etc/nsswitch.conf>."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</etc/nsswitch.conf>"
msgstr "I</etc/nsswitch.conf>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "System Databases and Name Service Switch configuration file"
msgstr ""
"Fichier de configuration des bases de données du système et de NSS (Name "
"Service Switch)"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD.  The I<pw_gecos> field is not "
"specified in POSIX, but is present on most implementations."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD. Le champ I<pw_gecos> n'est pas "
"spécifié dans POSIX, mais il est présent sur la plupart des implémentations."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    long bufsize;\n"
"    int s;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    long bufsize;\n"
"    int s;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s username\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage : %s nom utilisateur\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
"    if (bufsize == -1)          /* Value was indeterminate */\n"
"        bufsize = 16384;        /* Should be more than enough */\n"
msgstr ""
"    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
"    if (bufsize == -1)          /* Valeur indéterminée */\n"
"        bufsize = 16384;        /* Devrait largement suffire */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\"malloc\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\"malloc\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
"    if (result == NULL) {\n"
"        if (s == 0)\n"
"            printf(\"Not found\\en\");\n"
"        else {\n"
"            errno = s;\n"
"            perror(\"getpwnam_r\");\n"
"        }\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
"    if (result == NULL) {\n"
"        if (s == 0)\n"
"            printf(\"Non trouvé\\en\");\n"
"        else {\n"
"            errno = s;\n"
"            perror(\"getpwnam_r\");\n"
"        }\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"Name: %s; UID: %jd\\en\", pwd.pw_gecos,\n"
"           (intmax_t) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    printf(\"Nom : %s; UID : %jd\\en\", pwd.pw_gecos,\n"
"           (intmax_t) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), "
"B<getpwent>(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<nsswitch."
"conf>(5), B<passwd>(5)"
msgstr ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), "
"B<getpwent>(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<nsswitch."
"conf>(5), B<passwd>(5)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    long bufsize;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s username\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
"    if (bufsize == -1)          /* Value was indeterminate */\n"
"        bufsize = 16384;        /* Should be more than enough */\n"
"\\&\n"
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\"malloc\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
"    if (result == NULL) {\n"
"        if (s == 0)\n"
"            printf(\"Not found\\en\");\n"
"        else {\n"
"            errno = s;\n"
"            perror(\"getpwnam_r\");\n"
"        }\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"Name: %s; UID: %jd\\en\", pwd.pw_gecos,\n"
"           (intmax_t) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct passwd pwd;\n"
"    struct passwd *result;\n"
"    char *buf;\n"
"    long bufsize;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Utilisation : %s nom utilisateur\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);\n"
"    if (bufsize == -1)          /* Valeur indéterminée */\n"
"        bufsize = 16384;        /* Devrait largement suffire */\n"
"\\&\n"
"    buf = malloc(bufsize);\n"
"    if (buf == NULL) {\n"
"        perror(\\\"malloc\\\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    s = getpwnam_r(argv[1], &pwd, buf, bufsize, &result);\n"
"    if (result == NULL) {\n"
"        if (s == 0)\n"
"            printf(\"Non trouvé\\en\");\n"
"        else {\n"
"            errno = s;\n"
"            perror(\"getpwnam_r\");\n"
"        }\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"Nom : %s; UID : %jd\\en\", pwd.pw_gecos,\n"
"           (intmax_t) pwd.pw_uid);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
