# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-12-06 17:55+0100\n"
"PO-Revision-Date: 2023-05-08 10:51+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "clock"
msgstr "clock"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "clock - determine processor time"
msgstr "clock - Déterminer la durée d'utilisation du processeur"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<clock_t clock(void);>\n"
msgstr "B<clock_t clock(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<clock>()  function returns an approximation of processor time used by "
"the program."
msgstr ""
"La fonction B<clock>() renvoie une durée approximative d'utilisation du "
"processeur par le programme."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value returned is the CPU time used so far as a I<clock_t>; to get the "
"number of seconds used, divide by B<CLOCKS_PER_SEC>.  If the processor time "
"used is not available or its value cannot be represented, the function "
"returns the value I<(clock_t)\\ -1>."
msgstr ""
"La valeur renvoyée est le temps CPU écoulé, en unités d'horloge I<clock_t>, "
"pour obtenir une durée en secondes, divisez-la par B<CLOCKS_PER_SEC>. Si "
"l'heure processeur n'est pas disponible, ou si sa valeur ne peut pas être "
"représentée correctement, la valeur renvoyée est I<(clock_t)\\ -1>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<clock>()"
msgstr "B<clock>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"XSI requires that B<CLOCKS_PER_SEC> equals 1000000 independent of the actual "
"resolution."
msgstr ""
"XSI demande que B<CLOCKS_PER_SEC> soit égal à 1 000 000 indépendamment de la "
"résolution réelle."

#.  I have seen this behavior on Irix 6.3, and the OSF/1, HP/UX, and
#.  Solaris manual pages say that clock() also does this on those systems.
#.  POSIX.1-2001 doesn't explicitly allow this, nor is there an
#.  explicit prohibition. -- MTK
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On several other implementations, the value returned by B<clock>()  also "
"includes the times of any children whose status has been collected via "
"B<wait>(2)  (or another wait-type call).  Linux does not include the times "
"of waited-for children in the value returned by B<clock>().  The "
"B<times>(2)  function, which explicitly returns (separate) information about "
"the caller and its children, may be preferable."
msgstr ""
"Sur plusieurs autres implémentations, la valeur renvoyée par B<clock>() "
"inclut aussi le temps écoulé par l'exécution des processus fils dont les "
"statistiques ont été collectées par B<wait>(2) (ou une fonction "
"équivalente). Linux n'inclut pas le temps des enfants attendus dans la "
"valeur renvoyée par B<clock>(). La fonction B<times>(2), qui renvoie de "
"manière explicite et distinctes les informations sur l'appelant et ses "
"enfants, peut être préférable."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89."
msgstr "POSIX.1-2001, C89."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In glibc 2.17 and earlier, B<clock>()  was implemented on top of "
"B<times>(2).  For improved accuracy, since glibc 2.18, it is implemented on "
"top of B<clock_gettime>(2)  (using the B<CLOCK_PROCESS_CPUTIME_ID> clock)."
msgstr ""
"Dans la glibc version 2.17 et antérieures, B<clock>() était construite en "
"utilisant B<times>(2). Afin d'améliorer la précision, cette fonction est "
"construite depuis la glibc 2.18 en utilisant B<clock_gettime>(2) (qui "
"utilise l'horloge B<CLOCK_PROCESS_CPUTIME_ID>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The C standard allows for arbitrary values at the start of the program; "
"subtract the value returned from a call to B<clock>()  at the start of the "
"program to get maximum portability."
msgstr ""
"Le standard C autorise une valeur quelconque d'horloge au début du "
"programme\\ ; il faut donc utiliser la différence entre la valeur actuelle "
"et celle de B<clock>() au lancement du programme pour obtenir une "
"portabilité maximale."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that the time can wrap around.  On a 32-bit system where "
"B<CLOCKS_PER_SEC> equals 1000000 this function will return the same value "
"approximately every 72 minutes."
msgstr ""
"Notez que la valeur peut revenir à zéro. Sur un système 32\\ bits, lorsque "
"B<CLOCKS_PER_SEC> vaut 1\\ 000\\ 000, cette fonction redonnera les mêmes "
"valeurs toutes les 72 minutes environ."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<clock_gettime>(2), B<getrusage>(2), B<times>(2)"
msgstr "B<clock_gettime>(2), B<getrusage>(2), B<times>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-29"
msgstr "29 décembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, C99.  XSI requires that B<CLOCKS_PER_SEC> equals "
"1000000 independent of the actual resolution."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, C99.  XSI demande que B<CLOCKS_PER_SEC> soit "
"égal à 1 000 000 indépendamment de la résolution réelle."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
