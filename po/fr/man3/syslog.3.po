# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010, 2013.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012, 2013.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Lucien Gentis <lucien.gentis@waika9.com>, 2022-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:16+0100\n"
"PO-Revision-Date: 2024-03-12 01:32+0100\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: : VIM 8.2 with GTK3 GUI\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "syslog"
msgstr "syslog"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "closelog, openlog, syslog, vsyslog - send messages to the system logger"
msgstr ""
"closelog, openlog, syslog, vsyslog - Envoyer des messages vers le "
"journaliseur du système"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>syslog.hE<gt>>\n"
msgstr "B<#include E<lt>syslog.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int >I<facility>B<);>\n"
"B<void syslog(int >I<priority>B<, const char *>I<format>B<, ...);>\n"
"B<void closelog(void);>\n"
msgstr ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int >I<type_programme>B<);>\n"
"B<void syslog(int >I<priorité>B<, const char *>I<format>B<, ...);>\n"
"B<void closelog(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<void vsyslog(int >I<priority>B<, const char *>I<format>B<, va_list >I<ap>B<);>\n"
msgstr "B<void vsyslog(int >I<priorité>B<, const char *>I<format>B<, va_list >I<liste_arguments>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<vsyslog>():"
msgstr "B<vsyslog>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.19 :\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 et antérieures :\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "openlog()"
msgstr "openlog()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<openlog>()  opens a connection to the system logger for a program."
msgstr ""
"B<openlog>() ouvre une connexion vers le journaliseur du système pour un "
"programme."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The string pointed to by I<ident> is prepended to every message, and is "
"typically set to the program name.  If I<ident> is NULL, the program name is "
"used.  (POSIX.1-2008 does not specify the behavior when I<ident> is NULL.)"
msgstr ""
"La chaîne pointée par I<ident> est ajoutée en préfixe de chaque message et "
"son contenu est en général le nom du programme. Si la valeur de I<ident> est "
"NULL, le nom du programme est utilisé (POSIX.1-2008 ne précise pas le "
"comportement si la valeur de I<ident> est NULL)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<option> argument specifies flags which control the operation of "
"B<openlog>()  and subsequent calls to B<syslog>().  The I<facility> argument "
"establishes a default to be used if none is specified in subsequent calls to "
"B<syslog>().  The values that may be specified for I<option> and I<facility> "
"are described below."
msgstr ""
"L'argument I<option> précise les attributs contrôlant le fonctionnement de "
"B<openlog>() et des appels ultérieurs à B<syslog>(). L'argument "
"I<type_programme> définit une valeur par défaut à utiliser lorsque ce "
"paramètre n'est pas fourni lors des appels ultérieurs à B<syslog>(). Les "
"valeurs pour I<option> et I<type_programme> sont décrites plus bas."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The use of B<openlog>()  is optional; it will automatically be called by "
"B<syslog>()  if necessary, in which case I<ident> will default to NULL."
msgstr ""
"L'utilisation de B<openlog>() est optionnelle. Cette fonction sera "
"automatiquement invoquée par B<syslog>() si besoin. Dans ce cas, I<ident> "
"aura la valeur NULL par défaut."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "syslog() and vsyslog()"
msgstr "syslog() et vsyslog()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<syslog>()  generates a log message, which will be distributed by "
"B<syslogd>(8)."
msgstr ""
"B<syslog>() génère un message de journalisation qui sera distribué par "
"B<syslogd>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<priority> argument is formed by ORing together a I<facility> value and "
"a I<level> value (described below).  If no I<facility> value is ORed into "
"I<priority>, then the default value set by B<openlog>()  is used, or, if "
"there was no preceding B<openlog>()  call, a default of B<LOG_USER> is "
"employed."
msgstr ""
"L'argument I<priorité> est formé en effectuant un OU binaire entre les "
"valeurs de I<type_programme> et I<niveau> (voir description plus loin). Si "
"aucune valeur de I<type_programme> n'entre dans ce OU binaire, c'est la "
"valeur par défaut définie par B<openlog>() qui sera utilisée, ou, s'il n'y a "
"pas eu d'appel précédent à B<openlog>(), c'est la valeur de B<LOG_USER> qui "
"sera utilisée par défaut."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The remaining arguments are a I<format>, as in B<printf>(3), and any "
"arguments required by the I<format>, except that the two-character sequence "
"B<%m> will be replaced by the error message string I<strerror>(I<errno>).  "
"The format string need not include a terminating newline character."
msgstr ""
"Les arguments restants sont un I<format>, comme dans B<printf>(3) et tous "
"les arguments nécessaires pour ce I<format>, sauf que la séquence de deux "
"caractères B<%m> sera remplacée par la chaîne du message d'erreur "
"I<strerror>(I<errno>). L'ajout d'un saut de ligne final à la chaîne de "
"format n'est pas nécessaire."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<vsyslog>()  performs the same task as B<syslog>()  with the "
"difference that it takes a set of arguments which have been obtained using "
"the B<stdarg>(3)  variable argument list macros."
msgstr ""
"La fonction B<vsyslog>() réalise la même tâche que B<syslog>() à la "
"différence qu'elle prend un ensemble d'arguments obtenus à l'aide des macros "
"de B<stdarg>(3) pour les listes variables d'arguments."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "closelog()"
msgstr "closelog()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<closelog>()  closes the file descriptor being used to write to the system "
"logger.  The use of B<closelog>()  is optional."
msgstr ""
"B<closelog>() ferme le descripteur de fichier utilisé pour écrire au "
"journaliseur du système. L'utilisation de B<closelog>() est facultative."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<option>"
msgstr "Valeurs pour I<option>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<option> argument to B<openlog>()  is a bit mask constructed by ORing "
"together any of the following values:"
msgstr ""
"L'argument I<option> d'B<openlog>() est un masque de bits construit à l'aide "
"d'un OU binaire entre les constantes suivantes\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CONS>"
msgstr "B<LOG_CONS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Write directly to the system console if there is an error while sending to "
"the system logger."
msgstr ""
"Écrire directement sur la console système s'il y a une erreur durant la "
"transmission vers le journaliseur du système."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NDELAY>"
msgstr "B<LOG_NDELAY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Open the connection immediately (normally, the connection is opened when the "
"first message is logged).  This may be useful, for example, if a subsequent "
"B<chroot>(2)  would make the pathname used internally by the logging "
"facility unreachable."
msgstr ""
"Ouvrir la connexion immédiatement (normalement, la connexion est ouverte "
"lors de la journalisation du premier message). Cela peut s'avérer utile, par "
"exemple, lorsqu'un appel subséquent à B<chroot>(2) rend inaccessible le nom "
"de chemin utilisé en interne par le programme à l'origine du message de "
"journalisation."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NOWAIT>"
msgstr "B<LOG_NOWAIT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Don't wait for child processes that may have been created while logging the "
"message.  (The GNU C library does not create a child process, so this option "
"has no effect on Linux.)"
msgstr ""
"Ne pas attendre la fin des processus enfants qui ont pu être créés lors de "
"l'enregistrement du message. La bibliothèque GNU C ne créant pas de "
"processus enfant, cette option est sans effet sous Linux."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ODELAY>"
msgstr "B<LOG_ODELAY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The converse of B<LOG_NDELAY>; opening of the connection is delayed until "
"B<syslog>()  is called.  (This is the default, and need not be specified.)"
msgstr ""
"L'inverse de B<LOG_NDELAY> ; l'ouverture de la connexion est repoussée "
"jusqu'à l'invocation de B<syslog>(). C'est le comportement par défaut et "
"l'option n'a donc pas besoin d'être indiquée."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_PERROR>"
msgstr "B<LOG_PERROR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(Not in POSIX.1-2001 or POSIX.1-2008.)  Also log the message to I<stderr>."
msgstr ""
"Écrire sur I<stderr> également (pas dans POSIX.1-2001, ni dans POSIX.1-2008)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_PID>"
msgstr "B<LOG_PID>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Include the caller's PID with each message."
msgstr "Inclure le PID de l'appelant dans chaque message."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<facility>"
msgstr "Valeurs pour I<type_programme>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<facility> argument is used to specify what type of program is logging "
"the message.  This lets the configuration file specify that messages from "
"different facilities will be handled differently."
msgstr ""
"L'argument I<type_programme> permet d'indiquer le type du programme qui est "
"à l'origine de la journalisation du message. Cela permet au fichier de "
"configuration de spécifier que les messages seront traités différemment en "
"fonction du type de programme qui les a émis."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_AUTH>"
msgstr "B<LOG_AUTH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "security/authorization messages"
msgstr "Messages de sécurité/autorisation."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_AUTHPRIV>"
msgstr "B<LOG_AUTHPRIV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "security/authorization messages (private)"
msgstr "Messages de sécurité/autorisation (privés)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CRON>"
msgstr "B<LOG_CRON>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "clock daemon (B<cron> and B<at>)"
msgstr "Messages de démon horaire (B<cron> et B<at>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_DAEMON>"
msgstr "B<LOG_DAEMON>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "system daemons without separate facility value"
msgstr "Messages de démons du système sans type de programme particulier."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_FTP>"
msgstr "B<LOG_FTP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ftp daemon"
msgstr "Messages de démon ftp."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_KERN>"
msgstr "B<LOG_KERN>"

#.  LOG_KERN has the value 0; if used as a facility, zero translates to:
#.  "use the default facility".
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "kernel messages (these can't be generated from user processes)"
msgstr ""
"Messages du noyau (ils ne peuvent pas être produits par des processus "
"d’utilisateur)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_LOCAL0> through B<LOG_LOCAL7>"
msgstr "B<LOG_LOCAL0> jusqu’à B<LOG_LOCAL7>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "reserved for local use"
msgstr "Réservés pour des utilisations locales."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_LPR>"
msgstr "B<LOG_LPR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "line printer subsystem"
msgstr "Messages du sous-système d'impression."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_MAIL>"
msgstr "B<LOG_MAIL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mail subsystem"
msgstr "Messages du sous-système de courrier."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NEWS>"
msgstr "B<LOG_NEWS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "USENET news subsystem"
msgstr "Messages du sous-système des nouvelles USENET."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_SYSLOG>"
msgstr "B<LOG_SYSLOG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "messages generated internally by B<syslogd>(8)"
msgstr "Messages internes de B<syslogd>(8)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_USER> (default)"
msgstr "B<LOG_USER> (défaut)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "generic user-level messages"
msgstr "Messages utilisateur génériques."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_UUCP>"
msgstr "B<LOG_UUCP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "UUCP subsystem"
msgstr "Messages du sous-système UUCP."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<level>"
msgstr "Valeurs pour I<niveau>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This determines the importance of the message.  The levels are, in order of "
"decreasing importance:"
msgstr ""
"Cela détermine l'importance du message. Les niveaux, dans l'ordre "
"d'importance décroissante, sont\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_EMERG>"
msgstr "B<LOG_EMERG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "system is unusable"
msgstr "Le système est inutilisable."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ALERT>"
msgstr "B<LOG_ALERT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "action must be taken immediately"
msgstr "Des actions doivent être entreprises immédiatement."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CRIT>"
msgstr "B<LOG_CRIT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "critical conditions"
msgstr "Les conditions sont critiques."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ERR>"
msgstr "B<LOG_ERR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "error conditions"
msgstr "Des erreurs se produisent."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_WARNING>"
msgstr "B<LOG_WARNING>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "warning conditions"
msgstr "Des avertissements se présentent."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NOTICE>"
msgstr "B<LOG_NOTICE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "normal, but significant, condition"
msgstr "Évènement normal mais important."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_INFO>"
msgstr "B<LOG_INFO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "informational message"
msgstr "Message d'information simple."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_DEBUG>"
msgstr "B<LOG_DEBUG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "debug-level message"
msgstr "Message de débogage."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function B<setlogmask>(3)  can be used to restrict logging to specified "
"levels only."
msgstr ""
"La fonction B<setlogmask>(3) permet de restreindre l'enregistrement à "
"certains niveaux uniquement."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<openlog>(),\n"
"B<closelog>()"
msgstr ""
"B<openlog>(),\n"
"B<closelog>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<syslog>(),\n"
"B<vsyslog>()"
msgstr ""
"B<syslog>(),\n"
"B<vsyslog>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe env locale"
msgstr "MT-Safe env locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<syslog>()"
msgstr "B<syslog>()"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<openlog>()"
msgstr "B<openlog>()"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<closelog>()"
msgstr "B<closelog>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<vsyslog>()"
msgstr "B<vsyslog>()"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Aucun."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "4.2BSD, SUSv2, POSIX.1-2001."
msgstr "4.2BSD, SUSv2, POSIX.1-2001."

#.  .SH HISTORY
#.  4.3BSD documents
#.  .BR setlogmask ().
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "4.3BSD, SUSv2, POSIX.1-2001."
msgstr "4.3BSD, SUSv2, POSIX.1-2001."

#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "4.3BSD-Reno."
msgstr "4.3BSD-Reno."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 specifies only the B<LOG_USER> and B<LOG_LOCAL*> values for "
"I<facility>.  However, with the exception of B<LOG_AUTHPRIV> and B<LOG_FTP>, "
"the other I<facility> values appear on most UNIX systems."
msgstr ""
"POSIX.1-2001 indique uniquement les valeurs B<LOG_USER> et B<LOG_LOCAL*> "
"pour l'argument I<type_programme>. Néanmoins, à l'exception de "
"B<LOG_AUTHPRIV> et B<LOG_FTP>, les autres valeurs pour I<type_programme> "
"sont disponibles sur la plupart des systèmes UNIX."

#. #-#-#-#-#  archlinux: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR syslog ()
#.  function call appeared in 4.2BSD.
#.  4.3BSD documents
#.  .BR openlog (),
#.  .BR syslog (),
#.  .BR closelog (),
#.  and
#.  .BR setlogmask ().
#.  4.3BSD-Reno also documents
#.  .BR vsyslog ().
#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#. #-#-#-#-#  debian-unstable: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-41: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<LOG_PERROR> value for I<option> is not specified by POSIX.1-2001 or "
"POSIX.1-2008, but is available in most versions of UNIX."
msgstr ""
"La valeur B<LOG_PERROR> pour I<option> n'est pas spécifiée par POSIX.1-2001 "
"ou POSIX.1-2008, mais elle est disponible dans la plupart des versions "
"d'UNIX."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The argument I<ident> in the call of B<openlog>()  is probably stored as-"
"is.  Thus, if the string it points to is changed, B<syslog>()  may start "
"prepending the changed string, and if the string it points to ceases to "
"exist, the results are undefined.  Most portable is to use a string constant."
msgstr ""
"Le paramètre I<ident> durant l'appel à B<openlog>() est généralement stocké "
"tel quel. Ainsi, si la chaîne vers laquelle il pointe est modifiée, "
"B<syslog>() peut préfixer la chaîne modifiée, et si la chaîne cesse "
"d'exister, le résultat est indéfini. Le comportement le plus portable est "
"l'utilisation d'une chaîne constante."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Never pass a string with user-supplied data as a format, use the following "
"instead:"
msgstr ""
"Ne jamais passer directement une chaîne formatée par l'utilisateur, utilisez "
"plutôt :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "syslog(priority, \"%s\", string);\n"
msgstr "syslog(priority, \"%s\", chaîne);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<journalctl>(1), B<logger>(1), B<setlogmask>(3), B<syslog.conf>(5), "
"B<syslogd>(8)"
msgstr ""
"B<journalctl>(1), B<logger>(1), B<setlogmask>(3), B<syslog.conf>(5), "
"B<syslogd>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The functions B<openlog>(), B<closelog>(), and B<syslog>()  (but not "
"B<vsyslog>())  are specified in SUSv2, POSIX.1-2001, and POSIX.1-2008."
msgstr ""
"Les fonctions B<openlog>(), B<closelog>() et B<syslog>() (mais pas "
"B<vsyslog>()) sont spécifiées dans SUSv2, POSIX.1-2001 et POSIX.1-2008."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
