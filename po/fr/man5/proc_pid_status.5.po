# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2024.
# Lucien Gentis <lucien.gentis@waika9.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: 2024-05-04 11:55+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Gvim\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_status"
msgstr "proc_pid_status"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/status - memory usage and status information"
msgstr "/proc/pid/status - Information d'utilisation et d'état de la mémoire"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</status>"
msgstr "I</proc/>pidI</status>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Provides much of the information in I</proc/>pidI</stat> and I</proc/>pidI</"
"statm> in a format that's easier for humans to parse.  Here's an example:"
msgstr ""
"Essentiel des informations de I</proc/>pidI</stat> et I</proc/>pidI</statm> "
"dans un format plus facile à lire pour les humains. Voici un exemple :"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< cat /proc/$$/status>\n"
"Name:   bash\n"
"Umask:  0022\n"
"State:  S (sleeping)\n"
"Tgid:   17248\n"
"Ngid:   0\n"
"Pid:    17248\n"
"PPid:   17200\n"
"TracerPid:      0\n"
"Uid:    1000    1000    1000    1000\n"
"Gid:    100     100     100     100\n"
"FDSize: 256\n"
"Groups: 16 33 100\n"
"NStgid: 17248\n"
"NSpid:  17248\n"
"NSpgid: 17248\n"
"NSsid:  17200\n"
"VmPeak:\t  131168 kB\n"
"VmSize:\t  131168 kB\n"
"VmLck:\t       0 kB\n"
"VmPin:\t       0 kB\n"
"VmHWM:\t   13484 kB\n"
"VmRSS:\t   13484 kB\n"
"RssAnon:\t   10264 kB\n"
"RssFile:\t    3220 kB\n"
"RssShmem:\t       0 kB\n"
"VmData:\t   10332 kB\n"
"VmStk:\t     136 kB\n"
"VmExe:\t     992 kB\n"
"VmLib:\t    2104 kB\n"
"VmPTE:\t      76 kB\n"
"VmPMD:\t      12 kB\n"
"VmSwap:\t       0 kB\n"
"HugetlbPages:          0 kB\t\t# 4.4\n"
"CoreDumping:\t0                       # 4.15\n"
"Threads:        1\n"
"SigQ:   0/3067\n"
"SigPnd: 0000000000000000\n"
"ShdPnd: 0000000000000000\n"
"SigBlk: 0000000000010000\n"
"SigIgn: 0000000000384004\n"
"SigCgt: 000000004b813efb\n"
"CapInh: 0000000000000000\n"
"CapPrm: 0000000000000000\n"
"CapEff: 0000000000000000\n"
"CapBnd: ffffffffffffffff\n"
"CapAmb:\t0000000000000000\n"
"NoNewPrivs:     0\n"
"Seccomp:        0\n"
"Seccomp_filters:        0\n"
"Speculation_Store_Bypass:       vulnerable\n"
"Cpus_allowed:   00000001\n"
"Cpus_allowed_list:      0\n"
"Mems_allowed:   1\n"
"Mems_allowed_list:      0\n"
"voluntary_ctxt_switches:        150\n"
"nonvoluntary_ctxt_switches:     545\n"
msgstr ""
"$B< cat /proc/$$/status>\n"
"Name:   bash\n"
"Umask:  0022\n"
"State:  S (sleeping)\n"
"Tgid:   17248\n"
"Ngid:   0\n"
"Pid:    17248\n"
"PPid:   17200\n"
"TracerPid:      0\n"
"Uid:    1000    1000    1000    1000\n"
"Gid:    100     100     100     100\n"
"FDSize: 256\n"
"Groups: 16 33 100\n"
"NStgid: 17248\n"
"NSpid:  17248\n"
"NSpgid: 17248\n"
"NSsid:  17200\n"
"VmPeak:\t  131168 kB\n"
"VmSize:\t  131168 kB\n"
"VmLck:\t       0 kB\n"
"VmPin:\t       0 kB\n"
"VmHWM:\t   13484 kB\n"
"VmRSS:\t   13484 kB\n"
"RssAnon:\t   10264 kB\n"
"RssFile:\t    3220 kB\n"
"RssShmem:\t       0 kB\n"
"VmData:\t   10332 kB\n"
"VmStk:\t     136 kB\n"
"VmExe:\t     992 kB\n"
"VmLib:\t    2104 kB\n"
"VmPTE:\t      76 kB\n"
"VmPMD:\t      12 kB\n"
"VmSwap:\t       0 kB\n"
"HugetlbPages:          0 kB\t\t# 4.4\n"
"CoreDumping:\t0                       # 4.15\n"
"Threads:        1\n"
"SigQ:   0/3067\n"
"SigPnd: 0000000000000000\n"
"ShdPnd: 0000000000000000\n"
"SigBlk: 0000000000010000\n"
"SigIgn: 0000000000384004\n"
"SigCgt: 000000004b813efb\n"
"CapInh: 0000000000000000\n"
"CapPrm: 0000000000000000\n"
"CapEff: 0000000000000000\n"
"CapBnd: ffffffffffffffff\n"
"CapAmb:\t0000000000000000\n"
"NoNewPrivs:     0\n"
"Seccomp:        0\n"
"Seccomp_filters:        0\n"
"Speculation_Store_Bypass:       vulnerable\n"
"Cpus_allowed:   00000001\n"
"Cpus_allowed_list:      0\n"
"Mems_allowed:   1\n"
"Mems_allowed_list:      0\n"
"voluntary_ctxt_switches:        150\n"
"nonvoluntary_ctxt_switches:     545\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The fields are as follows:"
msgstr "Les champs sont les suivants :"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Name>"
msgstr "I<Name>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Command run by this process.  Strings longer than B<TASK_COMM_LEN> (16) "
"characters (including the terminating null byte) are silently truncated."
msgstr ""
"Commande exécutée par ce processus. Les chaînes plus longues que "
"B<TASK_COMM_LEN> (16) caractères (incluant l’octet NULL final) sont "
"silencieusement tronquées."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Umask>"
msgstr "I<Umask>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Process umask, expressed in octal with a leading zero; see B<umask>(2).  "
"(Since Linux 4.7.)"
msgstr ""
"umask du processus, exprimé en octal avec un zéro en tête (depuis "
"Linux 4.7.). Consulter B<umask>(2)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<State>"
msgstr "I<State>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Current state of the process.  One of \"R (running)\", \"S (sleeping)\", \"D "
"(disk sleep)\", \"T (stopped)\", \"t (tracing stop)\", \"Z (zombie)\", or "
"\"X (dead)\"."
msgstr ""
"État actuel du processus parmi les valeurs : « R (running) », « S "
"(sleeping) », « D (disk sleep) », « T (stopped) », « T (tracing stop) », « Z "
"(zombie) » ou « X (dead) »."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Tgid>"
msgstr "I<Tgid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Thread group ID (i.e., Process ID)."
msgstr "ID du groupe de threads (c'est-à-dire ID du processus)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Ngid>"
msgstr "I<Ngid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "NUMA group ID (0 if none; since Linux 3.13)."
msgstr "ID de groupe NUMA (0 si absent ; depuis Linux 3.13)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Pid>"
msgstr "I<Pid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Thread ID (see B<gettid>(2))."
msgstr "ID de thread (consulter B<gettid>(2))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<PPid>"
msgstr "I<PPid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "PID of parent process."
msgstr "PID du processus parent."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<TracerPid>"
msgstr "I<TracerPid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "PID of process tracing this process (0 if not being traced)."
msgstr "PID du processus traçant ce processus (0 s'il n'est pas tracé)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Uid>"
msgstr "I<Uid>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Gid>"
msgstr "I<Gid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Real, effective, saved set, and filesystem UIDs (GIDs)."
msgstr ""
"Ensemble des UID (GID) réel, effectif, sauvé et de système de fichiers."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<FDSize>"
msgstr "I<FDSize>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Number of file descriptor slots currently allocated."
msgstr "Nombre de slots de descripteurs de fichier actuellement alloués."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Groups>"
msgstr "I<Groups>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Supplementary group list."
msgstr "Liste des groupes supplémentaires."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<NStgid>"
msgstr "I<NStgid>"

#.  commit e4bc33245124db69b74a6d853ac76c2976f472d5
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Thread group ID (i.e., PID) in each of the PID namespaces of which I<pid> is "
"a member.  The leftmost entry shows the value with respect to the PID "
"namespace of the process that mounted this procfs (or the root namespace if "
"mounted by the kernel), followed by the value in successively nested inner "
"namespaces.  (Since Linux 4.1.)"
msgstr ""
"ID de groupe de threads (c’est-à-dire PID) dans chacun des espaces de noms "
"PID dont I<pid> est membre. L’enregistrement le plus à gauche affiche la "
"valeur selon l’espace de noms PID du processus ayant monté ce procfs (ou "
"l’espace de noms root si monté par le noyau), suivi de la valeur dans les "
"espaces de noms imbriqués successifs (depuis Linux 4.1)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<NSpid>"
msgstr "I<NSpid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Thread ID in each of the PID namespaces of which I<pid> is a member.  The "
"fields are ordered as for I<NStgid>.  (Since Linux 4.1.)"
msgstr ""
"ID de thread dans chaque espace de noms PID dont I<pid> est membre. Les "
"champs sont ordonnés comme pour I<NStgid> (depuis Linux 4.1)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<NSpgid>"
msgstr "I<NSpgid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Process group ID in each of the PID namespaces of which I<pid> is a member.  "
"The fields are ordered as for I<NStgid>.  (Since Linux 4.1.)"
msgstr ""
"ID de groupe de processus dans chaque espace de noms PID dont I<pid> est "
"membre. Les champs sont ordonnés comme pour I<NStgid> (depuis Linux 4.1)x"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<NSsid>"
msgstr "I<NSsid>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"descendant namespace session ID hierarchy Session ID in each of the PID "
"namespaces of which I<pid> is a member.  The fields are ordered as for "
"I<NStgid>.  (Since Linux 4.1.)"
msgstr ""
"ID de session dans chaque espace de noms PID dont I<pid> est membre. Les "
"champs sont ordonnés comme pour I<NStgid> (depuis Linux 4.1.)"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmPeak>"
msgstr "I<VmPeak>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Peak virtual memory size."
msgstr "Taille de pic de mémoire virtuelle."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmSize>"
msgstr "I<VmSize>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Virtual memory size."
msgstr "Taille de la mémoire virtuelle."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmLck>"
msgstr "I<VmLck>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Locked memory size (see B<mlock>(2))."
msgstr "Taille de mémoire verrouillée (consulter B<mlock>(3))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmPin>"
msgstr "I<VmPin>"

#.  commit bc3e53f682d93df677dbd5006a404722b3adfe18
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Pinned memory size (since Linux 3.2).  These are pages that can't be moved "
"because something needs to directly access physical memory."
msgstr ""
"Taille de mémoire épinglée (depuis Linux 3.2). Ce sont des pages qui ne "
"peuvent être déplacées parce que quelque chose a besoin d’accéder "
"directement à la mémoire physique."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmHWM>"
msgstr "I<VmHWM>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Peak resident set size (\"high water mark\").  This value is inaccurate; see "
"I</proc/>pidI</statm> above."
msgstr ""
"Taille de pic d’ensemble résident (« high water mark »). Cette valeur est "
"imprécise, consulter I</proc/>pidI</statm> ci-dessus."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmRSS>"
msgstr "I<VmRSS>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Resident set size.  Note that the value here is the sum of I<RssAnon>, "
"I<RssFile>, and I<RssShmem>.  This value is inaccurate; see I</proc/>pidI</"
"statm> above."
msgstr ""
"Taille d’ensemble résident. Il est à remarquer que cette valeur ici est la "
"somme de I<RssAnon>, I<RssFile> et I<RssShmem>. Cette valeur est imprécise, "
"consulter I</proc/>pidI</statm> ci-dessus."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<RssAnon>"
msgstr "I<RssAnon>"

#.  commit bf9683d6990589390b5178dafe8fd06808869293
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Size of resident anonymous memory.  (since Linux 4.5).  This value is "
"inaccurate; see I</proc/>pidI</statm> above."
msgstr ""
"Taille de la mémoire anonyme résidente (depuis Linux 4.5). Cette valeur est "
"imprécise, consulter I</proc/>pidI</statm> ci-dessus."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<RssFile>"
msgstr "I<RssFile>"

#.  commit bf9683d6990589390b5178dafe8fd06808869293
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Size of resident file mappings.  (since Linux 4.5).  This value is "
"inaccurate; see I</proc/>pidI</statm> above."
msgstr ""
"Taille des mappages résidents de fichier (depuis Linux 4.5). Cette valeur "
"est imprécise, consulter I</proc/>pidI</statm> ci-dessus."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<RssShmem>"
msgstr "I<RssShmem>"

#.  commit bf9683d6990589390b5178dafe8fd06808869293
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Size of resident shared memory (includes System V shared memory, mappings "
"from B<tmpfs>(5), and shared anonymous mappings).  (since Linux 4.5)."
msgstr ""
"Taille de la mémoire résidente partagée (inclut la mémoire partagée de "
"System V, les mappages de B<tmpfs>(5) et les mappages partagés anonymes) "
"(depuis Linux 4.5)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmData>"
msgstr "I<VmData>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmStk>"
msgstr "I<VmStk>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmExe>"
msgstr "I<VmExe>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Size of data, stack, and text segments.  This value is inaccurate; see I</"
"proc/>pidI</statm> above."
msgstr ""
"Taille des données, de pile et des segments de texte. Cette valeur est "
"imprécise, consulter I</proc/>pidI</statm> ci-dessus."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmLib>"
msgstr "I<VmLib>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Shared library code size."
msgstr "Taille du code de bibliothèque partagée."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmPTE>"
msgstr "I<VmPTE>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Page table entries size (since Linux 2.6.10)."
msgstr "Taille des enregistrements de table de pages (depuis Linux 2.6.10)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmPMD>"
msgstr "I<VmPMD>"

#.  commit dc6c9a35b66b520cf67e05d8ca60ebecad3b0479
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Size of second-level page tables (added in Linux 4.0; removed in Linux 4.15)."
msgstr ""
"Taille des tables de pages de second niveau (ajouté dans Linux 4.0, retiré "
"dans Linux 4.15)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<VmSwap>"
msgstr "I<VmSwap>"

#.  commit b084d4353ff99d824d3bc5a5c2c22c70b1fba722
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Swapped-out virtual memory size by anonymous private pages; shmem swap usage "
"is not included (since Linux 2.6.34).  This value is inaccurate; see I</proc/"
">pidI</statm> above."
msgstr ""
"Taille de mémoire virtuelle mise en espace d’échange par des pages privées "
"anonymes. L’utilisation d’espace d’échange n’est pas incluse (depuis "
"Linux 2.6.34). Cette valeur est imprécise, consulter I</proc/>pidI</statm> "
"ci-dessus."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<HugetlbPages>"
msgstr "I<HugetlbPages>"

#.  commit 5d317b2b6536592a9b51fe65faed43d65ca9158e
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Size of hugetlb memory portions (since Linux 4.4)."
msgstr "Taille des sections de mémoire hugetlb (depuis Linux 4.4)."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<CoreDumping>"
msgstr "I<CoreDumping>"

#.  commit c643401218be0f4ab3522e0c0a63016596d6e9ca
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Contains the value 1 if the process is currently dumping core, and 0 if it "
"is not (since Linux 4.15).  This information can be used by a monitoring "
"process to avoid killing a process that is currently dumping core, which "
"could result in a corrupted core dump file."
msgstr ""
"Ce champ contient la valeur 1 si le processus réalise actuellement un cliché "
"du système et 0 dans le cas contraire (depuis Linux 4.15). Cette information "
"peut être utilisée par un processus de supervision pour éviter de tuer un "
"processus réalisant actuellement un cliché, ce qui pourrait aboutir à la "
"corruption du fichier de cliché."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Threads>"
msgstr "I<Threads>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Number of threads in process containing this thread."
msgstr "Nombre de threads dans le processus contenant ce thread."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<SigQ>"
msgstr "I<SigQ>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This field contains two slash-separated numbers that relate to queued "
"signals for the real user ID of this process.  The first of these is the "
"number of currently queued signals for this real user ID, and the second is "
"the resource limit on the number of queued signals for this process (see the "
"description of B<RLIMIT_SIGPENDING> in B<getrlimit>(2))."
msgstr ""
"Ce champ contient deux nombres séparés par une barre oblique, qui se "
"réfèrent aux signaux en attente pour l'identifiant d'utilisateur réel de ce "
"processus. Le premier est le nombre de signaux en attente pour cet "
"identifiant d'utilisateur réel ; le second est la limite du nombre de "
"signaux pouvant être mis en attente pour ce processus (consulter la "
"description de B<RLIMIT_SIGPENDING> dans B<getrlimit>(2))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<SigPnd>"
msgstr "I<SigPnd>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<ShdPnd>"
msgstr "I<ShdPnd>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mask (expressed in hexadecimal)  of signals pending for thread and for "
"process as a whole (see B<pthreads>(7)  and B<signal>(7))."
msgstr ""
"Masque (en hexadécimal) de signaux en attente pour le thread et pour le "
"processus dans son ensemble (consulter B<pthreads>(7) et B<signal>(7))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<SigBlk>"
msgstr "I<SigBlk>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<SigIgn>"
msgstr "I<SigIgn>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<SigCgt>"
msgstr "I<SigCgt>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Masks (expressed in hexadecimal)  indicating signals being blocked, ignored, "
"and caught (see B<signal>(7))."
msgstr ""
"Masques (en hexadécimal) indiquant les signaux bloqués, ignorés et "
"interceptés (consulter B<signal>(7))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<CapInh>"
msgstr "I<CapInh>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<CapPrm>"
msgstr "I<CapPrm>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<CapEff>"
msgstr "I<CapEff>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Masks (expressed in hexadecimal)  of capabilities enabled in inheritable, "
"permitted, and effective sets (see B<capabilities>(7))."
msgstr ""
"Masques (en hexadécimal) des capacités actives dans les ensembles héritables "
"permis et effectifs (consulter B<capabilities>(7))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<CapBnd>"
msgstr "I<CapBnd>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Capability bounding set, expressed in hexadecimal (since Linux 2.6.26, see "
"B<capabilities>(7))."
msgstr ""
"Ensemble des limitations de capacité en hexadécimal (depuis Linux 2.6.26, "
"consulter B<capabilities>(7))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<CapAmb>"
msgstr "I<CapAmb>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Ambient capability set, expressed in hexadecimal (since Linux 4.3, see "
"B<capabilities>(7))."
msgstr ""
"Ensemble ambiant des capacités en hexadécimal (depuis Linux 4.3, consulter "
"B<capabilities>(7))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<NoNewPrivs>"
msgstr "I<NoNewPrivs>"

#.  commit af884cd4a5ae62fcf5e321fecf0ec1014730353d
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Value of the I<no_new_privs> bit (since Linux 4.10, see B<prctl>(2))."
msgstr ""
"Valeur du bit I<no_new_privs> (depuis Linux 4.10, consulter B<prctl>(2))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Seccomp>"
msgstr "I<Seccomp>"

#.  commit 2f4b3bf6b2318cfaa177ec5a802f4d8d6afbd816
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Seccomp mode of the process (since Linux 3.8, see B<seccomp>(2)).  0 means "
"B<SECCOMP_MODE_DISABLED>; 1 means B<SECCOMP_MODE_STRICT>; 2 means "
"B<SECCOMP_MODE_FILTER>.  This field is provided only if the kernel was built "
"with the B<CONFIG_SECCOMP> kernel configuration option enabled."
msgstr ""
"Mode seccomp du processus (depuis Linux 3.8, consulter B<seccomp>(2)). 0 "
"signifie B<SECCOMP_MODE_DISABLED>, 1 B<SECCOMP_MODE_STRICT>, 2 "
"B<SECCOMP_MODE_FILTER>. Ce champ est fourni seulement si le noyau a été "
"construit avec l’option de configuration B<CONFIG_SECCOMP> activée."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Seccomp_filters>"
msgstr "I<Seccomp_filters>"

#.  commit c818c03b661cd769e035e41673d5543ba2ebda64
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Number of seccomp filters attached to the process (since Linux 5.9, see "
"B<seccomp>(2))."
msgstr ""
"Nombre de filtres seccomp attachés au processus (depuis Linux 5.9, consultez "
"B<seccomp>(2))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Speculation_Store_Bypass>"
msgstr "I<Speculation_Store_Bypass>"

#.  commit fae1fa0fc6cca8beee3ab8ed71d54f9a78fa3f64
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Speculation flaw mitigation state (since Linux 4.17, see B<prctl>(2))."
msgstr ""
"État de mitigation du défaut « Speculation » (depuis Linux 4.17, consulter "
"B<prctl>(2))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Cpus_allowed>"
msgstr "I<Cpus_allowed>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Hexadecimal mask of CPUs on which this process may run (since Linux 2.6.24, "
"see B<cpuset>(7))."
msgstr ""
"Masque en hexadécimal des processeurs sur lesquels le processus peut "
"s'exécuter (depuis Linux 2.6.24, consulter B<cpuset>(7))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Cpus_allowed_list>"
msgstr "I<Cpus_allowed_list>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Same as previous, but in \"list format\" (since Linux 2.6.26, see "
"B<cpuset>(7))."
msgstr ""
"Pareil que précédemment, mais au format liste (depuis Linux 2.6.26, "
"consulter B<cpuset>(7))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Mems_allowed>"
msgstr "I<Mems_allowed>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mask of memory nodes allowed to this process (since Linux 2.6.24, see "
"B<cpuset>(7))."
msgstr ""
"Masque des nœuds mémoire autorisés pour ce processus (depuis Linux 2.6.24, "
"consulter B<cpuset>(7))."

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<Mems_allowed_list>"
msgstr "I<Mems_allowed_list>"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<voluntary_ctxt_switches>"
msgstr "I<voluntary_ctxt_switches>"

#. type: TQ
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<nonvoluntary_ctxt_switches>"
msgstr "I<nonvoluntary_ctxt_switches>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Number of voluntary and involuntary context switches (since Linux 2.6.23)."
msgstr ""
"Nombre de basculements de contexte, volontaires ou non (depuis Linux 2.6.23)."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-23"
msgstr "23 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
