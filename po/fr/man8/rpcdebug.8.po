# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Sylvain Cherrier <sylvain.cherrier@free.fr>, 2006, 2007, 2008, 2009.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2007.
# Dominique Simen <dominiquesimen@hotmail.com>, 2009.
# Nicolas Sauzède <nsauzede@free.fr>, 2009.
# Romain Doumenc <rd6137@gmail.com>, 2010, 2011.
# David Prévot <david@tilapin.org>, 2011, 2012, 2014.
# Denis Mugnier <myou72@orange.fr>, 2011.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: nfs-utils\n"
"POT-Creation-Date: 2024-12-22 07:37+0100\n"
"PO-Revision-Date: 2013-05-30 17:56+0200\n"
"Last-Translator: Cédric Boutillier <boutil@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "rpcdebug"
msgstr "rpcdebug"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "5 Jul 2006"
msgstr "5 juillet 2006"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "rpcdebug - set and clear NFS and RPC kernel debug flags"
msgstr ""
"rpcdebug - Activer et supprimer les drapeaux de débogage du noyau pour NFS "
"et RPC"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rpcdebug> B<-vh>"
msgstr "B<rpcdebug> B<-vh>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rpcdebug> B<-m> I<module>"
msgstr "B<rpcdebug> B<-m> I<module>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rpcdebug> B<-m> I<module> B<-s> I<flags>..."
msgstr "B<rpcdebug> B<-m> I<module> B<-s> I<flags>..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rpcdebug> B<-m> I<module> B<-c> I<flags>..."
msgstr "B<rpcdebug> B<-m> I<module> B<-c> I<flags>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<rpcdebug> command allows an administrator to set and clear the Linux "
"kernel's NFS client and server debug flags.  Setting these flags causes the "
"kernel to emit messages to the system log in response to NFS activity; this "
"is typically useful when debugging NFS problems."
msgstr ""
"La commande B<rpcdebug> permet à l'administrateur d'activer ou supprimer les "
"drapeaux de débogage du noyau du serveur et du client NFS. La définition de "
"ces drapeaux entraîne l'émission par le noyau de messages à destination du "
"système de journalisation (« logs ») en rapport avec l'activité NFS. Ceci "
"est très pratique lors du débogage de problèmes NFS."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The first form in the synopsis can be used to list all available debug "
"flags.  The second form shows the currently set debug flags for the given "
"module.  The third form sets one or more flags, and the fourth form clears "
"one or more flags."
msgstr ""
"La première forme du synopsis peut servir à lister l'ensemble des drapeaux "
"de débogage disponible. La deuxième forme affiche les drapeaux de débogage "
"actuellement définis pour le module indiqué. La troisième forme définit un "
"ou plusieurs drapeaux, et la quatrième supprime un ou plusieurs drapeaux."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value B<all> may be used to set or clear all the flags for the given "
"module."
msgstr ""
"La valeur B<all> peut servir à activer ou supprimer tous les drapeaux du "
"module indiqué."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>"
msgstr "B<-c>"

#.  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Clear the given debug flags."
msgstr "Supprimer les drapeaux de débogage indiqués."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#.  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Print a help message and exit.  When combined with the B<-v> option, also "
"prints the available debug flags."
msgstr ""
"Afficher un message d'aide et terminer. Si combinée à l'option B<-v>, "
"afficher aussi les drapeaux de débogage disponibles."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>I< module>"
msgstr "B<-m>I< module>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Specify which module's flags to set or clear.  Available modules are:"
msgstr ""
"Définir les drapeaux de modules à activer ou à supprimer. Les modules "
"disponibles sont :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<nfsd>"
msgstr "B<nfsd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The NFS server."
msgstr "Le serveur NFS."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<nfs>"
msgstr "B<nfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The NFS client."
msgstr "Le client NFS."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<nlm>"
msgstr "B<nlm>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The Network Lock Manager, in either an NFS client or server."
msgstr ""
"Le gestionnaire de verrous réseau (« Network Lock Manager »), que ce soit "
"pour le client ou le serveur NFS."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<rpc>"
msgstr "B<rpc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The Remote Procedure Call module, in either an NFS client or server."
msgstr ""
"Le module appel de procédures distantes (« Remote Procedure Call »), que ce "
"soit pour le client ou le serveur NFS."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#.  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Set the given debug flags."
msgstr "Activer les drapeaux de débogage indiqués."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#.  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Increase the verbosity of B<rpcdebug>'s output."
msgstr "Augmenter la verbosité de la sortie de B<rpcdebug>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B</proc/sys/sunrpc/{rpc,nfs,nfsd,nlm}_debug>"
msgstr "B</proc/sys/sunrpc/{rpc,nfs,nfsd,nlm}_debug>"

#.  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "procfs-based interface to kernel debug flags."
msgstr ""
"Interface vers les drapeaux de débogage du noyau basé sur le système de "
"fichiers procfs."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#.  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<rpc.nfsd>(8), B<nfs>(5), B<syslogd>(8)."
msgstr "B<rpc.nfsd>(8), B<nfs>(5), B<syslogd>(8)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#.  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Bugs can be found or reported at B<http://nfs.sf.net/>."
msgstr ""
"Les bogues peuvent être consultés ou signalés sur B<http://nfs.sf.net/>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Program by Olaf Kirch E<lt>okir@suse.deE<gt> and E<lt>frederic.jolly@bull."
"ext.netE<gt>.  Manpage by Greg Banks E<lt>gnb@melbourne.sgi.comE<gt>."
msgstr ""
"Le programme est d'Olaf Kirch E<lt>okir@suse.deE<gt> et de E<lt>frederic."
"jolly@bull.ext.netE<gt>. La page de manuel par Greg Banks E<lt>gnb@melbourne."
"sgi.comE<gt>."
