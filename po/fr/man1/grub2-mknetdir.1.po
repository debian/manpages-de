# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.0\n"
"POT-Creation-Date: 2024-12-06 18:00+0100\n"
"PO-Revision-Date: 2024-12-08 14:23+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKNETDIR"
msgstr "GRUB-MKNETDIR"

#. type: TH
#: fedora-41 mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "October 2024"
msgstr "Octobre 2024"

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.12"
msgstr "GRUB 2.12"

#. type: TH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "grub-mknetdir - prepare a GRUB netboot directory."
msgstr "grub-mknetdir – Préparer un répertoire d'amorçage réseau pour GRUB"

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-mknetdir> [I<\\,OPTION\\/>...]"
msgstr "B<grub-mknetdir> [I<\\,OPTION\\/>...]"

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Prepares GRUB network boot images at net_directory/subdir assuming "
"net_directory being TFTP root."
msgstr ""
"Préparer les images d'amorçage réseau de GRUB dans I<répertoire_réseau/sous-"
"répertoire> en présumant que I<répertoire_réseau> est le répertoire racine "
"TFTP."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--appended-signature-size>=I<\\,SIZE\\/>"
msgstr "B<--appended-signature-size>=I<\\,TAILLE\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Add a note segment reserving SIZE bytes for an appended signature"
msgstr ""
"Ajouter une partie I<NOTE> réservant I<TAILLE> octets pour l'ajout d'une "
"signature."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--compress>=I<\\,no\\/>|xz|gz|lzo"
msgstr "B<--compress>=I<\\,no\\/>|xz|gz|lzo"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "compress GRUB files [optional]"
msgstr "compresser les fichiers GRUB [optionnel]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "disable shim_lock verifier"
msgstr "Désactiver le vérificateur de shim_lock."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--dtb>=I<\\,FILE\\/>"
msgstr "B<--dtb>=I<\\,FICHIER\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed a specific DTB"
msgstr "intégrer un DTB spécifique"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,RÉP\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"Utiliser les images et les modules dans I<RÉP> [/usr/lib/grub/"
"E<lt>platformE<gt> par défaut]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--fonts>=I<\\,FONTS\\/>"
msgstr "B<--fonts>=I<\\,FONTES\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install FONTS [default=unicode]"
msgstr "Installer des FONTES [unicode par défaut]."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--install-modules>=I<\\,MODULES\\/>"
msgstr "B<--install-modules>=I<\\,MODULES\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install only MODULES and their dependencies [default=all]"
msgstr ""
"Installer seulement les I<MODULES> et leurs dépendances [tous par défaut]."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,FICHIER\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed FILE as public key for signature checking"
msgstr ""
"Intégrer I<FICHIER> comme clé publique pour la vérification de signature."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--locale-directory>=I<\\,DIR\\/> use translations under DIR"
msgstr "B<--locale-directory>=I<\\,RÉP\\/> Utiliser les traductions PRÉSENTES dans I<RÉP>."

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[default=/usr/share/locale]"
msgstr "[/usr/share/locale par défaut]"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--locales>=I<\\,LOCALES\\/>"
msgstr "B<--locales>=I<\\,LOCALES\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install only LOCALES [default=all]"
msgstr "Installer seulement les I<LOCALES> [toutes par défaut]."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--modules>=I<\\,MODULES\\/>"
msgstr "B<--modules>=I<\\,MODULES\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "pre-load specified modules MODULES"
msgstr "précharger les MODULES indiqués"

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--sbat>=I<\\,FILE\\/>"
msgstr "B<--sbat>=I<\\,FICHIER\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "SBAT metadata"
msgstr "Métadonnées SBAT."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--themes>=I<\\,THEMES\\/>"
msgstr "B<--themes>=I<\\,THÈMES\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install THEMES [default=starfield]"
msgstr "Installer les I<THÈMES> [starfield par défaut]."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "Afficher des messages détaillés."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--x509key>=I<\\,FILE\\/>"
msgstr "B<-x>, B<--x509key>=I<\\,FICHIER\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed FILE as an x509 certificate for signature checking"
msgstr ""
"Intégrer I<FICHIER> comme certificat x509 pour la vérification de la "
"signature."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--core-compress>=I<\\,xz\\/>|none|auto"
msgstr "B<--core-compress>=I<\\,xz\\/>|none|auto"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "choose the compression to use for core image"
msgstr "Choisir le type de compression à utiliser pour l'image essentielle."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--net-directory>=I<\\,DIR\\/>"
msgstr "B<--net-directory>=I<\\,RÉP\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "root directory of TFTP server"
msgstr "Répertoire racine du serveur TFPT."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--subdir>=I<\\,DIR\\/>"
msgstr "B<--subdir>=I<\\,RÉP\\/>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "relative subdirectory on network server"
msgstr "Sous-répertoire relatif sur le serveur du réseau."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "Afficher l’aide-mémoire."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "Afficher un court message pour l’utilisation."

#. type: TP
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print program version"
msgstr "Afficher la version du programme."

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Les paramètres obligatoires ou facultatifs pour les options de forme longue "
"le sont aussi pour les options correspondantes de forme courte."

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Signaler toute erreur à E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-mkimage>(1)"
msgstr "B<grub-mkimage>(1)"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mknetdir> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mknetdir> programs are properly installed "
"at your site, the command"
msgstr ""
"La documentation complète de B<grub-mknetdir> est disponible dans un manuel "
"Texinfo. Si les programmes B<info> et B<grub-mknetdir> sont correctement "
"installés, la commande"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<info grub-mknetdir>"
msgstr "B<info grub-mknetdir>"

#. type: Plain text
#: fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "devrait vous donner accès au manuel complet."

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "November 2024"
msgstr "Novembre 2004"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use images and modules under DIR [default=/usr/share/grub2/"
"E<lt>platformE<gt>]"
msgstr ""
"Utiliser les images et les modules dans RÉP [/usr/share/grub2/"
"E<lt>platformE<gt>]"

#. type: TP
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-cli>"
msgstr "B<--disable-cli>"

#. type: Plain text
#: opensuse-tumbleweed
msgid "disabled command line interface access"
msgstr "Désactiver l'accès à l'interface en ligne de commande."
