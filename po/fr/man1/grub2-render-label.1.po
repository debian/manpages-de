# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-06 18:00+0100\n"
"PO-Revision-Date: 2024-06-01 19:01+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB-RENDER-LABEL"
msgstr "GRUB-RENDER-LABEL"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "October 2024"
msgstr "Octobre 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GRUB 2.12"
msgstr "GRUB 2.12"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "grub-render-label - generate a .disk_label for Apple Macs."
msgstr "grub-render-label - Créer un .disk_label pour les Mac d'Apple"

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<grub-render-label> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-render-label> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Render Apple .disk_label."
msgstr "Produire un .disk_label pour Apple."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--bgcolor>=I<\\,COLOR\\/>"
msgstr "B<-b>, B<--bgcolor>=I<\\,COULEUR\\/>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use COLOR for background"
msgstr "Utiliser I<COULEUR> pour l'arrière-plan."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--color>=I<\\,COLOR\\/>"
msgstr "B<-c>, B<--color>=I<\\,COULEUR\\/>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use COLOR for text"
msgstr "Utiliser I<COULEUR> pour le texte."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--font>=I<\\,FILE\\/>"
msgstr "B<-f>, B<--font>=I<\\,FICHIER\\/>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use FILE as font (PF2)."
msgstr "Utiliser I<FICHIER> comme fonte (PF2)."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--input>=I<\\,FILE\\/>"
msgstr "B<-i>, B<--input>=I<\\,FICHIER\\/>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "read text from FILE."
msgstr "Lire le texte à partir de I<FICHIER>."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FICHIER\\/>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "set output filename. Default is STDOUT"
msgstr "Déterminer le fichier de sortie. Par défaut la sortie standard."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--text>=I<\\,STRING\\/>"
msgstr "B<-t>, B<--text>=I<\\,CHAÎNE\\/>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "set the label to render"
msgstr "Définir l'étiquette à afficher."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print verbose messages."
msgstr "Afficher des messages détaillés."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "give this help list"
msgstr "Afficher l’aide-mémoire."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "give a short usage message"
msgstr "Afficher un court message pour l’utilisation."

#. type: TP
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print program version"
msgstr "Afficher la version du programme."

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Les paramètres obligatoires ou facultatifs pour les options de forme longue "
"le sont aussi pour les options correspondantes de forme courte."

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Signaler toute erreur à E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-render-label> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-render-label> programs are properly "
"installed at your site, the command"
msgstr ""
"La documentation complète de B<grub-render-label> est disponible dans un "
"manuel Texinfo. Si les programmes B<info> et B<grub-render-label> sont "
"correctement installés, la commande"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info grub-render-label>"
msgstr "B<info grub-render-label>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "devrait vous donner accès au manuel complet."

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "November 2024"
msgstr "Novembre 2004"
