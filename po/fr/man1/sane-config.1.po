# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-12-22 07:38+0100\n"
"PO-Revision-Date: 2024-03-07 00:05+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "sane-config"
msgstr "sane-config"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "10 Jul 2008"
msgstr "10 juillet 2008"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux mageia-cauldron
msgid "sane-config - get information about the installed version of libsane"
msgstr "sane-config — Informations sur la version installée de libsane"

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<sane-config [--prefix] [--exec-prefix] [--libs] [--cflags] [--ldflags] [--"
"version] [--help >I<[OPTION]>B<]>"
msgstr ""
"B<sane-config [--prefix] [--exec-prefix] [--libs] [--cflags] [--ldflags] [--"
"version] [--help >I<[OPTION]>B<]>"

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<sane-config> is a tool that is used to determine the compiler and linker "
"flags that should be used to compile and link SANE frontends to a SANE "
"backend library (libsane)."
msgstr ""
"B<sane-config> est un outil servant à déterminer les drapeaux du compilateur "
"et de l'éditeur de lien qui doivent être utilisés pour compiler et éditer "
"les liens des frontaux de SANE avec une bibliothèque de dorsal de SANE "
"(libsane)."

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<sane-config> accepts the following options (you can't use more than one "
"option at the same time):"
msgstr ""
"B<sane-config> accepte les options suivantes (une seule option peut être "
"utilisée à la fois) :"

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Print the currently installed version of libsane on the standard output."
msgstr ""
"Afficher sur la sortie standard la version de libsane installée actuellement."

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<--help OPTION>"
msgstr "B<--help OPTION>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Print a short usage message. If I<OPTION> is specified, help for that option "
"(e.g.  B<--libs>)  is printed (if available)."
msgstr ""
"Afficher un court message d'aide. Si une I<OPTION> est indiquée, c'est une "
"aide pour cette option (par exemple B<--libs>) qui est affichée (si elle est "
"disponible)."

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<--libs>"
msgstr "B<--libs>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Print the additional libraries that are necessary to link a SANE frontend to "
"libsane."
msgstr ""
"Afficher les bibliothèques supplémentaires nécessaires pour lier un frontal "
"de SANE à libsane."

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<--ldflags>"
msgstr "B<--ldflags>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Print the linker flags that are necessary to link a SANE frontend to libsane."
msgstr ""
"Afficher les drapeaux de l'éditeur de lien nécessaires pour lier un frontal "
"de SANE à libsane."

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<--cflags>"
msgstr "B<--cflags>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Print the compiler flags that are necessary to compile a SANE frontend."
msgstr ""
"Afficher les drapeaux du compilateur nécessaires pour compiler un frontal de "
"SANE."

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<--prefix>"
msgstr "B<--prefix>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Print the prefix used during compilation of libsane."
msgstr "Afficher le préfixe utilisé pendant la compilation de libsane."

#. type: TP
#: archlinux mageia-cauldron
#, no-wrap
msgid "B<--exec-prefix>"
msgstr "B<--exec-prefix>"

#. type: Plain text
#: archlinux mageia-cauldron
msgid "Print the exec-prefix used during compilation of libsane."
msgstr ""
"Afficher le préfixe d'exécution utilisé pendant la compilation de libsane."

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux mageia-cauldron
msgid "B<sane>(7)"
msgstr "B<sane>(7)"

#. type: SH
#: archlinux mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"This manual page was written by Julien BLACHE E<lt>I<jblache@debian."
"org>E<gt>I<,> for the Debian GNU/Linux system (but may be used by others)."
msgstr ""
"Cette page de manuel a été écrite par Julien BLACHE E<lt>I<jblache@debian."
"org>E<gt>I<,> pour le système Debian GNU/Linux (mais peut être utilisée par "
"d'autres)."
