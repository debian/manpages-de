# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-12-06 18:05+0100\n"
"PO-Revision-Date: 2023-08-24 07:38+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "mmd"
msgstr "mmd"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "21Nov24"
msgstr "21 novembre 2024"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "mtools-4.0.46"
msgstr "mtools-4.0.46"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nom"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mmd - make an MSDOS subdirectory"
msgstr "mmd – Créer un sous-répertoire MS-DOS"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "Note d'avertissement"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Cette page de manuel a été automatiquement générée depuis la documentation "
"texinfo de mtools, et pourrait ne pas être complètement fidèle ou complète. "
"Voir la fin de cette page de manuel pour les détails."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Description"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The \\&CW<mmd> command is used to make an MS-DOS subdirectory. Its syntax is:"
msgstr ""
"La commande \\&CW<mmd> est utilisée pour créer un sous-répertoire MS-DOS. Sa "
"syntaxe est la suivante :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\&\\&CW<mmd> [\\&CW<-D> I<clash_option>] I<msdosdirectory> "
"[ \\&I<msdosdirectories>\\&... ]"
msgstr ""
"\\&\\&CW<mmd> [\\&CW<-D> I<clash_option>] I<répertoire_msdos> "
"[ \\&I<répertoires_msdos>\\&... ]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\&\\&CW<Mmd> makes a new directory on an MS-DOS file system. An error "
"occurs if the directory already exists."
msgstr ""
"\\&\\&CW<Mmd> crée un nouveau répertoire dans un système de fichiers MS-DOS. "
"Une erreur survient si le répertoire existe déjà."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr "Voir aussi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Documentation texinfo de mtools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "Visualisation de la documentation\\ texi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Cette page de manuel a été générée automatiquement depuis la documentation "
"texinfo de mtools. Cependant, ce mécanisme n'est qu'approximatif et quelques "
"points, tels que les références croisées, les notes de bas de page et les "
"index sont perdus lors de ce processus de conversion. En effet, ces éléments "
"n'ont pas de représentation appropriée dans le format des pages de manuel. "
"De plus, toutes les informations n'ont pas été importées dans les pages de "
"manuel. Il est donc fortement recommandé d’utiliser la documentation texinfo "
"originale. Voir la fin de cette page de manuel pour des instructions sur "
"comment visualiser la documentation texinfo."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Pour générer une copie imprimable depuis la documentation texinfo, exécuter "
"les commandes suivantes :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Pour générer une copie HTML, exécuter :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"\\&Un fichier HTML préconstruit peut être trouvé sur la page \\%http://www."
"gnu.org/software/mtools/manual/mtools.html"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Pour générer une copie info (navigable avec le mode info d’Emacs), exécuter :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"La documentation texinfo a un meilleur rendu lorsqu'elle est imprimée ou "
"visualisée en HTML. En effet, dans la version info, certains exemples sont "
"difficiles à lire à cause des conventions d'échappement utilisées dans info."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10Jul21"
msgstr "10 juillet 2021"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

#. type: TH
#: debian-unstable opensuse-leap-16-0
#, no-wrap
msgid "21Mar23"
msgstr "21 mars 2023"

#. type: TH
#: debian-unstable opensuse-leap-16-0
#, no-wrap
msgid "mtools-4.0.43"
msgstr "mtools-4.0.43"

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "28Sep24"
msgstr "28Sep24"

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.45"
msgstr "mtools-4.0.45"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "02Jun24"
msgstr "2 juin 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "mtools-4.0.44"
msgstr "mtools-4.0.44"
