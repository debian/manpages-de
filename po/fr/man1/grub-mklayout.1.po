# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-10-04 17:51+0200\n"
"PO-Revision-Date: 2023-10-07 06:53+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MKLAYOUT"
msgstr "GRUB-MKLAYOUT"

#. type: TH
#: archlinux
#, no-wrap
msgid "September 2024"
msgstr "Septembre 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-3"
msgstr "GRUB 2:2.12-3"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-mklayout - generate a GRUB keyboard layout file"
msgstr "grub-mklayout – Générer un fichier de disposition de clavier pour GRUB"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-mklayout> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-mklayout> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"grub-mklayout processes a keyboard layout description in B<keymaps>(5)  "
"format into a format that can be used by GRUB's B<keymap> command."
msgstr ""
"grub-mklayout transforme une description de disposition de clavier au format "
"de B<keymaps>(5) en un format qui peut être utilisé par la commande "
"B<keymap> de GRUB."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Generate GRUB keyboard layout from Linux console one."
msgstr ""
"Générer une disposition de clavier pour GRUB à partir de celle d'une console "
"Linux."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-i>, B<--input>=I<\\,FILE\\/>"
msgstr "B<-i>, B<--input>=I<\\,FICHIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set input filename. Default is STDIN"
msgstr ""
"Définir le nom du fichier d'entrée. Par défaut c'est l'entrée standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FICHIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set output filename. Default is STDOUT"
msgstr "Déterminer le fichier de sortie. Par défaut la sortie standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "Afficher des messages détaillés."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "Afficher l’aide-mémoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "Afficher un court message pour l’utilisation."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "Afficher la version du programme."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Les paramètres obligatoires ou facultatifs pour les options de forme longue "
"le sont aussi pour les options correspondantes de forme courte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Signaler toute erreur à E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-mklayout> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mklayout> programs are properly installed "
"at your site, the command"
msgstr ""
"La documentation complète de B<grub-mklayout> est disponible dans un manuel "
"Texinfo. Si les programmes B<info> et B<grub-mklayout> sont correctement "
"installés, la commande"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-mklayout>"
msgstr "B<info grub-mklayout>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "devrait vous donner accès au manuel complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2024"
msgstr "Février 2024"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.12-1~bpo12+1"
msgstr "GRUB 2.12-1~bpo12+1"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2024"
msgstr "Juillet 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-5"
msgstr "GRUB 2.12-5"
