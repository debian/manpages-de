# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2024-03-29 09:57+0100\n"
"PO-Revision-Date: 2022-08-20 12:07+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "WHEREIS"
msgstr "WHEREIS"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm
msgid ""
"whereis - locate the binary, source, and manual page files for a command"
msgstr ""
"whereis - Rechercher les fichiers exécutables, les sources et les pages de "
"manuel d'une commande"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm
msgid "B<whereis> [options] [B<-BMS> I<directory>... B<-f>] I<name>..."
msgstr "B<whereis> [I<options>] [B<-BMS> I<répertoire> ... B<-f>] I<nom> ..."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<whereis> locates the binary, source and manual files for the specified "
"command names. The supplied names are first stripped of leading pathname "
"components. Prefixes of B<s.> resulting from use of source code control are "
"also dealt with. B<whereis> then attempts to locate the desired program in "
"the standard Linux places, and in the places specified by B<$PATH> and "
"B<$MANPATH>."
msgstr ""
"B<whereis> recherche les fichiers binaires, de source et de manuel pour les "
"I<nom>s de commande indiqués. Les noms des fichiers sont obtenus en "
"supprimant le chemin d'accès. Les préfixes de la forme B<s.> utilisés pour "
"le contrôle du code source sont également pris en charge. B<whereis> "
"recherche le programme demandé aux endroits normalisés de Linux et aux "
"endroits indiqués par B<$PATH> et B<$MANPATH>."

#. type: Plain text
#: debian-bookworm
msgid ""
"The search restrictions (options B<-b>, B<-m> and B<-s>) are cumulative and "
"apply to the subsequent I<name> patterns on the command line. Any new search "
"restriction resets the search mask. For example,"
msgstr ""
"Les restrictions de recherche (option B<-b>, B<-m> et B<-s>) sont "
"cumulatives et s’appliquent aux motifs I<nom> suivants sur la ligne de "
"commande. Toute nouvelle restriction de recherche réinitialise le masque de "
"recherche. Par exemple,"

#. type: Plain text
#: debian-bookworm
msgid "B<whereis -bm ls tr -m gcc>"
msgstr "B<whereis -bm ls tr -m gcc>"

#. type: Plain text
#: debian-bookworm
msgid ""
"searches for \"ls\" and \"tr\" binaries and man pages, and for \"gcc\" man "
"pages only."
msgstr ""
"recherche les binaires et pages de manuel pour « ls » et « tr », mais "
"seulement les pages de manuel pour « gcc »."

#. type: Plain text
#: debian-bookworm
msgid ""
"The options B<-B>, B<-M> and B<-S> reset search paths for the subsequent "
"I<name> patterns. For example,"
msgstr ""
"Les options B<-B>, B<-M> et B<-S> réinitialisent les chemins de recherche "
"pour les motifs I<nom> suivants. Par exemple,"

#. type: Plain text
#: debian-bookworm
msgid "B<whereis -m ls -M /usr/share/man/man1 -f cal>"
msgstr "B<whereis -m ls -M /usr/share/man/man1 -f cal>"

# NOTE: s/searchs/searches/
#. type: Plain text
#: debian-bookworm
msgid ""
"searches for \"B<ls>\" man pages in all default paths, but for \"cal\" in "
"the I</usr/share/man/man1> directory only."
msgstr ""
"recherche les pages de manuel « B<ls> » dans tous les chemins par défaut, "
"mais seulement dans le répertoire I</usr/share/man/man1> pour « cal »."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: debian-bookworm
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: debian-bookworm
msgid "Search for binaries."
msgstr "Chercher les exécutables."

#. type: Plain text
#: debian-bookworm
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: debian-bookworm
msgid "Search for manuals."
msgstr "Chercher les manuels."

#. type: Plain text
#: debian-bookworm
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: debian-bookworm
msgid "Search for sources."
msgstr "Chercher les sources."

#. type: Plain text
#: debian-bookworm
msgid "B<-u>"
msgstr "B<-u>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Only show the command names that have unusual entries. A command is said to "
"be unusual if it does not have just one entry of each explicitly requested "
"type. Thus \\(aqB<whereis -m -u *>\\(aq asks for those files in the current "
"directory which have no documentation file, or more than one."
msgstr ""
"Ne montrer que les noms de commande qui ont des entrées inhabituelles. Une "
"commande est dite inhabituelle si elle n’a pas une seule entrée pour chaque "
"type demandé explicitement. Ainsi, « B<whereis -m -u *> » recherche les "
"fichiers du répertoire actuel qui n'ont soit pas de fichier de "
"documentation, soit plus d’un."

#. type: Plain text
#: debian-bookworm
msgid "B<-B> I<list>"
msgstr "B<-B> I<liste>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the places where B<whereis> searches for binaries, by a whitespace-"
"separated list of directories."
msgstr ""
"Limiter les répertoires où B<whereis> cherche les exécutables, à l’aide "
"d’une I<liste> de répertoires séparés par des espaces."

#. type: Plain text
#: debian-bookworm
msgid "B<-M> I<list>"
msgstr "B<-M> I<liste>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the places where B<whereis> searches for manuals and documentation in "
"Info format, by a whitespace-separated list of directories."
msgstr ""
"Limiter les répertoires où B<whereis> cherche les manuels et la "
"documentation au format Info, à l’aide d’une I<liste> de répertoires séparés "
"par des espaces."

#. type: Plain text
#: debian-bookworm
msgid "B<-S> I<list>"
msgstr "B<-S> I<liste>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the places where B<whereis> searches for sources, by a whitespace-"
"separated list of directories."
msgstr ""
"Limiter les répertoires où B<whereis> cherche les sources, à l’aide d’une "
"I<liste> de répertoires séparés par des espaces."

#. type: Plain text
#: debian-bookworm
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Terminates the directory list and signals the start of filenames. It I<must> "
"be used when any of the B<-B>, B<-M>, or B<-S> options is used."
msgstr ""
"Terminer la liste de répertoires et signaler le début des noms de fichiers. "
"Cette option est B<obligatoire> quand une des options B<-B>, B<-M> ou B<-S> "
"est utilisée."

#. type: Plain text
#: debian-bookworm
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Output the list of effective lookup paths that B<whereis> is using. When "
"none of B<-B>, B<-M>, or B<-S> is specified, the option will output the hard-"
"coded paths that the command was able to find on the system."
msgstr ""
"Afficher la liste des chemins effectifs de recherche que B<whereis> utilise. "
"Si ni B<-B>, ni B<-M> ni B<-S> ne sont indiquées, les chemins codés en dur "
"que la commande a pu trouver sur le système seront affichés."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Afficher la version puis quitter."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILE SEARCH PATHS"
msgstr "CHEMINS DE RECHERCHE DE FICHIERS"

#. type: Plain text
#: debian-bookworm
msgid ""
"By default B<whereis> tries to find files from hard-coded paths, which are "
"defined with glob patterns. The command attempts to use the contents of "
"B<$PATH> and B<$MANPATH> environment variables as default search path. The "
"easiest way to know what paths are in use is to add the B<-l> listing "
"option. Effects of the B<-B>, B<-M>, and B<-S> are displayed with B<-l>."
msgstr ""
"Par défaut, B<whereis> essaye de trouver les fichiers définis avec des "
"motifs joker, dans les chemins codés en dur. La commande tente d’utiliser le "
"contenu des variables d’environnement B<$PATH> et B<$MANPATH> comme chemins "
"de recherche par défaut. La façon la plus facile de connaître les chemins "
"utilisés est d’ajouter l’option d’affichage B<-l>. Les effets de B<-B>, B<-"
"M> et B<-S> sont affichés avec B<-l>."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENVIRONNEMENT"

#. type: Plain text
#: debian-bookworm
msgid "B<WHEREIS_DEBUG>=all"
msgstr "B<WHEREIS_DEBUG=all>"

#. type: Plain text
#: debian-bookworm
msgid "enables debug output."
msgstr "Activer la sortie de débogage."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: debian-bookworm
msgid ""
"To find all files in I</usr/bin> which are not documented in I</usr/man/"
"man1> or have no source in I</usr/src>:"
msgstr ""
"Pour trouver tous les fichiers dans I</usr/bin> non documentés dans I</usr/"
"man/man1> ou sans source dans I</usr/src> :"

#. type: Plain text
#: debian-bookworm
msgid "B<cd /usr/bin> B<whereis -u -ms -M /usr/man/man1 -S /usr/src -f *>"
msgstr "B<cd /usr/bin> B<whereis -u -ms -M /usr/man/man1 -S /usr/src -f *>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<whereis> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<whereis> fait partie du paquet util-linux qui peut être "
"téléchargé de"
