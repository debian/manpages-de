# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Rafael Fontenelle <rafaelff@gnome.org>, 2020-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.1.0\n"
"POT-Creation-Date: 2024-10-04 17:57+0200\n"
"PO-Revision-Date: 2024-09-25 14:50-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 46.1\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PACTREE"
msgstr "PACTREE"

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-08-24"
msgstr "24 agosto 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib 1\\&.10\\&.6"
msgstr "Pacman-contrib 1\\&.10\\&.6"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib Manual"
msgstr "Manual do pacman-contrib"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux
msgid "pactree - package dependency tree viewer"
msgstr "pactree - visualizador da árvore de dependências de pacote"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux
msgid "I<pactree> [options] E<lt>packageE<gt>"
msgstr "I<pactree> [opções] E<lt>pacoteE<gt>"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux
msgid "I<pactree> produces a dependency tree for a package\\&."
msgstr "I<pactree> produz uma árvore de dependências para um pacote\\&."

#. type: Plain text
#: archlinux
msgid ""
"By default, a tree-like output is generated, but with the I<--graph> option, "
"a Graphviz description is generated\\&."
msgstr ""
"Por padrão, uma saída em formato de árvore é gerada, mas com a opção I<--"
"graph>, uma descrição Graphviz é gerada\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPÇÕES"

#. type: Plain text
#: archlinux
msgid "B<-a, --ascii>"
msgstr "B<-a, --ascii>"

#. type: Plain text
#: archlinux
msgid ""
"Use ASCII characters for tree formatting\\&. By default, pactree will use "
"Unicode line drawing characters if it is able to detect that the locale "
"supports them\\&."
msgstr ""
"Usa caracteres ASCII para formatação de árvore\\&. Por padrão, pactree usará "
"caracteres de desenho de linha Unicode se for capaz de detectar que o local "
"os suporta\\&."

#. type: Plain text
#: archlinux
msgid "B<-c, --color>"
msgstr "B<-c, --color>"

#. type: Plain text
#: archlinux
msgid "Colorize output\\&."
msgstr "Colore a saída\\&."

#. type: Plain text
#: archlinux
msgid "B<--config E<lt>fileE<gt>>"
msgstr "B<--config E<lt>arquivoE<gt>>"

#. type: Plain text
#: archlinux
msgid "Specify an alternate pacman configuration file\\&."
msgstr "Especifica um arquivo de configuração do pacman alternativo\\&."

#. type: Plain text
#: archlinux
msgid "B<-b, --dbpath>"
msgstr "B<-b, --dbpath>"

#. type: Plain text
#: archlinux
msgid "Specify an alternative database location\\&."
msgstr "Especifica um local de base de dados alternativo\\&."

#. type: Plain text
#: archlinux
msgid "B<--debug>"
msgstr "B<--debug>"

#. type: Plain text
#: archlinux
msgid "Print log messages produced by libalpm\\&."
msgstr "Exibe as mensagens de log produzidas por libalpm\\&."

#. type: Plain text
#: archlinux
msgid "B<-d, --depth E<lt>numE<gt>>"
msgstr "B<-d, --depth E<lt>númeroE<gt>>"

#. type: Plain text
#: archlinux
msgid ""
"Limits the number of levels of dependency to show\\&. A zero means show the "
"named package only, one shows the packages that are directly required\\&."
msgstr ""
"Limita o número de níveis de dependência para mostrar\\&. Um zero significa "
"mostrar apenas o pacote nomeado, um mostra os pacotes que são diretamente "
"necessários\\&."

#. type: Plain text
#: archlinux
msgid "B<--gpgdir E<lt>dirE<gt>>"
msgstr "B<--gpgdir E<lt>dirE<gt>>"

#. type: Plain text
#: archlinux
msgid ""
"Specify an alternate GnuPG directory for verifying database signatures "
"(default is /etc/pacman\\&.d/gnupg)\\&."
msgstr ""
"Especifica um diretório GnuPG alternativo para verificar as assinaturas da "
"base de dados (o padrão é /etc/pacman\\&.d/gnupg)\\&."

#. type: Plain text
#: archlinux
msgid "B<-g, --graph>"
msgstr "B<-g, --graph>"

#. type: Plain text
#: archlinux
msgid ""
"Generate a Graphviz description\\&. If this option is given, the I<--color> "
"and I<--linear> options are ignored\\&."
msgstr ""
"Gera uma descrição do Graphviz\\&. Se esta opção for fornecida, as opções "
"I<--color> e I<--linear> são ignoradas\\&."

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: archlinux
msgid "Display syntax and command-line options\\&."
msgstr "Exibe a sintaxe e as opções de linha de comando\\&."

#. type: Plain text
#: archlinux
msgid "B<-l, --linear>"
msgstr "B<-l, --linear>"

#. type: Plain text
#: archlinux
msgid "Prints package names at the start of each line, one per line\\&."
msgstr "Exibe os nomes de pacotes no início de cada linha, um por linha\\&."

#. type: Plain text
#: archlinux
msgid "B<-o, --optional[=depth]>"
msgstr "B<-o, --optional[=profundidade]>"

#. type: Plain text
#: archlinux
msgid ""
"Additionally prints optional dependencies up to a certain depth, default 1 "
"for immediate optional dependencies\\&. When used in conjunction with I<-r> "
"it shows which packages it is optional for\\&. In Graphviz mode, produce "
"dotted lines\\&. Negative values mean infinite depth\\&."
msgstr ""
"Além disso, exibe as dependências opcionais até uma certa profundidade, "
"padrão 1 para dependências opcionais imediatas\\&. Quando usado em conjunto "
"com I<-r>, mostra para quais pacotes são opcionais\\&. No modo Graphviz, "
"produz linhas pontilhadas\\&. Valores negativos significam profundidade "
"infinita\\&."

#. type: Plain text
#: archlinux
msgid "B<-r, --reverse>"
msgstr "B<-r, --reverse>"

#. type: Plain text
#: archlinux
msgid "Show packages that depend on the named package\\&."
msgstr "Mostra os pacotes que dependem do pacote nomeado\\&."

#. type: Plain text
#: archlinux
msgid "B<-s, --sync>"
msgstr "B<-s, --sync>"

#. type: Plain text
#: archlinux
msgid "Read package data from sync databases instead of local database\\&."
msgstr ""
"Lê os dados do pacote de bases de dados de sincronização em vez da base de "
"dados local\\&."

#. type: Plain text
#: archlinux
msgid "B<-u, --unique>"
msgstr "B<-u, --unique>"

#. type: Plain text
#: archlinux
msgid "List dependent packages once\\&. Implies I<--linear>\\&."
msgstr "Lista os pacotes dependentes uma vez\\&. Implica em I<--linear>\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux
msgid "B<pacman>(8), B<pacman.conf>(5), B<makepkg>(8)"
msgstr "B<pacman>(8), B<pacman.conf>(5), B<makepkg>(8)"

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux
msgid ""
"Bugs? You must be kidding; there are no bugs in this software\\&. But if we "
"happen to be wrong, file an issue with as much detail as possible at https://"
"gitlab\\&.archlinux\\&.org/pacman/pacman-contrib/-/issues/new\\&."
msgstr ""
"Bugs? Você deve estar brincando; não há erros neste software\\&. Mas se por "
"acaso estivermos errados, envie um relatório de erro com o máximo de "
"detalhes possível para https://gitlab\\&.archlinux\\&.org/pacman/pacman-"
"contrib/-/issues/new\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORES"

#. type: Plain text
#: archlinux
msgid "Current maintainers:"
msgstr "Atuais mantenedores:"

#. type: Plain text
#: archlinux
msgid "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"
msgstr "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"

#. type: Plain text
#: archlinux
msgid "Daniel M\\&. Capella E<lt>polyzen@archlinux\\&.orgE<gt>"
msgstr "Daniel M\\&. Capella E<lt>polyzen@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the pacman-contrib\\&."
"git repository\\&."
msgstr ""
"Para contribuidores adicionais, use git shortlog -s no repositório do pacman-"
"contrib\\&.git\\&."
