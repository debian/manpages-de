# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-29 09:44+0100\n"
"PO-Revision-Date: 2022-06-27 21:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LSIPC"
msgstr "LSIPC"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Команди користувача"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm
msgid ""
"lsipc - show information on IPC facilities currently employed in the system"
msgstr "lsipc — виведення даних щодо можливостей IPC, які залучено у системі"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm
msgid "B<lsipc> [options]"
msgstr "B<lsipc> [параметри]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lsipc> shows information on the System V inter-process communication "
"facilities for which the calling process has read access."
msgstr ""
"B<lsipc> виводить дані щодо можливостей взаємодії між процесорами у System "
"V, до читання яких має доступ процес, яким викликано програму."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm
msgid "B<-i>, B<--id> I<id>"
msgstr "B<-i>, B<--id> I<ідентифікатор>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Show full details on just the one resource element identified by I<id>. This "
"option needs to be combined with one of the three resource options: B<-m>, "
"B<-q> or B<-s>. It is possible to override the default output format for "
"this option with the B<--list>, B<--raw>, B<--json> or B<--export> option."
msgstr ""
"Показати подробиці щодо лише одного елемента ресурсів, який вказано за "
"допомогою аргументу I<ідентифікатор>. Цей параметр має бути поєднано із "
"одним з трьох параметрів ресурсів: B<-m>, B<-q> або B<-s>. Можна "
"перевизначити типовий формат виведення для цього параметра за допомогою "
"параметра B<--list>, B<--raw>, B<--json> або B<--export>."

#. type: Plain text
#: debian-bookworm
msgid "B<-g>, B<--global>"
msgstr "B<-g>, B<--global>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Show system-wide usage and limits of IPC resources. This option may be "
"combined with one of the three resource options: B<-m>, B<-q> or B<-s>. The "
"default is to show information about all resources."
msgstr ""
"Показати загальносистемне використання і обмеження для ресурсів IPC. Цей "
"параметр можна поєднати із одним із трьох параметрів ресурсів: B<-m>, B<-q> "
"або B<-s>. Типово буде показано відомості щодо усіх ресурсів."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Resource options"
msgstr "Параметри ресурсів"

#. type: Plain text
#: debian-bookworm
msgid "B<-m>, B<--shmems>"
msgstr "B<-m>, B<--shmems>"

#. type: Plain text
#: debian-bookworm
msgid "Write information about active shared memory segments."
msgstr ""
"Записати відомості щодо активних сегментів пам'яті спільного використання."

#. type: Plain text
#: debian-bookworm
msgid "B<-q>, B<--queues>"
msgstr "B<-q>, B<--queues>"

#. type: Plain text
#: debian-bookworm
msgid "Write information about active message queues."
msgstr "Записати відомості щодо активних черг повідомлень."

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--semaphores>"
msgstr "B<-s>, B<--semaphores>"

#. type: Plain text
#: debian-bookworm
msgid "Write information about active semaphore sets."
msgstr "Записати відомості щодо активних наборів семафорів."

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Output formatting"
msgstr "Форматування виводу"

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--creator>"
msgstr "B<-c>, B<--creator>"

#. type: Plain text
#: debian-bookworm
msgid "Show creator and owner."
msgstr "Показати автора і власника."

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--export>"
msgstr "B<-e>, B<--export>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"value characters are hex-escaped (\\(rsxE<lt>codeE<gt>). See also option B<--"
"shell>."
msgstr ""
"Вивести дані у формі пар ключ=\"значення\". Усі потенційно небезпечні "
"символи значень буде екрановано (\\(rsxE<lt>кодE<gt>). Див. також параметр "
"B<--shell>."

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr "B<-J>, B<--json>"

#. type: Plain text
#: debian-bookworm
msgid "Use the JSON output format."
msgstr "Скористатися форматом виведення JSON."

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Use the list output format. This is the default, except when B<--id> is used."
msgstr ""
"Скористатися форматом виведення списком. Це типовий формат, якщо не вказано "
"B<--id>."

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--newline>"
msgstr "B<-n>, B<--newline>"

#. type: Plain text
#: debian-bookworm
msgid "Display each piece of information on a separate line."
msgstr "Вивести кожен з фрагментів даних у окремому рядку."

#. type: Plain text
#: debian-bookworm
msgid "B<--noheadings>"
msgstr "B<--noheadings>"

#. type: Plain text
#: debian-bookworm
msgid "Do not print a header line."
msgstr "Не виводити рядок заголовка."

#. type: Plain text
#: debian-bookworm
msgid "B<--notruncate>"
msgstr "B<--notruncate>"

#. type: Plain text
#: debian-bookworm
msgid "Don\\(cqt truncate output."
msgstr "Не обрізати виведені дані."

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<список>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns."
msgstr ""
"Визначити, які стовпчики слід використовувати для виведення. Скористайтеся "
"параметром B<--help>, щоб переглянути список підтримуваних стовпчиків."

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: debian-bookworm
msgid "Print size in bytes rather than in human readable format."
msgstr "Вивести розмір у байтах, а не у зручному для читання форматі."

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--raw>"
msgstr "B<-r>, B<--raw>"

#. type: Plain text
#: debian-bookworm
msgid "Raw output (no columnation)."
msgstr "Виведення без обробки (без поділу на стовпчики)."

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--time>"
msgstr "B<-t>, B<--time>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Write time information. The time of the last control operation that changed "
"the access permissions for all facilities, the time of the last B<msgsnd>(2) "
"and B<msgrcv>(2) operations on message queues, the time of the last "
"B<shmat>(2) and B<shmdt>(2) operations on shared memory, and the time of the "
"last B<semop>(2) operation on semaphores."
msgstr ""
"Записати відомості щодо часу. Час останньої дії з керування, яка вносила "
"зміни до прав доступу для усіх можливостей, час останніх дій B<msgsnd>(2) "
"або B<msgrcv>(2) щодо черг повідомлень, час останніх дій B<shmat>(2) і "
"B<shmdt>(2) над пам'яттю спільного використання та час останньої дії "
"B<semop>(2) над семафорами."

#. type: Plain text
#: debian-bookworm
msgid "B<--time-format> I<type>"
msgstr "B<--time-format> I<тип>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Display dates in short, full or iso format. The default is short, this time "
"format is designed to be space efficient and human readable."
msgstr ""
"Вивести дати у скороченому, повному форматі або форматі ISO. Типовим "
"форматом є скорочений. Цей формат часу розроблено для заощадження місця із "
"збереженням зручності читання."

#. type: Plain text
#: debian-bookworm
msgid "B<-P>, B<--numeric-perms>"
msgstr "B<-P>, B<--numeric-perms>"

#. type: Plain text
#: debian-bookworm
msgid "Print numeric permissions in PERMS column."
msgstr "Вивести у стовпчику PERMS числові значення прав доступу."

#. type: Plain text
#: debian-bookworm
msgid "B<-y>, B<--shell>"
msgstr "B<-y>, B<--shell>"

#. type: Plain text
#: debian-bookworm
msgid ""
"The column name will be modified to contain only characters allowed for "
"shell variable identifiers. This is usable, for example, with B<--export>. "
"Note that this feature has been automatically enabled for B<--export> in "
"version 2.37, but due to compatibility issues, now it\\(cqs necessary to "
"request this behavior by B<--shell>."
msgstr ""
"Назву стовпчика буде змінено так, щоб вона містила лише символи, які є "
"дозволеними для ідентифікаторів змінних командної оболонки. Це корисно, "
"зокрема, з B<--export>. Зауважте, що цю можливість було автоматично "
"увімкнено для B<--export> у версії 2.37, але через проблеми із сумісністю у "
"нових версіях потрібно викликати таку поведінку за допомогою параметра B<--"
"shell>."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr "СТАН ВИХОДУ"

#. type: Plain text
#: debian-bookworm
msgid "0"
msgstr "0"

#. type: Plain text
#: debian-bookworm
msgid "if OK,"
msgstr "все добре,"

#. type: Plain text
#: debian-bookworm
msgid "1"
msgstr "1"

#. type: Plain text
#: debian-bookworm
msgid "if incorrect arguments specified,"
msgstr "якщо вказано помилкові аргументи,"

#. type: Plain text
#: debian-bookworm
msgid "2"
msgstr "2"

#. type: Plain text
#: debian-bookworm
msgid "if a serious error occurs."
msgstr "сталася серйозна помилка."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "HISTORY"
msgstr "ЖУРНАЛ"

#. type: Plain text
#: debian-bookworm
msgid "The B<lsipc> utility is inspired by the B<ipcs>(1) utility."
msgstr ""
"Джерелом натхнення для написання допоміжної програми B<lsipc> була допоміжна "
"програма B<ipcs>(1)."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<ipcmk>(1), B<ipcrm>(1), B<msgrcv>(2), B<msgsnd>(2), B<semget>(2), "
"B<semop>(2), B<shmat>(2), B<shmdt>(2), B<shmget>(2), B<sysvipc>(7)"
msgstr ""
"B<ipcmk>(1), B<ipcrm>(1), B<msgrcv>(2), B<msgsnd>(2), B<semget>(2), "
"B<semop>(2), B<shmat>(2), B<shmdt>(2), B<shmget>(2), B<sysvipc>(7)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lsipc> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<lsipc> є частиною пакунка util-linux, який можна отримати з"
