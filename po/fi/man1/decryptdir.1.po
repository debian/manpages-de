# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-08-02 17:15+0200\n"
"PO-Revision-Date: 2023-04-30 07:52+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CRYPTDIR"
msgstr "CRYPTDIR"

#. type: TH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "1 January 1993"
msgstr "1. tammikuuta 1993"

#. type: SH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid "cryptdir - encrypt/decrypt all files in a directory"
msgstr "cryptdir - salaa/selvitä kaikki tiedostot hakemistossa"

#. type: SH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<cryptdir> [ I<dir> ]"
msgstr "B<cryptdir> [ I<hakemisto> ]"

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<decryptdir> [ I<dir> ]"
msgstr "B<decryptdir> [ I<hakemisto> ]"

#. type: SH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "INTRODUCTION"
msgstr "ESITTELY"

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<cryptdir> encrypts all files in the current directory (or the given "
"directory if one is provided as an argument).  When called as decryptdir (i."
"e., same program, different name), all files are decrypted."
msgstr ""
"B<cryptdir> salaa kaikki tiedostot nykyisessä hakemistossa (tai parametrinä "
"annetussa hakemistossa). Kun ohjelma käynnistetään komennolla decryptdir "
"(sama ohjelma, eri nimi), kaikki tiedostot selvitetään."

#. type: SH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "HUOMAUTUKSET"

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When encrypting, you are prompted twice for the password as a precautionary "
"measure.  It would be a disaster to encrypt files a password that wasn't "
"what you intended."
msgstr ""

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In contrast, when decrypting, you are only prompted once.  If it's the wrong "
"password, no harm done."
msgstr ""
"Selvitettäessä salasanaa kysytään vain kerran. Jos salasana on väärä, mitään "
"vahinkoa ei tapahdu."

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Encrypted files have the suffix .crypt appended.  This prevents files from "
"being encrypted twice.  The suffix is removed upon decryption.  Thus, you "
"can easily add files to an encrypted directory and run cryptdir on it "
"without worrying about the already encrypted files."
msgstr ""
"Salattujen tiedostojen nimiin lisätään pääte .crypt . Tällä estetään saman "
"tiedoston salaaminen kahdesti. Lisätty pääte poistetaan selvitettäessä.  "
"Voit siis helposti lisätä tiedostoja salattuun hakemistoon, ja ajaa "
"cryptdirin siellä välittämättä ennestään salatuista tiedostoista."

#. type: SH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGIT"

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid "The man page is longer than the program."
msgstr "Man-sivu on pidempi kuin itse ohjelma."

#. type: SH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<\"Exploring Expect: A Tcl-Based Toolkit for Automating Interactive "
"Programs\"> by Don Libes, O'Reilly and Associates, January 1995."
msgstr ""
"I<\"Exploring Expect: A Tcl-Based Toolkit for Automating Interactive "
"Programs\"> by Don Libes, O'Reilly and Associates, January 1995."

#. type: SH
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: archlinux opensuse-leap-16-0 opensuse-tumbleweed
msgid "Don Libes, National Institute of Standards and Technology"
msgstr "Don Libes, National Institute of Standards and Technology"
