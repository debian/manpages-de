# #-#-#-#-#  min-002-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
# #-#-#-#-#  min-003-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
# #-#-#-#-#  min-004-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
# #-#-#-#-#  min-010-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2025.
# #-#-#-#-#  min-020-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2025.
# #-#-#-#-#  min-100-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2025.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2024-12-06 18:05+0100\n"
"PO-Revision-Date: 2025-01-08 07:48+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  min-002-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"
"#-#-#-#-#  min-003-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"
"#-#-#-#-#  min-004-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"
"#-#-#-#-#  min-010-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.0\n"
"#-#-#-#-#  min-020-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.0\n"
"#-#-#-#-#  min-100-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MKDIR"
msgstr "MKDIR"

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2024"
msgstr "Augustus 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Opdrachten voor gebruikers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mkdir - make directories"
msgstr "mkdir - mappen aanmaken"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<mkdir> [I<\\,OPTION\\/>]... I<\\,DIRECTORY\\/>..."
msgstr "B<mkdir> [I<\\,OPTIE\\/>]... I<\\,MAP\\/>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Create the DIRECTORY(ies), if they do not already exist."
msgstr "Maak de MAP(pen) aan, als ze nog niet bestaan."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Een verplicht argument bij een lange optie is ook verplicht voor de korte "
"optie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--mode>=I<\\,MODE\\/>"
msgstr "B<-m>, B<--mode>=I<\\,MODUS\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "set file mode (as in chmod), not a=rwx - umask"
msgstr "in te stellen rechten (zie 'chmod'), i.p.v. 'a=rwx - umask'"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--parents>"
msgstr "B<-p>, B<--parents>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"no error if existing, make parent directories as needed, with their file "
"modes unaffected by any B<-m> option."
msgstr ""
"geen fout melden indien bestaand, maak ouder map indien nodig aan, met hun "
"bestand modi onaangetast door enige B<-m> optie. "

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print a message for each created directory"
msgstr "een melding geven voor elke aangemaakte map"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-Z>"
msgstr "B<-Z>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"set SELinux security context of each created directory to the default type"
msgstr "de SELinux-beveiligingscontext van doelbestand op standaard instellen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--context>[=I<\\,CTX\\/>]"
msgstr "B<--context>[=I<\\,CNTXT\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"like B<-Z>, or if CTX is specified then set the SELinux or SMACK security "
"context to CTX"
msgstr ""
"zoals B<-Z>, of als CNTXT is opgegeven zet dan de SELinux en SMACK "
"beveiligingscontext op CNTXT"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "toon de helptekst en stop"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "toon programmaversie en stop"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by David MacKenzie."
msgstr "Geschreven door David MacKenzie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTEREN VAN BUGS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Online hulp bij GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Meld alle vertaalfouten op E<lt>https://translationproject.org/team/"
"nl.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dit is vrije software: u mag het vrijelijk wijzigen en verder verspreiden. "
"Deze software kent GEEN GARANTIE, voor zover de wet dit toestaat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<mkdir>(2)"
msgstr "B<mkdir>(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/mkdirE<gt>"
msgstr ""
"Volledige documentatie op: E<lt>https://www.gnu.org/software/coreutils/"
"mkdirE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) mkdir invocation\\(aq"
msgstr "of lokaal via: info \\(aq(coreutils) mkdir invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "Oktober 2024"

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "September 2024"
msgstr "September 2024"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "November 2024"
msgstr "November 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "Augustus 2023"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Januari 2024"
