# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2024-09-06 18:24+0200\n"
"PO-Revision-Date: 2022-06-22 21:14+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "PSFXTABLE"
msgstr "PSFXTABLE"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "9 Dec 1999"
msgstr "9. december 1999"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "kbd"
msgstr "kbd"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "psfxtable - handle Unicode character tables for console fonts"
msgstr "psfxtable - håndter Unicode-tegntabeller for konsolskrifttyper"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<psfxtable> [B<-i> I<infont>] [B<-o> I<outfont>] [B<-it> I<intable>] [B<-"
"ot> I<outtable>] [B<-nt>]"
msgstr ""
"B<psfxtable> [B<-i> I<ind_skrift>] [B<-o> I<ud_skrift>] [B<-it> "
"I<ind_tabel>] [B<-ot> I<ud_tabel>] [B<-nt>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: IX
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "psfxtable command"
msgstr "psfxtable kommando"

#. type: IX
#: archlinux fedora-41 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\fLpsfxtable\\fR command"
msgstr "\\fLpsfxtable\\fR kommando"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<psfxtable> handles the embedded Unicode character table for .psf format "
"console fonts. It reads a font and possibly a table and writes a font and/or "
"a table.  B<psfaddtable>(1), B<psfgettable>(1)  and B<psfstriptable>(1)  are "
"links to it."
msgstr ""
"B<psfxtable> håndterer den indlejrede Unicode-tegntabel for "
"konsolskrifttyper i formatet .psf. Programmet læser en skrifttype og "
"muligvis en tabel og skriver en skrifttype og/eller en tabel. "
"B<psfaddtable>(1), B<psfgettable>(1) og B<psfstriptable>(1) er lænker til "
"programmet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each of the filenames I<infont>, I<outfont>, I<intable>, and I<outtable> may "
"be replaced by a single dash (-), in which case standard input or standard "
"output is used.  If no I<-i> option is given, the font is read from standard "
"input.  If no I<-it> or I<-o> or I<-ot> option is given, no input table is "
"read or no output font or output table is written."
msgstr ""
"Hvert af filnavnene I<ind_skrift>, I<ud_skrift>, I<ind_tabel> og I<ud_tabel> "
"kan erstattes af en enkel bindestreg (-). I de tilfælde bruges standardind "
"og standardud. Hvis ikke tilvalget I<-i> er angivet, læses skrifttypen fra "
"standardind. Hvis tilvalgene I<-it> eller I<-o> eller I<-ot> er angivet, så "
"læses inddatatabellen ikke eller ingen uddataskrifttype eller uddatatabel "
"skrives."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By default the output font (if any) will have a Unicode table when either "
"the input font has one, or an explicit table (which overrides an input font "
"table) has been provided.  The option I<-nt> causes output of a font without "
"table.  When I<outfont> is requested it will get a psf1 header when infont "
"has a psf1 header and I<intable> does not have sequences and a psf2 header "
"otherwise."
msgstr ""
"Som standard vil uddataskrifttypen (om nogen) have en Unicode-tabel, når "
"enten inddataskrifttypen har en, eller en eksplicit tabel (der overskriver "
"en inddataskrifttypetabel) er specificeret. Tilvalget I<-nt> gør, at "
"resultatet for en skrifttype er uden tabel. Når der anmodes om I<ud_skrift>, "
"så vil den få et psf1-teksthoved, når indskrifttypen har et psf1-teksthoved "
"og I<indtabel> ikke har sekvenser og et psf2-teksthoved i alle andre "
"tilfælde."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<setfont>(8), B<psfaddtable>(1), B<psfgettable>(1), B<psfstriptable>(1)"
msgstr ""
"B<setfont>(8), B<psfaddtable>(1), B<psfgettable>(1), B<psfstriptable>(1)"
