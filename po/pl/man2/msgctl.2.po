# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Rafał Lewczuk <R.Lewczuk@elka.pw.edu.p>, 1999.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2013.
# Michał Kułach <michal.kulach@gmail.com>, 2013, 2014, 2016, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-12-06 18:05+0100\n"
"PO-Revision-Date: 2024-05-06 19:15+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "msgctl"
msgstr "msgctl"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "msgctl - System V message control operations"
msgstr "msgctl - steruje kolejkami komunikatów Systemu V"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/msg.hE<gt>>\n"
msgstr "B<#include E<lt>sys/msg.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int msgctl(int >I<msqid>B<, int >I<op>B<, struct msqid_ds *>I<buf>B<);>\n"
msgstr "B<int msgctl(int >I<msqid>B<, int >I<op>B<, struct msqid_ds *>I<buf>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<msgctl>()  performs the control operation specified by I<op> on the "
"System\\ V message queue with identifier I<msqid>."
msgstr ""
"B<msgctl>() wykonuje operację określoną przez parametr I<op> na kolejce "
"komunikatów Systemu\\ V o identyfikatorze I<msqid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<msqid_ds> data structure is defined in I<E<lt>sys/msg.hE<gt>> as "
"follows:"
msgstr ""
"Struktura danych I<msqid_ds> jest zdefiniowana w I<E<lt>sys/msg.hE<gt>> "
"następująco:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct msqid_ds {\n"
"    struct ipc_perm msg_perm;   /* Ownership and permissions */\n"
"    time_t          msg_stime;  /* Time of last msgsnd(2) */\n"
"    time_t          msg_rtime;  /* Time of last msgrcv(2) */\n"
"    time_t          msg_ctime;  /* Time of creation or last\n"
"                                   modification by msgctl() */\n"
"    unsigned long   msg_cbytes; /* # of bytes in queue */\n"
"    msgqnum_t       msg_qnum;   /* # number of messages in queue */\n"
"    msglen_t        msg_qbytes; /* Maximum # of bytes in queue */\n"
"    pid_t           msg_lspid;  /* PID of last msgsnd(2) */\n"
"    pid_t           msg_lrpid;  /* PID of last msgrcv(2) */\n"
"};\n"
msgstr ""
"struct msqid_ds {\n"
"    struct ipc_perm msg_perm;   /* Własności i uprawnienia */\n"
"    time_t          msg_stime;  /* Czas ostatniego msgsnd(2) */\n"
"    time_t          msg_rtime;  /* Czas ostatniego msgrcv(2) */\n"
"    time_t          msg_ctime;  /* Czas utworzenia lub ostatniej\n"
"                                   zmiany przez msgctl() */\n"
"    unsigned long   msg_cbytes; /* # bajtów w kolejce */\n"
"    msgqnum_t       msg_qnum;   /* # liczba komunikatów w kolejce */\n"
"    msglen_t        msg_qbytes; /* Maksymalna # bajtów w kolejce */\n"
"    pid_t           msg_lspid;  /* PID ostatniego msgsnd(2) */\n"
"    pid_t           msg_lrpid;  /* PID ostatniego msgrcv(2) */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The fields of the I<msqid_ds> structure are as follows:"
msgstr "Pola struktury I<msqid_ds> są następujące:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_perm>"
msgstr "I<msg_perm>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is an I<ipc_perm> structure (see below) that specifies the access "
"permissions on the message queue."
msgstr ""
"Jest to struktura I<ipc_perm> (zob. niżej), która określa prawa dostępu do "
"kolejki komunikatów."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_stime>"
msgstr "I<msg_stime>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Time of the last B<msgsnd>(2)  system call."
msgstr "Czas ostatniego wykonania funkcji systemowej B<msgsnd>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_rtime>"
msgstr "I<msg_rtime>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Time of the last B<msgrcv>(2)  system call."
msgstr "Czas ostatniego wykonania funkcji systemowej B<msgrcv>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_ctime>"
msgstr "I<msg_ctime>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Time of creation of queue or time of last B<msgctl>()  B<IPC_SET> operation."
msgstr ""
"Czas utworzenia kolejki lub czas ostatniej operacji B<IPC_SET> B<msgctl>()."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_cbytes>"
msgstr "I<msg_cbytes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Number of bytes in all messages currently on the message queue.  This is a "
"nonstandard Linux extension that is not specified in POSIX."
msgstr ""
"Liczba bajtów we wszystkich komunikatach znajdujących się aktualnie w "
"kolejce. Jest to niestandardowe rozszerzenie Linuksa, które nie jest "
"określone przez POSIX."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_qnum>"
msgstr "I<msg_qnum>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Number of messages currently on the message queue."
msgstr "Liczba komunikatów znajdujących się aktualnie w kolejce."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_qbytes>"
msgstr "I<msg_qbytes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Maximum number of bytes of message text allowed on the message queue."
msgstr "Maksymalna liczba bajtów tekstu komunikatu, na jaką pozwala kolejka."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_lspid>"
msgstr "I<msg_lspid>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ID of the process that performed the last B<msgsnd>(2)  system call."
msgstr ""
"Identyfikator procesu, który ostatni wykonał funkcję systemową B<msgsnd>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<msg_lrpid>"
msgstr "I<msg_lrpid>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "ID of the process that performed the last B<msgrcv>(2)  system call."
msgstr ""
"identyfikator procesu, który ostatni wykonał funkcję systemową B<msgrcv>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<ipc_perm> structure is defined as follows (the highlighted fields are "
"settable using B<IPC_SET>):"
msgstr ""
"Struktura I<ipc_perm> jest zdefiniowana następująco (wyróżnione pola można "
"ustawić za pomocą B<IPC_SET>):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct ipc_perm {\n"
"    key_t          __key;       /* Key supplied to msgget(2) */\n"
"    uid_t          B<uid>;         /* Effective UID of owner */\n"
"    gid_t          B<gid>;         /* Effective GID of owner */\n"
"    uid_t          cuid;        /* Effective UID of creator */\n"
"    gid_t          cgid;        /* Effective GID of creator */\n"
"    unsigned short B<mode>;        /* Permissions */\n"
"    unsigned short __seq;       /* Sequence number */\n"
"};\n"
msgstr ""
"struct ipc_perm {\n"
"    key_t          __key;       /* Klucz podany w msgget(2) */\n"
"    uid_t          B<uid>;         /* Efektywny UID właściciela */\n"
"    gid_t          B<gid>;         /* Efektywny GID właściciela */\n"
"    uid_t          cuid;        /* Efektywny UID twórcy */\n"
"    gid_t          cgid;        /* Efektywny GID twórcy */\n"
"    unsigned short B<mode>;        /* Uprawnienia */\n"
"    unsigned short __seq;       /* Numer sekwencji */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The least significant 9 bits of the I<mode> field of the I<ipc_perm> "
"structure define the access permissions for the message queue.  The "
"permission bits are as follows:"
msgstr ""
"Najmniej znaczące 9 bitów pola I<mode> struktury I<ipc_perm> definiuje "
"uprawnienia dostępu do kolejki komunikatów. Istnieją następujące bity "
"uprawnień:"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "0400"
msgstr "0400"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Read by user"
msgstr "Odczyt przez użytkownika"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "0200"
msgstr "0200"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Write by user"
msgstr "Zapis przez użytkownika"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "0040"
msgstr "0040"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Read by group"
msgstr "Odczyt przez grupę"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "0020"
msgstr "0020"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Write by group"
msgstr "Zapis przez grupę"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "0004"
msgstr "0004"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Read by others"
msgstr "Odczyt przez pozostałych"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "0002"
msgstr "0002"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Write by others"
msgstr "Zapis przez pozostałych"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Bits 0100, 0010, and 0001 (the execute bits) are unused by the system."
msgstr ""
"Bity 0100, 0010 i 0001 (bity praw do uruchamiania) nie są przez system "
"wykorzystywane."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Valid values for I<op> are:"
msgstr "Poprawne wartości parametru I<op> to:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<IPC_STAT>"
msgstr "B<IPC_STAT>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Copy information from the kernel data structure associated with I<msqid> "
"into the I<msqid_ds> structure pointed to by I<buf>.  The caller must have "
"read permission on the message queue."
msgstr ""
"Kopiowanie informacji ze struktury kontrolnej kolejki komunikatów "
"skojarzonej z I<msqid> do struktury wskazywanej przez I<buf>. Wywołujący "
"musi mieć prawo odczytu kolejki komunikatów."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<IPC_SET>"
msgstr "B<IPC_SET>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Write the values of some members of the I<msqid_ds> structure pointed to by "
"I<buf> to the kernel data structure associated with this message queue, "
"updating also its I<msg_ctime> member."
msgstr ""
"Zapis wartości niektórych pól struktury B<msqid_ds> wskazywanej przez "
"parametr I<buf> do struktury kontrolnej kolejki komunikatów.  Pole "
"B<msg_ctime> zostanie automatycznie uaktualnione."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following members of the structure are updated: I<msg_qbytes>, "
"I<msg_perm.uid>, I<msg_perm.gid>, and (the least significant 9 bits of)  "
"I<msg_perm.mode>."
msgstr ""
"Zaktualizowane mogą również zostać następujące pola tej struktury: "
"I<msg_qbytes>, I<msg_perm.uid>, I<msg_perm.gid> i (9 najmniej znaczących "
"bitów z)  I<msg_perm.mode>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The effective UID of the calling process must match the owner (I<msg_perm."
"uid>)  or creator (I<msg_perm.cuid>)  of the message queue, or the caller "
"must be privileged.  Appropriate privilege (Linux: the B<CAP_SYS_RESOURCE> "
"capability) is required to raise the I<msg_qbytes> value beyond the system "
"parameter B<MSGMNB>."
msgstr ""
"Efektywny identyfikator użytkownika musi wskazywać na właściciela "
"(I<msg_perm.uid>)  lub na twórcę (I<msg_perm.uid>)  kolejki komunikatów albo "
"proces wywołujący musi być uprzywilejowany.  Odpowiednie uprawnienia (Linux: "
"przywilej B<CAP_SYS_RESOURCE>)  są również wymagane, aby nadać polu "
"B<msg_qbytes> wartość większą niż parametr systemowy B<MSGMNB>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<IPC_RMID>"
msgstr "B<IPC_RMID>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Immediately remove the message queue, awakening all waiting reader and "
"writer processes (with an error return and I<errno> set to B<EIDRM>).  The "
"calling process must have appropriate privileges or its effective user ID "
"must be either that of the creator or owner of the message queue.  The third "
"argument to B<msgctl>()  is ignored in this case."
msgstr ""
"Usuwa natychmiast kolejkę komunikatów. Wznawia wszystkie procesy oczekujące "
"na zapis lub odczyt z kolejki (wywołania, które się wykonywały zasygnalizują "
"błąd i ustawią zmienną B<errno> na B<EIDRM>). Proces wywołujący tę funkcję "
"musi mieć odpowiednie uprawnienia albo jego efektywny identyfikator "
"użytkownika musi wskazywać na twórcę lub na właściciela kolejki komunikatów. "
"Trzeci argument do B<msgctl>() jest w tym wypadku ignorowany."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<IPC_INFO> (Linux-specific)"
msgstr "B<IPC_INFO> (specyficzne dla Linuksa)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Return information about system-wide message queue limits and parameters in "
"the structure pointed to by I<buf>.  This structure is of type I<msginfo> "
"(thus, a cast is required), defined in I<E<lt>sys/msg.hE<gt>> if the "
"B<_GNU_SOURCE> feature test macro is defined:"
msgstr ""
"Zwraca w strukturze, na którą wskazuje I<buf>, informacje o systemowych "
"ograniczeniach i parametrach kolejek komunikatów. Struktura jest typu "
"I<msginfo> (dlatego wymagane jest rzutowanie) i jest zdefiniowana w "
"I<E<lt>sys/msg.hE<gt>>, pod warunkiem, że zdefiniowano również makro "
"B<_GNU_SOURCE>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct msginfo {\n"
"    int msgpool; /* Size in kibibytes of buffer pool\n"
"                    used to hold message data;\n"
"                    unused within kernel */\n"
"    int msgmap;  /* Maximum number of entries in message\n"
"                    map; unused within kernel */\n"
"    int msgmax;  /* Maximum number of bytes that can be\n"
"                    written in a single message */\n"
"    int msgmnb;  /* Maximum number of bytes that can be\n"
"                    written to queue; used to initialize\n"
"                    msg_qbytes during queue creation\n"
"                    (msgget(2)) */\n"
"    int msgmni;  /* Maximum number of message queues */\n"
"    int msgssz;  /* Message segment size;\n"
"                    unused within kernel */\n"
"    int msgtql;  /* Maximum number of messages on all queues\n"
"                    in system; unused within kernel */\n"
"    unsigned short msgseg;\n"
"                 /* Maximum number of segments;\n"
"                    unused within kernel */\n"
"};\n"
msgstr ""
"struct msginfo {\n"
"    int msgpool; /* Rozmiar w kibibajtach puli buforów\n"
"                    używanej do przechowywania danych\n"
"                    komunikatu; nieużywane przez jądro */\n"
"    int msgmap;  /* Maksymalna liczba wpisów w mapie komuni-\n"
"                    katu; nieużywane przez jądro */\n"
"    int msgmax;  /* Maksymalna liczba bajtów, które można\n"
"                    zapisać w pojedynczej wiadomości */\n"
"    int msgmnb;  /* Maksymalna liczba bajtów, które można\n"
"                    zapisać do kolejki; używane do inicjowania\n"
"                    msg_qbytes podczas tworzenia kolejki\n"
"                    (msgget(2)) */\n"
"    int msgmni;  /* Maksymalna liczba kolejek komunikatów */\n"
"    int msgssz;  /* Rozmiar segmentu komunikatu;\n"
"                    nieużywane przez jądro */\n"
"    int msgtql;  /* Maksymalna liczba komunikatów we wszystkich\n"
"                    kolejkach w systemie; nieużywane przez jądro */\n"
"    unsigned short msgseg;\n"
"                 /* Maksymalna liczba segmentów;\n"
"                    nieużywane przez jądro */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<msgmni>, I<msgmax>, and I<msgmnb> settings can be changed via I</proc> "
"files of the same name; see B<proc>(5)  for details."
msgstr ""
"Ustawienia I<msgmni>, I<msgmax> oraz I<msgmnb> można zmienić za pomocą "
"plików I</proc> o nazwach takich samych, jak nazwy tych ustawień; szczegóły "
"można znaleźć w podręczniku B<proc>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MSG_INFO> (Linux-specific)"
msgstr "B<MSG_INFO> (specyficzne dla Linuksa)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Return a I<msginfo> structure containing the same information as for "
"B<IPC_INFO>, except that the following fields are returned with information "
"about system resources consumed by message queues: the I<msgpool> field "
"returns the number of message queues that currently exist on the system; the "
"I<msgmap> field returns the total number of messages in all queues on the "
"system; and the I<msgtql> field returns the total number of bytes in all "
"messages in all queues on the system."
msgstr ""
"Zwraca strukturę I<msginfo> zawierającą te same informacje co w przypadku "
"B<IPC_INFO>, z tym  wyjątkiem, że w następujących polach zwracane są "
"informacje o zasobach systemowych wykorzystywanych przez kolejki "
"komunikatów: pole I<msgpool> zwraca liczbę kolejek komunikatów istniejących "
"obecnie w systemie; pole I<msgmap> zwraca całkowitą liczbę komunikatów we "
"wszystkich kolejkach w systemie, a pole I<msgtql> zwraca całkowitą liczbę "
"bajtów we wszystkich komunikatach z wszystkich kolejek w systemie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MSG_STAT> (Linux-specific)"
msgstr "B<MSG_STAT> (specyficzne dla Linuksa)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Return a I<msqid_ds> structure as for B<IPC_STAT>.  However, the I<msqid> "
"argument is not a queue identifier, but instead an index into the kernel's "
"internal array that maintains information about all message queues on the "
"system."
msgstr ""
"Zwraca strukturę I<msqid_ds>, taką jak dla B<IPC_STAT>. Jednakże parametr "
"I<msqid> nie jest identyfikatorem kolejki, ale indeksem wewnętrznej tablicy "
"jądra przechowującej informacje o wszystkich kolejkach w systemie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<MSG_STAT_ANY> (Linux-specific, since Linux 4.17)"
msgstr "B<MSG_STAT_ANY> (specyficzne dla Linuksa, od Linuks 4.17)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Return a I<msqid_ds> structure as for B<MSG_STAT>.  However, I<msg_perm."
"mode> is not checked for read access for I<msqid> meaning that any user can "
"employ this operation (just as any user may read I</proc/sysvipc/msg> to "
"obtain the same information)."
msgstr ""
"Zwraca strukturę I<msqid_ds>, jak dla B<MSG_STAT>. Jednak I<msg_perm.mode> "
"nie jest sprawdzany pod kątem uprawnień odczytu do I<msqid> co oznacza, że "
"każdy użytkownik może wykonać tę operację (podobnie jak każdy użytkownik "
"może odczytać I</proc/sysvipc/msg>, pozyskując te same informacje)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<IPC_STAT>, B<IPC_SET>, and B<IPC_RMID> return 0.  A successful "
"B<IPC_INFO> or B<MSG_INFO> operation returns the index of the highest used "
"entry in the kernel's internal array recording information about all message "
"queues.  (This information can be used with repeated B<MSG_STAT> or "
"B<MSG_STAT_ANY> operations to obtain information about all queues on the "
"system.)  A successful B<MSG_STAT> or B<MSG_STAT_ANY> operation returns the "
"identifier of the queue whose index was given in I<msqid>."
msgstr ""
"W razie powodzenia B<IPC_STAT>, B<IPC_SET> i B<IPC_RMID> zwracają 0. "
"Pomyślnie zakończone operacje B<IPC_INFO> i B<MSG_INFO> zwracają indeks "
"najwyższego używanego wpisu w wewnętrznej tablicy jądra przechowującej "
"informacje o wszystkich kolejkach komunikatów. (Informacji tej można użyć w "
"operacjach B<MSG_STAT> lub B<MSG_STAT_ANY>, aby otrzymać informacje o "
"wszystkich kolejkach w systemie). Pomyślnie zakończona operacja B<MSG_STAT> "
"lub B<MSG_STAT_ANY> zwraca identyfikator kolejki o indeksie przekazanym w "
"I<msqid>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "On failure, -1 is returned and I<errno> is set to indicate the error."
msgstr ""
"W razie niepowodzenia zwracane jest -1 i ustawiane jest I<errno> wskazując "
"rodzaj błędu."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The argument I<op> is equal to B<IPC_STAT> or B<MSG_STAT>, but the calling "
"process does not have read permission on the message queue I<msqid>, and "
"does not have the B<CAP_IPC_OWNER> capability in the user namespace that "
"governs its IPC namespace."
msgstr ""
"Parametr I<op> jest równy B<IPC_STAT> lub B<MSG_STAT>, ale proces wywołujący "
"funkcję nie ma prawa do odczytu kolejki komunikatów wskazywanej przez "
"I<msqid> ani nie ma przywileju B<CAP_IPC_OWNER> (ang. capability) w "
"przestrzeni nazw użytkownika, która zarządza jego przestrzenią nazw IPC."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The argument I<op> has the value B<IPC_SET> or B<IPC_STAT>, but the address "
"pointed to by I<buf> isn't accessible."
msgstr ""
"Argument I<op> ma wartość B<IPC_SET> lub B<IPC_STAT>, ale I<buf> wskazuje na "
"niedostępny obszar pamięci."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EIDRM>"
msgstr "B<EIDRM>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The message queue was removed."
msgstr "Kolejka komunikatów została usunięta."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Invalid value for I<op> or I<msqid>.  Or: for a B<MSG_STAT> operation, the "
"index value specified in I<msqid> referred to an array slot that is "
"currently unused."
msgstr ""
"Niepoprawna wartość parametru I<op> lub I<msqid>. Albo: w przypadku operacji "
"B<MSG_STAT> wartość indeksu podana w parametrze I<msqid> odwoływała się do "
"obecnie nieużywanego elementu tablicy."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The argument I<op> has the value B<IPC_SET> or B<IPC_RMID>, but the "
"effective user ID of the calling process is not the creator (as found in "
"I<msg_perm.cuid>)  or the owner (as found in I<msg_perm.uid>)  of the "
"message queue, and the caller is not privileged (Linux: does not have the "
"B<CAP_SYS_ADMIN> capability)."
msgstr ""
"Argument I<op> jest równy B<IPC_SET> lub B<IPC_RMID>, ale proces wywołujący "
"funkcję nie jest twórcą (określonym w I<msg_perm.cuid>) ani właścicielem "
"(określonym w I<msg_perm.uid>) kolejki komunikatów, a wywołujący nie jest "
"uprzywilejowany (Linux: nie ma przywileju B<CAP_SYS_ADMIN>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An attempt (B<IPC_SET>)  was made to increase I<msg_qbytes> beyond the "
"system parameter B<MSGMNB>, but the caller is not privileged (Linux: does "
"not have the B<CAP_SYS_RESOURCE> capability)."
msgstr ""
"Podjęto próbę (B<IPC_SET>) zwiększenia I<msg_qbytes> ponad parametr "
"systemowy B<MSGMNB>, lecz wywołujący nie jest uprzywilejowany (Linux: nie ma "
"przywileju B<CAP_SYS_RESOURCE>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#.  SVID does not document the EIDRM error condition.
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4."
msgstr "POSIX.1-2001, SVr4."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Various fields in the I<struct msqid_ds> were typed as I<short> under Linux "
"2.2 and have become I<long> under Linux 2.4.  To take advantage of this, a "
"recompilation under glibc-2.1.91 or later should suffice.  (The kernel "
"distinguishes old and new calls by an B<IPC_64> flag in I<op>.)"
msgstr ""
"Niektóre pola struktury I<struct msqid_ds> były w Linuksie 2.2 typu "
"I<short>, ale stały się typu I<long> w Linuksie 2.4. Aby to wykorzystać, "
"powinna wystarczyć rekompilacja pod glibc-2.1.91 lub nowszą. (Jądro "
"rozróżnia stare wywołania od nowych za pomocą znacznika B<IPC_64> w I<op>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<IPC_INFO>, B<MSG_STAT>, and B<MSG_INFO> operations are used by the "
"B<ipcs>(1)  program to provide information on allocated resources.  In the "
"future these may modified or moved to a I</proc> filesystem interface."
msgstr ""
"Operacje B<IPC_INFO>, B<MSG_STAT> oraz B<MSG_INFO> są używane przez program "
"B<ipcs>(1) w celu dostarczenia informacji o zajmowanych zasobach. W "
"przyszłości operacje te mogą zostać zmodyfikowane lub przeniesione do "
"interfejsu systemu plików I</proc>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<capabilities>(7), "
"B<mq_overview>(7), B<sysvipc>(7)"
msgstr ""
"B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<capabilities>(7), "
"B<mq_overview>(7), B<sysvipc>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 grudnia 2022 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<int msgctl(int >I<msqid>B<, int >I<cmd>B<, struct msqid_ds *>I<buf>B<);>\n"
msgstr "B<int msgctl(int >I<msqid>B<, int >I<cmd>B<, struct msqid_ds *>I<buf>B<);>\n"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"B<msgctl>()  performs the control operation specified by I<cmd> on the "
"System\\ V message queue with identifier I<msqid>."
msgstr ""
"B<msgctl>() wykonuje operację określoną przez parametr I<cmd> na kolejce "
"komunikatów Systemu\\ V o identyfikatorze I<msqid>."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid "Valid values for I<cmd> are:"
msgstr "Poprawne wartości parametru I<cmd> to:"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The argument I<cmd> is equal to B<IPC_STAT> or B<MSG_STAT>, but the calling "
"process does not have read permission on the message queue I<msqid>, and "
"does not have the B<CAP_IPC_OWNER> capability in the user namespace that "
"governs its IPC namespace."
msgstr ""
"Parametr I<cmd> jest równy B<IPC_STAT> lub B<MSG_STAT>, ale proces "
"wywołujący funkcję nie ma prawa do odczytu kolejki komunikatów wskazywanej "
"przez I<msqid> ani nie ma przywileju B<CAP_IPC_OWNER> (ang. capability) w "
"przestrzeni nazw użytkownika, która zarządza jego przestrzenią nazw IPC."

#
#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The argument I<cmd> has the value B<IPC_SET> or B<IPC_STAT>, but the address "
"pointed to by I<buf> isn't accessible."
msgstr ""
"Parametr I<cmd> ma wartość B<IPC_SET> lub B<IPC_STAT>, ale I<buf> wskazuje "
"na niedostępny obszar pamięci."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Invalid value for I<cmd> or I<msqid>.  Or: for a B<MSG_STAT> operation, the "
"index value specified in I<msqid> referred to an array slot that is "
"currently unused."
msgstr ""
"Niepoprawna wartość parametru I<cmd> lub I<msqid>. Albo: w przypadku "
"operacji B<MSG_STAT> wartość indeksu podana w parametrze I<msqid> odwoływała "
"się do obecnie nieużywanego elementu tablicy."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The argument I<cmd> has the value B<IPC_SET> or B<IPC_RMID>, but the "
"effective user ID of the calling process is not the creator (as found in "
"I<msg_perm.cuid>)  or the owner (as found in I<msg_perm.uid>)  of the "
"message queue, and the caller is not privileged (Linux: does not have the "
"B<CAP_SYS_ADMIN> capability)."
msgstr ""
"Parametr I<cmd> jest równy B<IPC_SET> lub B<IPC_RMID>, ale proces wywołujący "
"funkcję nie jest twórcą (określonym w I<msg_perm.cuid>) ani właścicielem "
"(określonym w I<msg_perm.uid>) kolejki komunikatów, a wywołujący nie jest "
"uprzywilejowany (Linux: nie ma przywileju B<CAP_SYS_ADMIN>)."

#.  SVID does not document the EIDRM error condition.
#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, SVr4."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Various fields in the I<struct msqid_ds> were typed as I<short> under Linux "
"2.2 and have become I<long> under Linux 2.4.  To take advantage of this, a "
"recompilation under glibc-2.1.91 or later should suffice.  (The kernel "
"distinguishes old and new calls by an B<IPC_64> flag in I<cmd>.)"
msgstr ""
"Niektóre pola struktury I<struct msqid_ds> były w Linuksie 2.2 typu "
"I<short>, ale stały się typu I<long> w Linuksie 2.4. Aby to wykorzystać, "
"powinna wystarczyć rekompilacja pod glibc-2.1.91 lub nowszą. (Jądro "
"rozróżnia stare wywołania od nowych za pomocą znacznika B<IPC_64> w I<cmd>)."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
