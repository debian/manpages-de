# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-12-06 17:54+0100\n"
"PO-Revision-Date: 2023-03-31 21:14+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.1\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BASE64"
msgstr "BASE64"

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2024"
msgstr "sierpień 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "base64 - base64 encode/decode data and print to standard output"
msgstr "base64 - koduje/dekoduje dane i wyświetla je na standardowe wyjście"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<base64> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]"
msgstr "B<base64> [I<OPCJE>]... [I<PLIK>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Base64 encode or decode FILE, or standard input, to standard output."
msgstr ""
"Base64 koduje lub dekoduje I<PLIK> lub standardowe wejście na standardowe "
"wyjście."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"Jeśli nie podano I<PLIKU> lub jako I<PLIK> podano B<->, czyta standardowe "
"wejście."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumenty, które są obowiązkowe dla długich opcji, są również obowiązkowe "
"dla krótkich."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--decode>"
msgstr "B<-d>, B<--decode>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "decode data"
msgstr "dekoduje dane"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-garbage>"
msgstr "B<-i>, B<--ignore-garbage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "when decoding, ignore non-alphabet characters"
msgstr "podczas dekodowania ignoruje znaki spoza alfabetu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--wrap>=I<\\,COLS\\/>"
msgstr "B<-w>, B<--wrap>=I<KOLUMNY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"wrap encoded lines after COLS character (default 76).  Use 0 to disable line "
"wrapping"
msgstr ""
"zawija zakodowane wiersze po I<KOLUMNACH> znaków (domyślnie 76). Proszę użyć "
"0, aby wyłączyć zawijanie wierszy"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The data are encoded as described for the base64 alphabet in RFC 4648.  When "
"decoding, the input may contain newlines in addition to the bytes of the "
"formal base64 alphabet.  Use B<--ignore-garbage> to attempt to recover from "
"any other non-alphabet bytes in the encoded stream."
msgstr ""
"Kodowane dane są opisane w alfabecie base64 w dokumencie RFC 4648. Podczas "
"dekodowania wejście może zawierać oprócz bajtów formalnego alfabetu base64 "
"także znaki nowego wiersza. Proszę użyć B<--ignore-garbage>, aby spróbować "
"odzyskać zakodowany strumień zawierający inne bajty spoza alfabetu."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Simon Josefsson."
msgstr "Napisane przez Simona Josefssona."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Strona internetowa z pomocą GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"O błędach tłumaczenia programu prosimy poinformować przez E<lt>https://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Jest to wolne oprogramowanie: można je zmieniać i rozpowszechniać. Nie ma "
"ŻADNEJ GWARANCJI, w granicach określonych przez prawo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<basenc>(1)"
msgstr "B<basenc>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/base64E<gt>"
msgstr ""
"Pełna dokumentacja: E<lt>https://www.gnu.org/software/coreutils/base64E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) base64 invocation\\(aq"
msgstr ""
"lub lokalnie, za pomocą B<info \\(aq(coreutils) base64 invocation\\(aq>"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "wrzesień 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "październik 2024"

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "September 2024"
msgstr "wrzesień 2024"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "November 2024"
msgstr "listopad 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "sierpień 2023"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "styczeń 2024"
