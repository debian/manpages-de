# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gwidon S. Naskrent <naskrent@hoth.amu.edu.pl>, 1999.
# Wojtek Kotwica <wkotwica@post.pl>, 2000.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-12-06 18:08+0100\n"
"PO-Revision-Date: 2016-04-27 18:39+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "PR"
msgstr "PR"

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2024"
msgstr "sierpień 2024"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "pr - convert text files for printing"
msgstr "pr - konwertuje pliki tekstowe do drukowania"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pr> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<pr> [I<OPCJA>]... [I<PLIK>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Paginate or columnate FILE(s) for printing."
msgstr "Dzieli lub układa w kolumny I<PLIK(I)> do drukowania."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"Jeśli nie podano I<PLIKU> lub jako I<PLIK> podano B<->, czyta standardowe "
"wejście."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumenty, które są obowiązkowe dla długich opcji, są również obowiązkowe "
"dla krótkich."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "+FIRST_PAGE[:LAST_PAGE], B<--pages>=I<\\,FIRST_PAGE[\\/>:LAST_PAGE]"
msgstr "B<+>I<PIERWSZA-STRONA>[B<:>I<OSTATNIA-STRONA>], B<--pages=>I<PIERWSZA-STRONA>[B<:>I<OSTATNIA-STRONA>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "begin [stop] printing with page FIRST_[LAST_]PAGE"
msgstr ""
"rozpoczyna [kończy] drukowanie od I<PIERWSZEJ->[I<OSTATNIEJ->]I<STRONY>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-COLUMN>, B<--columns>=I<\\,COLUMN\\/>"
msgstr "B<->I<KOLUMNY>, B<--columns>=I<KOLUMNY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"output COLUMN columns and print columns down, unless B<-a> is used. Balance "
"number of lines in the columns on each page"
msgstr ""
"wyświetla wyjście w danej liczbie I<KOLUMN> i wypełnia je pionowo, chyba że "
"użyto opcji B<-a>. Dopasowuje liczbę wierszy w kolumnach do każdej strony"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--across>"
msgstr "B<-a>, B<--across>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print columns across rather than down, used together with B<-COLUMN>"
msgstr ""
"wypełnia kolumny w poprzek, nie w dół; używane razem z opcją B<->I<KOLUMNY>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--show-control-chars>"
msgstr "B<-c>, B<--show-control-chars>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use hat notation (^G) and octal backslash notation"
msgstr ""
"używa notacji z daszkiem B<^> (np. ^G) i notacji ósemkowej z odwrotnym "
"ukośnikiem"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--double-space>"
msgstr "B<-d>, B<--double-space>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "double space the output"
msgstr "używa podwójnych odstępów między wierszami wyjścia"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--date-format>=I<\\,FORMAT\\/>"
msgstr "B<-D>, B<--date-format>=I<FORMAT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use FORMAT for the header date"
msgstr "używa daty w nagłówku w podanym I<FORMACIE>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-e[CHAR[WIDTH]]>, B<--expand-tabs>[=I<\\,CHAR[WIDTH]\\/>]"
msgstr "B<-e>[I<ZNAK>[I<SZEROKOŚĆ>]], B<--expand-tabs>[=I<ZNAK>[I<SZEROKOŚĆ>]]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "expand input CHARs (TABs) to tab WIDTH (8)"
msgstr ""
"rozwija wejściowe I<ZNAKI> (lub tabulację) do szerokości tabulacji "
"I<SZEROKOŚĆ> (domyślnie 8)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>, B<-f>, B<--form-feed>"
msgstr "B<-F>, B<-f>, B<--form-feed>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use form feeds instead of newlines to separate pages (by a 3-line page "
"header with B<-F> or a 5-line header and trailer without B<-F>)"
msgstr ""
"używa znaku końca strony zamiast nowego wiersza do oddzielania stron (przez "
"trzywierszowy nagłówek z opcją B<-F> lub pięciowierszowy nagłówek i stopkę "
"bez opcji B<-F>)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--header>=I<\\,HEADER\\/>"
msgstr "B<-h>, B<--header>=I<NAGŁÓWEK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use a centered HEADER instead of filename in page header, B<-h> \"\" prints "
"a blank line, don't use B<-h>\"\""
msgstr ""
"w nagłówku strony używa wyśrodkowanego I<NAGŁÓWKA> zamiast nazwy pliku, B<-"
"h> I<\"\"> wyświetla pusty wiersz, proszę nie używać B<-h>I<\"\">"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i[CHAR[WIDTH]]>, B<--output-tabs>[=I<\\,CHAR[WIDTH]\\/>]"
msgstr "B<-i>[I<ZNAK>[I<SZEROKOŚĆ>]], B<--output-tabs>[=I<ZNAK>[I<SZEROKOŚĆ>]]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "replace spaces with CHARs (TABs) to tab WIDTH (8)"
msgstr ""
"zastępuje spacje I<ZNAKAMI> (lub tabulatorami) do I<SZEROKOŚCI> (domyślnie 8)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-J>, B<--join-lines>"
msgstr "B<-J>, B<--join-lines>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"merge full lines, turns off B<-W> line truncation, no column alignment, B<--"
"sep-string>[=I<\\,STRING\\/>] sets separators"
msgstr ""
"scala pełne wiersze, wyłącza obcinanie wierszy przez B<-W>, nie wykonuje "
"wyrównania kolumn; B<--sep-string>[B<=>I<ŁAŃCUCH>] ustawia separatory"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--length>=I<\\,PAGE_LENGTH\\/>"
msgstr "B<-l>, B<--length>=I<DŁUGOŚĆ-STRONY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"set the page length to PAGE_LENGTH (66) lines (default number of lines of "
"text 56, and with B<-F> 63).  implies B<-t> if PAGE_LENGTH E<lt>= 10"
msgstr ""
"ustawia długość strony na liczbę wierszy podanych w I<DŁUGOŚCI-STRONY> "
"(domyślna wartość przy podaniu tej opcji to 66, a bez niej 56 lub, z opcją "
"B<-F>, 63). I<DŁUGOŚĆ-STRONY> E<lt>=10 implikuje B<-t>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--merge>"
msgstr "B<-m>, B<--merge>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"print all files in parallel, one in each column, truncate lines, but join "
"lines of full length with B<-J>"
msgstr ""
"wyświetla wszystkie pliki równolegle, każdy w swojej kolumnie, przycina "
"wiersze, ale łączy wiersze pełnej długości z opcją B<-J>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-n[SEP[DIGITS]]>, B<--number-lines>[=I<\\,SEP[DIGITS]\\/>]"
msgstr "B<-n>[I<SEPARATOR>[I<CYFRY>]], B<--number-lines>[B<=>I<SEPARATOR>[I<CYFRY>]]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"number lines, use DIGITS (5) digits, then SEP (TAB), default counting starts "
"with 1st line of input file"
msgstr ""
"numeruje wiersze, używa liczby I<CYFR> (lub 5), następnie I<SEPARATORA> (lub "
"tabulatora), domyślnie liczy od pierwszego wiersza pliku wejściowego"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-N>, B<--first-line-number>=I<\\,NUMBER\\/>"
msgstr "B<-N>, B<--first-line-number>=I<NUMER>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"start counting with NUMBER at 1st line of first page printed (see "
"+FIRST_PAGE)"
msgstr ""
"zaczyna numerację pierwszego wiersza pierwszej wynikowej strony od I<NUMERU> "
"(patrz też B<+>I<PIERWSZA-STRONA>)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--indent>=I<\\,MARGIN\\/>"
msgstr "B<-o>, B<--indent>=I<MARGINES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"offset each line with MARGIN (zero) spaces, do not affect B<-w> or B<-W>, "
"MARGIN will be added to PAGE_WIDTH"
msgstr ""
"przesuwa całą stronę o I<MARGINES> (domyślnie zero) spacji, nie wpływa na B<-"
"w> ani B<-W>; I<MARGINES> jest dodawany do I<SZEROKOŚCI-STRONY>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--no-file-warnings>"
msgstr "B<-r>, B<--no-file-warnings>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "omit warning when a file cannot be opened"
msgstr "nie wypisuje ostrzeżeń, jeśli nie można otworzyć pliku"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s[CHAR]>, B<--separator>[=I<\\,CHAR\\/>]"
msgstr "B<-s>[I<ZNAK>], B<--separator>[B<=>I<ZNAK>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"separate columns by a single character, default for CHAR is the "
"E<lt>TABE<gt> character without B<-w> and 'no char' with B<-w>.  B<-s[CHAR]> "
"turns off line truncation of all 3 column options (B<-COLUMN>|-a B<-COLUMN>|-"
"m) except B<-w> is set"
msgstr ""
"oddziela kolumny pojedynczym znakiem, domyślnie, bez opcji B<-w> I<ZNAKIEM> "
"jest tabulator, a z B<-w> \"brak znaku\". B<-s>[I<ZNAK>] wyłącza obcinanie "
"wierszy we wszystkich trzech opcjach kolumn B<->I<KOLUMNA>, B<-a -"
">I<KOLUMNA>, B<-m>, z wyjątkiem przypadku, gdy ustawiona jest opcja B<-w>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-S[STRING]>, B<--sep-string>[=I<\\,STRING\\/>]"
msgstr "B<-S>[I<ŁAŃCUCH>], B<--sep-string>[B<=>I<ŁAŃCUCH>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"separate columns by STRING, without B<-S>: Default separator E<lt>TABE<gt> "
"with B<-J> and E<lt>spaceE<gt> otherwise (same as B<-S>\" \"), no effect on "
"column options"
msgstr ""
"oddziela kolumny I<ŁAŃCUCHEM>,\n"
"bez B<-S>: domyślnym separatorem jest tabulator z opcją B<-J> lub spacja w "
"przeciwnym wypadku (równoważne B<-S>I<\"\">), nie ma wpływu na opcje "
"dotyczące kolumn"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--omit-header>"
msgstr "B<-t>, B<--omit-header>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "omit page headers and trailers; implied if PAGE_LENGTH E<lt>= 10"
msgstr ""
"pomija nagłówek i stopkę strony; włączane również gdy I<DŁUGOŚĆ-STRONY> "
"E<lt>=10"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--omit-pagination>"
msgstr "B<-T>, B<--omit-pagination>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"omit page headers and trailers, eliminate any pagination by form feeds set "
"in input files"
msgstr ""
"pomija nagłówki i stopki, ignoruje formatowanie stron przez znaki nowej "
"strony w plikach wejściowych"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--show-nonprinting>"
msgstr "B<-v>, B<--show-nonprinting>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use octal backslash notation"
msgstr "używa ósemkowej notacji z odwrotnym ukośnikiem"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--width>=I<\\,PAGE_WIDTH\\/>"
msgstr "B<-w>, B<--width>=I<SZEROKOŚĆ-STRONY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"set page width to PAGE_WIDTH (72) characters for multiple text-column output "
"only, B<-s[char]> turns off (72)"
msgstr ""
"ustawia szerokość strony na I<SZEROKOŚĆ-STRONY> znaki (domyślnie 72), tylko "
"dla wyjścia wielokolumnowego; B<-s>[I<ZNAK>] wyłącza wartość domyślną (72)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-W>, B<--page-width>=I<\\,PAGE_WIDTH\\/>"
msgstr "B<-W>, B<--page-width>=I<SZEROKOŚĆ-STRONY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"set page width to PAGE_WIDTH (72) characters always, truncate lines, except "
"B<-J> option is set, no interference with B<-S> or B<-s>"
msgstr ""
"ustawia zawsze szerokość strony na I<SZEROKOŚĆ-STRONY> znaków (domyślnie "
"72), z wyjątkiem podania opcji B<-J>; nie oddziaływa z opcjami B<-S> ani B<-"
"s>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Pete TerMaat and Roland Huebner."
msgstr "Napisane przez Pete'a TerMaata i Rolanda Huebnera."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Strona internetowa z pomocą GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"O błędach tłumaczenia programu prosimy poinformować przez E<lt>https://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Jest to wolne oprogramowanie: można je zmieniać i rozpowszechniać. Nie ma "
"ŻADNEJ GWARANCJI, w granicach określonych przez prawo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/prE<gt>"
msgstr ""
"Pełna dokumentacja: E<lt>https://www.gnu.org/software/coreutils/prE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) pr invocation\\(aq"
msgstr "lub lokalnie, za pomocą B<info \\(aq(coreutils) pr invocation\\(aq>"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "wrzesień 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "październik 2024"

#. type: TH
#: fedora-41 opensuse-tumbleweed
#, no-wrap
msgid "September 2024"
msgstr "wrzesień 2024"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "November 2024"
msgstr "listopad 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "sierpień 2023"

#. type: TH
#: mageia-cauldron opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: mageia-cauldron opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "styczeń 2024"
