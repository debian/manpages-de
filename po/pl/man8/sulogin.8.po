# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Wojtek Kotwica <wkotwica@post.pl>, 1999.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2016, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-03-29 09:53+0100\n"
"PO-Revision-Date: 2022-02-13 19:09+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "SULOGIN"
msgstr "SULOGIN"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 maja 2022 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr "Administracja systemem"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: debian-bookworm
msgid "sulogin - single-user login"
msgstr "sulogin - login w trybie jednoużytkownikowym"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: debian-bookworm
msgid "B<sulogin> [options] [I<tty>]"
msgstr "B<sulogin> [I<opcje>] [I<tty>]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<sulogin> is invoked by B<init> when the system goes into single-user mode."
msgstr ""
"B<sulogin> jest przywoływany przez B<init>, gdy system przechodzi w tryb "
"jednoużytkownikowy."

#. type: Plain text
#: debian-bookworm
msgid "The user is prompted:"
msgstr "Wyświetlany jest następujący komunikat zachęty:"

#. type: Plain text
#: debian-bookworm
msgid ""
"Give root password for system maintenance (or type Control-D for normal "
"startup):"
msgstr ""
"B<Give root password for system maintenance (or type Control-D for normal "
"startup):> (Podaj hasło roota do konserwacji systemu lub wciśnij ^D aby "
"kontynuować zwykły start)"

#. type: Plain text
#: debian-bookworm
msgid ""
"If the root account is locked and B<--force> is specified, no password is "
"required."
msgstr ""
"Gdy konto roota jest zablokowane i podano opcję B<--force>, hasło nie jest "
"wymagane."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<sulogin> will be connected to the current terminal, or to the optional "
"I<tty> device that can be specified on the command line (typically I</dev/"
"console>)."
msgstr ""
"B<sulogin> będzie podłączony do bieżącego terminala lub do opcjonalnego "
"urządzenia I<tty> podanego w wierszu polecenia zwykle (I</dev/console>)."

#. type: Plain text
#: debian-bookworm
msgid ""
"When the user exits from the single-user shell, or presses control-D at the "
"prompt, the system will continue to boot."
msgstr ""
"Po tym, jak użytkownik wyjdzie z trybu jednoużytkownikowego lub wciśnie "
"control-D po wyświetleniu zachęty, system będzie kontynuował rozruch."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--force>"
msgstr "B<-e>, B<--force>"

#. type: Plain text
#: debian-bookworm
msgid ""
"If the default method of obtaining the root password from the system via "
"B<getpwnam>(3) fails, then examine I</etc/passwd> and I</etc/shadow> to get "
"the password. If these files are damaged or nonexistent, or when root "
"account is locked by \\(aq!\\(aq or \\(aq*\\(aq at the begin of the password "
"then B<sulogin> will B<start a root shell without asking for a password>."
msgstr ""
"Jeśli domyślna metoda pozyskania hasła roota z systemu za pomocą "
"B<getpwnam>(3) nie powiedzie się, to sprawdzane są pliki I</etc/passwd> i I</"
"etc/shadow>. Jeśli są one uszkodzone lub nie istnieją lub gdy konto roota "
"jest zablokowane symbolem B<!> lub B<*> na początku hasła, to B<sulogin "
"wywoła powłokę roota bez pytania o hasło>."

#. type: Plain text
#: debian-bookworm
msgid ""
"Only use the B<-e> option if you are sure the console is physically "
"protected against unauthorized access."
msgstr ""
"Opcji B<-e> należy używać tylko, gdy jest się pewnym, że konsola jest "
"fizycznie zabezpieczona przed nieautoryzowanym dostępem."

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--login-shell>"
msgstr "B<-p>, B<--login-shell>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specifying this option causes B<sulogin> to start the shell process as a "
"login shell."
msgstr ""
"Podanie ten opcji spowoduje, że B<sulogin> uruchomi proces powłoki jako "
"powłokę zgłoszeniową."

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--timeout> I<seconds>"
msgstr "B<-t>, B<--timeout> I<sekundy>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the maximum amount of time to wait for user input. By default, "
"B<sulogin> will wait forever."
msgstr ""
"Określa maksymalny czas oczekiwania na reakcję użytkownika. Domyślnie "
"B<sulogin> będzie czekał wiecznie."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Wyświetla ten tekst i kończy pracę."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Wyświetla informacje o wersji i kończy działanie."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ŚRODOWISKO"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<sulogin> looks for the environment variable B<SUSHELL> or B<sushell> to "
"determine what shell to start. If the environment variable is not set, it "
"will try to execute root\\(cqs shell from I</etc/passwd>. If that fails, it "
"will fall back to I</bin/sh>."
msgstr ""
"B<sulogin> szuka zmiennych środowiskowych B<SUSHELL> lub B<sushell>, aby "
"określić którą powłokę uruchomić. Jeśli zmienna środowiskowa nie jest "
"ustawiona, to program spróbuje wykonać powłokę roota z pliku I</etc/passwd>. "
"Jeśli to również się nie powiedzie, to na końcu wykorzystana zostanie "
"powłoka I</bin/sh>."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORZY"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<sulogin> was written by Miquel van Smoorenburg for sysvinit and later "
"ported to util-linux by Dave Reisner and Karel Zak."
msgstr ""
"B<sulogin> było napisane przez Miquela van Smoorenburga dla sysvinit i "
"później przeportowane dla util-linux przez Dave'a Reisnera i Karela Zaka."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Raporty o błędach proszę zgłaszać pod adresem"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "DOSTĘPNOŚĆ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<sulogin> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Polecenie B<sulogin> jest częścią pakietu util-linux i można je pobrać ze "
"strony"
