# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Robert Luberda <robert@debian.org>, 2006, 2012.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-12-06 18:09+0100\n"
"PO-Revision-Date: 2024-03-31 18:15+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_map_files"
msgstr "proc_pid_map_files"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/map_files/ - memory-mapped files"
msgstr "/proc/pid/map_files/ - pliki przypisane do pamięci"

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: TP
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</map_files/> (since Linux 3.3)"
msgstr "I</proc/>pidI</map_files/> (od Linuksa 3.3)"

#.  commit 640708a2cff7f81e246243b0073c66e6ece7e53e
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This subdirectory contains entries corresponding to memory-mapped files (see "
"B<mmap>(2)).  Entries are named by memory region start and end address pair "
"(expressed as hexadecimal numbers), and are symbolic links to the mapped "
"files themselves.  Here is an example, with the output wrapped and "
"reformatted to fit on an 80-column display:"
msgstr ""
"Podkatalog zawiera wpisy odnoszące się do plików zmapowanych do pamięci "
"(patrz B<mmap>(2)). Wpisy są nazwane jako pary adresów: początku i końca "
"obszaru pamięci (jako liczby szesnastkowe) i są dowiązaniami symbolicznymi "
"do samych zmapowanych plików. Oto przykład, zmodyfikowany aby zmieścić się w "
"80 kolumnowym terminalu:"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"#B< ls -l /proc/self/map_files/>\n"
"lr--------. 1 root root 64 Apr 16 21:31\n"
"            3252e00000-3252e20000 -E<gt> /usr/lib64/ld-2.15.so\n"
"\\&...\n"
msgstr ""
"#B< ls -l /proc/self/map_files/>\n"
"lr--------. 1 root root 64 Apr 16 21:31\n"
"            3252e00000-3252e20000 -E<gt> /usr/lib64/ld-2.15.so\n"
"\\&...\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Although these entries are present for memory regions that were mapped with "
"the B<MAP_FILE> flag, the way anonymous shared memory (regions created with "
"the B<MAP_ANON | MAP_SHARED> flags)  is implemented in Linux means that such "
"regions also appear on this directory.  Here is an example where the target "
"file is the deleted I</dev/zero> one:"
msgstr ""
"Choć te wpisy są dostępne dla obszarów pamięci przydzielonych flagą "
"B<MAP_FILE>, to sposób w jaki zaimplementowane jest anonimowe dzielenie "
"pamięci (obszary utworzone flagami B<MAP_ANON | MAP_SHARED>) oznacza, że "
"tego typu obszary również pojawią się w tym katalogu. Oto przykład, gdzie "
"plikiem docelowym jest usunięty I</dev/zero>:"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"lrw-------. 1 root root 64 Apr 16 21:33\n"
"            7fc075d2f000-7fc075e6f000 -E<gt> /dev/zero (deleted)\n"
msgstr ""
"lrw-------. 1 root root 64 Apr 16 21:33\n"
"            7fc075d2f000-7fc075e6f000 -E<gt> /dev/zero (deleted)\n"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Permission to access this file is governed by a ptrace access mode "
"B<PTRACE_MODE_READ_FSCREDS> check; see B<ptrace>(2)."
msgstr ""
"Uprawnienie dostępu do tego pliku zależy od sprawdzenia trybu dostępu "
"ptrace: B<PTRACE_MODE_READ_FSCREDS>; zob. B<ptrace>(2)."

#.  commit bdb4d100afe9818aebd1d98ced575c5ef143456c
#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Until Linux 4.3, this directory appeared only if the "
"B<CONFIG_CHECKPOINT_RESTORE> kernel configuration option was enabled."
msgstr ""
"Do Linuksa 4.3 ten katalog istniał tylko jeśli włączono opcję konfiguracyjną "
"jądra B<CONFIG_CHECKPOINT_RESTORE>."

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Capabilities are required to read the contents of the symbolic links in this "
"directory: before Linux 5.9, the reading process requires B<CAP_SYS_ADMIN> "
"in the initial user namespace; since Linux 5.9, the reading process must "
"have either B<CAP_SYS_ADMIN> or B<CAP_CHECKPOINT_RESTORE> in the initial (i."
"e. root) user namespace."
msgstr ""
"Do odczytu zawartości dowiązań symbolicznych w tym katalogu potrzebne są "
"przywileje: przed Linuksem 5.9, proces odczytujący musi mieć przywilej "
"B<CAP_SYS_ADMIN> w pierwotnej przestrzeni nazw użytkownika; od Linuksa 5.9, "
"proces odczytujący musi mieć B<CAP_SYS_ADMIN> lub B<CAP_CHECKPOINT_RESTORE> "
"w pierwotnej (tj. roota) przestrzeni nazw użytkownika."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15 sierpnia 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
