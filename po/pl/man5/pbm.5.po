# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 2000.
# Robert Luberda <robert@debian.org>, 2014.
# Michał Kułach <michal.kulach@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-10-04 17:57+0200\n"
"PO-Revision-Date: 2024-04-30 18:57+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "The PBM Format"
msgstr "Format PBM"

#. type: TH
#: debian-bookworm fedora-41 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "27 November 2013"
msgstr "27 listopada 2013"

#. type: TH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr "dokumentacja netpbm"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "pbm - Netpbm bi-level image format"
msgstr "pbm - dwupoziomowy format obrazu Netpbm"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr "Program jest częścią B<Netpbm>(1)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The PBM format is a lowest common denominator monochrome file format.  It "
"serves as the common language of a large family of bitmap image conversion "
"filters.  Because the format pays no heed to efficiency, it is simple and "
"general enough that one can easily develop programs to convert to and from "
"just about any other graphics format, or to manipulate the image."
msgstr ""
"Format PBM jest najmniejszym wspólnym mianownikiem dla monochromatycznego "
"formatu pliku. Jest wspólnym językiem większej rodziny filtrów konwersji "
"obrazów bitmapowych. Ponieważ format nie przejmuje się wydajnością, jest "
"prosty i wystarczająco ogólny, tak że każdy może tworzyć programy "
"konwertujące bitmapy z lub do innych formatów graficznych albo "
"przetwarzające obrazy."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The name \"PBM\" is an acronym derived from \"Portable Bit Map.\""
msgstr ""
"Nazwa \\[Bq]PBM\\[rq] jest akronimem, pochodzącym od słów \\[Bq]Przenośna "
"BitMapa\\[rq]."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is not a format that one would normally use to store a file or to "
"transmit it to someone -- it's too expensive and not expressive enough for "
"that.  It's just an intermediary format.  In it's purest use, it lives only "
"in a pipe between two other programs."
msgstr ""
"Nie jest to format, którego ktokolwiek normalnie by użył do przechowywania "
"pliku lub do przesłania go do kogoś innego \\(em jest zbyt drogi i mało "
"wyrazisty do tego. Jest to po prostu format przejściowy. Najczęściej jest "
"używany w potokach między dwoma programami."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "THE LAYOUT"
msgstr "UKŁAD"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The format definition is as follows."
msgstr "Definicja formatu jest następująca:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A PBM file consists of a sequence of one or more PBM images. There are no "
"data, delimiters, or padding before, after, or between images."
msgstr ""
"Plik PBM składa się z sekwencji jednego lub więcej obrazów PBM. Nie "
"występują żadne inne dane, separatory i wyrównania przed obrazami, po nich "
"ani między nimi."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Each PBM image consists of the following:"
msgstr "W skład każdego obrazu PBM wchodzą:"

#. type: IP
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A \"magic number\" for identifying the file type.  A pbm image's magic "
"number is the two characters \"P4\"."
msgstr ""
"\"Numer magiczny\", identyfikujący rodzaj pliku. Numerem magicznym pliku pbm "
"są dwa znaki: \"P4\"."

#
#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Whitespace (blanks, TABs, CRs, LFs)."
msgstr "Biała spacja (spacje, tabulacje, CR, LF)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The width in pixels of the image, formatted as ASCII characters in decimal."
msgstr "Szerokość obrazu pikselach, sformatowana jako dziesiętne znaki ASCII."

#
#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Whitespace."
msgstr "Biała spacja."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The height in pixels of the image, again in ASCII decimal."
msgstr "Wysokość obrazu w pikselach, znów jako dziesiętne znaki ASCII."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "A single whitespace character (usually a newline)."
msgstr "Pojedynczy znak białej spacji (zwykle znak nowego wiersza)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A raster of Height rows, in order from top to bottom.  Each row is Width "
"bits, packed 8 to a byte, with don't care bits to fill out the last byte in "
"the row.  Each bit represents a pixel: 1 is black, 0 is white.  The order of "
"the pixels is left to right.  The order of their storage within each file "
"byte is most significant bit to least significant bit.  The order of the "
"file bytes is from the beginning of the file toward the end of the file."
msgstr ""
"Raster o liczbie wierszy określonej przez wysokość obrazu, od góry do dołu. "
"Każdy wiersz składa się z bitów w liczbie zadanej przez szerokość obrazu. "
"Bity są pakowane po 8 w bajt. Ostatni bajt w wierszu jest wypełniony "
"nieznaczącymi bitami. Każdy bit reprezentuje piksel: 1 to czarny, a 0 to "
"biały. Porządek pikseli jest od lewej do prawej. Porządek ich przechowywania "
"w każdym bajcie pliku jest od najbardziej znaczącego bitu do najmniej "
"znaczącego. Porządek bajtów w pliku jest od początku pliku do końca pliku."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A row of an image is horizontal.  A column is vertical.  The pixels in the "
"image are square and contiguous."
msgstr ""
"Wiersz jest poziomy. Kolumna jest pionowa. Piksele w obrazie są kwadratowe i "
"ciągłe."

#. type: Plain text
#: debian-bookworm fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"Before the whitespace character that delimits the raster, any characters "
"from a \"#\" through the next carriage return or newline character, is a "
"comment and is ignored.  Note that this is rather unconventional, because a "
"comment can actually be in the middle of what you might consider a token.  "
"Note also that this means if you have a comment right before the raster, the "
"newline at the end of the comment is not sufficient to delimit the raster."
msgstr ""
"Przed znakiem białej spacji, który rozgranicza raster, wszelkie znaki "
"zaczynające się od \\[Bq]#\\[rq] do następnego powrotu karetki lub znaku "
"nowego wiersza są komentarzem i są ignorowane. Proszę zauważyć, że jest to "
"dość niekonwencjonalne, ponieważ komentarz może znajdować się w środku "
"czegoś, co zwykło się uważać za token. Oznacza to również, że jeśli "
"komentarz występuje zaraz przed rastrem, znak nowego wiersza na końcu "
"komentarza nie jest wystarczający, aby rozgraniczyć raster."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"All characters referred to herein are encoded in ASCII.  \"newline\" refers "
"to the character known in ASCII as Line Feed or LF.  A \"white space\" "
"character is space, CR, LF, TAB, VT, or FF (I.e. what the ANSI standard C "
"isspace() function calls white space)."
msgstr ""
"Wszystkie znaki, o których tu mowa są zakodowane w ASCII. Znak nowego "
"wiersza, odnosi się do znaku nowego wiersza ASCII (LF). Biały znak to "
"spacja, powrót karetki (CR), znak nowego wiersza (LF), tabulator (TAB), "
"tabulator pionowy (VT) lub wysuw strony (FF) \\[em] tj. to, co funkcja "
"isspace() standardu C ANSI nazywa białym znakiem."

#. type: SS
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Plain PBM"
msgstr "Prosty PBM"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There is actually another version of the PBM format, even more simplistic, "
"more lavishly wasteful of space than PBM, called Plain PBM.  Plain PBM "
"actually came first, but even its inventor couldn't stand its recklessly "
"squanderous use of resources after a while and switched to what we now know "
"as the regular PBM format.  But Plain PBM is so redundant -- so overstated "
"-- that it's virtually impossible to break.  You can send it through the "
"most liberal mail system (which was the original purpose of the PBM format) "
"and it will arrive still readable.  You can flip a dozen random bits and "
"easily piece back together the original image.  And we hardly need to define "
"the format here, because you can decode it by inspection."
msgstr ""
"Istnieje inna wersja formatu PBM, nawet jeszcze bardziej uproszczona, "
"jeszcze bardziej nieprzejmująca się zajmowaną przestrzenią niż PBM, zwana "
"\"prostym PBM\". Prosty PBM w zasadzie pojawił się wcześniej, ale nawet jego "
"twórca nie mógł znieść marnowania zasobów i po jakimś czasie wymyślił to co "
"dziś jest znane jako zwykły format PBM. Ale plik w formacie prostego PBM "
"jest tak nadmiarowy, tak przesadzony, że nie ma możliwości jego uszkodzenia. "
"Można wysyłać go przez nawet najbardziej liberalny system pocztowy (co było "
"oryginalnym celem formatu PBM), a plik i tak będzie wciąż czytelny. Można "
"zamienić tuzin przypadkowo wybranych bitów, a potem z łatwością "
"odtworzyć oryginalny obraz. Nie ma potrzeby definiowania tego formatu tutaj, "
"ponieważ łatwo można go rozszyfrować przez analizowanie."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Netpbm programs generate Raw PBM format instead of Plain PBM by default, but "
"the E<.UR index.html#commonoptions> common option E<.UE> \\& B<-plain> "
"chooses Plain PBM."
msgstr ""
"Program Netpbm tworzy domyślnie Surowy format PBM zamiast Prostego PBM, lecz "
"E<.UR index.html#commonoptions> opcja ogólna E<.UE> \\& B<-plain> wybiera "
"Prosty PBM."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The difference is:"
msgstr "Różnice to:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "There is exactly one image in a file."
msgstr "Plik zawiera dokładnie jeden obraz."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The \"magic number\" is \"P1\" instead of \"P4\"."
msgstr "\"Magiczny numer\" to \"P1\" zamiast \"P4\"."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each pixel in the raster is represented by a byte containing ASCII '1' or "
"'0', representing black and white respectively.  There are no fill bits at "
"the end of a row."
msgstr ""
"Każdy piksel w rastrze jest reprezentowany przez bajt zawierający znaki "
"ASCII: \"1\" lub \"0\", oznaczające odpowiednio kolor czarny lub biały. "
"Koniec wiersza nie zawiera bitów wyrównania."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "White space in the raster section is ignored."
msgstr "Biała spacja w sekcji rastra jest ignorowana."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You can put any junk you want after the raster, if it starts with a white "
"space character."
msgstr ""
"Po zakończeniu sekcji rastra można podać dowolne śmieci, jakie tylko się "
"chce, pod warunkiem, że zaczynają się od białej spacji."

#
#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "No line should be longer than 70 characters."
msgstr "Linia nie powinna być dłuższa niż 70 znaków."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Here is an example of a small image in the plain PBM format."
msgstr "Oto przykład małego obrazu w prostym formacie PBM:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"P1\n"
"# feep.pbm\n"
"24 7\n"
"0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n"
"0 1 1 1 1 0 0 1 1 1 1 0 0 1 1 1 1 0 0 1 1 1 1 0\n"
"0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 1 0\n"
"0 1 1 1 0 0 0 1 1 1 0 0 0 1 1 1 0 0 0 1 1 1 1 0\n"
"0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0\n"
"0 1 0 0 0 0 0 1 1 1 1 0 0 1 1 1 1 0 0 1 0 0 0 0\n"
"0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n"
msgstr ""
"P1\n"
"# feep.pbm\n"
"24 7\n"
"0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n"
"0 1 1 1 1 0 0 1 1 1 1 0 0 1 1 1 1 0 0 1 1 1 1 0\n"
"0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 1 0\n"
"0 1 1 1 0 0 0 1 1 1 0 0 0 1 1 1 0 0 0 1 1 1 1 0\n"
"0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0\n"
"0 1 0 0 0 0 0 1 1 1 1 0 0 1 1 1 1 0 0 1 0 0 0 0\n"
"0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "There is a newline character at the end of each of these lines."
msgstr "Na końcu każdego wiersza znajduje się znak nowego wiersza."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You can generate the Plain PBM format from the regular PBM format (first "
"image in the file only) with the B<pnmtoplainpnm> program."
msgstr ""
"Program B<pnmtoplainpnm>  umożliwia wygenerowanie Prostego formatu PBM z "
"regularnego formatu PBM (tylko pierwszy obraz)."

#
#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Programs that read this format should be as lenient as possible, accepting "
"anything that looks remotely like a bitmap."
msgstr ""
"Programy, odczytujące ten format powinny być jak najwyrozumialsze, "
"przyjmując wszystko, co z daleka wygląda na bitmapę."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "INTERNET MEDIA TYPE"
msgstr "IDENTYFIKATOR INTERNETOWY (MIME)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"No Internet Media Type (aka MIME type, content type) for PBM has been "
"registered with IANA, but the value CW<image/x-portable-bitmap> is "
"conventional."
msgstr ""
"Dla formatu PBM nie zarejestrowano internetowego identyfikatora formatu "
"danych (Internet Media Type \\[em] MIME) w IANA, lecz konwencjonalna jest "
"wartość CW<image/x-portable-bitmap>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that the PNM Internet Media Type CW<image/x-portable-anymap> also "
"applies."
msgstr "Proszę zauważyć, że CW<image/x-portable-anymap> dotyczy też PNM."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILE NAME"
msgstr "NAZWA PLIKU"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"There are no requirements on the name of a PBM file, but the convention is "
"to use the suffix \".pbm\".  \"pnm\" is also conventional, for cases where "
"distinguishing between the particular subformats of PNM is not convenient."
msgstr ""
"Nie ma wymagań odnośnie nazwy pliku PBM, lecz przyjęło się rozszerzenie "
"\\[Bq].pbm\\[rq]. Konwencjonalne jest również \\[Bq]pnm\\[rq] w przypadkach, "
"gdy nie jest wygodne rozróżnianie pomiędzy danymi podformatami PNM."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COMPATIBILITY"
msgstr "ZGODNOŚĆ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Before July 2000, there could be at most one image in a PBM file.  As a "
"result, most tools to process PBM files ignore (and don't read) any data "
"after the first image."
msgstr ""
"Przed lipcem 2000 roku plik PBM mógł zawierać tylko jeden obraz. W wyniku "
"tego większość narzędzi przetwarzających pliki PBM ignoruje (nie czyta) "
"danych po pierwszym obrazie."

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
msgid ""
"B<libnetpbm>(1)  \\&, B<pnm>(1)  \\&, B<pgm>(1)  \\&, B<ppm>(1)  \\&, "
"B<pam>(1)  \\&, B<programs that process PBM>(1)  \\&"
msgstr ""
"B<libnetpbm>(1)  \\&, B<pnm>(1)  \\&, B<pgm>(1)  \\&, B<ppm>(1)  \\&, "
"B<pam>(1)  \\&, programy, które przetwarzają PBM"

#. type: SH
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr "ŹRÓDŁO DOKUMENTU"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""
"Niniejszą stronę podręcznika wygenerowano za pomocą narzędzia Netpbm "
"\\[Bq]makeman\\[rq] ze źródeł HTML. Główna dokumentacja jest dostępna pod "
"adresem"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/pbm.html>"
msgstr "B<http://netpbm.sourceforge.net/doc/pbm.html>"

#. type: TH
#: debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "24 January 2024"
msgid "27 June 2024"
msgstr "24 stycznia 2024 r."

#. type: Plain text
#: debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Before the whitespace character that delimits the raster, any characters "
"from a \"#\" to but not including the next carriage return or newline "
"character, or end of file, is a comment and is ignored."
msgstr ""

#. type: Plain text
#: debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"(Before June 2024, the carriage return or line feed was specified to be\n"
"  part of the comment and ignored, but in the 22 years that comments had\n"
"  existed in the specification, Netpbm never implemented that).\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<libnetpbm>(3)  \\&, B<pnm>(5)  \\&, B<pgm>(5)  \\&, B<ppm>(5)  \\&, "
"B<pam>(5)  \\&, B<programs that process PBM>(1)  \\&"
msgstr ""
"B<libnetpbm>(3)  \\&, B<pnm>(5)  \\&, B<pgm>(5)  \\&, B<ppm>(5)  \\&, "
"B<pam>(5)  \\&, programy, które przetwarzają PBM"
