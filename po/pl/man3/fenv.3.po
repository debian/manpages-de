# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2001.
# Robert Luberda <robert@debian.org>, 2013, 2017, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-12-06 17:58+0100\n"
"PO-Revision-Date: 2024-04-17 18:35+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "fenv"
msgstr "fenv"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"feclearexcept, fegetexceptflag, feraiseexcept, fesetexceptflag, "
"fetestexcept, fegetenv, fegetround, feholdexcept, fesetround, fesetenv, "
"feupdateenv, feenableexcept, fedisableexcept, fegetexcept - floating-point "
"rounding and exception handling"
msgstr ""
"feclearexcept, fegetexceptflag, feraiseexcept, fesetexceptflag, "
"fetestexcept, fegetenv, fegetround, feholdexcept, fesetround, fesetenv, "
"feupdateenv, feenableexcept, fedisableexcept, fegetexcept - zaokrąglanie "
"zmiennoprzecinkowe i obsługa wyjątków"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Biblioteka matematyczna (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>fenv.hE<gt>>\n"
msgstr "B<#include E<lt>fenv.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int feclearexcept(int >I<excepts>B<);>\n"
"B<int fegetexceptflag(fexcept_t *>I<flagp>B<, int >I<excepts>B<);>\n"
"B<int feraiseexcept(int >I<excepts>B<);>\n"
"B<int fesetexceptflag(const fexcept_t *>I<flagp>B<, int >I<excepts>B<);>\n"
"B<int fetestexcept(int >I<excepts>B<);>\n"
msgstr ""
"B<int feclearexcept(int >I<excepts>B<);>\n"
"B<int fegetexceptflag(fexcept_t *>I<flagp>B<, int >I<excepts>B<);>\n"
"B<int feraiseexcept(int >I<excepts>B<);>\n"
"B<int fesetexceptflag(const fexcept_t *>I<flagp>B<, int >I<excepts>B<);>\n"
"B<int fetestexcept(int >I<excepts>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int fegetround(void);>\n"
"B<int fesetround(int >I<rounding_mode>B<);>\n"
msgstr ""
"B<int fegetround(void);>\n"
"B<int fesetround(int >I<rounding_mode>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int fegetenv(fenv_t *>I<envp>B<);>\n"
"B<int feholdexcept(fenv_t *>I<envp>B<);>\n"
"B<int fesetenv(const fenv_t *>I<envp>B<);>\n"
"B<int feupdateenv(const fenv_t *>I<envp>B<);>\n"
msgstr ""
"B<int fegetenv(fenv_t *>I<envp>B<);>\n"
"B<int feholdexcept(fenv_t *>I<envp>B<);>\n"
"B<int fesetenv(const fenv_t *>I<envp>B<);>\n"
"B<int feupdateenv(const fenv_t *>I<envp>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These eleven functions were defined in C99, and describe the handling of "
"floating-point rounding and exceptions (overflow, zero-divide, etc.)."
msgstr ""
"Tych jedenaście funkcji zdefiniowanych w C99 realizuje obsługę "
"zmiennoprzecinkowego zaokrąglania i wyjątków (nadmiar, dzielenie przez zero, "
"itp.)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Exceptions"
msgstr "Wyjątki"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<divide-by-zero> exception occurs when an operation on finite numbers "
"produces infinity as exact answer."
msgstr ""
"Wyjątek I<divide-by-zero> (dzielenie przez zero) występuje, gdy dokładnym "
"wynikiem operacji na liczbach skończonych jest nieskończoność."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<overflow> exception occurs when a result has to be represented as a "
"floating-point number, but has (much) larger absolute value than the largest "
"(finite) floating-point number that is representable."
msgstr ""
"Wyjątek I<overflow> (nadmiar) występuje, gdy wynik musi być przedstawiony "
"jako liczba zmiennoprzecinkowa, ale ma on (dużo) większą wartość bezwzględną "
"niż największa (skończona) liczba zmiennoprzecinkowa mająca przedstawienie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<underflow> exception occurs when a result has to be represented as a "
"floating-point number, but has smaller absolute value than the smallest "
"positive normalized floating-point number (and would lose much accuracy when "
"represented as a denormalized number)."
msgstr ""
"Wyjątek I<underflow> (niedomiar) występuje, gdy wynik musi być przedstawiony "
"jako liczba zmiennoprzecinkowa, ale ma mniejszą wartość bezwzględną niż "
"najmniejsza dodatnia znormalizowana liczba zmiennoprzecinkowa (i nastąpiłaby "
"duża utrata precyzji, gdyby przedstawić go jako liczbę nieznormalizowaną)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<inexact> exception occurs when the rounded result of an operation is "
"not equal to the infinite precision result.  It may occur whenever "
"I<overflow> or I<underflow> occurs."
msgstr ""
"Wyjątek I<inexact> (niedokładny) występuje, gdy wynik operacji zaokrąglenia "
"nie jest równy wynikowi o nieskończonej precyzji. Może on towarzyszyć "
"wystąpieniu I<overflow> lub I<underflow>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<invalid> exception occurs when there is no well-defined result for an "
"operation, as for 0/0 or infinity - infinity or sqrt(-1)."
msgstr ""
"Wyjątek I<invalid> (nieprawidłowy) występuje, gdy operacja nie ma dobrze "
"zdefiniowanego wyniku, przykłady: 0/0, nieskończoność - nieskończoność lub "
"sqrt(-1)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Exception handling"
msgstr "Obsługa wyjątków"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Exceptions are represented in two ways: as a single bit (exception present/"
"absent), and these bits correspond in some implementation-defined way with "
"bit positions in an integer, and also as an opaque structure that may "
"contain more information about the exception (perhaps the code address where "
"it occurred)."
msgstr ""
"Wyjątki są reprezentowane na dwa sposoby: jako pojedyncze bity (wyjątek "
"obecny/nieobecny), które to bity odpowiadają w pewien zależny od "
"implementacji sposób pozycjom bitowym w liczbie całkowitej, i jako "
"nieprzezroczysta struktura, która może zawierać więcej informacji o "
"wyjątkach (zapewne adres kodu, gdzie wyjątek wystąpił)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each of the macros B<FE_DIVBYZERO>, B<FE_INEXACT>, B<FE_INVALID>, "
"B<FE_OVERFLOW>, B<FE_UNDERFLOW> is defined when the implementation supports "
"handling of the corresponding exception, and if so then defines the "
"corresponding bit(s), so that one can call exception handling functions, for "
"example, using the integer argument B<FE_OVERFLOW>|B<FE_UNDERFLOW>.  Other "
"exceptions may be supported.  The macro B<FE_ALL_EXCEPT> is the bitwise OR "
"of all bits corresponding to supported exceptions."
msgstr ""
"Każde z makr B<FE_DIVBYZERO>, B<FE_INEXACT>, B<FE_INVALID>, B<FE_OVERFLOW>, "
"B<FE_UNDERFLOW> jest zdefiniowane, gdy implementacja wspiera obsługę "
"odpowiedniego wyjątku, i wówczas definiuje odpowiedni(e) bit(y), "
"umożliwiając wywoływanie funkcji obsługi wyjątków, na przykład podając "
"argument całkowity B<FE_OVERFLOW>|B<FE_UNDERFLOW>. Dla innych wyjątków może "
"nie być wsparcia. Makro B<FE_ALL_EXCEPT> jest bitowym OR wszystkich bitów "
"odpowiadającym wspieranym wyjątkom."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feclearexcept>()  function clears the supported exceptions represented "
"by the bits in its argument."
msgstr ""
"Funkcja B<feclearexcept>() zeruje wspierane wyjątki reprezentowane przez "
"bity jej argumentu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fegetexceptflag>()  function stores a representation of the state of "
"the exception flags represented by the argument I<excepts> in the opaque "
"object I<*flagp>."
msgstr ""
"Funkcja B<fegetexceptflag>() umieszcza odwzorowanie stanu znaczników "
"przypisanych wyjątkom reprezentowanym przez argument I<excepts> w "
"nieprzezroczystym obiekcie I<*flagp>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feraiseexcept>()  function raises the supported exceptions represented "
"by the bits in I<excepts>."
msgstr ""
"Funkcja B<feraiseexcept>() zgłasza wspierane wyjątki reprezentowane przez "
"bity I<excepts>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fesetexceptflag>()  function sets the complete status for the "
"exceptions represented by I<excepts> to the value I<*flagp>.  This value "
"must have been obtained by an earlier call of B<fegetexceptflag>()  with a "
"last argument that contained all bits in I<excepts>."
msgstr ""
"Funkcja B<fesetexceptflag>() ustawia pełny stan o wartości I<*flagp> dla "
"wyjątków reprezentowanych przez I<excepts>. Wartość ta musi być otrzymana "
"jako wynik wcześniejszego wywołania B<fegetexceptflag>() z ostatnim "
"argumentem zawierającym wszystkie bity I<excepts>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fetestexcept>()  function returns a word in which the bits are set "
"that were set in the argument I<excepts> and for which the corresponding "
"exception is currently set."
msgstr ""
"Funkcja B<fetestexcept>() zwraca słowo z ustawionymi tymi bitami, które są "
"ustawione w argumencie I<excepts> i dla których jest obecnie ustawiony "
"odpowiedni wyjątek."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Rounding mode"
msgstr "Tryb zaokrąglania"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The rounding mode determines how the result of floating-point operations is "
"treated when the result cannot be exactly represented in the significand.  "
"Various rounding modes may be provided: round to nearest (the default), "
"round up (toward positive infinity), round down (toward negative infinity), "
"and round toward zero."
msgstr ""
"Tryb zaokrąglania określa w jaki sposób traktuje się wynik operacji "
"zmiennoprzecinkowej, jeśli nie może być on dokładnie reprezentowany w "
"mantysie. Mogą być dostarczane różne tryby zaokrągleń: zaokrąglanie do "
"najbliższej liczby (domyślnie), zaokrąglanie w górę (w kierunku dodatniej "
"nieskończoności), zaokrąglanie w dół (w kierunku ujemnej nieskończoności) i "
"zaokrąglanie w kierunku zera."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each of the macros B<FE_TONEAREST>, B<FE_UPWARD>, B<FE_DOWNWARD>, and "
"B<FE_TOWARDZERO> is defined when the implementation supports getting and "
"setting the corresponding rounding direction."
msgstr ""
"Każde z makr B<FE_TONEAREST>, B<FE_UPWARD>, B<FE_DOWNWARD> oraz "
"B<FE_TOWARDZERO> jest zdefiniowane, gdy implementacja wspiera pobieranie i "
"ustawianie odpowiedniego kierunku zaokrąglania."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fegetround>()  function returns the macro corresponding to the current "
"rounding mode."
msgstr ""
"Funkcja B<fegetround>() zwraca makro odpowiadające bieżącemu trybowi "
"zaokrąglania."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fesetround>()  function sets the rounding mode as specified by its "
"argument and returns zero when it was successful."
msgstr ""
"Funkcja B<fesetround>() ustawia tryb zaokrąglania podany jako jej argument i "
"zwraca zero, gdy się powiedzie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"C99 and POSIX.1-2008 specify an identifier, B<FLT_ROUNDS>, defined in "
"I<E<lt>float.hE<gt>>, which indicates the implementation-defined rounding "
"behavior for floating-point addition.  This identifier has one of the "
"following values:"
msgstr ""
"C99 i POSIX.1-2008 określają identyfikator B<FLT_ROUNDS> definiowany w "
"I<E<lt>float.hE<gt>> wskazujący na zależny od implementacji sposób "
"zaokrąglania podczas dodawania zmiennoprzecinkowego. Identyfikator ten może "
"przyjmować jedną z poniższych wartości:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-1>"
msgstr "B<-1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The rounding mode is not determinable."
msgstr "Nie można określić trybu zaokrąglania."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Rounding is toward 0."
msgstr "Zaokrąglanie w kierunku 0."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Rounding is toward nearest number."
msgstr "Zaokrąglanie w kierunku najbliższej liczby."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Rounding is toward positive infinity."
msgstr "Zaokrąglanie w kierunku dodatniej nieskończoności."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<3>"
msgstr "B<3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Rounding is toward negative infinity."
msgstr "Zaokrąglanie w kierunku ujemnej nieskończoności."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Other values represent machine-dependent, nonstandard rounding modes."
msgstr ""
"Inne wartości oznaczają tryby zaokrąglania niestandardowe, zależne od "
"architektury komputera."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value of B<FLT_ROUNDS> should reflect the current rounding mode as set "
"by B<fesetround>()  (but see BUGS)."
msgstr ""
"Wartość B<FLT_ROUNDS> powinna odzwierciedlać bieżący tryb zaokrąglania "
"ustawiony przez B<fesetround>() (patrz także USTERKI)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Floating-point environment"
msgstr "Środowisko zmiennoprzecinkowe"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The entire floating-point environment, including control modes and status "
"flags, can be handled as one opaque object, of type I<fenv_t>.  The default "
"environment is denoted by B<FE_DFL_ENV> (of type I<const fenv_t\\ *>).  This "
"is the environment setup at program start and it is defined by ISO C to have "
"round to nearest, all exceptions cleared and a nonstop (continue on "
"exceptions) mode."
msgstr ""
"Całe środowisko zmiennoprzecinkowe, włączając w to tryby sterowania i "
"znaczniki stanu, może być obsługiwane jako jeden nieprzezroczysty obiekt "
"typu I<fenv_t>.  Środowisko domyślne jest określone przez B<FE_DFL_ENV> "
"(typu I<const fenv_t\\ *>). Są to ustawienia środowiska przy uruchomieniu "
"programu i są one zdefiniowane przez ISO C jako: zaokrąglanie do "
"najbliższej, wszystkie wyjątki wyzerowane i tryb nieprzerywany (kontynuacja "
"w przypadku wystąpienia wyjątku)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fegetenv>()  function saves the current floating-point environment in "
"the object I<*envp>."
msgstr ""
"Funkcja B<fegetenv>() zachowuje bieżące środowisko zmiennoprzecinkowe w "
"obiekcie I<*envp>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feholdexcept>()  function does the same, then clears all exception "
"flags, and sets a nonstop (continue on exceptions) mode, if available.  It "
"returns zero when successful."
msgstr ""
"Funkcja B<feholdexcept>() robi to samo, a następnie zeruje wszystkie "
"znaczniki wyjątków i ustawia tryb nieprzerywany (kontynuacja w przypadku "
"wystąpienia wyjątku), o ile to możliwe. Zwraca zero, gdy się powiedzie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fesetenv>()  function restores the floating-point environment from the "
"object I<*envp>.  This object must be known to be valid, for example, the "
"result of a call to B<fegetenv>()  or B<feholdexcept>()  or equal to "
"B<FE_DFL_ENV>.  This call does not raise exceptions."
msgstr ""
"Funkcja B<fesetenv>() odtwarza środowisko zmiennoprzecinkowe z obiektu "
"I<*envp>. Obiekt ten musi być znany jako poprawny, na przykład jako wynik "
"wywołania B<fegetenv>() lub B<feholdexcept>() lub jako równy B<FE_DFL_ENV>. "
"To wywołanie nie zgłasza wyjątków."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feupdateenv>()  function installs the floating-point environment "
"represented by the object I<*envp>, except that currently raised exceptions "
"are not cleared.  After calling this function, the raised exceptions will be "
"a bitwise OR of those previously set with those in I<*envp>.  As before, the "
"object I<*envp> must be known to be valid."
msgstr ""
"Funkcja B<feupdateenv>() instaluje środowisko zmiennoprzecinkowe odwzorowane "
"w obiekcie I<*envp>, poza tym, że obecnie zgłoszone wyjątki nie są zerowane. "
"Po jej wywołaniu, zgłoszone wyjątki będą bitowym OR tych zgłoszonych "
"wcześniej oraz zawartych w I<*envp>. Jak poprzednio, obiekt I<*envp> musi "
"być znany jako poprawny."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#.  Earlier seven of these functions were listed as returning void.
#.  This was corrected in Corrigendum 1 (ISO/IEC 9899:1999/Cor.1:2001(E))
#.  of the C99 Standard.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions return zero on success and nonzero if an error occurred."
msgstr ""
"Funkcje te zwracają zero, gdy się powiodą, lub wartość niezerową, gdy "
"wystąpi błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<feclearexcept>(),\n"
"B<fegetexceptflag>(),\n"
"B<feraiseexcept>(),\n"
"B<fesetexceptflag>(),\n"
"B<fetestexcept>(),\n"
"B<fegetround>(),\n"
"B<fesetround>(),\n"
"B<fegetenv>(),\n"
"B<feholdexcept>(),\n"
"B<fesetenv>(),\n"
"B<feupdateenv>(),\n"
"B<feenableexcept>(),\n"
"B<fedisableexcept>(),\n"
"B<fegetexcept>()"
msgstr ""
"B<feclearexcept>(),\n"
"B<fegetexceptflag>(),\n"
"B<feraiseexcept>(),\n"
"B<fesetexceptflag>(),\n"
"B<fetestexcept>(),\n"
"B<fegetround>(),\n"
"B<fesetround>(),\n"
"B<fegetenv>(),\n"
"B<feholdexcept>(),\n"
"B<fesetenv>(),\n"
"B<feupdateenv>(),\n"
"B<feenableexcept>(),\n"
"B<fedisableexcept>(),\n"
"B<fegetexcept>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-bezpieczne"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008, IEC 60559 (IEC 559:1989), ANSI/IEEE 854."
msgstr "C11, POSIX.1-2008, IEC 60559 (IEC 559:1989), ANSI/IEEE 854."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C99, POSIX.1-2001.  glibc 2.1."
msgstr "C99, POSIX.1-2001.  glibc 2.1."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "glibc notes"
msgstr "Uwagi dla glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If possible, the GNU C Library defines a macro B<FE_NOMASK_ENV> which "
"represents an environment where every exception raised causes a trap to "
"occur.  You can test for this macro using B<#ifdef>.  It is defined only if "
"B<_GNU_SOURCE> is defined.  The C99 standard does not define a way to set "
"individual bits in the floating-point mask, for example, to trap on specific "
"flags.  Since glibc 2.2, glibc supports the functions B<feenableexcept>()  "
"and B<fedisableexcept>()  to set individual floating-point traps, and "
"B<fegetexcept>()  to query the state."
msgstr ""
"O ile to możliwe, biblioteka GNU C definiuje makro B<FE_NOMASK_ENV> "
"odwzorowujące środowisko, w którym każde zgłoszenie wyjątku powoduje "
"wystąpienie pułapki. Można sprawdzać wartość tego makra za pomocą B<#ifdef>. "
"Jest ono zdefiniowane jedynie, gdy zdefiniowane jest B<_GNU_SOURCE>. "
"Standard C99 nie określa sposobu ustawiania poszczególnych bitów w masce "
"zmiennoprzecinkowej, na przykład aby przechwytywać tylko wybrane znaczniki. "
"Od glibc 2.2, glibc obsługuje funkcje B<feenableexcept>() i "
"B<fedisableexcept>() ustawiające wybrane znaczniki zmiennoprzecinkowe oraz "
"B<fegetexcept>() odpytującą o stan."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>fenv.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>         /* Patrz feature_test_macros(7) */\n"
"B<#include E<lt>fenv.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int feenableexcept(int >I<excepts>B<);>\n"
"B<int fedisableexcept(int >I<excepts>B<);>\n"
"B<int fegetexcept(void);>\n"
msgstr ""
"B<int feenableexcept(int >I<excepts>B<);>\n"
"B<int fedisableexcept(int >I<excepts>B<);>\n"
"B<int fegetexcept(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<feenableexcept>()  and B<fedisableexcept>()  functions enable "
"(disable) traps for each of the exceptions represented by I<excepts> and "
"return the previous set of enabled exceptions when successful, and -1 "
"otherwise.  The B<fegetexcept>()  function returns the set of all currently "
"enabled exceptions."
msgstr ""
"Funkcje B<feenableexcept>() i B<fedisableexcept>() włączają (wyłączają) "
"pułapkowanie poszczególnych wyjątków odwzorowanych w przez I<excepts> i "
"zwracają poprzednie ustawienie pułapkowania wyjątków, jeśli się powiodą, a "
"-1 w pozostałych przypadkach. Funkcja B<fegetexcept>() zwraca bieżące "
"ustawienie pułapkowania wyjątków."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "USTERKI"

#.  Aug 08, glibc 2.8
#.  See http://gcc.gnu.org/ml/gcc/2002-02/msg01535.html
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"C99 specifies that the value of B<FLT_ROUNDS> should reflect changes to the "
"current rounding mode, as set by B<fesetround>().  Currently, this does not "
"occur: B<FLT_ROUNDS> always has the value 1."
msgstr ""
"C99 określa, że wartość B<FLT_ROUNDS> powinna odzwierciedlać zmiany "
"bieżącego trybu zaokrąglania ustawiane przez B<fesetround>(). Obecnie to nie "
"działa: B<FLT_ROUNDS> ma zawsze wartość 1."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<math_error>(7)"
msgstr "B<math_error>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#. type: Plain text
#: debian-bookworm
msgid "These functions were added in glibc 2.1."
msgstr "Funkcje te dodano w glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "IEC 60559 (IEC 559:1989), ANSI/IEEE 854, C99, POSIX.1-2001."
msgstr "IEC 60559 (IEC 559:1989), ANSI/IEEE 854, C99, POSIX.1-2001."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
