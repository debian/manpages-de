# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2014.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-12-06 17:59+0100\n"
"PO-Revision-Date: 2024-04-19 21:19+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "fwide"
msgstr "fwide"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "fwide - set and determine the orientation of a FILE stream"
msgstr "fwide - ustawia i określa orientację strumienia FILE"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int fwide(FILE *>I<stream>B<, int >I<mode>B<);>\n"
msgstr "B<int fwide(FILE *>I<stream>B<, int >I<mode>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Wymagane ustawienia makr biblioteki glibc (patrz B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<fwide>():"
msgstr "B<fwide>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500 || _ISOC99_SOURCE\n"
"        || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500 || _ISOC99_SOURCE\n"
"        || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When I<mode> is zero, the B<fwide>()  function determines the current "
"orientation of I<stream>.  It returns a positive value if I<stream> is wide-"
"character oriented, that is, if wide-character I/O is permitted but char I/O "
"is disallowed.  It returns a negative value if I<stream> is byte "
"oriented\\[em]that is, if char I/O is permitted but wide-character I/O is "
"disallowed.  It returns zero if I<stream> has no orientation yet; in this "
"case the next I/O operation might change the orientation (to byte oriented "
"if it is a char I/O operation, or to wide-character oriented if it is a wide-"
"character I/O operation)."
msgstr ""
"Gdy I<mode> jest zerem, funkcja B<fwide>() podaje bieżącą orientację "
"strumienia I<stream>. Zwraca wartość dodatnią, gdy I<stream> jest "
"zorientowany szerokoznakowo, tzn. gdy wejście/wyjście szerokich znaków jest "
"dozwolone, a wejście/wyjście dla znaków typu char nie jest dozwolone. "
"Wartość ujemna jest zwracana, gdy I<stream> jest zorientowany bajtowo, tzn. "
"gdy wejście/wyjście dla znaków typu char jest dozwolone, a wejście/wyjście "
"dla szerokich znaków nie jest dozwolone. Zero jest zwracane, gdy strumień "
"I<stream> nie ma jeszcze orientacji; w tym przypadku następna operacja "
"wejścia/wyjścia może zmienić orientację (na bajtową, jeśli będzie to "
"operacja wejścia/wyjścia znaków typu char, lub na szerokoznakową, jeśli "
"będzie to operacja wejścia/wyjścia szerokich znaków)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Once a stream has an orientation, it cannot be changed and persists until "
"the stream is closed."
msgstr ""
"Raz uzyskana przez strumień orientacja nie może zostać zmieniona aż do "
"zamknięcia strumienia."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When I<mode> is nonzero, the B<fwide>()  function first attempts to set "
"I<stream>'s orientation (to wide-character oriented if I<mode> is greater "
"than 0, or to byte oriented if I<mode> is less than 0).  It then returns a "
"value denoting the current orientation, as above."
msgstr ""
"Gdy I<mode> jest różne od zera, to funkcja B<fwide>() najpierw próbuje "
"ustawić orientację strumienia I<stream> (na szerokoznakową, gdy I<mode> jest "
"większe od 0, lub na bajtową, gdy I<mode> jest mniejsze od 0). Następnie "
"zwraca wartość określającą bieżącą orientację, jak opisano powyżej."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<fwide>()  function returns the stream's orientation, after possibly "
"changing it.  A positive return value means wide-character oriented.  A "
"negative return value means byte oriented.  A return value of zero means "
"undecided."
msgstr ""
"Funkcja B<fwide>() zwraca orientację strumienia, po jej ewentualnej "
"zamianie. Zwrócenie wartości dodatniej oznacza orientację szerokoznakową. "
"Zwrócenie wartości ujemnej oznacza orientację bajtową. Zwrócenie wartości "
"zero oznacza orientację nieokreśloną."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-unstable fedora-41 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Wide-character output to a byte oriented stream can be performed through the "
"B<fprintf>(3)  function with the B<%lc> and B<%ls> directives."
msgstr ""
"Zapisanie szerokich znaków do strumienia zorientowanego bajtowo może zostać "
"wykonane za pomocą funkcji B<fprintf>(3) przy użyciu dyrektyw  B<%lc> oraz "
"B<%ls>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Char oriented output to a wide-character oriented stream can be performed "
"through the B<fwprintf>(3)  function with the B<%c> and B<%s> directives."
msgstr ""
"Zapisanie znaków typu char do strumienia zorientowanego szerokoznakowo  może "
"być wykonane za pomocą funkcji B<fwprintf>(3) przy użyciu dyrektyw B<%c> "
"oraz B<%s>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<fprintf>(3), B<fwprintf>(3)"
msgstr "B<fprintf>(3), B<fwprintf>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
