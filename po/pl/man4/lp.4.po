# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Piotr Pogorzelski <piotr.pogorzelski@ippt.gov.pl>, 1996.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-12-06 18:03+0100\n"
"PO-Revision-Date: 2016-05-25 19:38+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "lp"
msgstr "lp"

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 maja 2024 r."

#. type: TH
#: archlinux debian-unstable fedora-41 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "lp - line printer devices"
msgstr "lp - urządzenia drukarek wierszowych"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/lp.hE<gt>>\n"
msgstr "B<#include E<lt>linux/lp.hE<gt>>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "KONFIGURACJA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<lp>[0\\[en]2] are character devices for the parallel line printers; they "
"have major number 6 and minor number 0\\[en]2.  The minor numbers correspond "
"to the printer port base addresses 0x03bc, 0x0378, and 0x0278.  Usually they "
"have mode 220 and are owned by user I<root> and group I<lp>.  You can use "
"printer ports either with polling or with interrupts.  Interrupts are "
"recommended when high traffic is expected, for example, for laser printers.  "
"For typical dot matrix printers, polling will usually be enough.  The "
"default is polling."
msgstr ""
"Pliki B<lp>[0\\[en]2] są urządzeniami znakowymi obsługującymi drukarki "
"dołączone do portów równoległych; numer główny tych urządzeń jest równy 6, "
"numer podrzędny 0\\[en]2. Numery podrzędne odpowiadają adresom bazowym portu "
"drukarki: 0x03bc, 0x0378 i 0x0278. Zwykle prawa dostępu do plików tych "
"urządzeń wynoszą 220, a ich właścicielem jest użytkownik I<root> i grupa "
"I<lp>. Porty drukarki mogą być obsługiwane zarówno przy wykorzystaniu "
"próbkowania, jak i przerwań. Stosowanie przerwań jest zalecane, gdy "
"spodziewamy się dużego ruchu, np. wynikającego z używania drukarki "
"laserowej. W przypadku zwykłych drukarek mozaikowych wystarczające jest "
"próbkowanie. Domyślnym trybem obsługi portu jest próbkowanie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following B<ioctl>(2)  calls are supported:"
msgstr "Obsługiwane są następujące wywołania funkcji B<ioctl>(2):"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPTIME, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPTIME, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the amount of time that the driver sleeps before rechecking the printer "
"when the printer's buffer appears to be filled to I<arg>.  If you have a "
"fast printer, decrease this number; if you have a slow printer, then "
"increase it.  This is in hundredths of a second, the default 2 being 0.02 "
"seconds.  It influences only the polling driver."
msgstr ""
"Ustawia okres, przez jaki sterownik jest uśpiony, zanim powtórnie sprawdzi "
"stan drukarki w sytuacji, gdy bufor drukarki zdaje się być wypełnionym do "
"I<arg>. Jeśli drukarka jest szybka, można zmniejszyć ten parametr.  Jeśli "
"drukarka jest wolna, należy zwiększyć ten parametr. Jest to liczba "
"określająca czas w setnych częściach sekundy, wartość domyślna wynosi 2, co "
"oznacza 0,02 sekundy. Parametr ten wpływa jedynie na tryb próbkowania."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPCHAR, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPCHAR, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the maximum number of busy-wait iterations which the polling driver "
"does while waiting for the printer to get ready for receiving a character to "
"I<arg>.  If printing is too slow, increase this number; if the system gets "
"too slow, decrease this number.  The default is 1000.  It influences only "
"the polling driver."
msgstr ""
"Ustawia na I<argv> maksymalną liczbę iteracji aktywnego oczekiwania (busy-"
"wait), jakie wykonuje sterownik próbkujący czekając, aż drukarka będzie "
"gotowa na otrzymywanie znaków. Jeśli drukowanie odbywa się zbyt wolno należy "
"zwiększyć tę liczbę; jeśli system za bardzo zwalnia \\(em należy ją "
"zmniejszyć. Domyślną wartością tego parametru jest 1000. Parametr ma wpływ "
"na sterownik jedynie w trybie próbkowania."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPABORT, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPABORT, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, the printer driver will retry on errors, otherwise it will "
"abort.  The default is 0."
msgstr ""
"Jeśli parametr I<arg> jest równy 0, wtedy sterownik drukarki będzie "
"powtarzał operację po wystąpieniu błędu; w przeciwnym wypadku przerwie "
"działanie. Standardową wartością tego parametru jest 0."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPABORTOPEN, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPABORTOPEN, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, B<open>(2)  will be aborted on error, otherwise error will "
"be ignored.  The default is to ignore it."
msgstr ""
"Jeśli I<arg> jest równy 0, funkcja B<open>(2) zostanie przerwana po "
"wystąpieniu błędu; w przeciwnym wypadku błąd zostanie zignorowany. "
"Standardowe zachowanie polega na ignorowaniu błędów."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPCAREFUL, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPCAREFUL, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, then the out-of-paper, offline, and error signals are "
"required to be false on all writes, otherwise they are ignored.  The default "
"is to ignore them."
msgstr ""
"Jeśli I<arg> jest równy 0, wtedy wymaga się, aby sygnały \"out-of-paper\", "
"\"offline\" oraz sygnały błędów były puste (false) we wszystkich operacjach "
"zapisu, w przeciwnym wypadku są ignorowane. Domyślnie są ignorowane. "

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPWAIT, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPWAIT, int >argB<)>"

#.  FIXME . Actually, since Linux 2.2, the default is 1
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the number of busy waiting iterations to wait before strobing the "
"printer to accept a just-written character, and the number of iterations to "
"wait before turning the strobe off again, to I<arg>.  The specification says "
"this time should be 0.5 microseconds, but experience has shown the delay "
"caused by the code is already enough.  For that reason, the default value is "
"0.  This is used for both the polling and the interrupt driver."
msgstr ""
"Ustawia na I<arg> liczbę iteracji aktywnego oczekiwania (busy-wait) przed "
"spróbkowaniem, czy drukarka zaakceptowała właśnie zapisany znak, oraz liczbę "
"iteracji, które należy przeczekać przed kolejnym próbkowaniem. Specyfikacje "
"określają ten czas jako 0.5 mikrosekundy, lecz doświadczenie wykazało, że "
"opóźnienie spowodowane wykonaniem kodu jest już wystarczające. Z tego powodu "
"wartością domyślną dla tego ustawienia jest 0. Jest ono wykorzystywane w "
"sterowniku zarówno w trybie próbkowania, jak i w trybie przerwań."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPSETIRQ, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPSETIRQ, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This B<ioctl>(2)  requires superuser privileges.  It takes an I<int> "
"containing the new IRQ as argument.  As a side effect, the printer will be "
"reset.  When I<arg> is 0, the polling driver will be used, which is also "
"default."
msgstr ""
"To wywołanie funkcji B<ioctl>(2) wymaga uprawnień administratora. Funkcja "
"pobiera argument typu I<int> zawierający nowe przerwanie IRQ. Skutkiem "
"ubocznym jest inicjacja drukarki. Gdy I<arg> jest zerem, to stosowany będzie "
"sterownik z próbkowaniem; jest to równocześnie zachowanie domyślne."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPGETIRQ, int *>argB<)>"
msgstr "B<int ioctl(int >fdB<, LPGETIRQ, int *>argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Stores the currently used IRQ in I<arg>."
msgstr "Zachowuje w parametrze I<arg> aktualnie stosowane przerwanie IRQ."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPGETSTATUS, int *>argB<)>"
msgstr "B<int ioctl(int >fdB<, LPGETSTATUS, int *>argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Stores the value of the status port in I<arg>.  The bits have the following "
"meaning:"
msgstr ""
"Zachowuje wartość stanu portu w zmiennej I<arg>.  Poszczególne bity mają "
"następujące znaczenie:"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_PBUSY"
msgstr "LP_PBUSY"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "inverted busy input, active high"
msgstr "odwrócony sygnał zajętości"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_PACK"
msgstr "LP_PACK"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unchanged acknowledge input, active low"
msgstr "niezmienione potwierdzenie wejścia"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_POUTPA"
msgstr "LP_POUTPA"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unchanged out-of-paper input, active high"
msgstr "niezmieniony sygnał braku papieru"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_PSELECD"
msgstr "LP_PSELECD"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unchanged selected input, active high"
msgstr "niezmieniony sygnał wyboru"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LP_PERRORP"
msgstr "LP_PERRORP"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "unchanged error input, active low"
msgstr "niezmieniony sygnał błędu"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Refer to your printer manual for the meaning of the signals.  Note that "
"undocumented bits may also be set, depending on your printer."
msgstr ""
"Znaczenia sygnałów należy szukać w dokumentacji drukarki. Należy pamiętać, "
"że dla niektórych drukarek ustawiane mogą być też nieudokumentowane tu bity."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPRESET)>"
msgstr "B<int ioctl(int >fdB<, LPRESET)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Resets the printer.  No argument is used."
msgstr "Inicjuje drukarkę. Nie stosuje się żadnych parametrów."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#.  .SH AUTHORS
#.  The printer driver was originally written by Jim Weigand and Linus
#.  Torvalds.
#.  It was further improved by Michael K.\& Johnson.
#.  The interrupt code was written by Nigel Gamble.
#.  Alan Cox modularized it.
#.  LPCAREFUL, LPABORT, LPGETSTATUS were added by Chris Metcalf.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I</dev/lp*>"
msgstr "I</dev/lp*>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-41 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<chmod>(1), B<chown>(1), B<mknod>(1), B<lpcntl>(8), B<tunelp>(8)"
msgstr "B<chmod>(1), B<chown>(1), B<mknod>(1), B<lpcntl>(8), B<tunelp>(8)"

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (niewydane)"
