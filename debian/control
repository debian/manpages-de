Source: manpages-l10n
Maintainer: Dr. Helge Kreutzmann <debian@helgefjell.de>
Uploaders: Dr. Tobias Quathamer <toddy@debian.org>
Section: doc
Priority: optional
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: po4a (>> 0.71)
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/manpages-l10n
Vcs-Git: https://salsa.debian.org/debian/manpages-l10n.git
Homepage: https://manpages-l10n-team.pages.debian.net/manpages-l10n/

Package: manpages-cs
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Czech man pages
 This package contains manual pages translated into Czech,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-cs-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Czech development manpages
 This package contains the Linux development manual pages
 translated into Czech.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-da
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Danish man pages
 This package contains manual pages translated into Danish,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-da-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Danish development manpages
 This package contains the Linux development manual pages
 translated into Danish.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-de
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3), ghostscript (<< 10.01.2~dfsg-1)
Replaces: ghostscript (<< 10.01.2~dfsg-1)
Suggests: man-browser,
          manpages
Description: German man pages
 This package contains manual pages translated into German,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-de-dev
Architecture: all
Multi-Arch: foreign
Breaks: util-linux-locales (<< 2.39.1-3)
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: German development manpages
 This package contains the Linux development manual pages
 translated into German.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-el
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages
Description: Greek man pages
 This package contains manual pages translated into Greek,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-es
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Conflicts: manpages-es-extra
Description: Spanish man pages
 This package contains manual pages translated into Spanish,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-es-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Conflicts: manpages-es-extra
Description: Spanish development manpages
 This package contains the Linux development manual pages
 translated into Spanish.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-fi
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Finnish man pages
 This package contains manual pages translated into Finnish,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 6 = Games etc.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-fr
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages
Breaks: util-linux-locales (<< 2.39.1-3)
Description: French man pages
 This package contains manual pages translated into French,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-fr-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Breaks: util-linux-locales (<< 2.39.1-3)
Description: French development manpages
 This package contains the Linux development manual pages
 translated into French.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-hu
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Hungarian man pages
 This package contains manual pages translated into Hungarian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-id
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Indonesian man pages
 This package contains manual pages translated into Indonesian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-it
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Italian man pages
 This package contains manual pages translated into Italian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-it-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Italian development manpages
 This package contains the Linux development manual pages
 translated into Italian.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-ko
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Korean man pages
 This package contains manual pages translated into Korean,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-ko-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Korean development manpages
 This package contains the Linux development manual pages
 translated into Korean.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-mk
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages
Description: Macedonian man pages
 This package contains manual pages translated into Macedonian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-nb
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages
Description: Norwegian man pages
 This package contains manual pages translated into Norwegian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-nl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Dutch man pages
 This package contains manual pages translated into Dutch,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-nl-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Dutch development manpages
 This package contains the Linux development manual pages
 translated into Dutch.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-pl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Polish man pages
 This package contains manual pages translated into Polish,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-pl-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Polish development manpages
 This package contains the Linux development manual pages
 translated into Polish.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-pt-br
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages
Description: Brazilian Portuguese man pages
 This package contains manual pages translated into Brazilian
 Portuguese, originating from a large number of projects which do
 not ship translated man pages themselves, including the traditional
 Linux manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-pt-br-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Brazilian Portuguese development manpages
 This package contains the Linux development manual pages
 translated into Brazilian Portuguese.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-ro
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Romanian man pages
 This package contains manual pages translated into Romanian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-ro-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Romanian development manpages
 This package contains the Linux development manual pages
 translated into Romanian.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-ru
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages
Description: Russian man pages
 This package contains manual pages translated into Russian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-ru-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Russian development manpages
 This package contains the Linux development manual pages
 translated into Russian.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-sr
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Serbian man pages
 This package contains manual pages translated into Serbian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-sv
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages
Description: Swedish man pages
 This package contains manual pages translated into Swedish,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-uk
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages
Description: Ukrainian man pages
 This package contains manual pages translated into Ukrainian,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 4 = Devices (e.g. hd, sd).
  * 5 = File formats and protocols, syntaxes of several system
        files (e.g. wtmp, /etc/passwd, nfs).
  * 6 = Games etc.
  * 7 = Conventions and standards, macro packages, etc.
        (e.g. nroff, ascii).
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.

Package: manpages-uk-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: util-linux-locales (<< 2.39.1-3)
Suggests: man-browser,
          manpages-dev,
          glibc-doc
Description: Ukrainian development manpages
 This package contains the Linux development manual pages
 translated into Ukrainian.
 .
 The following sections are included:
  * 2 = Linux system calls.
  * 3 = Libc calls (note that a more comprehensive source of
        information may be found in one of the libc-doc packages).
 .
 The English package manpages-dev contains additional manual pages
 which have not been translated yet.

Package: manpages-vi
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: man-browser,
          manpages
Description: Vietnamese man pages
 This package contains manual pages translated into Vietnamese,
 originating from a large number of projects which do not ship
 translated man pages themselves, including the traditional Linux
 manpages. The exact projects and man pages might change from
 release to release, so please look at the actually shipped files
 for the current set.
 .
 The following sections are included:
  * 1 = User programs (e.g. ls, ln)
  * 8 = System administration commands.
 .
 The original (software) packages contain additional manual pages
 which have not been translated yet.
